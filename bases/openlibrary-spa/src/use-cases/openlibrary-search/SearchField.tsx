'use client';

import React, { SyntheticEvent, useState } from 'react';
import SearchIcon from '@/components/icons/Search';
import { Dropdown } from 'flowbite-react';
import { defaultSearchParams, defaultSearchResults, useSearch } from '.';
import type { SearchParams, SearchTopic } from '@/services/openlibrary/api';
import type { SearchFieldProps } from './types';

const styles = {
    field: (searchInProgress: boolean) =>
        ['relative container', searchInProgress ? 'animate-pulse' : ''].join(
            ' '
        ),
    icon: [
        'stroke-2',
        'absolute flex items-center inset-y-0 start-0 ps-3',
        'text-gray-500 dark:text-gray-400',
        'pointer-events-none',
    ].join(' '),
    input: [
        'block w-full p-4 ps-10 pe-48 text-sm',
        'border rounded-lg',
        'text-gray-900 bg-gray-50 dark:text-white dark:bg-gray-700 dark:placeholder-gray-400',
        'border-gray-300 dark:border-gray-600',
        'focus:ring-blue-500 focus:border-blue-500 dark:focus:ring-blue-500 dark:focus:border-blue-500',
        'autofill:bg-gray-50 autofill:dark:bg-gray-700',
    ].join(' '),
    actions: ['absolute end-2.5 bottom-2.5', 'flex items-center gap-2'].join(
        ' '
    ),
    dropdown: [''].join(' '),
    submit: [
        'rounded-lg px-4 py-2',
        'text-sm font-medium',
        'text-white bg-blue-700 dark:bg-blue-600',
        'hover:bg-blue-800 dark:hover:bg-blue-700',
        'focus:ring-4 focus:outline-none focus:ring-blue-300 dark:focus:ring-blue-800',
    ].join(' '),
};

export const searchTopics = {
    works: 'Works',
    authors: 'Authors',
};

const SearchField = (props: SearchFieldProps) => {
    const { state, setTopic, setQuery, setParams, setResults, doSearch } =
        useSearch();
    const { topic, params } = state;

    const [searchInProgress, setSearchInProgress] = useState(false);

    const onSearch =
        (topic: SearchTopic, params: SearchParams) =>
        async (e: SyntheticEvent) => {
            e.preventDefault();
            if (!params.q) return;

            setSearchInProgress(true);
            const searchResults = await doSearch(topic, params);
            setResults(searchResults || defaultSearchResults);
            setSearchInProgress(false);
        };

    const onTopicSelection = (topic: SearchTopic) => () => {
        setTopic(topic);
        setParams({ ...defaultSearchParams, q: params.q });
        setResults(defaultSearchResults);
    };

    const onQueryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const query = e.target.value;
        setQuery(query);
    };

    return (
        <form action="">
            <label htmlFor="openlibrary-search" className="sr-only">
                Search
            </label>
            <div className={styles.field(searchInProgress)}>
                <div className={styles.icon}>
                    <SearchIcon className="h-4 w-4" />
                </div>
                <input
                    type="search"
                    id="openlibrary-search"
                    className={styles.input}
                    placeholder="Name, title, ..."
                    value={params.q}
                    onChange={onQueryChange}
                />
                <div className={styles.actions}>
                    <div className={styles.dropdown}>
                        <Dropdown label={searchTopics[topic]} inline>
                            {Object.keys(searchTopics).map(key => (
                                <Dropdown.Item
                                    key={key}
                                    onClick={onTopicSelection(
                                        key as SearchTopic
                                    )}
                                >
                                    {searchTopics[key as SearchTopic]}
                                </Dropdown.Item>
                            ))}
                        </Dropdown>
                    </div>
                    <button
                        type="submit"
                        className={styles.submit}
                        onClick={onSearch(topic, params)}
                        disabled={searchInProgress}
                    >
                        <span>Search</span>
                    </button>
                </div>
            </div>
        </form>
    );
};

export default SearchField;
