import { createContext, useContext, useReducer } from 'react';
import { SearchResults } from '@/domain/SearchResults';
import {
    SearchParams,
    SearchTopic,
    authorsSearch,
    worksSearch,
} from '@/services/openlibrary/api';
import {
    SearchContext,
    SearchEvent,
    SearchProviderProps,
    SearchState,
} from './types';

// search state: search topic, parameters and results

export const defaultSearchParams: SearchParams = {
    q: '',
    page: 1,
    limit: 10,
    // not set by default
    // fields: '',
    // sort: '',
    // lang: '',
};

export const defaultSearchResults: SearchResults = {
    items: [],
    count: 0,
    offset: 0,
};

export const searchInitialState = {
    topic: 'works' as SearchTopic,
    params: defaultSearchParams,
    results: defaultSearchResults,
};

// reducer function: manages state (produces next state from a
// previous one and an event)

export const searchReducer = (
    state: SearchState,
    event: SearchEvent
): SearchState => {
    switch (event.type) {
        case 'SET_SEARCH_TOPIC':
            return { ...state, topic: event.payload };
        case 'SET_SEARCH_PARAMS':
            return { ...state, params: event.payload };
        case 'SET_SEARCH_QUERY':
            return {
                ...state,
                params: { ...state.params, q: event.payload },
            };
        case 'SET_SEARCH_PAGE':
            return {
                ...state,
                params: { ...state.params, page: event.payload },
            };
        case 'SET_SEARCH_LIMIT':
            return {
                ...state,
                params: { ...state.params, limit: event.payload },
            };
        case 'SET_SEARCH_RESULTS':
            return { ...state, results: event.payload };
    }
};

// search context and provider: provides search state and
// accessors / mutators for it

export const searchContext = createContext<SearchContext | null>(null);

// to get context within components

export const useSearch = () => {
    const ctx = useContext(searchContext);
    if (!ctx) {
        throw new Error(
            'components using search context must be wrapped in search context provider'
        );
    } else {
        return ctx;
    }
};

// to provide context to child components

export const SearchProvider = ({ children }: SearchProviderProps) => {
    const [state, dispatch] = useReducer(searchReducer, searchInitialState);

    // actions: execute (async) tasks, dispatch events

    const setTopic = (topic: SearchTopic) => {
        dispatch({ type: 'SET_SEARCH_TOPIC', payload: topic });
    };

    const setParams = (params: SearchParams) => {
        dispatch({ type: 'SET_SEARCH_PARAMS', payload: params });
    };

    const setQuery = (query: string) => {
        dispatch({ type: 'SET_SEARCH_QUERY', payload: query });
    };

    const setPage = (page: number) => {
        dispatch({ type: 'SET_SEARCH_PAGE', payload: page });
    };

    const setLimit = (limit: number) => {
        dispatch({ type: 'SET_SEARCH_LIMIT', payload: limit });
    };

    const setResults = (results: SearchResults) => {
        dispatch({ type: 'SET_SEARCH_RESULTS', payload: results });
    };

    const doSearch = async (topic: SearchTopic, params: SearchParams) => {
        switch (topic) {
            case 'works':
                return worksSearch(params);
            case 'authors':
                return authorsSearch(params);
        }
    };

    const ctx = {
        state,
        setTopic,
        setParams,
        setQuery,
        setPage,
        setLimit,
        setResults,
        doSearch,
    };

    return (
        <searchContext.Provider value={ctx}>
            {children}
        </searchContext.Provider>
    );
};
