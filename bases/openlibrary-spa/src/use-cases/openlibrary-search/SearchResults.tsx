'use client';

import type {
    AuthorSearchResult,
    WorkSearchResult,
} from '@/domain/SearchResults';
import React, { useState } from 'react';
import { Pagination, Table } from 'flowbite-react';
import { SearchResultsProps } from './types';
import { useSearch } from '.';
import {
    SearchParams,
    SearchTopic,
    authorPhotoUrl,
    editionCoverUrl,
} from '@/services/openlibrary/api';
import NoResults from './NoResults';
import Link from 'next/link';

const paginationTheme = {
    pages: {
        selector: {
            active: [
                'bg-blue-50 text-blue-600 ',
                'dark:border-gray-700 dark:bg-gray-700 dark:text-white',
                'hover:bg-cyan-100 hover:text-cyan-700',
            ].join(' '),
        },
        previous: {
            base: 'hidden',
        },
        next: {
            base: 'hidden',
        },
    },
};

const styles = {
    paginationContainer: [
        'flex items-center justify-between gap-4',
        'text-gray-500 dark:text-gray-400',
    ].join(' '),
    pagination: (pageChangeInProgress: boolean) =>
        [
            pageChangeInProgress ? 'animate-pulse pointer-events-none' : '',
        ].join(' '),
    cover: ['h-16'].join(' '),
    photo: ['h-16'].join(' '),
    table: {
        body: ['divide-y'].join(' '),
        row: ['bg-white ', 'dark:border-gray-700 dark:bg-gray-800'].join(' '),
    },
};

const getTotalPages = (totalCount: number) => Math.ceil(totalCount / 10);

const SearchResults = (props: SearchResultsProps) => {
    const { state, setPage, setResults, doSearch } = useSearch();
    const { topic, params, results } = state;
    const [pageChangeInProgress, setPageChangeInProgress] = useState(false);

    const onPageChange =
        (topic: SearchTopic, params: SearchParams) =>
        async (page: number) => {
            if (page === params.page) return;

            setPageChangeInProgress(true);

            const newParams = { ...params, page };
            const newPage = await doSearch(topic, newParams);

            if (newPage) {
                setResults(newPage);
                setPage(page);
            }

            setPageChangeInProgress(false);
        };

    if (!results.items?.length) return <NoResults />;

    const pagination =
        getTotalPages(results.count) > 1 ? (
            <div className={styles.paginationContainer}>
                <div>
                    {results.offset + 1}&ndash;
                    {results.offset + results.items.length} of {results.count}{' '}
                    total
                </div>
                <Pagination
                    theme={paginationTheme}
                    className={styles.pagination(pageChangeInProgress)}
                    layout="pagination"
                    currentPage={params.page}
                    totalPages={getTotalPages(results.count)}
                    onPageChange={onPageChange(topic, params)}
                />
            </div>
        ) : null;

    let resultsTable;
    switch (topic) {
        case 'works': {
            resultsTable = (
                <Table>
                    <Table.Head>
                        <Table.HeadCell>Cover</Table.HeadCell>
                        <Table.HeadCell>Title</Table.HeadCell>
                        <Table.HeadCell>Author</Table.HeadCell>
                    </Table.Head>
                    <Table.Body className={styles.table.body}>
                        {results.items.map((result, index) => {
                            const work = result as WorkSearchResult;
                            return (
                                <Table.Row
                                    key={work.olid + index}
                                    className={styles.table.row}
                                >
                                    <Table.Cell>
                                        <img
                                            className={styles.cover}
                                            src={editionCoverUrl(
                                                work.editions
                                                    .coverEditionOlid,
                                                'S'
                                            )}
                                            alt={work.title}
                                        />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Link href={`/works/${work.olid}`}>
                                            {work.title},{' '}
                                            {Math.min(...work.publishYears)}
                                        </Link>
                                    </Table.Cell>
                                    <Table.Cell>{work.authorName}</Table.Cell>
                                </Table.Row>
                            );
                        })}
                    </Table.Body>
                </Table>
            );
            break;
        }
        case 'authors': {
            resultsTable = (
                <Table>
                    <Table.Head>
                        <Table.HeadCell>Photo</Table.HeadCell>
                        <Table.HeadCell>Name</Table.HeadCell>
                        <Table.HeadCell>Known for</Table.HeadCell>
                    </Table.Head>
                    <Table.Body className={styles.table.body}>
                        {results.items.map((result, index) => {
                            const author = result as AuthorSearchResult;
                            return (
                                <Table.Row
                                    key={author.olid + index}
                                    className={styles.table.row}
                                >
                                    <Table.Cell>
                                        <img
                                            className={styles.photo}
                                            src={authorPhotoUrl(
                                                author.olid,
                                                'S'
                                            )}
                                            alt={author.name}
                                        />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Link
                                            href={`/authors/${author.olid}`}
                                        >
                                            {author.name}
                                        </Link>
                                    </Table.Cell>
                                    <Table.Cell>
                                        {author.works?.top}
                                    </Table.Cell>
                                </Table.Row>
                            );
                        })}
                    </Table.Body>
                </Table>
            );
            break;
        }
    }

    return (
        <>
            {resultsTable}
            {pagination}
        </>
    );
};

export default SearchResults;
