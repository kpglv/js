import React from 'react';
import Link from 'next/link';
import SearchIcon from '@/components/icons/Search';

interface NoResultsProps {}

const styles = {
    container: [
        'flex flex-col items-center justify-center text-center',
        'h-full',
    ].join(' '),
    icon: [
        'h-36 w-36',
        'lg:h-48 w-48',
        'stroke-1',
        'mt-8 mb-4 translate-x-8',
    ].join(' '),
    message: ['prose dark:prose-invert lg:prose-xl'].join(' '),
};

const NoResults = (props: NoResultsProps) => {
    return (
        <div className={styles.container}>
            <div className={styles.message}>
                <h4>There are currently no results.</h4>
                <p>
                    {' '}
                    Learn <Link href="help">how to search</Link>.
                </p>
            </div>
        </div>
    );
};

export default NoResults;
