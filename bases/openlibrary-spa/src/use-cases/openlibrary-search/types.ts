import { SearchResults } from '@/domain/SearchResults';
import { SearchParams, SearchTopic } from '@/services/openlibrary/api';

// search state

export interface SearchState {
    topic: SearchTopic;
    params: SearchParams;
    results: SearchResults;
}

// search events (called 'actions' in React docs)

export interface SetTopic {
    type: 'SET_SEARCH_TOPIC';
    payload: SearchTopic;
}

export interface SetParams {
    type: 'SET_SEARCH_PARAMS';
    payload: SearchParams;
}

export interface SetQuery {
    type: 'SET_SEARCH_QUERY';
    payload: string;
}

export interface SetPage {
    type: 'SET_SEARCH_PAGE';
    payload: number;
}

export interface SetLimit {
    type: 'SET_SEARCH_LIMIT';
    payload: number;
}

export interface SetSearchResults {
    type: 'SET_SEARCH_RESULTS';
    payload: SearchResults;
}

export type SearchEvent =
    | SetTopic
    | SetParams
    | SetQuery
    | SetPage
    | SetLimit
    | SetSearchResults;

// search context and provider: provides search state and
// accessors / mutators for it; also actions

export interface SearchContext {
    state: SearchState;
    // accessors/mutators
    setTopic: (topic: SearchTopic) => void;
    setParams: (params: SearchParams) => void;
    setQuery: (query: string) => void;
    setPage: (page: number) => void;
    setLimit: (limit: number) => void;
    setResults: (results: SearchResults) => void;
    // actions
    doSearch: (
        topic: SearchTopic,
        params: SearchParams
    ) => Promise<SearchResults | null>;
}

export interface SearchProviderProps {
    children: React.ReactNode;
}

// search field component

export interface SearchFieldProps {}

// search results component

export interface SearchResultsProps {}
