import React from 'react';
import { getCats } from '@/services/cats/api';
import CatsList from '@/components/example/CatsList';

const Home = async () => {
    // server-side component can get the data directly, without
    // using an API route
    const cats = await getCats({ limit: '2' });

    return (
        <div className="flex flex-col gap-4 p-4">
            <CatsList cats={cats} />
        </div>
    );
};

export default Home;
