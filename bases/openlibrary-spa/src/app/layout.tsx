import type { Metadata } from 'next';
import { Flowbite, ThemeModeScript } from 'flowbite-react';
import theme from '@/app/theme';
import '@/app/globals.css';

export const metadata: Metadata = {
    title: 'Open Library SPA',
    description: 'Search books and authors using Open Library APIs',
};

interface RootLayoutProps {
    children: React.ReactNode;
}

const MainLayout = ({ children }: RootLayoutProps) => {
    return (
        <html lang="en" className="no-scrollbar" suppressHydrationWarning>
            <head>
                <ThemeModeScript />
            </head>
            <Flowbite theme={{ theme }}>{children}</Flowbite>
        </html>
    );
};

export default MainLayout;
