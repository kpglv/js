import Link from 'next/link';
import React from 'react';

const styles = {
    container: ['flex flex-col'].join(' '),
    page: ['flex flex-col', 'h-full', 'prose dark:prose-invert'].join(' '),
    title: ['prose dark:prose-invert', 'lg:prose-xl', 'mb-4'].join(' '),
};

const Help = () => {
    return (
        <div className={styles.container}>
            <div className={styles.page}>
                <div className={styles.title}>
                    <h2>About the App</h2>
                </div>
                <p>
                    This is a demo application created to showcase my skills
                    in modern frontend development.
                </p>
                <section>
                    <h3>Tools, Libraries and Frameworks Used</h3>
                    <ul>
                        <li>
                            <Link
                                href="https://www.typescriptlang.org/"
                                target="_blank"
                            >
                                TypeScript
                            </Link>{' '}
                            language
                        </li>
                        <li>
                            <Link href="https://react.dev/" target="_blank">
                                React
                            </Link>{' '}
                            library for creating modular, reactive UIs
                        </li>
                        <li>
                            <Link
                                href="https://www.flowbite-react.com/docs/getting-started/introduction"
                                target="_blank"
                            >
                                Flowbite
                            </Link>{' '}
                            library of reusable components
                        </li>
                        <li>
                            <Link
                                href="https://tailwindcss.com/"
                                target="_blank"
                            >
                                Tailwind CSS
                            </Link>{' '}
                            styling framework
                        </li>
                        <li>
                            <Link
                                href="https://nextjs.org/docs"
                                target="_blank"
                            >
                                Next.js
                            </Link>{' '}
                            site and application framework
                        </li>
                    </ul>
                </section>
                <section>
                    <h3>Development Concerns Demonstrated</h3>
                    <ol>
                        <li>Describing domain and use cases</li>
                        <li>Implementing services for</li>
                        <ul>
                            <li>Fetching data from REST API</li>
                            <li>
                                Parsing, serializing and validating the data
                                (using{' '}
                                <Link
                                    href="https://ajv.js.org/"
                                    target="_blank"
                                >
                                    Ajv
                                </Link>{' '}
                                and{' '}
                                <Link
                                    href="https://ajv.js.org/json-type-definition.html"
                                    target="_blank"
                                >
                                    JSON Type Definition
                                </Link>{' '}
                                )
                            </li>
                        </ul>
                        <li>
                            Testing the services (using{' '}
                            <Link href="https://jestjs.io/" target="_blank">
                                Jest
                            </Link>{' '}
                            testing framework)
                        </li>
                        <li>
                            Designing and implementing reusable visual
                            components (using React functional components and
                            hooks)
                        </li>
                        <li>Creating an application</li>
                        <ul>
                            <li>
                                Composing responsive views from reusable
                                components (using semantic markup)
                            </li>
                            <li>Routing</li>
                            <li>
                                State management (using React hooks and
                                context)
                            </li>
                            <li>Data fetching</li>
                            <li>Styling and theming</li>
                            <li>Configuring (via environment variables)</li>
                            <li>Using SSR (Server-Side Rendering)</li>
                            <li>
                                Deploying (on{' '}
                                <Link
                                    href="https://vercel.com/"
                                    target="_blank"
                                >
                                    Vercel
                                </Link>
                                )
                            </li>
                        </ul>
                    </ol>
                </section>
            </div>
        </div>
    );
};

export default Help;
