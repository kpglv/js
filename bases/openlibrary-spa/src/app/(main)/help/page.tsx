import Link from 'next/link';
import React from 'react';

const styles = {
    container: ['flex flex-col'].join(' '),
    page: ['flex flex-col', 'h-full', 'prose dark:prose-invert'].join(' '),
    title: ['prose dark:prose-invert', 'lg:prose-xl', 'mb-4'].join(' '),
};

const Help = () => {
    return (
        <div className={styles.container}>
            <div className={styles.page}>
                <div className={styles.title}>
                    <h2>Get Help</h2>
                </div>
                <p>
                    You can search works and authors by arbitrary phrases, and
                    fine-tune your results by using advanced queries.
                </p>
                <section>
                    <h3>Basic Search</h3>
                    <ol>
                        <li>
                            Enter the phrase that you want to search by (e.g.,{' '}
                            <code>The Lord of the Rings</code>.
                        </li>
                        <li>
                            Select the search topic from the dropdown list
                            (e.g., <code>Works</code>).
                        </li>
                        <li>
                            Press the <kbd>Search</kbd> button.
                        </li>
                        <li>
                            If there are more than one page of results, there
                            is a control below to navigate the pages.
                        </li>
                    </ol>
                    <p></p>
                </section>
                <section>
                    <h3>Advanced Queries</h3>
                    <p>
                        Queries can be structured to allow for more
                        fine-grained results. For example, search for{' '}
                        <code>author:Twain</code>
                        in <code>Works</code> will return books whose
                        author&apos;s name contains <code>Twain</code>.
                    </p>
                    <p>
                        Query prefixes can also be combined. A query for{' '}
                        <code>person:Frodo person:Bilbo</code> will return
                        books that feature Frodo AND Bilbo; a query for{' '}
                        <code>
                            subject:(&quot;travel&quot; OR &quot;dogs&quot;)
                        </code>{' '}
                        will return books about travel OR dogs.
                    </p>
                    <p>Query prefixes available include</p>
                    <ul>
                        <li>
                            <code>title</code>:
                        </li>
                        <li>
                            <code>author</code>
                        </li>
                        <li>
                            <code>subject</code>
                        </li>
                        <li>
                            <code>place</code>
                        </li>
                        <li>
                            <code>person</code>
                        </li>
                        <li>
                            <code>language</code>
                        </li>
                        <li>
                            <code>publisher</code>
                        </li>
                    </ul>
                    <p>
                        See{' '}
                        <Link
                            href="https://openlibrary.org/search/howto"
                            target="_blank"
                        >
                            Open Library Search Tips
                        </Link>{' '}
                        for more details.
                    </p>
                </section>
            </div>
        </div>
    );
};

export default Help;
