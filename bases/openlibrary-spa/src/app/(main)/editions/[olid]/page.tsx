// a page that is reached by dynamic route, the route parameters
// are passed in 'params' prop

interface EditionPageParams {
  olid: string;
}

interface EditionPageProps {
  params: EditionPageParams;
}

const EditionPage = ({ params }: EditionPageProps) => {
  return (
    <h1>Edition {params.olid}</h1>
  );
};

export default EditionPage;
