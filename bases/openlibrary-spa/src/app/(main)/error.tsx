'use client';

import { useEffect } from 'react';

interface ErrorPageProps {
    error: Error & { digest?: string };
    reset: () => void;
}

const errorClasses = [
    'col-span-4 md:col-span-8 lg:col-span-12',
    'flex flex-col justify-center items-center gap-8 p-8',
    'text-center',
].join(' ');

const ErrorPage = ({ error, reset }: ErrorPageProps) => {
    useEffect(() => {
        console.error(error);
    }, [error]);

    return (
        <section className={errorClasses}>
            <div className="prose dark:prose-invert">
                <h2>Something went wrong!</h2>
                <button
                    onClick={
                        // attempt to recover by trying to re-render the segment
                        () => reset()
                    }
                >
                    Try again
                </button>
            </div>
        </section>
    );
};

export default ErrorPage;
