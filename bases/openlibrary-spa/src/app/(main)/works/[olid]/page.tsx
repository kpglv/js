// a page that is reached by dynamic route, the route parameters
// are passed in 'params' prop

import { editionCoverUrl, work } from '@/services/openlibrary/api';
import { Badge, Card } from 'flowbite-react';

interface WorkPageParams {
    olid: string;
}

interface WorkPageProps {
    params: WorkPageParams;
}

const styles = {
    page: ['flex flex-col'].join(' '),
    title: ['prose dark:prose-invert', 'lg:prose-xl', 'mb-4'].join(' '),
    card: ['min-w-full'].join(' '),
    cardTitle: ['text-2xl font-bold tracking-tight'].join(' '),
    cardSectionTitle: ['text-xl font-bold tracking-tight'].join(' '),
    cardImage: [].join(' '),
    cardBadgeList: ['flex flex-wrap gap-2'].join(' '),
};

const WorkPage = async ({ params }: WorkPageProps) => {
    const aWork = await work(params.olid, {});

    if (!aWork) {
        return <div>Sorry, can&apos;t fetch the work description</div>;
    }

    return (
        <div className={styles.page}>
            <Card className={styles.card}>
                <h3 className={styles.cardTitle}>{aWork.title}</h3>
                {aWork.description ? (
                    <p>{aWork.description}</p>
                ) : (
                    <div>No description available.</div>
                )}

                {aWork.people.length ? (
                    <>
                        <h3 className={styles.cardSectionTitle}>People</h3>
                        <div className={styles.cardBadgeList}>
                            {aWork.people.map(name => (
                                <Badge key={name} color="gray">
                                    {name}
                                </Badge>
                            ))}
                        </div>
                    </>
                ) : null}
                {aWork.places.length ? (
                    <>
                        <h3 className={styles.cardSectionTitle}>Places</h3>
                        <div className={styles.cardBadgeList}>
                            {aWork.places.map(place => (
                                <Badge key={place} color="gray">
                                    {place}
                                </Badge>
                            ))}
                        </div>
                    </>
                ) : null}
            </Card>
        </div>
    );
};

export default WorkPage;
