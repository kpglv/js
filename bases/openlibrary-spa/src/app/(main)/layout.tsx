'use client';
import { Inter } from 'next/font/google';
import { twMerge } from 'tailwind-merge';
import '@/app/globals.css';

import Header from '@/components/app-shell/Header';
import Drawer from '@/components/app-shell/Drawer';
import Footer from '@/components/app-shell/Footer';
import Metrics from '@/components/Metrics';
import { SearchProvider } from '@/use-cases/openlibrary-search';

const inter = Inter({ subsets: ['latin'] });

interface MainLayoutProps {
    children: React.ReactNode;
}

const styles = {
    body: [
        'min-h-screen',
        'text-gray-900 bg-gray-50',
        'dark:text-gray-50 dark:bg-gray-900',
    ].join(' '),
    main: ['min-h-screen h-auto py-24 px-4', 'md:ml-64 md:px-16'].join(' '),
};

const MainLayout = ({ children }: MainLayoutProps) => {
    return (
        <body className={twMerge(styles.body, inter.className)}>
            <Header />
            <Drawer />
            <SearchProvider>
                <main className={styles.main}>{children}</main>
            </SearchProvider>
            <Footer />
            <Metrics section="app metrics" />
        </body>
    );
};

export default MainLayout;
