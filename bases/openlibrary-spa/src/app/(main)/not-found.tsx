import Link from 'next/link';
import EmptyIcon from '@/components/icons/Empty';

const styles = {
    section: [
        'col-span-4 md:col-span-8 lg:col-span-12',
        'h-full',
        'flex flex-col justify-center items-center gap-8 p-8',
    ].join(' '),
    icon: ['h-36 w-36', 'lg:h-48 w-48'].join(' '),
    prose: [
        'prose dark:prose-invert',
        'lg:prose-xl',
        'flex flex-col justify-center items-center text-center',
    ].join(' '),
};

export default function NotFound() {
    return (
        <section className={styles.section}>
            <div className={styles.prose}>
                <h2>Not Found</h2>
                <p className="text-gray-700 dark:text-gray-400">
                    Could not find requested resource
                </p>
                <EmptyIcon className={styles.icon} />
                <p>
                    <Link href="/">Return Home</Link>
                </p>
            </div>
        </section>
    );
}
