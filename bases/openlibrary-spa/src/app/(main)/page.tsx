'use client';

import React from 'react';
import { SearchProvider } from '@/use-cases/openlibrary-search';
import SearchField from '@/use-cases/openlibrary-search/SearchField';
import SearchResults from '@/use-cases/openlibrary-search/SearchResults';

const styles = {
    page: ['flex flex-col', 'h-full'].join(' '),
    title: ['prose dark:prose-invert', 'lg:prose-xl', 'mb-8'].join(' '),
    search: ['flex flex-col gap-8', 'h-full'].join(' '),
};

const Home = () => {
    return (
        <div className={styles.page}>
            <div className={styles.title}>
                <h2>Search books and authors</h2>
            </div>
            <div className={styles.search}>
                <SearchField />
                <SearchResults />
            </div>
        </div>
    );
};

export default Home;
