// a page that is reached by dynamic route, the route parameters
// are passed in 'params' prop

import { author, authorPhotoUrl } from '@/services/openlibrary/api';
import { Card } from 'flowbite-react';

interface AuthorPageParams {
    olid: string;
}

interface AuthorPageProps {
    params: AuthorPageParams;
}

const styles = {
    page: ['flex flex-col'].join(' '),
    title: ['prose dark:prose-invert', 'lg:prose-xl', 'mb-4'].join(' '),
    card: ['min-w-full'].join(' '),
    cardTitle: ['text-2xl font-bold tracking-tight'].join(' '),
    cardSectionTitle: ['text-xl font-bold tracking-tight'].join(' '),
};

const AuthorPage = async ({ params }: AuthorPageProps) => {
    const anAuthor = await author(params.olid, {});

    if (!anAuthor) {
        return <div>Sorry, can&apos;t fetch the author description</div>;
    }

    return (
        <div className={styles.page}>
            <Card className={styles.card}>
                <h3 className={styles.cardTitle}>{anAuthor.name}</h3>
                <img
                    src={authorPhotoUrl(params.olid, 'L')}
                    alt={params.olid}
                />
                <h3 className={styles.cardSectionTitle}>Biography</h3>
                {anAuthor.bio ? (
                    <p>{anAuthor.bio}</p>
                ) : (
                    <div>No biography avaiable.</div>
                )}
            </Card>
        </div>
    );
};

export default AuthorPage;
