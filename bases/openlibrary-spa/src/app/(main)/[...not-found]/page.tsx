import { notFound } from 'next/navigation';

// workaround: not-found page with multiple root layouts
// https://github.com/vercel/next.js/discussions/50034

export default function NotFoundDummy() {
    notFound();
}