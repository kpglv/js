import { getCats } from '@/services/cats/api';

export const GET = async (request: Request) => {
    const { searchParams } = new URL(request.url);
    const limit = searchParams.get('limit') || undefined;

    const params = { limit };
    const data = await getCats(params);

    return Response.json({ data });
};
