import { JTDSchemaType } from 'ajv/dist/jtd';

export interface AuthorIDPayload {
    key: string;
}

export const AuthorIDPayloadSchema: JTDSchemaType<AuthorIDPayload> = {
    properties: {
        key: {
            type: 'string',
        },
    },
    additionalProperties: true,
};

export interface LanguageIDPayload {
    key: string;
}

export const LanguageIDPayloadSchema: JTDSchemaType<LanguageIDPayload> = {
    properties: {
        key: {
            type: 'string',
        },
    },
    additionalProperties: true,
};

export interface EditionPayload {
    key: string;
    title: string;
    authors: AuthorIDPayload[];
    publishers: string[];
    publish_date: string;
    contributions?: string[];
    number_of_pages?: number;
    languages?: LanguageIDPayload[];
    first_sentence?: {
        type: string;
        value: string;
    };
    isbn_10?: string[];
    isbn_13?: string[];
}

export const EditionPayloadSchema: JTDSchemaType<EditionPayload> = {
    properties: {
        key: {
            type: 'string',
        },
        title: {
            type: 'string',
        },
        authors: {
            elements: AuthorIDPayloadSchema,
        },
        publishers: {
            elements: {
                type: 'string',
            },
        },
        publish_date: {
            type: 'string',
        },
    },
    optionalProperties: {
        contributions: {
            elements: {
                type: 'string',
            },
        },
        number_of_pages: {
            type: 'uint32',
        },
        languages: {
            elements: LanguageIDPayloadSchema,
        },
        first_sentence: {
            properties: {
                type: {
                    type: 'string',
                },
                value: {
                    type: 'string',
                },
            },
        },
        isbn_10: {
            elements: {
                type: 'string',
            },
        },
        isbn_13: {
            elements: {
                type: 'string',
            },
        },
    },
    additionalProperties: true,
};
