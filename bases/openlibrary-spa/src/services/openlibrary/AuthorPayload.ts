import { JTDSchemaType } from 'ajv/dist/jtd';
import { type LinkPayload, LinkPayloadSchema } from './LinkPayload';

export interface AuthorPayload {
    key: string;
    name: string;
    bio: string;
    personal_name?: string;
    fuller_name?: string;
    alternate_names?: string[];
    birth_date?: string;
    links?: LinkPayload[];
    wikipedia?: string;
}

export const AuthorPayloadSchema: JTDSchemaType<AuthorPayload> = {
    properties: {
        key: {
            type: 'string',
        },
        name: {
            type: 'string',
        },
        bio: {
            type: 'string',
        },
    },
    optionalProperties: {
        personal_name: {
            type: 'string',
        },
        fuller_name: {
            type: 'string',
        },
        alternate_names: {
            elements: {
                type: 'string',
            },
        },
        birth_date: {
            type: 'string',
        },
        // add link from 'wikipedia'
        links: {
            elements: LinkPayloadSchema,
        },
        wikipedia: {
            type: 'string',
        },
    },
    additionalProperties: true,
};
