// import 'server-only';

import type { JTDParser, ValidateFunction } from 'ajv/dist/types';
import type { Author } from '@/domain/Author';
import type { AuthorPayload } from './AuthorPayload';

import {
    parseAuthorPayload,
    parseEditionPayload,
    parseImageDescriptionPayload,
    parseRatingsPayload,
    parseReadingStatsPayload,
    parseSearchResultsPayload,
    parseSubjectPayload,
    parseWorkEditionIDsPayload,
    parseWorkPayload,
    payloadToAuthor,
    payloadToEdition,
    payloadToImageDescription,
    payloadToRatings,
    payloadToReadingStats,
    payloadToSearchResults,
    payloadToSubject,
    payloadToWork,
    payloadToWorkEditionIDs,
} from '@/services/openlibrary';
import {
    validateAuthor,
    validateEdition,
    validateImageDescription,
    validateRatings,
    validateReadingStats,
    validateSearchResults,
    validateSubject,
    validateWork,
    validateWorkEditionIDs,
} from '@/domain';
import { SearchResultsPayload } from './SearchResultPayload';
import { SearchResults } from '@/domain/SearchResults';

const openLibraryApiBaseUrl = process.env.NEXT_PUBLIC_OPENLIBRARY_API_URL;
const openLibraryCoversApiBaseUrl =
    process.env.NEXT_PUBLIC_OPENLIBRARY_COVER_API_URL;

const worksSearchUrl = `${openLibraryApiBaseUrl}/search.json`;
const authorsSearchUrl = `${openLibraryApiBaseUrl}/search/authors.json`;

const authorsUrl = `${openLibraryApiBaseUrl}/authors`;
const worksUrl = `${openLibraryApiBaseUrl}/works`;
const editionsUrl = `${openLibraryApiBaseUrl}/books`;
const subjectsUrl = `${openLibraryApiBaseUrl}/subjects`;

const authorPhotosUrl = `${openLibraryCoversApiBaseUrl}/a/olid`;
const editionCoversUrl = `${openLibraryCoversApiBaseUrl}/b/olid`;

/**
 * Open Library search API parameters.
 * See {@link https://openlibrary.org/dev/docs/api/search}
 * and {@link https://openlibrary.org/search/howto}.
 */
export interface SearchParams {
    q: string;
    page: number;
    limit: number;
    fields?: string;
    sort?: string;
    lang?: string;
}

export type SearchTopic = 'works' | 'authors';

export interface EntityParams {}

export type ImageSize = 'S' | 'M' | 'L';

// search results

/**
 * Fetches, parses and returns search results.
 */
const search = async <P, R>(
    url: string,
    params: SearchParams,
    parse: JTDParser,
    coerce: (payload: P) => R,
    validate: ValidateFunction
) => {
    const searchUrl = new URL(url);
    for (const [key, value] of Object.entries(params)) {
        searchUrl.searchParams.set(key, value);
    }
    const response = await fetch(searchUrl);
    if (!response.ok) {
        console.error(`failed to fetch data from ${url}`);
        return null;
    }
    if (!response.ok) {
        console.error(`failed to fetch data from ${url}`);
        return null;
    }
    // using generic JSON parser for payloads, see issue #6
    /*
    const payload = parse(await response.text()) as P;
    if (!payload) {
        console.error(
            `failed to parse search results payload: ${parse.message} at ${parse.position}`
        );
        return null;
    }
    */
    const payload = await response.json();
    const searchResults = coerce(payload);
    if (!validate(searchResults)) {
        console.error(
            `failed to validate search results: ${JSON.stringify(
                validate.errors
            )}`
        );
        return null;
    }
    return searchResults;
};

/**
 * Returns search results for works.
 */
export const worksSearch = async (params: SearchParams) => {
    return search<SearchResultsPayload, SearchResults>(
        worksSearchUrl,
        params,
        parseSearchResultsPayload,
        payloadToSearchResults,
        validateSearchResults
    );
};

/**
 * Returns search results for authors.
 */
export const authorsSearch = async (params: SearchParams) => {
    return search<SearchResultsPayload, SearchResults>(
        authorsSearchUrl,
        params,
        parseSearchResultsPayload,
        payloadToSearchResults,
        validateSearchResults
    );
};

// entities

/**
 * Fetches, parses, and validates the entity payload; then coerces
 * the payload to entity, validates it and returns. Returns null
 * on errors, logs errors to stderr.
 * FIXME: probably only entity could be validated (use generic nonvalidating
 * parser for payload?)
 */
const entity = async <P, E>(
    url: URL,
    params: EntityParams,
    parse: JTDParser<P>,
    coerce: (payload: P) => E,
    validate: ValidateFunction<E>
): Promise<E | null> => {
    for (const [key, value] of Object.entries(params)) {
        url.searchParams.set(key, value);
    }
    const response = await fetch(url);
    if (!response.ok) {
        console.error(`failed to fetch data from ${url}`);
        return null;
    }
    // using generic JSON parser for payloads, see issue #6
    /*
    const payload = parse(await response.text());
    if (!payload) {
        console.error(
            `failed to parse entity payload: ${parse.message} at ${parse.position}`
        );
        return null;
    }
    */
    const payload = await response.json();
    const entity = coerce(payload);
    if (!validate(entity)) {
        console.error(
            `failed to validate entity: ${JSON.stringify(validate.errors)}`
        );
        return null;
    }
    return entity;
};

// author

/**
 * Returns an author.
 */
export const author = async (authorOlid: string, params: EntityParams) => {
    const url = new URL(`${authorsUrl}/${authorOlid}.json`);
    return entity<AuthorPayload, Author>(
        url,
        params,
        parseAuthorPayload,
        payloadToAuthor,
        validateAuthor
    );
};

/**
 * Returns author's photo description.
 */
export const authorPhotoDescription = async (
    authorOlid: string,
    params: EntityParams
) => {
    const url = new URL(`${authorPhotosUrl}/${authorOlid}.json`);
    return entity(
        url,
        params,
        parseImageDescriptionPayload,
        payloadToImageDescription,
        validateImageDescription
    );
};

/**
 * Returns an URL of author's photo.
 */
export const authorPhotoUrl = (authorOlid: string, size: ImageSize) => {
    return `${authorPhotosUrl}/${authorOlid}-${size}.jpg`;
};

// work

/**
 * Returns a work.
 */
export const work = async (workOlid: string, params: EntityParams) => {
    const url = new URL(`${worksUrl}/${workOlid}.json`);
    return entity(url, params, parseWorkPayload, payloadToWork, validateWork);
};

/**
 * Returns work's ratings.
 */
export const workRatings = (workOlid: string, params: EntityParams) => {
    const url = new URL(`${worksUrl}/${workOlid}/ratings.json`);
    return entity(
        url,
        params,
        parseRatingsPayload,
        payloadToRatings,
        validateRatings
    );
};

/**
 * Returns work's reading stats.
 */
export const workReadingStats = (workOlid: string, params: EntityParams) => {
    const url = new URL(`${worksUrl}/${workOlid}/bookshelves.json`);
    return entity(
        url,
        params,
        parseReadingStatsPayload,
        payloadToReadingStats,
        validateReadingStats
    );
};

/**
 * Returns work's edition IDs.
 */
export const workEditionIds = (workOlid: string, params: EntityParams) => {
    const url = new URL(`${worksUrl}/${workOlid}/editions.json`);
    return entity(
        url,
        params,
        parseWorkEditionIDsPayload,
        payloadToWorkEditionIDs,
        validateWorkEditionIDs
    );
};

// edition

/**
 * Returns an edition.
 */
export const edition = (editionOlid: string, params: EntityParams) => {
    const url = new URL(`${editionsUrl}/${editionOlid}.json`);
    return entity(
        url,
        params,
        parseEditionPayload,
        payloadToEdition,
        validateEdition
    );
};

/**
 * Returns an edition's cover description.
 */
export const editionCover = (editionOlid: string, params: EntityParams) => {
    const url = new URL(`${editionsUrl}/${editionOlid}.json`);
    return entity(
        url,
        params,
        parseImageDescriptionPayload,
        payloadToImageDescription,
        validateImageDescription
    );
};

/**
 * Returns an URL of edition's cover.
 */
export const editionCoverUrl = (editionOlid: string, size: ImageSize) => {
    return `${editionCoversUrl}/${editionOlid}-${size}.jpg`;
};

// subject

/**
 * Returns a subject by OLID.
 */
export const subject = (olid: string, params: EntityParams) => {
    const url = new URL(`${subjectsUrl}/${olid}.json`);
    return entity(
        url,
        params,
        parseSubjectPayload,
        payloadToSubject,
        validateSubject
    );
};
