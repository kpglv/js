import { JTDSchemaType } from 'ajv/dist/core';

export interface AuthorIDPayload {
    author: {
        key: string;
    };
}

export const AuthorIDPayloadSchema: JTDSchemaType<AuthorIDPayload> = {
    properties: {
        author: {
            properties: {
                key: {
                    type: 'string',
                },
            },
            additionalProperties: true,
        },
    },
    additionalProperties: true,
};

export interface WorkPayload {
    key: string;
    title: string;
    authors: AuthorIDPayload[];
    description: string;
    subjects?: string[];
    subject_people?: string[];
    subject_places?: string[];
    subject_times?: string[];
}

export const WorkPayloadSchema: JTDSchemaType<WorkPayload> = {
    properties: {
        key: {
            type: 'string',
        },
        title: {
            type: 'string',
        },
        authors: {
            elements: AuthorIDPayloadSchema,
        },
        description: {
            type: 'string',
        },
    },
    optionalProperties: {
        subjects: {
            elements: {
                type: 'string',
            },
        },
        subject_people: {
            elements: {
                type: 'string',
            },
        },
        subject_places: {
            elements: {
                type: 'string',
            },
        },
        subject_times: {
            elements: {
                type: 'string',
            },
        },
    },
    additionalProperties: true,
};

// work editions

export interface EditionIDsPayload {
    entries: { key: string }[];
    size: number;
}

export const EditionIDsPayloadSchema: JTDSchemaType<EditionIDsPayload> = {
    properties: {
        entries: {
            elements: {
                properties: {
                    key: {
                        type: 'string',
                    },
                },
                additionalProperties: true,
            },
        },
        size: {
            type: 'uint32',
        },
    },
    additionalProperties: true,
};

// work reading stats

export interface ReadingStatsPayload {
    counts: {
        already_read: number;
        currently_reading: number;
        want_to_read: number;
    };
}

export const ReadingStatsPayloadSchema: JTDSchemaType<ReadingStatsPayload> = {
    properties: {
        counts: {
            properties: {
                already_read: {
                    type: 'int32',
                },
                currently_reading: {
                    type: 'int32',
                },
                want_to_read: {
                    type: 'int32',
                },
            },
            additionalProperties: true,
        },
    },
    additionalProperties: true,
};

// work ratings

export interface RatingsPayload {
    counts: {
        '1': number;
        '2': number;
        '3': number;
        '4': number;
        '5': number;
    };
    summary: {
        average: number;
        count: number;
    };
}

export const RatingsPayloadSchema: JTDSchemaType<RatingsPayload> = {
    properties: {
        counts: {
            properties: {
                1: {
                    type: 'uint32',
                },
                2: {
                    type: 'uint32',
                },
                3: {
                    type: 'uint32',
                },
                4: {
                    type: 'uint32',
                },
                5: {
                    type: 'uint32',
                },
            },
            additionalProperties: true,
        },
        summary: {
            properties: {
                average: {
                    type: 'float64',
                },
                count: {
                    type: 'uint32',
                },
            },
            additionalProperties: true,
        },
    },
};
