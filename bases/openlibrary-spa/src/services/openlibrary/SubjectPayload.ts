import { JTDSchemaType } from 'ajv/dist/jtd';

export interface WorkIDPayload {
    key: string;
}

export const WorkIDPayloadSchema: JTDSchemaType<WorkIDPayload> = {
    properties: {
        key: {
            type: 'string',
        },
    },
    additionalProperties: true,
};

export interface SubjectPayload {
    key: string;
    name: string;
    subject_type: string;
    work_count?: number;
    works?: WorkIDPayload[];
}

export const SubjectPayloadSchema: JTDSchemaType<SubjectPayload> = {
    properties: {
        key: {
            type: 'string',
        },
        name: {
            type: 'string',
        },
        subject_type: {
            type: 'string',
        },
    },
    optionalProperties: {
        work_count: {
            type: 'uint32',
        },
        works: {
            elements: WorkIDPayloadSchema,
        },
    },
    additionalProperties: true,
};
