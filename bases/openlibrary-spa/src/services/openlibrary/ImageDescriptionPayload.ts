import { JTDSchemaType } from 'ajv/dist/jtd';

// image description (author photo, edition cover)

export interface ImageDescriptionPayload {
    olid: string;
    width: number;
    height: number;
    source_url?: string | null;
}

export const ImageDescriptionPayloadSchema: JTDSchemaType<ImageDescriptionPayload> =
    {
        properties: {
            olid: {
                type: 'string',
            },
            width: {
                type: 'uint32',
            },
            height: {
                type: 'uint32',
            },
        },
        optionalProperties: {
            source_url: {
                type: 'string',
                nullable: true,
            },
        },
        additionalProperties: true,
    };
