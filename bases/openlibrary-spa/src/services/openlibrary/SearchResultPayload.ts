import { JTDSchemaType } from 'ajv/dist/jtd';

export interface SearchResultsPayload {
    docs: SearchResultPayload[];
    numFound: number;
    start: number;
}

export type SearchResultPayload = (
    | WorkSearchResultPayload
    | AuthorSearchResultPayload
) & { type: string };

export interface WorkSearchResultPayload {
    key: string;
    title: string;
    author_name: string[];
    contributor: string[];
    publish_year: number[];
    publisher: string[];
    subject: string[];
    // editions
    cover_edition_key: string;
    edition_count: number;
    // ratings
    ratings_average: number;
    ratings_count: number;
    ratings_count_1: number;
    ratings_count_2: number;
    ratings_count_3: number;
    ratings_count_4: number;
    ratings_count_5: number;
    // reading stats
    already_read_count: number;
    readinglog_count: number;
    want_to_read_count: number;
}

export interface AuthorSearchResultPayload {
    key: string;
    name: string;
    work_count: number;
    top_work: string;
    top_subjects?: string[];
    birth_date?: string;
    date?: string;
    death_date?: string;
}

export const WorkSearchResultPayloadSchema: JTDSchemaType<WorkSearchResultPayload> =
    {
        properties: {
            key: { type: 'string' },
            title: { type: 'string' },
            author_name: { elements: { type: 'string' } },
            contributor: { elements: { type: 'string' } },
            publish_year: { elements: { type: 'uint32' } },
            publisher: { elements: { type: 'string' } },
            subject: { elements: { type: 'string' } },
            // editions
            cover_edition_key: { type: 'string' },
            edition_count: { type: 'uint32' },
            // ratings
            ratings_average: { type: 'float64' },
            ratings_count: { type: 'uint32' },
            ratings_count_1: { type: 'uint32' },
            ratings_count_2: { type: 'uint32' },
            ratings_count_3: { type: 'uint32' },
            ratings_count_4: { type: 'uint32' },
            ratings_count_5: { type: 'uint32' },
            // reading stats
            already_read_count: { type: 'uint32' },
            readinglog_count: { type: 'uint32' },
            want_to_read_count: { type: 'uint32' },
        },
        additionalProperties: true,
    };

export const AuthorSearchResultPayloadSchema: JTDSchemaType<AuthorSearchResultPayload> =
    {
        properties: {
            key: { type: 'string' },
            name: { type: 'string' },
            work_count: { type: 'uint32' },
            top_work: { type: 'string' },
        },
        optionalProperties: {
            top_subjects: { elements: { type: 'string' } },
            birth_date: { type: 'string' },
            date: { type: 'string' },
            death_date: { type: 'string' },
        },
        additionalProperties: true,
    };

// cannot type these schemas yet

export const SearchResultPayloadSchema = {
    discriminator: 'type',
    mapping: {
        work: WorkSearchResultPayloadSchema,
        author: AuthorSearchResultPayloadSchema,
    },
};

export const SearchResultsPayloadSchema = {
    properties: {
        docs: {
            elements: SearchResultPayloadSchema,
        },
        numFound: { type: 'uint32' },
        start: { type: 'uint32' },
    },
    additionalProperties: true,
};
