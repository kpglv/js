import { JTDSchemaType } from 'ajv/dist/jtd';

export interface LinkPayload {
    title: string;
    url: string;
}

export const LinkPayloadSchema: JTDSchemaType<LinkPayload> = {
    properties: {
        title: {
            type: 'string',
        },
        url: {
            type: 'string',
        },
    },
    additionalProperties: true,
};
