import Ajv from 'ajv/dist/jtd';
import { AuthorPayload, AuthorPayloadSchema } from './AuthorPayload';
import {
    AuthorIDPayload as WorkAuthorIDPayload,
    EditionIDsPayloadSchema,
    RatingsPayloadSchema,
    ReadingStatsPayloadSchema,
    WorkPayload,
    WorkPayloadSchema,
    EditionIDsPayload,
    ReadingStatsPayload,
    RatingsPayload,
} from './WorkPayload';
import {
    SubjectPayload,
    SubjectPayloadSchema,
    WorkIDPayload as SubjectWorkIDPayload,
} from './SubjectPayload';
import {
    ImageDescriptionPayload,
    ImageDescriptionPayloadSchema,
} from './ImageDescriptionPayload';
import {
    AuthorIDPayload as EditionAuthorIDPayload,
    EditionPayload,
    EditionPayloadSchema,
} from './EditionPayload';
import { LinkPayload, LinkPayloadSchema } from './LinkPayload';
import { Author, AuthorID } from '@/domain/Author';
import { Link } from '@/domain/Link';
import { Ratings, ReadingStats, Work, WorkID } from '@/domain/Work';
import { Edition, EditionID } from '@/domain/Edition';
import { Subject } from '@/domain/Subject';
import { ImageDescription } from '@/domain/ImageDescription';
import {
    AuthorSearchResultPayload,
    SearchResultsPayload,
    SearchResultsPayloadSchema,
    WorkSearchResultPayload,
} from './SearchResultPayload';
import {
    AuthorSearchResult,
    SearchResult,
    SearchResults,
    WorkSearchResult,
} from '@/domain/SearchResults';

const ajv = new Ajv({ allErrors: true });

// helper functions

const identity = (x: unknown) => x;

const keyToOlid = (key: string): string => {
    const match = /^.*\/(.*)$/.exec(key);
    if (match) {
        const [_, olid] = match;
        return olid;
    } else {
        return '';
    }
};

const keyToLanguageID = keyToOlid;

// validators, parsers, serializers, transformers

// search results

export const validateSearchResultsPayload = ajv.compile(
    SearchResultsPayloadSchema
);
export const parseSearchResultsPayload = ajv.compileParser(
    SearchResultsPayloadSchema
);
export const serializeSearchResultsPayload = ajv.compileSerializer(
    SearchResultsPayloadSchema
);

export const payloadToWorkSearchResult = (
    payload: WorkSearchResultPayload & { type: string }
): WorkSearchResult & { type: string } => ({
    type: payload.type,
    olid: keyToOlid(payload.key),
    title: payload.title,
    authorName: payload.author_name?.[0] || '',
    contributors: payload.contributor || [],
    subjects: payload.subject || [],
    publishYears: payload.publish_year || [],
    publisher: payload.publisher || [],
    editions: {
        coverEditionOlid: payload.cover_edition_key || '',
        editionCount: payload.edition_count || 0,
    },
    ratings: {
        summary: {
            average: payload.ratings_average || 0,
            count: payload.ratings_count || 0,
        },
        counts: {
            1: payload.ratings_count_1 || 0,
            2: payload.ratings_count_2 || 0,
            3: payload.ratings_count_3 || 0,
            4: payload.ratings_count_4 || 0,
            5: payload.ratings_count_5 || 0,
        },
    },
    readingStats: {
        alreadyRead: payload.already_read_count || 0,
        currentlyReading: payload.readinglog_count || 0,
        wantToRead: payload.want_to_read_count || 0,
    },
});

export const payloadToAuthorSearchResult = (
    payload: AuthorSearchResultPayload & { type: string }
): AuthorSearchResult & { type: string } => ({
    type: payload.type,
    olid: payload.key,
    name: payload.name,
    works: {
        subjects: payload.top_subjects || [],
        count: payload.work_count || 0,
        top: payload.top_work || '',
    },
    birthDate: payload?.birth_date || '',
    deathDate: payload?.death_date || '',
    lifeSpan: payload?.date || '',
});

export const payloadToSearchResults = (
    payload: SearchResultsPayload
): SearchResults => ({
    count: payload.numFound,
    offset: payload.start,
    items: payload.docs
        .map(item => {
            switch (item?.type) {
                case 'work':
                    return payloadToWorkSearchResult(
                        item as WorkSearchResultPayload & { type: string }
                    );
                case 'author':
                    return payloadToAuthorSearchResult(
                        item as AuthorSearchResultPayload & { type: string }
                    );
                default:
                    return null;
            }
        })
        .filter(x => !!x) as SearchResult[],
});

// author

export const validateAuthorPayload = ajv.compile(AuthorPayloadSchema);
export const parseAuthorPayload = ajv.compileParser(AuthorPayloadSchema);
export const serializeAuthorPayload =
    ajv.compileSerializer(AuthorPayloadSchema);

export const payloadToAuthorID = (
    payload: AuthorPayload | WorkAuthorIDPayload | EditionAuthorIDPayload
): AuthorID => {
    let olid = '';
    if ('key' in payload) {
        olid = keyToOlid(payload.key);
    } else if ('author' in payload && 'key' in payload.author) {
        olid = keyToOlid(payload.author.key);
    }
    return { olid };
};

export const payloadToAuthor = (payload: AuthorPayload): Author => ({
    olid: keyToOlid(payload.key),
    name: payload.name,
    bio: payload.bio || '',
    birthDate: payload.birth_date || '',
    otherNames: [
        ...(payload.alternate_names || []),
        payload.personal_name,
        payload.fuller_name,
    ].filter(identity) as string[],
    links: (payload.links || []).map(payloadToLink),
});

// work

export const validateWorkPayload = ajv.compile(WorkPayloadSchema);
export const parseWorkPayload = ajv.compileParser(WorkPayloadSchema);
export const serializeWorkPayload = ajv.compileSerializer(WorkPayloadSchema);
export const payloadToWorkID = (payload: SubjectWorkIDPayload): WorkID => ({
    olid: keyToOlid(payload.key),
});
export const payloadToWork = (payload: WorkPayload): Work => ({
    olid: keyToOlid(payload.key),
    title: payload.title,
    authors: payload.authors?.map(payloadToAuthorID) || [],
    // FIXME: see issue #6
    description:
        (payload as any).description?.value || payload.description || '',
    subjects: payload.subjects || [],
    people: payload.subject_people || [],
    places: payload.subject_places || [],
    times: payload.subject_times || [],
});

// work editions

export const validateWorkEditionIDsPayload = ajv.compile(
    EditionIDsPayloadSchema
);
export const parseWorkEditionIDsPayload = ajv.compileParser(
    EditionIDsPayloadSchema
);
export const serializeWorkEditionIDsPayload = ajv.compileSerializer(
    EditionIDsPayloadSchema
);
export const payloadToWorkEditionIDs = (
    payload: EditionIDsPayload
): EditionID[] => {
    return payload.entries.map(editionID => ({
        olid: keyToOlid(editionID.key),
    }));
};

// work reading stats

export const validateReadingStatsPayload = ajv.compile(
    ReadingStatsPayloadSchema
);
export const parseReadingStatsPayload = ajv.compileParser(
    ReadingStatsPayloadSchema
);
export const serializeReadingStatsPayload = ajv.compileSerializer(
    ReadingStatsPayloadSchema
);
export const payloadToReadingStats = (
    payload: ReadingStatsPayload
): ReadingStats => ({
    counts: {
        alreadyRead: payload?.counts?.already_read,
        currentlyReading: payload?.counts?.currently_reading,
        wantToRead: payload?.counts?.want_to_read,
    },
});

// work ratings

export const validateRatingsPayload = ajv.compile(RatingsPayloadSchema);
export const parseRatingsPayload = ajv.compileParser(RatingsPayloadSchema);
export const serializeRatingsPayload = ajv.compileSerializer(
    RatingsPayloadSchema
);
export const payloadToRatings = (payload: RatingsPayload): Ratings => ({
    counts: {
        1: payload?.counts?.['1'] || 0,
        2: payload?.counts?.['2'] || 0,
        3: payload?.counts?.['3'] || 0,
        4: payload?.counts?.['4'] || 0,
        5: payload?.counts?.['5'] || 0,
    },
    summary: {
        average: payload?.summary?.average || 0,
        count: payload?.summary?.count || 0,
    },
});

// edition

export const validateEditionPayload = ajv.compile(EditionPayloadSchema);
export const parseEditionPayload = ajv.compileParser(EditionPayloadSchema);
export const serializeEditionPayload = ajv.compileSerializer(
    EditionPayloadSchema
);
export const payloadToEdition = (payload: EditionPayload): Edition => ({
    olid: keyToOlid(payload.key),
    title: payload.title,
    authors: payload.authors.map(payloadToAuthorID),
    numberOfPages: payload.number_of_pages || NaN,
    publishers: payload.publishers,
    publishDate: payload.publish_date,
    languages: (payload.languages || []).map(lang =>
        keyToLanguageID(lang.key)
    ),
    contributions: payload.contributions || [],
    firstSentence: payload.first_sentence?.value || '',
});

// subject

export const validateSubjectPayload = ajv.compile(SubjectPayloadSchema);
export const parseSubjectPayload = ajv.compileParser(SubjectPayloadSchema);
export const serializeSubjectPayload = ajv.compileSerializer(
    SubjectPayloadSchema
);
export const payloadToSubject = (payload: SubjectPayload): Subject => ({
    olid: keyToOlid(payload.key),
    name: payload.name,
    type: payload.subject_type,
    worksCount: payload.work_count || NaN,
    works: (payload.works || []).map(payloadToWorkID),
});

// image description

export const validateImageDescriptionPayload = ajv.compile(
    ImageDescriptionPayloadSchema
);
export const parseImageDescriptionPayload = ajv.compileParser(
    ImageDescriptionPayloadSchema
);
export const serializeImageDescriptionPayload = ajv.compileSerializer(
    ImageDescriptionPayloadSchema
);
export const payloadToImageDescription = (
    payload: ImageDescriptionPayload
): ImageDescription => ({
    olid: payload.olid,
    width: payload.width,
    height: payload.height,
    sourceUrl: payload.source_url || '',
});

// link

export const validateLinkPayload = ajv.compile(LinkPayloadSchema);
export const parseLinkPayload = ajv.compileParser(LinkPayloadSchema);
export const serializeLinkPayload = ajv.compileSerializer(LinkPayloadSchema);
export const payloadToLink = (payload: LinkPayload): Link => ({
    title: payload.title,
    url: payload.url,
});
