import 'server-only';

export interface Cat {
    id: string;
    url: string;
}

export interface CatsParams {
    limit?: string;
}

export const catsUrl = 'https://api.thecatapi.com/v1/images/search';

export const getCats = async (params: CatsParams = {}, options = {}) => {
    const url = new URL(catsUrl);
    const { limit } = params;
    if (limit) {
        url.searchParams.set('limit', limit);
    }
    const cats = await fetch(url, options);
    if (!cats.ok) {
        throw new Error(`failed to fetch data from ${catsUrl}`);
    }
    return cats.json();
};
