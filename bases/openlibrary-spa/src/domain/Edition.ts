import { JTDSchemaType } from 'ajv/dist/jtd';
import { type AuthorID, AuthorIDSchema } from './Author';

export interface EditionID {
    olid: string;
}

export const EditionIDSchema: JTDSchemaType<EditionID> = {
    properties: {
        olid: {
            type: 'string',
        },
    },
};

export const EditionIDsSchema: JTDSchemaType<EditionID[]> = {
    elements: EditionIDSchema,
};

export interface Edition extends EditionID {
    title: string;
    authors: AuthorID[];
    numberOfPages: number;
    publishers: string[];
    publishDate: string;
    languages: string[];
    contributions: string[];
    firstSentence: string;
}

export const EditionSchema: JTDSchemaType<Edition> = {
    properties: {
        olid: {
            type: 'string',
        },
        title: {
            type: 'string',
        },
        numberOfPages: {
            type: 'uint32',
        },
        authors: {
            elements: AuthorIDSchema,
        },
        publishers: {
            elements: {
                type: 'string',
            },
        },
        publishDate: {
            type: 'string',
        },
        languages: {
            elements: {
                type: 'string',
            },
        },
        contributions: {
            elements: {
                type: 'string',
            },
        },
        firstSentence: {
            type: 'string',
        },
    },
    additionalProperties: true,
};
