import { JTDSchemaType } from 'ajv/dist/jtd';

// image description (author photo, edition cover)

export interface ImageDescription {
    olid: string;
    width: number;
    height: number;
    sourceUrl: string;
}

export const ImageDescriptionSchema: JTDSchemaType<ImageDescription> = {
    properties: {
        olid: {
            type: 'string',
        },
        width: {
            type: 'int32',
        },
        height: {
            type: 'int32',
        },
        sourceUrl: {
            type: 'string',
        },
    },
    additionalProperties: true,
};
