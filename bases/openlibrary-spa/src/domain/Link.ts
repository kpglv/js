import { JTDSchemaType } from 'ajv/dist/jtd';

export interface Link {
    title: string;
    url: string;
}

export const LinkSchema: JTDSchemaType<Link> = {
    properties: {
        title: {
            type: 'string',
        },
        url: {
            type: 'string',
        },
    },
    additionalProperties: true,
};
