import { JTDSchemaType } from 'ajv/dist/jtd';
import { type Link, LinkSchema } from './Link';

export interface AuthorID {
    olid: string;
}

export const AuthorIDSchema: JTDSchemaType<AuthorID> = {
    properties: {
        olid: {
            type: 'string',
        },
    },
};

export interface Author extends AuthorID {
    name: string;
    bio: string;
    birthDate: string;
    otherNames?: string[];
    links?: Link[];
}

export const AuthorSchema: JTDSchemaType<Author> = {
    properties: {
        olid: {
            type: 'string',
        },
        name: {
            type: 'string',
        },
        bio: {
            type: 'string',
        },
        birthDate: {
            type: 'string',
        },
    },
    optionalProperties: {
        otherNames: {
            elements: {
                type: 'string',
            },
        },
        links: {
            elements: LinkSchema,
        },
    },
    additionalProperties: true,
};
