import { JTDSchemaType } from 'ajv/dist/jtd';
import { type WorkID, WorkIDSchema } from './Work';

export interface Subject {
    olid: string;
    name: string;
    type: string;
    worksCount: number;
    works: WorkID[];
}

export const SubjectSchema: JTDSchemaType<Subject> = {
    properties: {
        olid: {
            type: 'string',
        },
        name: {
            type: 'string',
        },
        type: {
            type: 'string',
        },
        worksCount: {
            type: 'uint32',
        },
        works: {
            elements: WorkIDSchema,
        },
    },
    additionalProperties: true,
};
