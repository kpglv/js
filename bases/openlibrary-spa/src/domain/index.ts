import { AuthorSchema } from './Author';
import { RatingsSchema, ReadingStatsSchema, WorkSchema } from './Work';
import { SubjectSchema } from './Subject';
import { ImageDescriptionSchema } from './ImageDescription';
import { EditionIDsSchema, EditionSchema } from './Edition';
import { SearchResultsSchema } from './SearchResults';

import Ajv from 'ajv/dist/jtd';
const ajv = new Ajv({ allErrors: true });

// parsers, serializers, transformers, validators

// search results

export const validateSearchResults = ajv.compile(SearchResultsSchema);
export const parseSearchResults = ajv.compileParser(SearchResultsSchema);
export const serializeSearchResults =
    ajv.compileSerializer(SearchResultsSchema);

// author

export const validateAuthor = ajv.compile(AuthorSchema);
export const parseAuthor = ajv.compileParser(AuthorSchema);
export const serializeAuthor = ajv.compileSerializer(AuthorSchema);

// work

export const validateWork = ajv.compile(WorkSchema);
export const parseWork = ajv.compileParser(WorkSchema);
export const serializeWork = ajv.compileSerializer(WorkSchema);

// work edition IDs

export const validateWorkEditionIDs = ajv.compile(EditionIDsSchema);
export const parseWorkEditionIDs = ajv.compileParser(EditionIDsSchema);
export const serializeWorkEditionIDs =
    ajv.compileSerializer(EditionIDsSchema);

// work reading stats

export const validateReadingStats = ajv.compile(ReadingStatsSchema);
export const parseReadingStats = ajv.compileParser(ReadingStatsSchema);
export const serializeReadingStats =
    ajv.compileSerializer(ReadingStatsSchema);

// work ratings

export const validateRatings = ajv.compile(RatingsSchema);
export const parseRatings = ajv.compileParser(RatingsSchema);
export const serializeRatings = ajv.compileSerializer(RatingsSchema);

// edition

export const validateEdition = ajv.compile(EditionSchema);
export const parseEdition = ajv.compileParser(EditionSchema);
export const serializeEdition = ajv.compileSerializer(EditionSchema);

// subject

export const validateSubject = ajv.compile(SubjectSchema);
export const parseSubject = ajv.compileParser(SubjectSchema);
export const serializeSubject = ajv.compileSerializer(SubjectSchema);

// image description

export const validateImageDescription = ajv.compile(ImageDescriptionSchema);
export const parseImageDescription = ajv.compileParser(
    ImageDescriptionSchema
);
export const serializeImageDescription = ajv.compileSerializer(
    ImageDescriptionSchema
);
