import { JTDSchemaType } from 'ajv/dist/core';
import { type AuthorID, AuthorIDSchema } from './Author';

export interface WorkID {
    olid: string;
}

export const WorkIDSchema: JTDSchemaType<WorkID> = {
    properties: {
        olid: {
            type: 'string',
        },
    },
};

export interface Work extends WorkID {
    title: string;
    authors: AuthorID[];
    description: string;
    subjects: string[];
    people: string[];
    places: string[];
    times: string[];
}

export const WorkSchema: JTDSchemaType<Work> = {
    properties: {
        olid: {
            type: 'string',
        },
        title: {
            type: 'string',
        },
        authors: {
            elements: AuthorIDSchema,
        },
        description: {
            type: 'string',
        },
        subjects: {
            elements: {
                type: 'string',
            },
        },
        people: {
            elements: {
                type: 'string',
            },
        },
        places: {
            elements: {
                type: 'string',
            },
        },
        times: {
            elements: {
                type: 'string',
            },
        },
    },
    additionalProperties: true,
};

// work reading stats

export interface ReadingStats {
    counts: {
        alreadyRead: number;
        currentlyReading: number;
        wantToRead: number;
    };
}

export const ReadingStatsSchema: JTDSchemaType<ReadingStats> = {
    properties: {
        counts: {
            properties: {
                alreadyRead: {
                    type: 'int32',
                },
                currentlyReading: {
                    type: 'int32',
                },
                wantToRead: {
                    type: 'int32',
                },
            },
            additionalProperties: true,
        },
    },
    additionalProperties: true,
};

// work ratings

export interface Ratings {
    counts: {
        '1': number;
        '2': number;
        '3': number;
        '4': number;
        '5': number;
    };
    summary: {
        average: number;
        count: number;
    };
}

export const RatingsSchema: JTDSchemaType<Ratings> = {
    properties: {
        counts: {
            properties: {
                1: {
                    type: 'uint32',
                },
                2: {
                    type: 'uint32',
                },
                3: {
                    type: 'uint32',
                },
                4: {
                    type: 'uint32',
                },
                5: {
                    type: 'uint32',
                },
            },
            additionalProperties: true,
        },
        summary: {
            properties: {
                average: {
                    type: 'float64',
                },
                count: {
                    type: 'uint32',
                },
            },
            additionalProperties: true,
        },
    },
    additionalProperties: true,
};
