import { JTDSchemaType } from 'ajv/dist/jtd';

export interface SearchResults {
    items: SearchResult[];
    count: number;
    offset: number;
}

export type SearchResult = (WorkSearchResult | AuthorSearchResult) & {
    type: string;
};

export interface WorkSearchResult {
    olid: string;
    title: string;
    authorName: string;
    contributors: string[];
    subjects: string[];
    publishYears: number[];
    publisher: string[];
    editions: {
        coverEditionOlid: string;
        editionCount: number;
    };
    ratings: {
        summary: {
            average: number;
            count: number;
        };
        counts: {
            '1': number;
            '2': number;
            '3': number;
            '4': number;
            '5': number;
        };
    };
    readingStats: {
        alreadyRead: number;
        currentlyReading: number;
        wantToRead: number;
    };
}
export interface AuthorSearchResult {
    olid: string;
    name: string;
    works: {
        subjects: string[];
        count: number;
        top: string;
    };
    birthDate?: string;
    deathDate?: string;
    lifeSpan?: string;
}

export const WorkSearchResultSchema: JTDSchemaType<WorkSearchResult> = {
    properties: {
        olid: { type: 'string' },
        title: { type: 'string' },
        authorName: { type: 'string' },
        contributors: { elements: { type: 'string' } },
        subjects: { elements: { type: 'string' } },
        publishYears: { elements: { type: 'uint32' } },
        publisher: { elements: { type: 'string' } },
        editions: {
            properties: {
                coverEditionOlid: { type: 'string' },
                editionCount: { type: 'uint32' },
            },
            additionalProperties: true,
        },
        ratings: {
            properties: {
                summary: {
                    properties: {
                        average: { type: 'float64' },
                        count: { type: 'uint32' },
                    },
                    additionalProperties: true,
                },
                counts: {
                    properties: {
                        '1': { type: 'uint32' },
                        '2': { type: 'uint32' },
                        '3': { type: 'uint32' },
                        '4': { type: 'uint32' },
                        '5': { type: 'uint32' },
                    },
                    additionalProperties: true,
                },
            },
            additionalProperties: true,
        },
        readingStats: {
            properties: {
                alreadyRead: { type: 'uint32' },
                currentlyReading: { type: 'uint32' },
                wantToRead: { type: 'uint32' },
            },
            additionalProperties: true,
        },
    },
    additionalProperties: true,
};

export const AuthorSearchResultSchema: JTDSchemaType<AuthorSearchResult> = {
    properties: {
        olid: { type: 'string' },
        name: { type: 'string' },
        works: {
            properties: {
                subjects: { elements: { type: 'string' } },
                count: { type: 'uint32' },
                top: { type: 'string' },
            },
            additionalProperties: true,
        },
    },
    optionalProperties: {
        birthDate: { type: 'string' },
        deathDate: { type: 'string' },
        lifeSpan: { type: 'string' },
    },
    additionalProperties: true,
};

// cannot type these schemas yet

export const SearchResultSchema = {
    discriminator: 'type',
    mapping: {
        work: WorkSearchResultSchema,
        author: AuthorSearchResultSchema,
    },
};

export const SearchResultsSchema = {
    properties: {
        items: { elements: SearchResultSchema },
        count: { type: 'uint32' },
        offset: { type: 'uint32' },
    },
};
