import { Card } from 'flowbite-react';

const Drawer = () => {
    return (
        <aside
            className="fixed left-0 z-0 w-64 h-screen px-4 py-24 transition-transform -translate-x-full bg-white border-r border-gray-200 md:translate-x-0 dark:bg-gray-800 dark:border-gray-700"
            aria-label="aside-info"
        >
            <Card className="max-w-sm">
                <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                    New features ahead
                </h5>
                <p className="font-normal text-gray-700 dark:text-gray-400">
                    Maybe not soon, but coming nevertheless:
                </p>
                <ul className="list-disc list-inside">
                    <li>Favorite books</li>
                    <li>Import/export</li>
                </ul>
            </Card>
        </aside>
    );
};

export default Drawer;
