'use client';

import { DarkThemeToggle, Navbar } from 'flowbite-react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';

const search = '/';
const help = '/help';
const about = '/about';

const navbarTheme = {
    collapse: {
        list: [
            'mt-4',
            'flex flex-col',
            'md:mt-0 md:flex-row md:items-center md:space-x-8 md:text-sm md:font-medium',
        ].join(' '),
    },
    link: {
        active: {
            on: [
                'text-white dark:text-white bg-blue-700',
                'md:bg-transparent md:text-blue-700',
            ].join(' '),
        },
    },
};

const Header = () => {
    const pathname = usePathname();
    const isActive = (href: string) => href === pathname;

    return (
        <div className="fixed w-screen z-50 border-b border-gray-200 dark:bg-gray-800 dark:border-gray-700">
            <Navbar fluid theme={navbarTheme}>
                <Navbar.Brand as={Link} href="/">
                    <svg
                        className="mr-4 ml-2 h-6 sm:ml-0 sm:h-8 text-gray-800 dark:text-white"
                        aria-hidden="true"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 20 18"
                    >
                        <path
                            stroke="currentColor"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M10 16.5c0-1-8-2.7-9-2V1.8c1-1 9 .707 9 1.706M10 16.5V3.506M10 16.5c0-1 8-2.7 9-2V1.8c-1-1-9 .707-9 1.706"
                        />
                    </svg>
                    <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">
                        My Open Library
                    </span>
                </Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse>
                    <Navbar.Link
                        as={Link}
                        href={search}
                        active={isActive(search)}
                    >
                        Search
                    </Navbar.Link>
                    <Navbar.Link
                        as={Link}
                        href="/help"
                        active={isActive(help)}
                    >
                        Help
                    </Navbar.Link>
                    <Navbar.Link
                        as={Link}
                        href="/about"
                        active={isActive(about)}
                    >
                        About
                    </Navbar.Link>
                    <li className="m-2 md:m-0">
                        <DarkThemeToggle />
                    </li>
                </Navbar.Collapse>
            </Navbar>
        </div>
    );
};

export default Header;
