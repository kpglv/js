import { Footer } from 'flowbite-react';
import Link from 'next/link';

const styles = {
    footer: [
        'relative w-full z-50 border-t rounded-none',
        'bg-white dark:bg-gray-800',
        'border-gray-200 dark:border-gray-700',
    ].join(' '),
    grid: ['grid w-full grid-cols-2 gap-8 px-6 py-8', ' md:grid-cols-4'].join(
        ' '
    ),
    section: [].join(' '),
    internalLink: ['mb-4'].join(' '),
    copyright: [
        'p-4 border-t',
        'dark:text-gray-400 bg-gray-50 dark:bg-gray-700',
        'border-gray-200 dark:border-gray-700',
    ].join(' '),
};

const AFooter = () => {
    return (
        <Footer className={styles.footer}>
            <div className={styles.grid}>
                <div>
                    <Footer.Title title="Navigation" />
                    <Footer.LinkGroup>
                        <Link className={styles.internalLink} href="/">
                            Search
                        </Link>
                        <Link className={styles.internalLink} href="/help">
                            Help
                        </Link>
                        <Link className={styles.internalLink} href="/about">
                            About
                        </Link>
                    </Footer.LinkGroup>
                </div>
                <div>
                    <Footer.Title title="See also" />
                    <Footer.LinkGroup>
                        <Link href="/cats">Some cats</Link>
                    </Footer.LinkGroup>
                </div>
                <div>
                    <Footer.Title title="About Me" />
                    <Footer.LinkGroup>
                        <Footer.Link
                            href="https://kpglv.gitlab.io/personal"
                            target="_blank"
                        >
                            Home page
                        </Footer.Link>
                        <Footer.Link
                            href="https://gitlab.com/users/kpglv/projects"
                            target="_blank"
                        >
                            Gitlab
                        </Footer.Link>
                    </Footer.LinkGroup>
                </div>
                <div>
                    <Footer.Title title="Powered By" />
                    <Footer.LinkGroup>
                        <Footer.Link
                            href="https://openlibrary.org/developers/api"
                            target="_blank"
                        >
                            Open Library API
                        </Footer.Link>
                    </Footer.LinkGroup>
                </div>
            </div>
            <Footer.Copyright
                className={styles.copyright}
                by="Constantine Pigalev"
                year={2023}
            />
        </Footer>
    );
};

export default AFooter;
