import { type Cat } from '@/services/cats/api';

interface CatsListProps {
    cats: Cat[];
}

const Cats = async ({ cats }: CatsListProps) => {
    return (
        <div>
            {cats.map((cat: Cat) => {
                return <img key={cat.id} src={cat.url} alt="a cat" />;
            })}
        </div>
    );
};

export default Cats;
