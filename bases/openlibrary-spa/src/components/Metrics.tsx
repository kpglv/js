'use client'

import { useReportWebVitals } from 'next/web-vitals';

interface MetricsProps {
    section: string;
}

const Metrics = ({ section }: MetricsProps) => {
    useReportWebVitals((metric) => {
        if (metric.rating !== 'good') {
            console.log(section, metric)
        }
    });
    return null;
};

export default Metrics;