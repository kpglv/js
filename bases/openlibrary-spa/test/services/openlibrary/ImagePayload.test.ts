import {
    parseImageDescriptionPayload,
    payloadToImageDescription,
    serializeImageDescriptionPayload,
    validateImageDescriptionPayload,
} from '@/services/openlibrary';
import editionCoverPayload from '../../data/editions/OL7353617M.jpg.json';
import authorPhoto from '../../data/authors/OL23919A.jpg.json';
import { validateImageDescription } from '@/domain';

describe('example Image payload (edition cover)', () => {
    it('correctly validates, serializes, parses, and transforms', () => {
        const valid = validateImageDescriptionPayload(editionCoverPayload);
        if (!valid) {
            console.log(
                'validation errors:',
                validateImageDescriptionPayload.errors
            );
        }
        expect(valid).toBe(true);

        const imageString =
            serializeImageDescriptionPayload(editionCoverPayload);
        expect(typeof imageString).toBe('string');

        // result will be undefined if parse errors occur
        const parsedImagePayload = parseImageDescriptionPayload(imageString);
        if (parsedImagePayload === undefined) {
            console.log(
                'parsing errors:',
                parseImageDescriptionPayload.message,
                'at',
                parseImageDescriptionPayload.position
            );
        } else {
            expect(
                validateImageDescription(
                    payloadToImageDescription(parsedImagePayload)
                )
            ).toBe(true);
        }
        expect(parsedImagePayload).not.toBeUndefined();
    });
});

describe('example Image payload (author photo)', () => {
    it('correctly validates, serializes, parses, and transforms', () => {
        const valid = validateImageDescriptionPayload(authorPhoto);
        if (!valid) {
            console.log(
                'validation errors:',
                validateImageDescriptionPayload.errors
            );
        }
        expect(valid).toBe(true);

        const imageString = serializeImageDescriptionPayload(authorPhoto);
        expect(typeof imageString).toBe('string');

        // result will be undefined if parse errors occur
        const parsedImagePayload = parseImageDescriptionPayload(imageString);
        if (parsedImagePayload === undefined) {
            console.log(
                'parsing errors:',
                parseImageDescriptionPayload.message,
                'at',
                parseImageDescriptionPayload.position
            );
        } else {
            expect(
                validateImageDescription(
                    payloadToImageDescription(parsedImagePayload)
                )
            ).toBe(true);
        }
        expect(parsedImagePayload).not.toBeUndefined();
    });
});
