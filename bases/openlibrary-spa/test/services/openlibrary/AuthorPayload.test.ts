import {
    parseAuthorPayload,
    payloadToAuthor,
    serializeAuthorPayload,
    validateAuthorPayload,
} from '@/services/openlibrary';
import { validateAuthor } from '@/domain';
import authorPayload from '../../data/authors/OL23919A.json';

describe('example Author payload', () => {
    it('correctly validates, serializes, parses, and transforms', () => {
        // validation
        const valid = validateAuthorPayload(authorPayload);
        if (!valid) {
            console.log('validation errors:', validateAuthorPayload.errors);
        }
        expect(valid).toBe(true);

        // serialization
        const authorString = serializeAuthorPayload(authorPayload);
        expect(typeof authorString).toBe('string');

        // parsing
        // result will be undefined if parse errors occur
        const parsedAuthorPayload = parseAuthorPayload(authorString);
        if (parsedAuthorPayload === undefined) {
            console.log(
                'parsing errors:',
                parseAuthorPayload.message,
                'at',
                parseAuthorPayload.position
            );
        } else {
            // transforming
            const author = payloadToAuthor(parsedAuthorPayload);
            expect(validateAuthor(author)).toBe(true);
        }
        expect(parsedAuthorPayload).not.toBeUndefined();
    });
});
