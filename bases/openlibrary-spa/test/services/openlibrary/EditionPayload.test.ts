import {
    parseEditionPayload,
    payloadToEdition,
    serializeEditionPayload,
    validateEditionPayload,
} from '@/services/openlibrary';
import editionPayload from '../../data/editions/OL7353617M.json';
import { validateEdition } from '@/domain';

describe('example Edition payload', () => {
    it('correctly validates, serializes, parses, and transforms', () => {
        const valid = validateEditionPayload(editionPayload);
        if (!valid) {
            console.log('validation errors:', validateEditionPayload.errors);
        }
        expect(valid).toBe(true);

        const editionString = serializeEditionPayload(editionPayload);
        expect(typeof editionString).toBe('string');

        // result will be undefined if parse errors occur
        const parsedEditionPayload = parseEditionPayload(editionString);
        if (parsedEditionPayload === undefined) {
            console.log(
                'parsing errors:',
                parseEditionPayload.message,
                'at',
                parseEditionPayload.position
            );
        } else {
            expect(
                validateEdition(payloadToEdition(parsedEditionPayload))
            ).toBe(true);
        }
        expect(parsedEditionPayload).not.toBeUndefined();
    });
});
