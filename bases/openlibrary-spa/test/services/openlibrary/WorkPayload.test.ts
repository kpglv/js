import {
    parseWorkPayload,
    parseReadingStatsPayload,
    serializeReadingStatsPayload,
    serializeWorkPayload,
    validateReadingStatsPayload,
    validateWorkPayload,
    validateRatingsPayload,
    serializeRatingsPayload,
    parseRatingsPayload,
    validateWorkEditionIDsPayload,
    serializeWorkEditionIDsPayload,
    parseWorkEditionIDsPayload,
    payloadToWork,
    payloadToReadingStats,
    payloadToRatings,
    payloadToWorkEditionIDs,
} from '@/services/openlibrary';
import workPayload from '../../data/works/OL45804W.json';
import readingStatsPayload from '../../data/works/OL45804W.bookshelves.json';
import ratingsPayload from '../../data/works/OL45804W.ratings.json';
import editionsPayload from '../../data/works/OL45804W.editions.json';
import {
    validateRatings,
    validateReadingStats,
    validateWork,
    validateWorkEditionIDs,
} from '@/domain';

describe('example Work payload', () => {
    it('correctly validates, serializes, parses, and transforms', () => {
        const valid = validateWorkPayload(workPayload);
        if (!valid) {
            console.log('validation errors:', validateWorkPayload.errors);
        }
        expect(valid).toBe(true);

        const workString = serializeWorkPayload(workPayload);
        expect(typeof workString).toBe('string');

        // result will be undefined if parse errors occur
        const parsedWorkPayload = parseWorkPayload(workString);
        if (parsedWorkPayload === undefined) {
            console.log(
                'parsing errors:',
                parseWorkPayload.message,
                'at',
                parseWorkPayload.position
            );
        } else {
            expect(validateWork(payloadToWork(parsedWorkPayload))).toBe(true);
        }
        expect(parsedWorkPayload).not.toBeUndefined();
    });
});

describe('example Work reading stats payload', () => {
    it('correctly validates, serializes, parses, and transforms', () => {
        const valid = validateReadingStatsPayload(readingStatsPayload);
        if (!valid) {
            console.log(
                'validation errors:',
                validateReadingStatsPayload.errors
            );
        }
        expect(valid).toBe(true);

        const readingStatsString =
            serializeReadingStatsPayload(readingStatsPayload);
        expect(typeof readingStatsString).toBe('string');

        // result will be undefined if parse errors occur
        const parsedReadingStatsPayload =
            parseReadingStatsPayload(readingStatsString);
        if (parsedReadingStatsPayload === undefined) {
            console.log(
                'parsing errors:',
                parseReadingStatsPayload.message,
                'at',
                parseReadingStatsPayload.position
            );
        } else {
            expect(
                validateReadingStats(
                    payloadToReadingStats(parsedReadingStatsPayload)
                )
            ).toBe(true);
        }
        expect(parsedReadingStatsPayload).not.toBeUndefined();
    });
});

describe('example Work ratings payload', () => {
    it('correctly validates, serializes, parses, and transforms', () => {
        const valid = validateRatingsPayload(ratingsPayload);
        if (!valid) {
            console.log('validation errors:', validateRatingsPayload.errors);
        }
        expect(valid).toBe(true);

        const ratingsString = serializeRatingsPayload(ratingsPayload);
        expect(typeof ratingsString).toBe('string');

        // result will be undefined if parse errors occur
        const parsedRatingsPayload = parseRatingsPayload(ratingsString);
        if (parsedRatingsPayload === undefined) {
            console.log(
                'parsing errors:',
                parseRatingsPayload.message,
                'at',
                parseRatingsPayload.position
            );
        } else {
            expect(
                validateRatings(payloadToRatings(parsedRatingsPayload))
            ).toBe(true);
        }
        expect(parsedRatingsPayload).not.toBeUndefined();
    });
});

describe('example Work editions payload', () => {
    it('correctly validates, serializes, parses, and transforms', () => {
        const valid = validateWorkEditionIDsPayload(editionsPayload);
        if (!valid) {
            console.log(
                'validation errors:',
                validateWorkEditionIDsPayload.errors
            );
        }
        expect(valid).toBe(true);

        const ratingsString = serializeWorkEditionIDsPayload(editionsPayload);
        expect(typeof ratingsString).toBe('string');

        // result will be undefined if parse errors occur
        const parsedEditionIDsPayload =
            parseWorkEditionIDsPayload(ratingsString);
        if (parsedEditionIDsPayload === undefined) {
            console.log(
                'parsing errors:',
                parseWorkEditionIDsPayload.message,
                'at',
                parseWorkEditionIDsPayload.position
            );
        } else {
            expect(
                validateWorkEditionIDs(
                    payloadToWorkEditionIDs(parsedEditionIDsPayload)
                )
            ).toBe(true);
        }
        expect(parsedEditionIDsPayload).not.toBeUndefined();
    });
});
