import {
    parseSearchResultsPayload,
    payloadToSearchResults,
    serializeSearchResultsPayload,
    validateSearchResultsPayload,
} from '@/services/openlibrary';
import type {
    WorkSearchResultPayload,
    AuthorSearchResultPayload,
    SearchResultsPayload,
} from '@/services/openlibrary/SearchResultPayload';
import type { SearchResults } from '@/domain/SearchResults';
import { validateSearchResults } from '@/domain';
import authorsSearchResultPayload from '../../data/search/authors/twain.json';
import worksSearchResultPayload from '../../data/search/works/lotr-1.json';

// TODO: check payload/entity types (author, work)

describe('example author search results payload', () => {
    it('correctly validates, serializes, parses, and transforms', () => {
        // validation
        const valid = validateSearchResultsPayload(
            authorsSearchResultPayload
        );
        if (!valid) {
            console.log(
                'payload validation errors:',
                validateSearchResultsPayload.errors
            );
        }
        expect(valid).toBe(true);

        // serialization
        const searchResultsString = serializeSearchResultsPayload(
            authorsSearchResultPayload
        );
        expect(typeof searchResultsString).toBe('string');

        // parsing
        // result will be undefined if parse errors occur
        const parsedSearchResultsPayload =
            parseSearchResultsPayload(searchResultsString);
        if (parsedSearchResultsPayload === undefined) {
            console.log(
                'parsing errors:',
                parseSearchResultsPayload.message,
                'at',
                parseSearchResultsPayload.position
            );
        } else {
            // transforming
            const searchResults = payloadToSearchResults(
                parsedSearchResultsPayload as SearchResultsPayload
            );
            const ok = validateSearchResults(searchResults as SearchResults);
            if (!ok) {
                console.log(
                    'entity validation errors:',
                    validateSearchResults.errors
                );
            }
            expect(ok).toBe(true);
        }
        expect(parsedSearchResultsPayload).not.toBeUndefined();
    });
});

describe('example work search results payload', () => {
    it('correctly validates, serializes, parses, and transforms', () => {
        // validation
        const valid = validateSearchResultsPayload(worksSearchResultPayload);
        if (!valid) {
            console.log(
                'payload validation errors:',
                validateSearchResultsPayload.errors
            );
        }
        expect(valid).toBe(true);

        // serialization
        const searchResultsString = serializeSearchResultsPayload(
            worksSearchResultPayload
        );
        expect(typeof searchResultsString).toBe('string');

        // parsing
        // result will be undefined if parse errors occur
        const parsedSearchResultsPayload =
            parseSearchResultsPayload(searchResultsString);
        if (parsedSearchResultsPayload === undefined) {
            console.log(
                'parsing errors:',
                parseSearchResultsPayload.message,
                'at',
                parseSearchResultsPayload.position
            );
        } else {
            // transforming
            const searchResults = payloadToSearchResults(
                parsedSearchResultsPayload as SearchResultsPayload
            );
            const ok = validateSearchResults(searchResults as SearchResults);
            if (!ok) {
                console.log(
                    'entity validation errors:',
                    validateSearchResults.errors
                );
            }
            expect(ok).toBe(true);
        }
        expect(parsedSearchResultsPayload).not.toBeUndefined();
    });
});
