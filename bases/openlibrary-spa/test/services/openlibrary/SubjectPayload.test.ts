import {
    parseSubjectPayload,
    payloadToSubject,
    serializeSubjectPayload,
    validateSubjectPayload,
} from '@/services/openlibrary';
import subjectPayload from '../../data/subjects/History.json';
import { validateSubject } from '@/domain';

describe('example Subject payload', () => {
    it('correctly validates, serializes, parses, and transforms', () => {
        const valid = validateSubjectPayload(subjectPayload);
        if (!valid) {
            console.log('validation errors:', validateSubjectPayload.errors);
        }
        expect(valid).toBe(true);

        const subjectString = serializeSubjectPayload(subjectPayload);
        expect(typeof subjectString).toBe('string');

        // result will be undefined if parse errors occur
        const parsedSubjectPayload = parseSubjectPayload(subjectString);
        if (parsedSubjectPayload === undefined) {
            console.log(
                'parsing errors:',
                parseSubjectPayload.message,
                'at',
                parseSubjectPayload.position
            );
        } else {
            expect(
                validateSubject(payloadToSubject(parsedSubjectPayload))
            ).toBe(true);
        }
        expect(parsedSubjectPayload).not.toBeUndefined();
    });
});
