# openlibrary-spa

A base that provides a web interface (SPA) for a subset of Open Library APIs.

- Open Library https://openlibrary.org/
- Open Library API https://openlibrary.org/developers/api

## Overview

Uses React, Next.js and Flowbite component library.

- React https://react.dev/
- Next.js: Introduction https://nextjs.org/docs
- Flowbite - Tailwind CSS component library
  - Introduction https://flowbite.com/docs/getting-started/introduction/
  - Flowbite Blocks https://flowbite.com/blocks
  - Flowbite React
    https://www.flowbite-react.com/docs/getting-started/introduction
  - Flowbite React - Use with Next.js
    https://www.flowbite-react.com/docs/getting-started/nextjs

## Demo

Deployed at Vercel: https://js-openlibrary-spa.vercel.app/

## Getting Started

Run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see
the result.
