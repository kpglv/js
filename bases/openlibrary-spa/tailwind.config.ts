import type { Config } from 'tailwindcss';
import flowbite from 'flowbite/plugin';
import typography from '@tailwindcss/typography';

const config: Config = {
    content: [
        './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
        './src/components/**/*.{js,ts,jsx,tsx,mdx}',
        './src/app/**/*.{js,ts,jsx,tsx,mdx}',
        './src/use-cases/**/*.{js,ts,jsx,tsx,mdx}',
        '../../node_modules/flowbite-react/lib/**/*.js',
    ],
    theme: {
        extend: {},
    },
    plugins: [flowbite, typography],
};
export default config;
