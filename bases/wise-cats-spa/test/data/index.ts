import cats from './cats/random-10.json';
import bsdLinuxPhrase from './fortunes/bsd-linux.json';
import coinFlipPhrase from './fortunes/coniflip.json';
import crystalBallPhrase from './fortunes/crystal-ball.json';
import excusePhrase from './fortunes/excuses.json';
import githubZenPhrase from './fortunes/github-zen.json';
import procrastinationPhrase from './fortunes/procrastinate.json';

import { createWiseCats } from '../../src/domain/WiseCat';

export const someWiseCats = createWiseCats(cats.slice(4, 10), [
    bsdLinuxPhrase,
    coinFlipPhrase,
    crystalBallPhrase,
    excusePhrase,
    githubZenPhrase,
    procrastinationPhrase,
]);
