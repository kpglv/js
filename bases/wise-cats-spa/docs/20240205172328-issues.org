:PROPERTIES:
:ID:       4b4ff076-a91b-47d0-971a-27e1abd04f76
:END:
#+title: issues

* Tags

  - [[id:0a547a7a-0cce-4d73-b23b-3b55a035052f][index]]

* Issues
** TODO #1 Styling for active state is not applied to navigation links

   In header. Collapsed ones (in mobile menu) too. With bothg light and dark
   themes. Both in Histoire and in standalone app.

   Vue router tracks active
   state of ~<router-link />~ (and assigns its own classes) correctly. Update
   highlights (in Vue devtools) show that on route change both old and new
   router links are updated, but not others.

   Seems like =flowbite-vue='s ~<fwb-navbar-link />~ does not react to changes
   in ~:is-active~ prop (tried to change manually from Vue devtools). If set
   to literal true, navbar link is styled correctly (but does not react to
   subsequent changes either). No related issues in =flowbite-vue= project
   exist currently.

** TODO #2 Image loading in WiseCatCard affects its size

   Provide a default image to display while loading.

** DONE #3 Extract the Wise Cats app into a separate base

   Import its components and views in ~vue-histoire~ for development and
   testing. (These possibly will be reused in another base, using Nuxt.)
