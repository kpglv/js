import { zip } from 'lodash';

export interface Cat {
    id: string;
    url: string;
}

export interface Phrase {
    fortune: string;
}

export interface WiseCat {
    id: string;
    url: string;
    phrase: string;
}

/**
 * Returns a (slightly) wisened cat.
 */
export const createWiseCat = (cat: Cat, phrase: Phrase): WiseCat => ({
    id: cat.id,
    url: cat.url,
    phrase: phrase.fortune,
});

/**
 * Returns a whole lot of wisened cats. Each cat should have something to say.
 */
export const createWiseCats = (cats: Cat[], phrases: Phrase[]): WiseCat[] => {
    if (cats.length !== phrases.length)
        throw new Error('cats and phrases are not of the same length');
    return zip(cats, phrases).map(
        ([cat, phrase]: [Cat | undefined, Phrase | undefined]) =>
            createWiseCat(cat as Cat, phrase as Phrase)
    );
};
