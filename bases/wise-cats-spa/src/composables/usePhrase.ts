import { ref } from 'vue';
import { type UseFetchOptions, useFetch } from '@vueuse/core';
import {
    type FortuneKindDescription,
    type FortuneFetchOptions,
    fortuneKindDescriptions,
} from '../services/fortunesapi';
import { randomValue } from '../modules/utils';

/**
 * Fetches a random phrase of given kind (random kind by default).
 * The returned 'next' method will return a random phrase of the same kind.
 * The returned 'random' method will fetch a random phrase of random kind.
 * @param param0.apikey required by the Fortunes API to
 *  return JSON (instead of JSONP).
 * @param kind a kind of phrase to return.
 */
export const usePhrase = (
    { apikey, kind }: FortuneFetchOptions = {
        apikey: 'user@example.com',
    },
    useFetchOptions: UseFetchOptions = {}
) => {
    const kindDescription = ref<FortuneKindDescription>(
        kind
            ? fortuneKindDescriptions[kind]
            : (randomValue(fortuneKindDescriptions) as FortuneKindDescription)
    );
    // FIXME: implement proper query parameters handling
    const phraseUrl = ref(`${kindDescription.value.url}?apikey=${apikey}`);
    return {
        ...useFetch<string>(phraseUrl, {
            ...useFetchOptions,
            refetch: true,
        }),
        random: () => {
            kindDescription.value = randomValue(
                fortuneKindDescriptions
            ) as FortuneKindDescription;
            phraseUrl.value = `${kindDescription.value.url}?apikey=${apikey}`;
        },
        kind: kindDescription,
    };
};
