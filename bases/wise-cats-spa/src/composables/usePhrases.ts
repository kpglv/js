import { ref, shallowRef } from 'vue';
import {
    type FortuneFetchOptions,
    fetchFortunes,
    payloadToPhrase,
} from '../services/fortunesapi';
import { type Phrase } from '../domain/WiseCat';

/**
 * Fetches several random phrases of given kind (random kind by default).
 * @param param0.apikey required by the Fortunes API to
 *  return JSON (instead of JSONP).
 * @param kind a kind of phrases to return.
 */
export const usePhrases = (options?: FortuneFetchOptions | undefined) => {
    const isFetching = ref<boolean>(false);
    const phrases = shallowRef<Phrase[]>([]);
    const error = shallowRef(undefined);

    const execute = async () => {
        phrases.value = [];
        error.value = undefined;
        isFetching.value = true;

        fetchFortunes(options)
            .then(response => Promise.all(response.map(r => r.json())))
            .then(
                payloads =>
                    (phrases.value = payloads.map(p => payloadToPhrase(p)))
            )
            .catch(e => (error.value = e.message || e.name))
            .finally(() => (isFetching.value = false));
    };

    execute();

    return { isFetching, phrases, error, execute };
};
