import { useI18n } from 'vue-i18n';
import { availableLocales, defaultLocale } from '../i18n';
import { useStorage } from '@vueuse/core';
import { watch } from 'vue';

/**
 * Provides available languages, current language, forwards the
 * translation function from `vue-i18n`.
 * Persists selected language to localStorage.
 * Languages are represented as 2-letter codes, e.g, 'en'.
 * @param param0 options
 */
export const useLanguage = (options = { key: 'use-language-key' }) => {
    const { key } = options;
    const defaultState = availableLocales.find(
        l => l.value === defaultLocale
    );
    const state = useStorage(key, defaultState);
    const { t, locale: language } = useI18n();
    const languages = availableLocales;

    // load language state from storage, if present and different
    // from the current language
    if (state.value && state.value.value !== language.value) {
        language.value = state.value.value;
    }

    // watch changes in the locale and persist it to storage on change
    watch(language, () => {
        const newState = languages.find(l => l.value === language.value);
        if (newState) {
            state.value = newState;
        }
    });

    return { t, language, languages };
};
