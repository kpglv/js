import { computed, shallowRef } from 'vue';
import { useCats } from './useCats';
import { usePhrases } from './usePhrases';
import { createWiseCats } from '../domain/WiseCat';

/**
 * Fetches some cats and phrases, and returns slightly wisened cats
 * (10 by default).
 */
export const useWiseCats = () => {
    const {
        isFetching: isFetchingCats,
        error: catsError,
        execute: refetchCats,
        cats,
    } = useCats();

    const {
        isFetching: isFetchingPhrases,
        error: phrasesError,
        execute: refetchPhrases,
        phrases,
    } = usePhrases();

    const wiseningError = shallowRef<unknown>(null);

    const isFetching = computed(
        () => isFetchingCats.value || isFetchingPhrases.value
    );
    const error = computed(
        () => catsError.value || phrasesError.value || wiseningError.value
    );
    const execute = () => {
        refetchCats();
        refetchPhrases();
    };
    const wiseCats = computed(() => {
        if (error.value || cats.value.length != phrases.value.length) {
            return [];
        } else {
            try {
                return createWiseCats(cats.value, phrases.value);
            } catch (e) {
                wiseningError.value = e;
                return [];
            }
        }
    });

    return { isFetching, wiseCats, error, execute };
};
