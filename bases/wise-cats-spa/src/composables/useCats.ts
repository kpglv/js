import { computed, type MaybeRef } from 'vue';
import { type UseFetchOptions, useFetch } from '@vueuse/core';
import { payloadToCat, type CatPayload } from '../services/catapi';

export interface Options {
    limit: MaybeRef<number>;
}

/**
 * Fetches a list of cat descriptions.
 * @param param0.limit number of cats to fetch. The Cat API restricts
 *  this to 10 without an API key (actually, without an API key it
 *  correctly recognizes only 1 and 10).
 * @param useFetchOptions
 */
export const useCats = (
    { limit }: Options = { limit: 10 },
    useFetchOptions: UseFetchOptions = {}
) => {
    const catsUrl = `https://api.thecatapi.com/v1/images/search?limit=${limit}`;
    const { isFetching, data, error, execute } = useFetch<CatPayload[]>(
        catsUrl,
        useFetchOptions
    ).json();

    const cats = computed(() => {
        if (data.value) {
            return (data.value as CatPayload[]).map(payloadToCat);
        } else {
            return [];
        }
    });
    return { isFetching, cats, error, execute };
};
