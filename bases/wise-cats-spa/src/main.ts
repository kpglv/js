import { createApp } from 'vue';
import { createPinia } from 'pinia';
import router from './router';
import App from './components/app/App.vue';
import { i18n } from './i18n';
import './tailwind.css';

const app = createApp(App);
app.use(createPinia());
app.use(router);
app.use(i18n);

app.mount('#app');
