import { Phrase } from '../../domain/WiseCat';
import { randomValue } from '../../modules/utils';

export interface FortunePayload {
    fortune: string;
}

export interface FortuneKindDescription {
    id: string;
    name: string;
    url: string;
}
export interface FortuneFetchOptions {
    limit?: number;
    apikey?: string;
    kind?: keyof typeof fortuneKindDescriptions;
}

export const fortuneKindDescriptions = {
    excuse: {
        id: 'excuse',
        name: 'BOFH excuse',
        url: 'https://www.fortune.ninja/fortune/excuses.json',
    },
    bsdLinux: {
        id: 'bsdlinux',
        name: 'BSD/Linux fortune',
        url: 'https://www.fortune.ninja/fortune/bsd_linux.json',
    },
    crystalBall: {
        id: 'crystalBall',
        name: 'Crystal Ball',
        url: 'https://www.fortune.ninja/fortune/crystalball.json',
    },
    github: {
        id: 'github',
        name: 'Github Zen',
        url: 'https://www.fortune.ninja/fortune/github.json',
    },
    coinFlip: {
        id: 'coinFlip',
        name: 'Coin flip',
        url: 'https://www.fortune.ninja/fortune/coinflip.json',
    },
    procrastinationSlogan: {
        id: 'procrastination',
        name: 'Procrastination slogan',
        url: 'https://www.fortune.ninja/fortune/procrastinate.json',
    },
};
/**
 * Transforms payload returned by Fortunes API to a phrase.
 */
export const payloadToPhrase = (payload: FortunePayload) => payload as Phrase;

/**
 * Fetches several fortunes. Returns a promise that resolves with an
 * array of results or rejected if any invividual request fails.
 */
export const fetchFortunes = (options?: FortuneFetchOptions) => {
    const { apikey, limit, kind } = options || {
        limit: 10,
        apikey: 'user@example.com',
    };

    const fortunesCount = limit && limit > 0 ? limit : 1;
    const urls = [...Array(fortunesCount)].map(() => {
        const fortuneKindDescription = kind
            ? fortuneKindDescriptions[kind]
            : (randomValue(
                  fortuneKindDescriptions
              ) as FortuneKindDescription);
        // FIXME: implement proper query parameters handling
        return `${fortuneKindDescription.url}?apikey=${apikey}`;
    });
    const requests = urls.map(url => fetch(url));
    return Promise.all(requests);
};
