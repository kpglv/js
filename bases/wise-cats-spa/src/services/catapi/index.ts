import type { Cat } from '../../domain/WiseCat';

export interface CatPayload {
    id: string;
    url: string;
}

/**
 * Transforms payload returned by Cats API to a cat description.
 */
export const payloadToCat = (payload: CatPayload) => payload as Cat;
