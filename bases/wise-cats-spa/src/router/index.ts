import { createRouter, createWebHistory } from 'vue-router';
import HomePage from '../components/app/pages/Home.vue';

export const routes = [
    {
        path: '/',
        name: 'home',
        component: HomePage,
        meta: {
            title: 'Home',
        },
    },
    {
        path: '/help',
        name: 'help',
        // route level code-splitting
        // this generates a separate chunk (Help.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import('../components/app/pages/Help.vue'),
        meta: {
            title: 'Help',
        },
    },
    {
        path: '/about',
        name: 'about',
        component: () => import('../components/app/pages/About.vue'),
        meta: {
            title: 'About',
        },
    },
    {
        path: '/settings',
        name: 'settings',
        component: () => import('../components/app/pages/Settings.vue'),
        meta: {
            title: 'Settings',
        },
    },
];

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes,
});

export default router;
