/**
 * Returns the value of an attribute of the object selected at random.
 */
export const randomValue = (obj: Record<string, unknown>): unknown => {
    const values = Object.values(obj);
    const index = Math.floor(Math.random() * values.length);
    return values[index];
};

/**
 * Returns a random element from the array.
 */
export const randomElement = (arr: Array<unknown>) =>
    arr[Math.floor(Math.random() * arr.length)];

/**
 * Reloads the page in a browser, does nothing otherwise.
 */
export const reload = () => window && window.location.reload();
