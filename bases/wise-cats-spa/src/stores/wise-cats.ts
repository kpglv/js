import { ref, watchEffect } from 'vue';
import { defineStore } from 'pinia';
import { useWiseCats } from '../composables/useWiseCats';
import { WiseCat } from '../domain/WiseCat';

export const useWiseCatsStore = defineStore('wise-cats', () => {
    // refs are state, computeds are getters, functions are actions
    // (no separate mutations, it is all actions now)

    const {
        isFetching: isFetchingWiseCats,
        wiseCats: moreWiseCats,
        error: wiseCatsError,
        execute: getWiseCats,
    } = useWiseCats();

    const wiseCats = ref([] as WiseCat[]);

    // watch if there are more wise cats and add them to list
    watchEffect(() => {
        wiseCats.value = wiseCats.value.concat(moreWiseCats.value);
    });

    return {
        wiseCats,
        wiseCatsError,
        isFetchingWiseCats,
        getWiseCats,
    };
});
