export const messages = {
    en: {
        app: {
            title: 'My Wise Cats',
            nav: {
                home: 'Home',
                help: 'Help',
                about: 'About',
                settings: 'Settings',
            },
            components: {
                select: {
                    placeholder: 'Please select one',
                },
                wiseCatCard: {
                    cat: 'Cat',
                    says: 'says',
                },
                loading: 'Loading...',
            },
            pages: {
                home: {
                    title: 'Home',
                },
                help: {
                    title: 'Help',
                    description:
                        'There is nothing to learn here, just scroll the page and enjoy the cute and the wise.',
                },
                about: {
                    title: 'About',
                    overview:
                        'This is a demo application created to showcase my skills in modern frontend development.',
                    tools: {
                        title: 'Tools, Libraries and Frameworks Used',
                        typescript: 'language',
                        vue: 'library for creating modular, reactive UIs',
                        componentLibrary: 'library of reusable components',
                        styling: 'styling framework',
                        componentDevelopment:
                            'isolated component development and visual testing tool',
                        build: 'build tool',
                    },
                    concerns: {
                        title: 'Development Concerns Demonstrated',
                        componentDrivenDevelopment: {
                            pre: 'Designing and implementing reusable components with Vue 3 and Composition API (using',
                            post: 'approach with Histoire)',
                        },
                        composables: {
                            pre: 'Using third-party composables (from a collection at',
                            post: 'and implementing my own to reuse stateful logic',
                        },
                        creatingApp: {
                            title: 'Creating a single-page application',
                            composing:
                                'Composing responsive, semantic views from reusable components',
                            routing: {
                                pre: 'Routing (',
                                post: ')',
                            },
                            stateManagement: {
                                pre: 'State management (',
                                post: ')',
                            },
                            dataFetching: {
                                pre: 'Exchanging data with REST APIs (fetch,',
                                post: ')',
                            },
                            i18n: {
                                pre: 'Internationalization (',
                                post: ')',
                            },
                            theming: 'Styling and theming',
                        },
                    },
                },
                settings: {
                    title: 'Settings',
                    language: 'Language',
                    colorMode: 'Color mode',
                },
            },
            footer: {
                copyright: {
                    by: 'Constantine Pigalev,',
                    url: 'https://kpglv.gitlab.io/personal/en',
                    message: ' MIT license',
                },
                links: {
                    gitlab: 'My Gitlab',
                    catapi: 'Powered by The Cat API',
                    fortunesapi: 'Powered by Fortunes API',
                },
            },
            data: {
                fetchError: 'An error occurred while fetching data.',
                noData: 'There is nothing to show here yet.',
            },
            actions: {
                reload: 'Try again',
                goHome: 'Go Home',
                loadMore: 'More!',
            },
        },
    },
    ru: {
        app: {
            title: 'Мои Мудрые Коты',
            nav: {
                home: 'Главная',
                help: 'Помощь',
                about: 'О приложении',
                settings: 'Настройки',
            },
            components: {
                select: {
                    placeholder: 'Выберите вариант',
                },
                wiseCatCard: {
                    cat: 'Кот',
                    says: 'говорит',
                },
                loading: 'Пожалуйста, подождите...',
            },
            pages: {
                home: {
                    title: 'Главная',
                },
                help: {
                    title: 'Помощь',
                    description:
                        'Здесь нечему учиться, просто листайте ленту пушистой мудрости.',
                },
                about: {
                    title: 'О приложении',
                    overview:
                        'Это приложение призвано продемонстрировать мои навыки в современной фронтенд-разработке.',
                    tools: {
                        title: 'Инструменты, библиотеки и фреймворки',
                        typescript: '',
                        vue: 'библиотека для создания модульных, реактивных пользовательских интерфейсов',
                        componentLibrary:
                            'библиотека готовых визуальных компонентов',
                        styling: 'стилевой фреймворк',
                        componentDevelopment:
                            'среда изолированной разработки компонентов и визуального тестирования',
                        build: 'система сборки',
                    },
                    concerns: {
                        title: 'Аспекты разработки',
                        componentDrivenDevelopment: {
                            pre: 'Проектирование и реализация переиспользуемых компонентов с Vue 3 и Composition API (используя подход',
                            post: 'с Histoire)',
                        },
                        composables: {
                            pre: 'Использование сторонних composables (из коллекции',
                            post: 'и реализация собственных для переиспользования логики, связанной с реактивным состоянием',
                        },
                        creatingApp: {
                            title: 'Создание одностраничного приложения',
                            composing:
                                'Сборка адаптивных, семантичеcких представлений из переиспользуемых компонентов',
                            routing: {
                                pre: 'Маршрутизация (',
                                post: ')',
                            },
                            stateManagement: {
                                pre: 'Управление состоянием (',
                                post: ')',
                            },
                            dataFetching: {
                                pre: 'Обмен данными с REST API (fetch,',
                                post: ')',
                            },
                            i18n: {
                                pre: 'Интернационализация (',
                                post: ')',
                            },
                            theming: 'Темы оформления (цветовые схемы)',
                        },
                    },
                },
                settings: {
                    title: 'Настройки',
                    language: 'Язык',
                    colorMode: 'Цветовой режим',
                },
            },
            footer: {
                copyright: {
                    by: 'Константин Пигалев,',
                    url: 'https://kpglv.gitlab.io/personal/ru',
                    message: 'лицензия MIT',
                },
                links: {
                    gitlab: 'Мой Гитлаб',
                    catapi: 'Использует The Cat API',
                    fortunesapi: 'Использует Fortunes API',
                },
            },
            data: {
                fetchError: 'При загрузке данных произошла ошибка.',
                noData: 'Здесь пока ничего нет.',
            },
            actions: {
                reload: 'Попробовать снова',
                goHome: 'На главную',
                loadMore: 'Ещё!',
            },
        },
    },
};
