import { createI18n } from 'vue-i18n';
import { messages } from './messages';

export const defaultLocale = 'ru';
export const fallbackLocale = 'en';
export const availableLocales = [
    { value: 'ru', name: 'Русский' },
    { value: 'en', name: 'English' },
];

export const i18n = createI18n({
    legacy: false, // must be set to `false` to use Composition API
    locale: defaultLocale,
    fallbackLocale: fallbackLocale,
    messages,
});

export default i18n;
