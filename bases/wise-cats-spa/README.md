# wise-cats-spa

Simple Vue 3 SPA that uses The Cat API and Fortunes API.

- https://thecatapi.com
- https://www.fortune.ninja

See also `bases/vue-histoire` for visual tests for the app's components
and pages.

## Demo

Deployed on Vercel: https://js-wise-cats-spa.vercel.app/

## Getting Started

Start the development server

```
yarn dev
```

Generate a static production build

```
yarn build
```

Serve the production build for preview (must be built first)

```
yarn preview
```
