export type Sex = 'male' | 'female';

// FIXME: use actual formulae
export const basalMetabolicRate = (
    weight: number,
    height: number,
    age: number,
    sex: Sex
) => (weight + height + age) * (sex === 'male' ? 2 : 1);

// FIXME: use actual formulae
export const bodyMassIndex = (weight: number, height: number) =>
    weight / height ** 2;
