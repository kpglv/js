import reactLogo from './assets/react.svg';

function App() {
    return (
        <>
            <img src={reactLogo} alt="React" />
            <h1>Calorie Calculator</h1>
        </>
    );
}

export default App;
