import { configureStore, createListenerMiddleware } from '@reduxjs/toolkit';
import calorieCalculatorReducer, {
    computeBasalMetabolicRate,
    computeBodyMassIndex,
} from './calorieCalculatorSlice';

const listenerMiddleware = createListenerMiddleware();
listenerMiddleware.startListening({
    actionCreator: computeBasalMetabolicRate,
    effect: (action, listenerApi) => {
        console.log('BMR computed for:', action);
        console.log('using listener API:', listenerApi);
    },
});
listenerMiddleware.startListening({
    actionCreator: computeBodyMassIndex,
    effect: (action, listenerApi) => {
        console.log('BMI computed for:', action);
        console.log('using listener API:', listenerApi);
    },
});

export const store = configureStore({
    reducer: {
        calorieCalculator: calorieCalculatorReducer,
    },
    middleware: getDefaultMiddleware =>
        getDefaultMiddleware().prepend(listenerMiddleware.middleware),
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;

/* selectors */

export const getBasalMetabolicRate = (state: RootState) =>
    state.calorieCalculator.basalMetabolicRate;

export const getBodyMassIndex = (state: RootState) =>
    state.calorieCalculator.bodyMassIndex;
