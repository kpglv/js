import { type PayloadAction, createSlice } from '@reduxjs/toolkit';

import { type Sex, basalMetabolicRate, bodyMassIndex } from '../domain';

export interface BodyParameters {
    weight: number;
    height: number;
    age?: number;
    sex?: Sex;
}

export enum ActivityCoefficients {
    light = 1.2,
    medium = 1.4,
    heavy = 1.6,
}

export interface calorieCalculatorState {
    bodyParameters: BodyParameters | null;
    bodyMassIndex: number | null;
    basalMetabolicRate: number | null;
}

export const initialState: calorieCalculatorState = {
    bodyParameters: null,
    bodyMassIndex: null,
    basalMetabolicRate: null,
};

export const calorieCalculatorSlice = createSlice({
    name: 'calorieCalculator',
    initialState,
    reducers: {
        computeBasalMetabolicRate: (
            state,
            { payload }: PayloadAction<BodyParameters>
        ) => {
            const { weight, height, age, sex } = payload;
            state.basalMetabolicRate = basalMetabolicRate(
                weight,
                height!,
                age!,
                sex!
            );
        },
        computeBodyMassIndex: (
            state,
            { payload }: PayloadAction<BodyParameters>
        ) => {
            const { weight, height } = payload;
            state.bodyMassIndex = bodyMassIndex(weight, height);
        },
    },
});

export const { computeBasalMetabolicRate, computeBodyMassIndex } =
    calorieCalculatorSlice.actions;

export default calorieCalculatorSlice.reducer;
