import { test, expect } from '@playwright/test';

test.beforeEach(async ({ page }) => {
    await page.goto('http://localhost:5173');
});

test.describe('An example test', () => {
    test('Ensures that some text is present on the page', async ({
        page,
    }) => {
        await expect(page.getByText('Calorie')).toContainText('Calculator');
    });
});
