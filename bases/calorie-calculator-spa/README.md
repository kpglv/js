# calorie-calculator-spa

An SPA for calculating calorie intake levels (basal metabolic rate
and its derivatives) based on person's weight, height, age and sex.

## Getting Started

Development

```
yarn dev
```

Production build

```
yarn build
```

Run unit tests

```
yarn test:unit
```
