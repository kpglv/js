import type { Config } from 'tailwindcss';
import TailwindFormsPlugin from '@tailwindcss/forms';
import FlowbitePlugin from 'flowbite/plugin';

export default {
    content: [
        './src/**/*.{js,ts,jsx,tsx}',
        '../../node_modules/flowbite-react/lib/esm/**/*.js',
        // for builds that do not use shared node_modules (e.g., on Vercel)
        'node_modules/flowbite-react/lib/esm/**/*.js',
    ],
    darkMode: ['class'],
    theme: {
        extend: {},
    },
    plugins: [TailwindFormsPlugin, FlowbitePlugin],
} satisfies Config;
