# components-library-spa

A demonstration/visual testing app for the small library of React
components that reside in `components/react`.

Uses [Next.js](https://nextjs.org/).

## Getting Started

Run the development server

```bash
yarn dev
```

Open http://localhost:3000 with your browser to see the result.

## Application Structure

Static assets are in `public` directory, sources are in `src`
directory, where

- `app`: defines the route tree, contains components that
  are rendered for each route (pages, layouts)
- `components`: contains application-specific (not reusable)
  components
- `state`: contains parts of the application state
- `services`: contains modules that provide certain services
  for the application (like fetching data from external systems)