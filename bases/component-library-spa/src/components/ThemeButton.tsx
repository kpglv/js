'use client'

import { ComputerDesktopIcon, MoonIcon, SunIcon } from "@heroicons/react/24/outline";
import { useTheme } from "next-themes";
import { useEffect, useState } from "react";

const nextTheme = (currentTheme: string): string => {
    switch (currentTheme) {
        case 'system':
            return 'dark';
        case 'dark':
            return 'light';
        case 'light':
            return 'system';
        default:
            console.warn('unknown theme:', currentTheme);
            return 'system';
    }
};

const themeIcon = (nextTheme: string) => {
    switch (nextTheme) {
        case 'light':
            return <SunIcon className="h-6" />;
        case 'dark':
            return <MoonIcon className="h-6" />;
        default:
            return <ComputerDesktopIcon className="h-6" />;
    }
};

const themeButtonClasses = [
    "p-2.5 rounded-lg",
    "font-medium text-sm text-center",
    "flex items-center ",
    "hover:bg-primary-300 dark:hover:bg-primary-800",
    "focus:ring-4 focus:outline-none focus:ring-primary-300 dark:focus:ring-primary-800"
].join(' ');

const ThemeButton = () => {
    const [mounted, setMounted] = useState(false);
    const { theme, setTheme } = useTheme();

    useEffect(() => setMounted(true), []);
    if (!mounted) return null;

    const onClick = () => {
        setTheme(nextTheme(theme || 'system'));
    };

    return (
        <button
            type="button"
            className={themeButtonClasses}
            onClick={onClick}>
            {themeIcon(nextTheme(theme || 'system'))}
        </button >
    )
};

export default ThemeButton;