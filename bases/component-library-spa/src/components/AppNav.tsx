'use client'

import { useContext } from "react";
import { AppContext } from '@/components/providers/AppProvider';
import NavSection from './NavSection';
import NavItem from './NavItem';
import HorizontalLine from "./HorizontalLine";
import { AcademicCapIcon, ChatBubbleLeftRightIcon, ListBulletIcon, XMarkIcon } from "@heroicons/react/24/outline";
import { createPortal } from "react-dom";


const AppNav = () => {
    const { state, setMobileMenuOpen } = useContext(AppContext);
    const open = state.mobileMenuOpen;


    const nav = (
        <nav className="flex flex-col p-4 gap-4 w-full">
            <div className="flex flex-col">
                <div className="group py-2 flex items-center hover:px-2 hover:font-semibold transition-all">
                    <AcademicCapIcon className="h-6" />
                    <div className="w-2"></div>
                    <a href="#">Learn</a>
                </div>

                <div className="group py-2 flex items-center hover:px-2 hover:font-semibold transition-all">
                    <ListBulletIcon className="h-6" />
                    <div className="w-2"></div>
                    <a href="#">Explore</a>
                </div>

                <div className="group py-2 flex items-center hover:px-2 hover:font-semibold transition-all">
                    <ChatBubbleLeftRightIcon className="h-6" />
                    <div className="w-2"></div>
                    <a href="#">Discuss</a>
                </div>

            </div>

            <HorizontalLine />

            <NavSection title="Components">
                <NavItem href='/components/app-shell'>
                    App Shell
                </NavItem>
                <NavItem href='/components/display'>
                    Display
                </NavItem>
                <NavItem href='/components/input'>
                    Input
                </NavItem>
            </NavSection>

            <NavSection title="Examples">
                <NavItem href='/examples/graphql'>
                    GraphQL
                </NavItem>
            </NavSection>
        </nav>
    );

    const mobileNavScrimClasses = [
        "fixed inset-0 overflow-auto",
        "backdrop-blur-sm",
        "bg-black/20 dark:bg-slate-900/80"
    ].join(" ");

    const mobileNavClasses = [
        "w-[256px] min-h-screen",
        "relative transition-all",
        "bg-white dark:bg-primary-900",
        "text-primary-900 dark:text-white",
    ].join(" ");

    const mobileNavId = "app-mobile-nav";

    const closeMobileNav =
        () => setMobileMenuOpen && setMobileMenuOpen(false);

    const mobileNav = (
        <div
            id={mobileNavId}
            className={mobileNavScrimClasses}
            onClick={closeMobileNav}>
            <div className={mobileNavClasses}>
                <XMarkIcon
                    className="absolute right-4 top-4 h-6"
                    onClick={closeMobileNav} />
                {nav}
            </div>
        </div>
    );

    // FIXME: make it more animation-friendly
    return open ?
        createPortal(mobileNav, document.body, mobileNavId) : nav;
};

export default AppNav;