'use client'

import Link from 'next/link';
import { usePathname } from "next/navigation";

interface NavItemProps {
    href: string;
    children: React.ReactNode;
}

const navItemClasses = [
    "py-2 px-4 transition-al border-s border-slate-500/10 transition-all",
];

const inactiveNavItemClasses = [
    "dark:border-slate-300/50 dark:hover:border-white",
    "hover:font-semibold hover:border-slate-500/50 dark:border-slate-300/50"
];
const activeNavItemClasses = "font-semibold border-slate-500/50 dark:border-white";

const NavItem = ({ href, children }: NavItemProps) => {
    const pathname = usePathname();
    const classes = (href === pathname ? navItemClasses.concat(activeNavItemClasses) :
        navItemClasses.concat(inactiveNavItemClasses)).join(" ");

    return (
        <div className={classes}>
            <Link href={href}>
                {children}
            </Link>
        </div >
    );
};

export default NavItem;