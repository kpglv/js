'use client'

import type { AuthState, UserCredentials } from '@/state/auth';
import { authInitialState, authReducer } from "@/state/auth";
import React, { useReducer } from "react";

interface AuthContextValue {
    state: AuthState;
    login?: (credentials: UserCredentials) => void;
    logout?: () => void;
}

export const AuthContext =
    React.createContext<AuthContextValue>({ state: authInitialState });

export interface AuthProviderProps {
    children: React.ReactNode;
}

export const AuthProvider = ({ children }: AuthProviderProps) => {
    const [state, dispatch] = useReducer(authReducer, authInitialState);

    // actions: execute (async) tasks, dispatch events
    const login = async (_: UserCredentials) => {
        // FIXME:
        const user = await Promise.resolve({ name: "Joe" });
        console.log('logging in:', user);

        dispatch({ type: 'LOGIN', payload: user });
    }
    const logout = async () => {
        // FIXME:
        await Promise.resolve(null);
        console.log('logging out');

        dispatch({ type: 'LOGOUT' });
    };

    return (
        <AuthContext.Provider value={{ state, login, logout }}>
            {children}
        </AuthContext.Provider>
    );
};