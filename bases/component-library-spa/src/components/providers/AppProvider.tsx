'use client'

import React, { useReducer } from "react";
import type { AppState } from '@/state/app';
import { appInitialState, appReducer } from "@/state/app";

interface AppContextValue {
    state: AppState;
    setMobileMenuOpen?: (open: boolean) => void;
}

export const AppContext =
    React.createContext<AppContextValue>({ state: appInitialState });

export interface AppProviderProps {
    children: React.ReactNode;
}

export const AppProvider = ({ children }: AppProviderProps) => {
    const [state, dispatch] = useReducer(appReducer, appInitialState);

    // actions: execute (async) tasks, dispatch events

    const setMobileMenuOpen = (open: boolean) => {
        dispatch({ type: 'MOBILE_MENU_SET', payload: open });
    }

    return (
        <AppContext.Provider value={{ state, setMobileMenuOpen }}>
            {children}
        </AppContext.Provider>
    );
};