'use client'

import React, { useContext } from 'react';
import { Bars3Icon, XMarkIcon } from '@heroicons/react/24/outline';
import { AppContext } from '@/components/providers/AppProvider';

const MobileMenuButton = () => {
    const { state, setMobileMenuOpen } = useContext(AppContext);
    const open = state.mobileMenuOpen;

    const toggleMenu = () => setMobileMenuOpen &&
        setMobileMenuOpen(!open);

    return (
        open ?
            <XMarkIcon
                className="h-8 md:hidden"
                onClick={toggleMenu} /> :
            <Bars3Icon className="h-8 md:hidden"
                onClick={toggleMenu} />
    );
};

export default MobileMenuButton;