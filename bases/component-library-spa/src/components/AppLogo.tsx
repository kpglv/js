import { BeakerIcon } from "@heroicons/react/24/outline";
import Link from "next/link";

const AppLogo = () => {
    return (
        <Link href="/">
            <BeakerIcon className="h-8 hover:rotate-6 transition-transform" />
        </Link>
    );
};

export default AppLogo;