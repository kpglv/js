import React, { useContext } from "react";
import { AuthContext } from "@/components/providers/AuthProvider";
import Button from "@tailwind-components/input/Button";

const LoginButton = () => {
    const { state, login, logout } = useContext(AuthContext);
    const { user } = state;

    // FIXME:
    const doLogin = () => login && login({ username: "Joe", password: "topsecret" });
    const doLogout = () => logout && logout();

    return (
        <Button onClick={user ? doLogout : doLogin}>
            {user ? `Logout ${user.name}` : "Login"}
        </Button>
    );
};
export default LoginButton;