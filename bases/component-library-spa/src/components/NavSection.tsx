import React from 'react';

interface navSectionProps {
    title: string;
    children: React.ReactNode;
}

const NavSection = ({ title, children }: navSectionProps) => {
    return (
        <div className="flex flex-col py-2 gap-2">
            <div className="pb-2">
                {title}
            </div>
            <div className="">
                {children}
            </div>
        </div>
    );
};

export default NavSection;