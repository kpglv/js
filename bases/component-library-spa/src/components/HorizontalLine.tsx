const HorizontalLine = () => {
    return (
        <div className="border-b border-slate-500/50 dark:border-slate-300/30"></div>
    );
};

export default HorizontalLine;