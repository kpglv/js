export interface AppState {
    mobileMenuOpen?: boolean;
}

export interface AppEvent {
    type: 'MOBILE_MENU_SET';
    payload?: boolean;
}

export const appInitialState = {
    mobileMenuOpen: false,
};

export const appReducer = (state: AppState, event: AppEvent): AppState => {
    switch (event.type) {
        case 'MOBILE_MENU_SET':
            return { ...state, mobileMenuOpen: event.payload };
        default:
            return state;
    }
};
