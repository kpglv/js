// TODO: extract domain types to separate module(s)

export interface User {
    name: string;
}

export interface UserCredentials {
    username: string;
    password: string;
}

export interface AuthState {
    user?: User;
}

export interface AuthEvent {
    type: 'LOGIN' | 'LOGOUT';
    payload?: User;
}

export const authInitialState = {};

export const authReducer = (state: AuthState, event: AuthEvent): AuthState => {
    switch (event.type) {
        case 'LOGIN':
            return { ...state, user: event.payload };
        case 'LOGOUT':
            return authInitialState;
        default:
            return state;
    }
};
