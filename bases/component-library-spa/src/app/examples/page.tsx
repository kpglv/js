"use client"

import React, { useEffect } from 'react';
import Image from 'next/image'
import { ArrowRightCircleIcon } from '@heroicons/react/24/outline';

import UnstyledButton from "@unstyled-components/input/Button";
import TailwindButton from "@tailwind-components/input/Button";
import CssModulesButton from "@css-modules-components/input/Button";

import hljs from 'highlight.js/lib/core';
import javascript from 'highlight.js/lib/languages/javascript';
import 'highlight.js/styles/default.css';
import LoginButton from '@/components/LoginButton';

hljs.registerLanguage('javascript', javascript);

export default function Home() {

  useEffect(() => {
    hljs.highlightAll();
  }, []);

  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <div className="z-10 max-w-5xl w-full items-center justify-between font-mono text-sm lg:flex">
        <p className="fixed left-0 top-0 flex w-full justify-center border-b border-gray-300 bg-gradient-to-b from-zinc-200 pb-6 pt-8 backdrop-blur-2xl dark:border-neutral-800 dark:bg-zinc-800/30 dark:from-inherit lg:static lg:w-auto  lg:rounded-xl lg:border lg:bg-gray-200 lg:p-4 lg:dark:bg-zinc-800/30">
          Get started by editing&nbsp;
          <code className="font-mono font-bold">src/app/page.tsx</code>
        </p>
        <div className="fixed bottom-0 left-0 flex h-48 w-full items-end justify-center bg-gradient-to-t from-white via-white dark:from-black dark:via-black lg:static lg:h-auto lg:w-auto lg:bg-none">
          <a
            className="pointer-events-none flex place-items-center gap-2 p-8 lg:pointer-events-auto lg:p-0"
            href="https://vercel.com?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
            target="_blank"
            rel="noopener noreferrer"
          >
            By{' '}
            <Image
              src="/vercel.svg"
              alt="Vercel Logo"
              className="dark:invert"
              width={100}
              height={24}
              priority
            />
          </a>
        </div>
      </div>

      <div className="relative flex place-items-center before:absolute before:h-[300px] before:w-[480px] before:-translate-x-1/2 before:rounded-full before:bg-gradient-radial before:from-white before:to-transparent before:blur-2xl before:content-[''] after:absolute after:-z-20 after:h-[180px] after:w-[240px] after:translate-x-1/3 after:bg-gradient-conic after:from-sky-200 after:via-blue-200 after:blur-2xl after:content-[''] before:dark:bg-gradient-to-br before:dark:from-transparent before:dark:to-blue-700 before:dark:opacity-10 after:dark:from-sky-900 after:dark:via-[#0141ff] after:dark:opacity-40 before:lg:h-[360px] z-[-1]">
        <Image
          className="relative dark:drop-shadow-[0_0_0.3rem_#ffffff70] dark:invert"
          src="/next.svg"
          alt="Next.js Logo"
          width={180}
          height={37}
          priority
        />
      </div>

      <div className="mb-32 grid text-center lg:max-w-5xl lg:w-full lg:mb-0 lg:grid-cols-4 lg:text-left">
        <a
          href="https://nextjs.org/docs?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Docs{' '}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Find in-depth information about Next.js features and API.
          </p>
        </a>

        <a
          href="https://nextjs.org/learn?utm_source=create-next-app&utm_medium=appdir-template-tw&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Learn{' '}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Learn about Next.js in an interactive course with&nbsp;quizzes!
          </p>
        </a>

        <a
          href="https://vercel.com/templates?framework=next.js&utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Templates{' '}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Explore starter templates for Next.js.
          </p>
        </a>

        <a
          href="https://vercel.com/new?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
          className="group rounded-lg border border-transparent px-5 py-4 transition-colors hover:border-gray-300 hover:bg-gray-100 hover:dark:border-neutral-700 hover:dark:bg-neutral-800/30"
          target="_blank"
          rel="noopener noreferrer"
        >
          <h2 className={`mb-3 text-2xl font-semibold`}>
            Deploy{' '}
            <span className="inline-block transition-transform group-hover:translate-x-1 motion-reduce:transform-none">
              -&gt;
            </span>
          </h2>
          <p className={`m-0 max-w-[30ch] text-sm opacity-50`}>
            Instantly deploy your Next.js site to a shareable URL with Vercel.
          </p>
        </a>

        <UnstyledButton onClick={() => console.log('unstyled button clicked')}>
          Unstyled
        </UnstyledButton>
        <TailwindButton onClick={() => console.log('tailwind button clicked')}>
          Tailwind CSS
        </TailwindButton>
        <CssModulesButton
          onClick={() => console.log('css modules button clicked')}>
          CSS Modules
        </CssModulesButton>

        <LoginButton />

        <article className="prose prose-slate dark:prose-invert lg:prose-xl mt-4">
          <h1>Garlic bread with cheese: What the science tells us</h1>
          <p>
            For years parents have espoused the health benefits of eating garlic bread with cheese to their
            children, with the food earning such an iconic status in our culture that kids will often dress
            up as warm, cheesy loaf for Halloween.
          </p>
          <p>
            But a recent study shows that the celebrated appetizer may be linked to a series of rabies cases
            springing up around the country.
          </p>
          <h2>What to expect from here on out</h2>
          <p>What follows from here is just a bunch of absolute nonsense I&apos;ve written to dogfood the plugin itself. It includes every sensible typographic element I could think of, like <strong>bold text</strong>, unordered lists, ordered lists, code blocks, block quotes, <em>and even italics</em>.</p>
          <p>It&apos;s important to cover all of these use cases for a few reasons:</p>
          <ol>
            <li>We want everything to look good out of the box.</li>
            <li>Really just the first reason, that&apos;s the whole point of the plugin.</li>
            <li>Here&apos;s a third pretend reason though a list with three items looks more realistic than a list with two items.</li>
          </ol>
          <p>Now we&apos;re going to try out another header style.</p>
          <h3>Typography should be easy</h3>
          <p>So that&apos;s a header for you — with any luck if we&apos;ve done our job correctly that will look pretty reasonable.</p>
          <p>Something a wise person once told me about typography is:</p>
          <blockquote><p>Typography is pretty important if you don&apos;t want your stuff to look like trash. Make it good then it won&apos;t be bad.</p></blockquote>
          <p>It&apos;s probably important that images look okay here by default as well:</p>
          <figure>
            <img src="https://images.unsplash.com/photo-1556740758-90de374c12ad?ixlib=rb-1.2.1&amp;ixid=eyJhcHBfaWQiOjEyMDd9&amp;auto=format&amp;fit=crop&amp;w=1000&amp;q=80" alt="" />
            <figcaption>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</figcaption>
          </figure>
          <p>Now I&apos;m going to show you an example of an unordered list to make sure that looks good, too:</p>
          <ul>
            <li>So here is the first item in this list.</li>
            <li>In this example we&apos;re keeping the items short.</li>
            <li>Later, we&apos;ll use longer, more complex list items.</li>
          </ul>
          <p>And that&apos;s the end of this section.</p>
          <h2>What if we stack headings?</h2>
          <h3>We should make sure that looks good, too.</h3>
          <p>Sometimes you have headings directly underneath each other. In those cases you often have to undo the top margin on the second heading because it usually looks better for the headings to be closer together than a paragraph followed by a heading should be.</p>
          <h3>When a heading comes after a paragraph …</h3>
          <p>When a heading comes after a paragraph, we need a bit more space, like I already mentioned above. Now let&apos;s see what a more complex list would look like.</p>
          <ul>
            <li>
              <p><strong>I often do this thing where list items have headings.</strong></p>
              <p>For some reason I think this looks cool which is unfortunate because it&apos;s pretty annoying to get the styles right.</p>
              <p>I often have two or three paragraphs in these list items, too, so the hard part is getting the spacing between the paragraphs, list item heading, and separate list items to all make sense. Pretty tough honestly, you could make a strong argument that you just shouldn&apos;t write this way.</p>
            </li>
            <li>
              <p><strong>Since this is a list, I need at least two items.</strong></p>
              <p>I explained what I&apos;m doing already in the previous list item, but a list wouldn&apos;t be a list if it only had one item, and we really want this to look realistic. That&apos;s why I&apos;ve added this second list item so I actually have something to look at when writing the styles.</p>
            </li>
            <li>
              <p><strong>It&apos;s not a bad idea to add a third item either.</strong></p>
              <p>I think it probably would&apos;ve been fine to just use two items but three is definitely not worse, and since I seem to be having no trouble making up arbitrary things to type, I might as well include it.</p>
            </li>
          </ul>
          <p>After this sort of list I usually have a closing statement or paragraph, because it kinda looks weird jumping right to a heading.</p>
          <h2>Code should look okay by default.</h2>
          <p>I think most people are going to use <a href="https://highlightjs.org/">highlight.js</a> or <a href="https://prismjs.com/">Prism</a> or something if they want to style their code blocks but it wouldn&apos;t hurt to make them look <em>okay</em> out of the box, even with no syntax highlighting.</p>
          <p>Here&apos;s what a default <code>tailwind.config.js</code> file looks like at the time of writing:</p>
          <pre>
            <code className="js">
              {`
module.exports = {
  purge: [],
  theme: {
    extend: { },
  },
  variants: { },
  plugins: [],
}`}
            </code>
          </pre>
          <pre>
            <code className="js">
              {`import React from 'react';`}
            </code>
          </pre>
        </article>

      </div>
    </main>
  )
}
