'use client'

// based on https://www.apollographql.com/docs/react/get-started

import { useQuery, gql } from '@apollo/client';

interface QueryResults {
  id: string;
  name: string;
  description: string;
  photo: string;
}

const GET_LOCATIONS = gql`
  query GetLocations {
    locations {
      id
      name
      description
      photo
    }
  }`;

export default function GraphQLExamplePage() {
  const { loading, error, data } = useQuery(GET_LOCATIONS);

  if (loading) return <p className="p-4">Loading...</p>;
  if (error) return <p>Error : {error.message}</p>;

  return (
    <section className="p-8 col-span-4 md:col-span-8 lg:col-span-12">
      <article className="w-full prose lg:prose-xl dark:prose-invert">
        <h2>GraphQL example</h2>
        {data.locations.map(({ id, name, description, photo }: QueryResults) => (
          <div className="w-full" key={id}>
            <h3>{name}</h3>
            <img
              height="250"
              alt="location-reference"
              src={`${photo}`} />
            <h4>About this location:</h4>
            <p>{description}</p>
          </div>
        ))}
      </article>
    </section>
  );
}
