'use client'

import client from '@/services/graphql';
import { ApolloProvider } from '@apollo/client';

export default function Layout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <ApolloProvider client={client}>
      {children}
    </ApolloProvider>
  );
}
