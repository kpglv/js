const Home = () => {
  return (
    <section className="flex flex-col gap-8 col-span-4 md:col-span-8 lg:col-span-12 p-8">
      <article className="prose dark:prose-invert">
        <h2>Small Component Library</h2>
        <p>A collection of React components styled with Tailwind CSS.</p>
      </article>
    </section>
  );
}

export default Home;