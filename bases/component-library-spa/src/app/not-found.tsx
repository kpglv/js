import Link from 'next/link'

const notFoundClasses = [
    "col-span-4 md:col-span-8 lg:col-span-12",
    "flex flex-col justify-center items-center gap-8 p-8",
].join(" ");

export default function NotFound() {
    return (
        <section className={notFoundClasses}>
            <div className="prose dark:prose-invert">
                <h2>Not Found</h2>
                <p>Could not find requested resource</p>
                <Link href="/">Return Home</Link>
            </div>
        </section>
    )
}