;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil . (;; configure an org-roam directory for the workspace or building
         ;; block (project/base/component) documentation and task management
         ;; (use org-roam commands when visiting a file from the workspace or
         ;; building block)
         (eval . (setq-local
                  org-roam-directory
                  (expand-file-name "docs"
                                    (locate-dominating-file
                                     "." ".dir-locals.el"))))
         (eval . (setq-local
                  org-roam-db-location
                  (expand-file-name "org-roam.db"
                                    org-roam-directory))))))
