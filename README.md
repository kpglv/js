# js

A monorepo workspace for personal projects in JavaScript.

## Overview

This workspace follows the approach to code organization introduced by
[Polylith](https://polylith.gitbook.io/polylith/); see also the `clj`
repository's README for brief explanation how it is applied in this workspace
(and other my workspaces).

Probably these places would be most interesting to explore here:

-   `components/dependency-injection`: a simple DI container component, that
    uses only local dependencies (`components/dependency-graph`,
    `components/multimethods`, `components/tree-walk`) and should work on
    Node and in browser; see also `guides/hello-di` for its usage example
-   `bases/openlibrary-spa`: a React/Next.js SPA that uses public Open Library API
-   `bases/wise-cats-spa`: a Vue 3 SPA that uses public Fortunes API and
    The Cat API
-   `tools/vue-histoire`: an environment for Component Driven Development and visual
    testing with Vue 3, using Histoire

## Getting Started

This workspace uses [Lerna](https://lerna.js.org/) to run workspace-wide
tasks. Each building block (component, base, project) is a [Yarn
workspace](https://classic.yarnpkg.com/en/docs/workspaces/), so they can
depend on each other in the same way as on external libraries, and all their
dependencies can be kept together, deduplicated and shared. See `lerna.json`
and `package.json#/workspaces` for configuration options.

For more info on specific building blocks, see their documentation. All
building blocks can be found in `components`, `bases` and `projects`
directories.

There are also auxiliary (non-building) blocks like `assignments`, `guides`
and `tools`, that serve some special purpose --- a public demo project, usage
guide for some technology/library/framework, development/testing environment
etc.

### Examples

Run unit tests for all building blocks:

```
npx lerna run test
```

Run unit tests for a specific building block (the `@kpglv/multimethods` component,
the `openlibrary-spa` base):

```
npx lerna run test --scope=@kpglv/multimethods
npx lerna run test --scope=openlibrary-spa
```

Run unit tests for building blocks that are affected by changes introduced
since the specified revision:

```
npx lerna run test --since=origin/master
```

Explore the workspace dependency graph:

```
npx nx graph
```
