import { stat, mkdir, writeFile } from 'node:fs/promises';
import path from 'node:path';

/**
 * A note.
 */
interface Note {
    text: string;
    created: string;
}

/**
 * A collection of notes.
 */
interface Notes {
    created: string;
    notes: Note[];
}

/**
 * Checks if the dir exists, tries to create
 * the dir if it does not exist, and throws on failure.
 * @param dir
 */
export const ensureDirExists = async (dir: string) => {
    try {
        await stat(dir);
    } catch (e) {
        await mkdir(dir, { recursive: true });
    }
};

/**
 * Exports a collection of notes to text files, one note per file.
 * @param notes
 * @param options
 */
export const toFiles = async (
    { notes }: Notes,
    options?: { dir?: string; ext?: string }
) => {
    const { dir = '.', ext = 'txt' } = options || {};
    await ensureDirExists(dir);

    notes.forEach(async ({ text, created }) => {
        const title = text.match(/^.*$/m)?.[0] || 'untitled';
        const filename = path.join(dir, `${created}-${title}.${ext}`);
        await writeFile(filename, text);
    });
};

// usage examples (bun)

import notes from './data/notes.json';
await toFiles(notes, { dir: 'notes', ext: 'md' });
