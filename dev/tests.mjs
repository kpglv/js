// https://www.sohamkamani.com/nodejs/create-testing-framework/


// a simplified implementation of a testing library; allows to define and run
// tests


// the global registry of tests

const tests = [];

// assertions: should throw on failure

export const is = (x, y) => {
  if (x !== y) {
    throw new Error(`assertion failed: is(${x}, ${y})`);
  }
};

export const throws = (thunk) => {
  try {
    thunk();
  } catch (e) {
    return;
  }
  throw new Error(`assertion failed: throws(${thunk})`);
};

// defining tests

export const test = (name, fn) => {
  tests.push({ name, fn });
};

// running tests

export const runAll = () => {
  tests.forEach(t => {
    try {
      t.fn();
      console.log('✅', t.name);
    } catch (e) {
      console.log('❌', t.name);
      console.log(' ⤷', e.message);
    }
  });
};
