// https://www.geeksforgeeks.org/how-to-use-variadic-tuple-types-in-typescript/
// https://blog.dwgill.com/tricks-with-variadic-tuple-types-in-typescript-4/

/* Function Overloading with Variadic Tuples */

// Define a function that handles various types of arguments
function handleArgs<T extends unknown[]>(...args: T): void {
    const arg0 = args[0];
    console.log('arg 0:', arg0);
    // Iterate through the arguments and handle each one
    args.forEach((arg, index) => {
        console.log(`Argument ${index}:`, arg);
    });
}

// Usage example
handleArgs('Hello', 42, true, { key: 'value' });

/* Tuple Transformations */

// Type that appends a new element to a given tuple
type AppendToTuple<T extends any[], U> = [...T, U];

// Define a tuple
type OriginalTuple = [number, string];

// Append a boolean to the tuple
type NewTuple = AppendToTuple<OriginalTuple, boolean>;

// Usage example
const example: NewTuple = [42, 'hello', true];
console.log('transformed tuple:', example);

/* Typing Partial Function Application */

const partial =
    <Result, Args extends unknown[], RestArgs extends unknown[]>(
        fn: (...allArgs: [...Args, ...RestArgs]) => Result,
        ...firstArgs: Args
    ) =>
    (...restArgs: RestArgs) =>
        fn(...firstArgs, ...restArgs);

// Usage example

const baseFunction = (
    a: string,
    b: number,
    c: Record<string, number>,
    d: number[]
) => {
    console.log('a function of 4 args: %o, %o, %o, %o', a, b, c, d);
};

const partiallyAppliedBaseFunction = partial(baseFunction, 'foo', 10);
partiallyAppliedBaseFunction({ id: 1 }, [123, 345]);

/** Typing Arbitrary Variadic Functions */
// TODO:
