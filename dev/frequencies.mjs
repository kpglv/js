/**
 * Returns an array of words extracted from the string.
 */
const words = (string) => string.match(/\w+/g);

/**
 * Returns a map of frequencies of occurrence for words in the given array of
 * words.
 */
const frequencies = (words) => words.reduce((acc, val) => {
  if (acc[val] == null) {
    acc[val] = 1;
  } else {
    acc[val] += 1;
  };
  return acc;
}, {});

/**
 * Returns an array of formatted entries of the given mapping (object).
 */
const format = (mapping) =>  Object.entries(mapping)
      .map(([key, value]) => `${key} - ${value}`);


// usage example

format(frequencies(words("apple banana orange apple")))
  .forEach(entry => console.log(entry));
