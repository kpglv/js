// https://en.wikipedia.org/wiki/Basal_metabolic_rate
// https://www.calculator.net/calorie-calculator.html#reference

// about 70% of a human's total energy expenditure is due to the basal
// life processes taking place in the organs of the body; about 20%
// comes from physical activity and another 10% from thermogenesis

// BMR estimation formula coefficients

const originalHarrisBenedict = {
    name: 'original Harris-Benedict',
    male: {
        massCoefficient: 13.7516,
        heightCoefficient: 5.0033,
        ageCoefficient: -6.755,
        constant: 66.473,
    },
    female: {
        massCoefficient: 9.5634,
        heightCoefficient: 1.8496,
        ageCoefficient: -4.6756,
        constant: 655.0955,
    },
};

const revisedHarrisBenedict = {
    name: 'revised Harris-Benedict',
    male: {
        massCoefficient: 13.397,
        heightCoefficient: 4.799,
        ageCoefficient: -5.677,
        constant: 88.362,
    },
    female: {
        massCoefficient: 9.247,
        heightCoefficient: 3.098,
        ageCoefficient: -4.33,
        constant: 447.593,
    },
};

const mifflinStJeor = {
    name: 'Mifflin-St Jeor',
    male: {
        massCoefficient: 10.0,
        heightCoefficient: 6.25,
        ageCoefficient: -5,
        constant: 5,
    },
    female: {
        massCoefficient: 10.0,
        heightCoefficient: 6.25,
        ageCoefficient: -5,
        constant: -161,
    },
};

// BMR estimation formula

/**
 * Returns the value of daily basal metabolic rate (BMR) in kcal
 * estimated given the body mass, the body height and the age according
 * to estimation formula parameterized by corresponding coefficients and
 * a constant.
 * @param {Object} param0 mass (kg), height (cm), age (years) and
 *   sex ('male' or 'female)
 * @param {Object} param1 coefficients
 * @returns
 */
const basalMetabolicRate = (
    { mass, height, age },
    { massCoefficient, heightCoefficient, ageCoefficient, constant }
) =>
    massCoefficient * mass +
    heightCoefficient * height +
    ageCoefficient * age +
    constant;

const basalMetabolicRateBySex = (personParams, sex, formula) =>
    basalMetabolicRate(personParams, formula[sex]);

const formatBMR = ({ name }, formula, bmr) =>
    `BMR for ${name}, using ${formula.name} formula: ${Math.round(bmr)} kcal`;

// usage examples

const formulae = [
    originalHarrisBenedict,
    revisedHarrisBenedict,
    mifflinStJeor,
];

// example: a 55-year-old woman weighing 59 kg and 170 cm tall
const one = { name: '#1', mass: 59, height: 170, age: 55 };
formulae.forEach(f =>
    console.log(formatBMR(one, f, basalMetabolicRateBySex(one, 'female', f)))
);

// a 45-year-old man weighing 102 kg and 178 cm tall
const two = { name: '#2', mass: 102, height: 178, age: 45 };
formulae.forEach(f =>
    console.log(formatBMR(two, f, basalMetabolicRateBySex(two, 'male', f)))
);

// a 53-year-old woman weighing 98 kg and 170 cm tall
const three = { name: '#3', mass: 98, height: 170, age: 53 };
formulae.forEach(f =>
    console.log(
        formatBMR(three, f, basalMetabolicRateBySex(three, 'female', f))
    )
);

// a 75-year-old woman weighing 70 kg and 160 cm tall
const four = { name: '#4', mass: 70, height: 160, age: 75 };
formulae.forEach(f =>
    console.log(
        formatBMR(four, f, basalMetabolicRateBySex(four, 'female', f))
    )
);

// Body Mass Index (BMI)
// mass in kilograms, height in centimeters
const BMI = (mass, height) => mass / (height / 100.0) ** 2;

const argv = process.argv.slice(2);
if (argv.length < 4) {
    console.log(`usage: node weight.mjs <kg> <cm> <years> <male|female>`);
    process.exit(1);
}

const [mass, height, age, sex] = argv;
const data = {
    mass: parseFloat(mass),
    height: parseFloat(height),
    age: parseFloat(age),
};
console.log(
    formatBMR(
        data,
        mifflinStJeor,
        basalMetabolicRateBySex(data, sex, mifflinStJeor)
    )
);
const bmi = BMI(mass, height);
console.log(
    'BMI:',
    bmi,
    `(${bmi < 25 ? 'normal' : bmi < 30 ? 'overweight' : 'obese'})`
);
