
def identity (x):
    return x

def partial (f, *args):
    return lambda *more: f(*args, *more)

def walk (inner, outer, tree):
    if isinstance(tree, dict):
        return dict(map(lambda entry: [inner(entry[0]), inner(entry[1])], tree.items()))
    elif isinstance(tree, list):
        return outer(list(map(inner, tree)))
    else:
        return outer(tree)

def prewalk (f, tree):
    return walk(partial(prewalk, f), identity, f(tree))

def postwalk (f, tree):
    return walk(partial(postwalk, f), f, tree)


def preprint (x):
    print("prewalked:", x)
    return x

def postprint (x):
    print("postwalked:", x)
    return x

def prewalk_demo (tree):
    return prewalk(preprint, tree)

def postwalk_demo (tree):
    return postwalk(postprint, tree)

if __name__ == "__main__":
    def add(x, y):
        return x + y
    add2 = partial(add, 2)
    print(add(3, 4))
    print(add2(3))

    print(isinstance({}, dict))
    print(isinstance([], list))
    #print(list(map(lambda entry: (entry[0], entry[1]), {"id": 1, "name": "Joe"}.items())))

    tree = {"id": 1, "children": [{"id": 2, "age": 5}, {"id": 3, "age": 6}, 12]}
    #print(dict(tree.items()))
    #print(list(map(lambda entry: [identity(entry[0]), identity(entry[1])], tree.items())))

    #prewalk(print, tree)
    #postwalk(print, tree)
    #prewalk_demo(tree)
    #postwalk_demo(tree)

    def increment_id (value):
        if isinstance(value, dict) and value["id"] == 3:
            value["id"] = 300
        return value

    print(prewalk(increment_id, tree))
    print(postwalk(increment_id, tree))


    d = {'spam': [{'foo': 'bar'}]}

    def bar_to_baz (value):
        if isinstance(value, dict) and "foo" in value and value["foo"] == "bar":
            return {**value, "foo": "baz"}
        return value

    print(prewalk(bar_to_baz, d))
