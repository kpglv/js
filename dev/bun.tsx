import React from 'react';

interface FooProps {
    classes?: string;
    children?: React.ReactNode;
}

export const Foo = ({ classes, children }: FooProps) => (
    <div className={classes}>{children}</div>
);

// usage examples (bun)

console.log(typeof Foo({}));
console.log(Object.keys(Foo({})));
console.log(Foo({ classes: 'foo bar', children: 'Hello Bun + TSX!' }));
console.log(<Foo classes="bar baz">Hello Bun + TSX!</Foo>);
