import { invokedDirectly } from './helpers.mjs';

// https://clojure.org/reference/lazy
// https://en.wikipedia.org/wiki/Thunk
// https://stackoverflow.com/a/19862784

// delayed evaluations (thunks)

// marker symbol for delayed evaluations, to distinguish them from other
// functions (and to store a value once it was evaluated)
const value = Symbol('value');
export const nil = Symbol('nil');

// returns true if x is a delayed evaluation
export const isDelayed = x =>
    typeof x === 'function' && x.hasOwnProperty(value);

// returns true if delayed evaluation was already forced and has a value
export const isForced = delayed => delayed[value] !== nil;

// creating a delayed evaluation
export const delay = (fn, ...args) => {
    const delayed = () => fn(...args);
    delayed[value] = nil;
    return delayed;
};
// forcing a delayed evaluation
export const force = x => {
    if (isDelayed(x)) {
        if (isForced(x)) {
            return x[value];
        } else {
            const result = x();
            x[value] = result;
            return result;
        }
    } else {
        return x;
    }
};

// usage examples

if (invokedDirectly(import.meta.url)) {
    const inc = x => {
        console.log('thunk is evaluated');
        return x + 1;
    };

    const d = delay(inc, 0);
    console.log('thunk:', d);
    console.log('is it a thunk?', isDelayed(d));
    console.log('is arbitrary function a thunk?', isDelayed(inc), '\n');

    console.log('forced thunk:', force(d));
    console.log('forced thunk:', force(d));
    console.log('forced arbitrary function:', inc);
    console.log('forced a number:', 123);
}
