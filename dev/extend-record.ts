interface User {
    id: number;
    name: string;
}

type Manager = Record<string, string | number>;

type Extended<T> = T & { [key: string]: string };

const u: User = { id: 1, name: 'Joe' };

(u as Extended<User>).foo = 'bar';

console.log("user's foo is", (u as Extended<User>).foo);

const m: Manager = { id: 1, name: 'Mary', age: '34' };

m.foo = 'bar';

console.log("manager's foo is", m.foo);

const n: bigint = 122n;
