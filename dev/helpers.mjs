import { pathToFileURL } from 'url';

/**
 * Returns true if ES6 module with given URL was invoked directly
 * from command line (not imported from another module).
 */
export const invokedDirectly = moduleUrl =>
    moduleUrl === pathToFileURL(process.argv[1]).href;

// usage examples

if (invokedDirectly(import.meta.url)) {
    console.log('the helpers module was invoked directly');
}
