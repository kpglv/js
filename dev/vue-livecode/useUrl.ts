import { type Ref, ref, watch } from 'vue';

export const useUrl = (url: Ref) => {
    const data = ref();

    watch(url, async () => {
        data.value = await fetch(url.value);
    });

    return {
        data,
    };
};
