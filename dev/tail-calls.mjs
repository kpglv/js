// TODO: find a way to implement some kind of tail-call elimination (TCE)
// (not sure if possible without support in most JS engines)
//
// - tail calls explained
//   https://2ality.com/2015/06/tail-call-optimization.html
// - tail call implementation process in JS explained
//   (Proper Tail Calls (PTC) or Syntactic Tail Calls (STC))
//   https://stackoverflow.com/a/54721813
// - STC proposal https://github.com/tc39/proposal-ptc-syntax
//
// - trampoline (slow) https://stackoverflow.com/a/54719630
// - https://en.wikipedia.org/wiki/Trampoline_(computing)
//
// - tail calls, trampolines, continuations, loop/call/recur
//   and some more https://stackoverflow.com/q/57733363

// trampolining: loop, recur, call

// reifying normal/recursive function calls and making them lazy;
// a way to put the information that normally goes onto the call stack
// into an explicit data structure (a tree of normal and/or
// recursive function call expressions)
export const recur = (...args) => ({ recur, args });
export const call = (f, ...args) => ({ call, f, args });

// an interpreter for the tree of function call expressions created by
// invocations of call/recur
export const loop = (f, ...args) => {
    let ret = f(...args);
    while (ret) {
        if (ret.recur === recur) ret = f(...ret.args);
        else if (ret.call === call) ret = ret.f(...ret.args);
        else break;
    }
    return ret;
};

// usage examples

// a CPS example from Stackoverflow
const identity = x => x;
const foldr = (f, init, xs = []) =>
    loop((i = 0, k = identity) =>
        i >= xs.length
            ? call(k, init)
            : recur(i + 1, r => call(k, f(r, xs[i])))
    );

const small = [1, 2, 3];
const large = Array.from(Array(2e4), (_, n) => n + 1);
console.log(foldr((a, b) => `(${a}, ${b})`, 0, small));
console.log(foldr((a, b) => `(${a}, ${b})`, 0, large));

// a simple example that uses linear recursion
import { first, rest, isEmpty } from './core.mjs';
const find = (x, coll) =>
    isEmpty(coll)
        ? null
        : first(coll) === x
        ? first(coll)
        : find(x, rest(coll));
const find1 = (x, coll) =>
    loop(
        (x, coll) =>
            isEmpty(coll)
                ? null
                : first(coll) === x
                ? first(coll)
                : recur(x, rest(coll)),
        x,
        coll
    );

console.log(find(999, large));
// blows the stack for a coll of 20k elements,
// if the element is at the end or absent
// console.log(find(19999, large));
console.log(find1(9, large));
// does not blow the stack (but is quite slow indeed)
console.log(find1(19999, large));
console.log(find1(199999, large));

console.time('find1, 19999');
find1(19999, large);
console.timeEnd('find1, 19999');

console.time('for loop, 19999');
for (let i = 0; i < 2e4; i++) {
    large[i] === 19999;
}
console.timeEnd('for loop, 19999');
