const CycledIndexGenerator = function* (end) {
    let counter = 0;
    while (true) {
        yield counter;
        if (end === 0) continue;
        counter = (counter + 1) % end;
    }
};

// usage examples

const gen = CycledIndexGenerator(3);
console.log(gen);

for (let i = 0; i < 5; i++) {
    console.log('cycler 1 yields:', gen.next());
}

const gen1 = CycledIndexGenerator(0);

for (let i = 0; i < 5; i++) {
    console.log('cycler 2 yields:', gen1.next());
}
