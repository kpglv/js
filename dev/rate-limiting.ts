export type Fn<Args extends unknown[], ReturnValue> = (
    ...args: Args
) => ReturnValue;

export type VoidFn<Args extends unknown[]> = Fn<Args, void>;

export type TimeoutType = ReturnType<typeof setTimeout>;

// TODO: make them accept any function (not only void-returning ones)

/**
 * Returns a function that calls `fn` when invoked, with the same arguments.
 * Invocation attempts will be ignored until `delay` milliseconds passed after the last one.
 */
export const debounce = <Args extends unknown[]>(
    fn: VoidFn<Args>,
    delay: number
): VoidFn<Args> => {
    let timer: TimeoutType | undefined;
    return (...args) => {
        clearTimeout(timer);
        timer = setTimeout(() => {
            fn(...args);
        }, delay);
    };
};

/**
 *
 * Returns a function that calls `fn` when invoked, with the same arguments.
 * Invocation attempts will be successful only once in `delay` milliseconds.
 */
export const throttle = <Args extends unknown[]>(
    fn: VoidFn<Args>,
    delay: number
): VoidFn<Args> => {
    let timer: TimeoutType | null = null;
    return (...args) => {
        if (timer === null) {
            fn(...args);
            timer = setTimeout(() => {
                timer = null;
            }, delay);
        }
    };
};

// usage examples

const say = (
    id: number,
    name: string,
    properties: Record<string, unknown>,
    iteration: number
) =>
    console.log(
        `\tsay: id ${id}, name ${name}, has ${
            Object.keys(properties).length
        } properties: ${Object.keys(properties)}, invoked after iteration %o`,
        iteration
    );

const debouncedSay = debounce(say, 100);
const debouncedLog = debounce(console.log, 1000);
const throttledLog = throttle(console.log, 1000);

const iterations = 100;

console.log('calling rate-limited functions %o times:', iterations);
for (let i = 0; i < iterations; i++) {
    throttledLog(
        '\tthrottled console.log: invoked after iteration %o',
        i + 1
    );
    debouncedLog(
        '\tdebounced console.log: invoked after iteration %o',
        i + 1
    );
    debouncedSay(1, 'Joe', { age: 54, children: 8 }, i + 1);
}
