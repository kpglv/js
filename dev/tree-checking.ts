// task: given a record of checked states of leaves in the tree ('checked'/'unchecked'), find checked states
// of the root and the branches ('checked'/'unchecked'/'partially-checked') efficiently

// a tree consists of several kinds of nodes:
// - a leaf is a node without children
// - a branch is a node with a list of children (possibly empty)
// - a root is a node without parent; any node may represent a root node
//   (of some subtree of the original tree)

// while a leaf can be just checked or unchecked, a branch can be
// - unchecked, if it has only unchecked children (a branch with no children also qualifies)
// - checked, if it has only checked children
// - partially checked otherwise

type TreeNodeId = number;

interface TreeNode {
    children?: TreeNodeId[];
}

interface IndexedTree {
    ids: TreeNodeId[];
    nodes: Record<TreeNodeId, TreeNode>;
}

type TreeLeafCheckedState = 'checked' | 'unchecked';
type TreeBranchCheckedState = TreeLeafCheckedState | 'partially-checked';

/**
 * Returns a record that associates identifiers of all nodes in `tree`
 * under `rootNode` with their checked states, given `checkedLeaves`.
 *
 * Implements a tree-recursive algorithm (there can be more than one recursive call per invocation).
 *
 * Computational complexity seems like linear with the total number of nodes T
 * (both branches and leaves), each of which is visited (to determine its checked state)
 * only once.
 */
const getCheckedStates = (
    rootNodeId: TreeNodeId,
    checkedLeaves: Record<TreeNodeId, TreeLeafCheckedState>,
    tree: IndexedTree
): Record<TreeNodeId, TreeBranchCheckedState> => {
    const { nodes } = tree;
    const { children } = nodes[rootNodeId] ?? {};

    if (!children) {
        // root node has no children - it is a leaf; if it can be found
        // in checked leaves, it has the value specified there, else it is left unchecked
        // (non-recursive case - aka base case)
        return { [rootNodeId]: checkedLeaves[rootNodeId] ?? 'unchecked' };
    } else {
        // root node has some children - it is a branch; compute checked states of its children
        // (recursive case)
        const childrenCheckedStates = children.reduce(
            // beware that combining objects using spreading operator and creating new objects
            // may be way slower (like 2 - 3 times) than mutating alternatives like Object.assign(),
            // especially noticeable on large trees; left as it is here for clarity's sake
            (checkedStates, nodeId) => ({
                ...checkedStates,
                ...getCheckedStates(nodeId, checkedLeaves, tree),
            }),
            {} as Record<TreeNodeId, TreeBranchCheckedState>
        );

        // derive the (current) root node state from the states of its (direct) children;
        const rootNodeCheckedState = getBranchCheckedState(
            Object.values(childrenCheckedStates)
        );

        // return combined checked states of the root node and its children
        return {
            ...childrenCheckedStates,
            [rootNodeId]: rootNodeCheckedState,
        };
    }
};

/**
 * Returns the checked state of a branch node given a list
 * of its children's checked states.
 */
const getBranchCheckedState = (
    childrenCheckedStates: (TreeLeafCheckedState | TreeBranchCheckedState)[]
): TreeBranchCheckedState => {
    switch (childrenCheckedStates.length) {
        case 0:
            // children list is empty - the branch is unchecked
            return 'unchecked';
        case 1:
            // there is only one child - the branch has the same checked state as this child
            return childrenCheckedStates[0];
        default: {
            const firstChildState = childrenCheckedStates[0];
            // if a child with any other state than that of the first child is found,
            // the branch is necessarily partially checked and we can return immediately
            for (let i = 1; i < childrenCheckedStates.length; i++) {
                if (childrenCheckedStates[i] !== firstChildState)
                    return 'partially-checked';
            }
            // if no such child is found, the branch has the same state as its first child
            return firstChildState;
        }
    }
};

// usage examples

//       1        <- root
//     /  \
//    2    5      <- branches
//   /|   /|\
//  3 4  6 7 8    <- leaves

const tree: IndexedTree = {
    ids: [1, 2, 3, 4, 5, 6, 7, 8],
    nodes: {
        1: { children: [2, 5] },
        2: { children: [3, 4] },
        5: { children: [6, 7, 8] },
    },
};

const checkedLeaves: Record<TreeNodeId, TreeLeafCheckedState> = {
    3: 'checked',
    4: 'checked',
    7: 'checked',
    8: 'checked',
};

const checkedRootAndBranches = getCheckedStates(1, checkedLeaves, tree);

// 6 is unchecked, so 1 and 5 should be checked partially, and all other nodes should be checked

console.log('checked states:');
console.log('leaves (given):', checkedLeaves);
console.log('root and branches (computed):', checkedRootAndBranches);

// very hard to understand, and probably slower; do not try this at ho^W work!

const gcs = (rni, cl, t) =>
    (ccsr => ({
        ...ccsr,
        [rni]: (c =>
            !c.length
                ? 'unchecked'
                : c.length === 1
                ? c[0]
                : (() => {
                      for (let i = 1; i < c.length; i++) {
                          if (c[i] !== c[0]) return 'partially-checked';
                      }
                      return c[0];
                  })())(Object.values(ccsr)),
    }))(
        t.nodes[rni]?.children?.reduce(
            (cs, ni) => ({
                ...cs,
                ...gcs(ni, cl, t),
            }),
            {}
        ) ?? { [rni]: cl[rni] ?? 'unchecked' }
    );

console.log('it is evil, but it works:', gcs(1, checkedLeaves, tree));
