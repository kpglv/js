// based on https://www.geeksforgeeks.org/binary-search-in-javascript/

export type Index = number;
export type ComparisonResult = -1 | 0 | 1;
export type Comparator<E, K = E> = (
    arrayElement: E,
    elementOrKeyToFind: K
) => ComparisonResult;

/**
 * Returns index of `x` in the sorted array `xs`, or `undefined`.
 *
 * Equality of elements is determined by `===` operator by default.
 *
 * If a custom comparator is provided, it is used to determine equality of elements instead
 * (and the array must be sorted with respect to the value that is used for comparison).
 *
 * Generates a recursive computational process (but the algorithm is O(log N), so there's no need
 * to worry about stack consumption for any sensible N - e.g., search on an array containing
 * 1e8 elements will take 26 steps in the worst case).
 */
export const recursiveBinarySearch = <E, K = E>(
    x: K,
    xs: E[],
    comparator: Comparator<E, K>,
    log?: boolean
): Index | undefined => {
    return recursiveBinarySearchStep(
        x,
        xs,
        0,
        xs.length - 1,
        comparator,
        log
    );
};

/** A recursive binary search step; provided to make the public binary search function
    more convenient to use (no need to provide initial start and end indices) */
const recursiveBinarySearchStep = <E, K = E>(
    x: K,
    xs: E[],
    startIndex: Index,
    endIndex: Index,
    comparator: Comparator<E, K>,
    log?: boolean
): Index | undefined => {
    if (startIndex > endIndex) return;

    const middleIndex = Math.floor((startIndex + endIndex) / 2);
    if (log) {
        console.log(
            'recursive binary search step, guess: %o at index %o',
            xs[middleIndex],
            middleIndex
        );
    }

    const comparisonResult = comparator(xs[middleIndex], x);

    switch (comparisonResult) {
        case 0:
            return middleIndex;
        case -1:
            return recursiveBinarySearchStep(
                x,
                xs,
                middleIndex + 1,
                endIndex,
                comparator,
                log
            );
        case 1:
            return recursiveBinarySearchStep(
                x,
                xs,
                startIndex,
                middleIndex - 1,
                comparator,
                log
            );
    }
};

/** Default comparator */
const compare = <E>(arrayElement: E, elementToFind: E) =>
    arrayElement < elementToFind ? -1 : arrayElement > elementToFind ? 1 : 0;

/** Usage examples (e.g., `bun binary-search.ts`) */

/** On numbers */

const numbers = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 128, 256, 1024,
];

console.log('numbers: %o', numbers);

console.log(
    'binary search on numbers, finding index of 128 (should be 14):',
    recursiveBinarySearch(128, numbers, compare)
);

console.log(
    'binary search on numbers, finding index of 345 (absent):',
    recursiveBinarySearch(345, numbers, compare)
);

/** On arbitrary objects */

const objects = numbers.map(n => ({ id: n }));

console.log('objects: %o', objects);

console.log(
    'binary search on objects, finding index of object with ID 128 (should be 14):',
    recursiveBinarySearch({ id: 128 }, objects, (a, b) => compare(a.id, b.id))
);

console.log(
    'binary search on objects, finding index of object with ID 345 (absent):',
    recursiveBinarySearch({ id: 345 }, objects, (a, b) => compare(a.id, b.id))
);

/** On numeric intervals (non-overlapping, inclusive) */

const intervals: [number, number][] = numbers.map(n => [n * 10, n * 10 + 5]);

const withinInterval = (interval: [number, number], x: number): boolean =>
    x >= interval[0] && x <= interval[1];
const lessThanInterval = (interval: [number, number], x: number): boolean =>
    !withinInterval(interval, x) && x <= interval[0];

console.log('intervals: %o', intervals);

console.log(
    'binary search on (inclusive, non-overlapping) intervals, finding index of interval containing number 31 (should be 2):',
    recursiveBinarySearch(31, intervals, (interval, x) =>
        withinInterval(interval, x)
            ? 0
            : lessThanInterval(interval, x)
            ? 1
            : -1
    )
);

console.log(
    'binary search on (inclusive, non-overlapping) intervals, finding index of interval containing number 4096 (absent):',
    recursiveBinarySearch(4096, intervals, (interval, x) =>
        withinInterval(interval, x)
            ? 0
            : lessThanInterval(interval, x)
            ? 1
            : -1
    )
);

// Comparing performance of linear and binary search

/** Returns `n` first elements produced by `iterable`. */
const take = <T>(n: number, iterable: Iterable<T>): T[] => {
    const iterator = iterable[Symbol.iterator]();
    let elements: T[] = [];
    let counter = n;

    while (counter > 0) {
        const element = iterator.next();
        if (element.done) return elements;
        elements.push(element.value);
        counter--;
    }
    return elements;
};
console.log('take 4 numbers: %o', take(4, numbers));
console.log('take 0 numbers: %o', take(0, numbers));
console.log('take 100 numbers: %o', take(100, numbers));
console.log('take -1 numbers: %o', take(-1, numbers));

const range = function* (start: number = 0, end?: number) {
    let i = start;
    while (end != null ? i < end : true) {
        yield i++;
    }
};

const naturals = range();

console.log('first 5 naturals: %o', take(5, naturals));
console.log('first 0 naturals: %o', take(0, naturals));
console.log('first 100 naturals: %o', take(100, naturals));
console.log('first -1 naturals: %o', take(-1, naturals));

console.log('\n\ncomparing performance of linear and binary search:');
console.log('(worst case - no such element in the array)');

console.time('realizing 1e8 natural numbers');
const oneHundredMillion = take(1e8, naturals);
console.timeEnd('realizing 1e8 natural numbers');

console.time('linear search');
console.log(
    'found: %o',
    oneHundredMillion.find(element => element === -1)
);
console.timeEnd('linear search');

console.time('binary search');
console.log(
    'found: %o',
    recursiveBinarySearch(-1, oneHundredMillion, compare, true)
);
console.timeEnd('binary search');
