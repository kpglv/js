/** Types */

/**
 * Timeout chain.
 *
 * Allows to schedule a sequence of timeouts, executing some code
 * after each timeout ends.
 *
 * Can be in one of the two states: stopped and started; a new timeout chain is stopped.
 * Use `create` to create a timeout chain, `start` to start it, `stop` to stop it,
 * and `reset` to reset it to any timeout index. A chain with invalid current index
 * cannot be started.
 */
export interface TimeoutChain {
    /** Sequence of timeout durations (milliseconds) */
    timeouts: number[];
    /** Index of timeout duration that will be awaited next */
    currentTimeoutIndex: number;
    /** Is timeout chain started? */
    started: boolean;
    /** ID of the currently running timeout */
    timer?: ReturnType<typeof setTimeout>;
    /** Function that will be called when each timeout ends, with
        - index of the timeout that just ended
        - duration of the timeout that just ended
        - the timeout chain instance (with current timeout index updated and
          ready to start the next timeout) */
    onTimeoutEnd: TimeoutEventHandler;
}

/**
 * Timeout chain event handler.
 */
export type TimeoutEventHandler = (
    /** Index of the timeout */
    timeoutIndex: number,
    /** Duration of the timeout */
    timeoutDuration: number,
    /** Timeout chain instance */
    timeoutChain: TimeoutChain
) => void;

/** Public API */

/**
 * Returns a new timeout chain (not started, with given current timeout index).
 */
export const create = (
    timeouts: number[],
    currentTimeoutIndex: number,
    onTimeoutEnd: TimeoutEventHandler
): TimeoutChain => ({
    started: false,
    timeouts,
    currentTimeoutIndex,
    onTimeoutEnd,
});

/**
 * Returns the timeout chain in the started state.
 *
 * Invokes the step, until
 * - list of timeouts ends
 * - user stops the process
 */
export const start = async (
    timeoutChain: TimeoutChain
): Promise<TimeoutChain> => {
    timeoutChain.started = true;
    while (timeoutChain.started) {
        const { timeouts, currentTimeoutIndex } = timeoutChain;
        if (
            currentTimeoutIndex < 0 ||
            currentTimeoutIndex >= timeouts.length
        ) {
            timeoutChain.started = false;
            continue;
        }
        timeoutChain = await step(timeoutChain);
    }
    return timeoutChain;
};

/**
 * Returns the timeout chain in the stopped state.
 */
export const stop = (timeoutChain: TimeoutChain): TimeoutChain => {
    clearTimeout(timeoutChain.timer);
    timeoutChain.started = false;
    return timeoutChain;
};

/**
 * Returns the timeout chain with current timeout index reset to the given value (default 0).
 */
export const reset = (
    timeoutChain: TimeoutChain,
    currentTimeoutIndex: number = 0
) => {
    timeoutChain.currentTimeoutIndex = currentTimeoutIndex;
    return timeoutChain;
};

/** Implementation details */

/**
 * Executes one step of the timeout chain.
 *
 * Returns a promise that resolves to the timeout chain with updated state after the
 * timeout, whose duration is retrieved from the chain's timeouts list under the current
 * timeout index.
 * Starting with current timeout index:
 * - schedules a timeout with duration stored under the current index in the timeouts list
 *   (stopping any already scheduled timeouts beforehand)
 * - when timeout expires
 *   - invokes timeout end event handler
 *   - updates current timeout index
 *   - resolves promise timeout chain
 */
const step = async (timeoutChain: TimeoutChain): Promise<TimeoutChain> => {
    const { timeouts, currentTimeoutIndex, timer, onTimeoutEnd } =
        timeoutChain;
    const timeoutValue = timeouts[currentTimeoutIndex];
    return new Promise(resolve => {
        if (timer) clearTimeout(timer);
        timeoutChain.timer = setTimeout(() => {
            timeoutChain.currentTimeoutIndex++;
            onTimeoutEnd(currentTimeoutIndex, timeoutValue, timeoutChain);
            resolve(timeoutChain);
        }, timeoutValue);
    });
};

/** Usage examples (e.g., `bun timeout-chain.ts` ) */

const chain = create(
    [0, 100, 500, 1000, 2000, 100],
    0,
    (index, timeout, timeoutChain) => {
        console.log('timeout #%o ended, was %o ms', index, timeout);
    }
);
console.log('* created chain: %o', chain);
await start(chain);
console.log('* finished chain: %o', chain);

const stoppableChain = create(
    [0, 100, 2100, 2200, 2300],
    0,
    (index, timeout, timeoutChain) => {
        console.log(
            '(stoppable) timeout #%o ended, was %o ms',
            index,
            timeout
        );
        // after 3rd timeout end, stop the chain
        if (index === 2) {
            console.log('(stoppable) stopping the chain');
            stop(timeoutChain);
        }
    }
);
await start(stoppableChain);
console.log('(stoppable) resuming the chain');
await start(stoppableChain);

const resettableChain = create(
    [10, 11, 12, 13, 14, 15, 16],
    // start from 4th timeout
    3,
    (index, timeout, timeoutChain) => {
        console.log(
            '(resettable) timeout #%o ended, was %o ms',
            index,
            timeout
        );
        // after 5th timeout finished, reset the chain to 2nd timeout
        if (index === 4) {
            console.log('(resettable) resetting the chain to #1');
            // a bug - 2nd timeout will be omitted
            reset(timeoutChain, 1);
        }
        // and stop after the 3rd timeout
        if (index === 2) {
            console.log('(resettable) stopping the chain');
            stop(timeoutChain);
        }
    }
);
await start(resettableChain);

const invalidChain1 = create(
    [0, 100, 500, 1000, 2000, 100],
    // current timeout index out of bounds
    -8,
    (index, timeout, timeoutChain) => {
        console.log(
            '(invalid index 1) timeout #%o ended, was %o ms',
            index,
            timeout
        );
    }
);
await start(invalidChain1);

const invalidChain2 = create(
    [0, 100, 500, 1000, 2000, 100],
    // current timeout index out of bounds
    12,
    (index, timeout, timeoutChain) => {
        console.log(
            '(invalid index 2) timeout #%o ended, was %o ms',
            index,
            timeout
        );
    }
);
await start(invalidChain2);
