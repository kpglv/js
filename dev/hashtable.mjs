// https://www.freecodecamp.org/news/javascript-hash-table-associative-array-hashing-in-js/


// a simple hash table implementation with fixed number of buckets and string
// keys


/**
 * A simple hash table implementation. Uses chaining to resolve hash collisions.
 */
class HashTable {
  constructor() {
    this.table = new Array(4);
    this.size = 0;
  }

  /**
   * Retrieves a value under the key, returns the default value if there is no
   * such key, undefined if no default value provided.
   * @param key
   * @param notFound default value
   * @returns value
   */
  get(key, notFound) {
    const index = this.hash(key);
    const chain = this.table[index];
    if (chain && chain.length) {
      const foundEntry = chain.find(entry => entry[0] === key);
      if (foundEntry) {
        return foundEntry[1];
      }
    }
    return notFound;
  }

  /**
   * Puts the value under the key, replacing the old value if it already
   * exists.
   * @param key
   * @param value
   */
  set(key, value) {
    const index = this.hash(key);
    const chain = this.table[index];
    if (chain && chain.length) {
      // there are entries under this index (entries chain is present and not
      // empty)
      for (let i = 0; i < chain.length; i++) {
        // find the entry with given key in the chain
        if (chain[i] && chain[i][0] === key) {
          // the entry is found, update its value
          this.table[index][i][1] = value;
          return;
        }
      }
      // no existing entry found, insert a new one
      this.table[index].push([key, value]);
    } else {
      // no entries under this index (entries chain is absent or empty);
      // create the chain and insert a new entry
      this.table[index] = [];
      this.table[index].push([key, value]);
    }
    this.size++;
  }

  /**
   * Removes the key and the associated value. Returns true if the key did
   * exist and was removed.
   * @param key
   */
  remove(key) {
    const index = this.hash(key);
    const chain = this.table[index];
    if (chain && chain.length) {
      for (let i = 0; i < chain.length; i++) {
        if (chain[i] && chain[i][0] === key) {
          // if the entry with given key is found, delete it from the chain
          this.table[index].splice(i, 1);
          this.size--;
          return true;
        }
      }
    }
    return false;
  }

  hash(key) {
    let hash = 0;
    for (let i = 0; i < key.length; i++) {
      hash += key.charCodeAt(i);
    }
    return hash % this.table.length;
  }
}


const h = new HashTable();
h.set('Canada', 300);
h.set('France', 100);
h.set('Spain', 110);
console.log('hash table:', JSON.stringify(h, null, 2));

console.log('get a value:', h.get('Canada'));
console.log('remove a value', h.remove('Canada'));
console.log('remove already removed value', h.remove('Canada'));
console.log('hash table:', JSON.stringify(h, null, 2));

h.set('Canada', 400);
console.log('hash table:', JSON.stringify(h, null, 2));
