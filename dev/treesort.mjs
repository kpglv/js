export const naturalSortIds = tree => {
    if (!tree.children) {
        return [...tree.id];
    } else {
        return [...tree.id, ...tree.children.flatMap(naturalSort)];
    }
};

export const naturalSort = tree => {
    if (!tree.children) {
        return [tree];
    } else {
        return [tree, ...tree.children.flatMap(naturalSort)];
    }
};

// usage examples

const tree = {
    id: 'a',
    children: [
        { id: 'b', children: [{ id: 'c' }, { id: 'd' }] },
        { id: 'e' },
        { id: 'f', children: [{ id: 'g' }] },
        { id: 'h' },
    ],
};

console.log(naturalSortIds(tree));
console.log(naturalSort(tree));
