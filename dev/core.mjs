// TODO: use multimethods (at least for composite data structures)?

// TODO: need a way to optimize tail-recursive calls
// (to avoid consuming the stack and stack overflows)

// functions

/**
 * Returns its argument.
 */
export const identity = x => x;

/**
 * Applies f to the collection of arguments, as if it was
 * the argument list of f (spreading the collection).
 */
export const apply = (f, args) => f(...args);

/**
 * Partially applies f to args --- returns a function that,
 * when called, places these args before any other arguments supplied
 * on call.
 */
export const partial =
    (f, ...args) =>
    (...more) =>
        f(...args, ...more);

/**
 * Returns a function that complements the predicate --- returns
 * the opposite boolean value when called.
 */
export const complement =
    pred =>
    (...args) =>
        !pred(...args);

/**
 * Provides a default first argument for f if the first argument is absent,
 * undefined or null.
 */
export const fnil =
    (f, x) =>
    (...[first, ...rest]) =>
        first == null ? f(x, ...rest) : f(first, ...rest);

// numbers

export const isNumber = x => typeof x === 'number';
export const isInteger = x => isNumber(x) && Number.isInteger(x);
export const isZero = n => n === 0;
export const isPos = n => n > 0;
export const isNat = n => isZero(n) || isPos(n);
export const isNeg = complement(isNat);
export const isOdd = n => n % 2 != 0;
export const isEven = complement(isOdd);

export const inc = n => n + 1;
export const dec = n => n - 1;
export const add = (...args) => {
    if (isEmpty(args)) {
        return 0;
    } else {
        return first(args) + add(...rest(args));
    }
};
export const multiply = (...args) => {
    if (isEmpty(args)) {
        return 1;
    } else {
        return first(args) * multiply(...rest(args));
    }
};

// collections

export const isCollection = x => isSequential(x) || isAssociative(x);

// sequential

export const isArray = Array.isArray;
export const isSequential = isArray;

export const isEmpty = coll => !coll || coll.length === 0;
export const cons = (x, coll) => [x, ...coll];
export const conj = (x, coll) => [...coll, x];
export const first = coll => coll[0];
export const rest = coll => coll.slice(1);
export const last = coll => coll[coll.length - 1];

export const reduce = (f, init, coll) =>
    coll.reduce((acc, val) => f(acc, val), init);
// TODO: support variable number of colls
export const map = (f, coll) =>
    reduce((acc, val) => conj(f(val), acc), [], coll);
export const filter = (pred, coll) =>
    reduce((acc, val) => (pred(val) ? conj(val, acc) : acc), [], coll);

export const take = (n, coll) => {
    if (isZero(n) || isNeg(n) || isEmpty(coll)) {
        return [];
    } else {
        return cons(first(coll), take(dec(n), rest(coll)));
    }
};
export const drop = (n, coll) => {
    if (isZero(n) || isNeg(n) || isEmpty(n)) {
        return coll;
    } else {
        return drop(dec(n), rest(coll));
    }
};
export const partition = (n, coll) =>
    isZero(n) || isNeg(n) || isEmpty(coll)
        ? coll
        : cons(take(n, coll), partition(n, drop(n, coll)));
// TODO: requires map that supports several colls
export const zip = (ks, vs) => {};
// FIXME: support maps and sets, preserve coll type
export const into = (coll, xs) => {
    if (isSequential(coll)) {
        return [...coll, ...xs];
    } else if (isAssociative(coll)) {
        return { ...coll, ...Object.fromEntries(xs) };
    } else {
        throw new Error("unknown coll type passed to 'into'");
    }
};

// associative

export const isSet = x => x instanceof Set;
export const isMap = x => x instanceof Map;
export const isObject = x => typeof x === 'object' && !!x;
export const isAssociative = x =>
    isSet(x) || isMap(x) || isObject(x) || isArray(x);

// usage examples

console.log('\n===== functions =====');
console.log('complement of pos returns true for -1:', complement(isPos)(-1));
const foo = partial(identity, 'foo');
console.log(
    'partially applied identity always returns the supplied value:',
    foo(1),
    foo(2),
    foo()
);
console.log('applying add to [1, 2, 3]:', apply(add, [1, 2, 3]));
console.log(
    'nil-patched add returns supplied arg when called without args:',
    fnil(add, 123)()
);
console.log(
    'nil-patched add returns supplied arg when called with undefined first arg:',
    fnil(add, 1)(undefined, 2, 3)
);

console.log('\n===== numbers =====');
console.log('decreasing 12:', dec(12));
console.log('increasing 12:', inc(12));
console.log('12 is positive?', isPos(12));
console.log('12 is negative?', isNeg(12));
console.log('adding zero arguments:', add());
console.log('adding 10, 20 and 30:', add(10, 20, 30));
console.log('multiplying zero arguments:', multiply());
console.log('multiplying 10, 20 and 30:', multiply(10, 20, 30));

console.log('\n===== sequential collections =====');
const coll = [1, 2, 3, 'foo'];
console.log('coll:', coll);
console.log('consing 12 on the empty coll:', cons(12, []));
console.log('consing 12 on the coll:', cons(12, coll));
console.log('conjoining 12 on the empty coll:', conj(12, []));
console.log('conjoining 12 on the coll:', conj(12, coll));
console.log('first of the empty coll:', first([]));
console.log('first of the coll:', first(coll));
console.log('rest of the empty coll:', rest([]));
console.log('rest of the coll:', rest(coll));
console.log('last of the empty coll:', last([]));
console.log('last of the coll:', last(coll));

console.log('reducing [1, 2, 3] with add:', reduce(add, 0, [1, 2, 3]));
console.log('mapping [1, 2, 3] with inc:', map(inc, [1, 2, 3]));
console.log('filtering [1, 2, 3] with isOdd:', filter(isOdd, [1, 2, 3]));
console.log('taking 3 elements from the empty coll:', take(3, []));
console.log('taking 0 elements from the coll:', take(0, coll));
console.log('taking -1 elements from the coll:', take(-1, coll));
console.log('taking 3 elements from the coll:', take(3, coll));
console.log('taking 7 elements from the coll:', take(7, coll));
console.log('dropping 3 elements from the empty coll:', drop(3, []));
console.log('dropping 0 elements from the coll:', drop(0, coll));
console.log('dropping -1 elements from the coll:', drop(-1, coll));
console.log('dropping 3 elements from the coll:', drop(3, coll));
console.log('dropping 7 elements from the coll:', drop(7, coll));
// 10k blows the stack without TCE
// 100k blows the heap without --max-old-space-size=8192 node arg
// console.log('dropping 10k elements from a coll:', drop(10000, Array(10000)));
console.log('partitioning coll by 0:', partition(0, coll));
console.log('partitioning coll by -1:', partition(-1, coll));
console.log('partitioning coll by 1:', partition(1, coll));
console.log('partitioning coll by 2:', partition(2, coll));
console.log('partitioning coll by 3:', partition(3, coll));
console.log('partitioning coll by 4:', partition(4, coll));
console.log('partitioning coll by 5:', partition(5, coll));
console.log(
    'placing contents of [1, 2, 3] into an empty array:',
    into([], [1, 2, 3])
);
console.log(
    'placing contents of [1, 2, 3] into the coll:',
    into(coll, [1, 2, 3])
);
console.log(
    "placing contents of [['name', 'Joe'], ['age', 42]] into an empty object:",
    into({}, [
        ['name', 'Joe'],
        ['age', 42],
    ])
);
console.log(
    "placing contents of [['name', 'Joe'], ['age', 42]] into an {id: 1}:",
    into({ id: 1 }, [
        ['name', 'Joe'],
        ['age', 42],
    ])
);
