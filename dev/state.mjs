// https://clojure.org/reference/atoms

class Atom {
  #value;
  #validator;
  #watchers;

  constructor(initialValue) {
    this.#value = initialValue;
    this.#watchers = [];
  }

  /**
   * Returns the current value of the atom.
   */
  deref() {
    return this.#value;
  }

  /**
   * Resets the value of the atom to the new value. Returns the new value.
   * @param next the new value
   */
  reset(next) {
    // validator should throw on validation failure
    this.#validator && this.#validator(next);
    // probably could be implemented in terms of 'swap', but will be more
    // complex and less efficient (additional function should be created and
    // called to produce the next value); also, just an 'atomicSet' will
    // probably suffice, as it does not matter what exact value atom had had
    // before 'reset'
    while(true) {
      // take snapshot of the state
      const previous = this.#value;
      // try to reset the value
      if (!this.#atomicCompareAndSet(this.#value, previous, next)) {
        // retry the operation on failure (not likely, but still could happen
        // if JS was multithreaded)
        continue;
      }
      // return the new value on success
      return next;
    }
  }

  /**
   * Swaps the value of the atom with the result of calling fn with the
   * snapshot of the current value of the atom as it's first argument and args
   * as additional arguments. Will be retried if the current value of the atom
   * changes during the operation, so fn should not have side-effects. Returns
   * the new value.
   * @param fn function that produces new value
   * @param ...args additional arguments for the function
   */
  swap(fn, ...args) {
    while(true) {
      // take snapshot of the state
      const previous = this.#value;
      // compute the next value of the state
      const next = fn(previous, ...args);
      // validate the next value; validator should throw on validation failure
      this.#validator && this.#validator(next);
      // try to reset the value
      if (!this.#atomicCompareAndSet(this.#value, previous, next)) {
        // retry the operation on failure
        continue;
      }
      // run watchers
      this.#watchers.forEach(watcher => watcher(previous, next));
      // return the new value on success
      return next;
    }
  }

  validator(fn) {
    this.#validator = fn;
  }

  addWatcher(fn) {
    this.#watchers.push(fn);
  }

  removeWatcher(fn) {
    this.#watchers = this.#watchers.filter(watcher => watcher !== fn);
  }

  /**
   * Sets the current value of the atom to the next value if the current value
   * does not differ from the previous value.
   *
   * Should have CAS (Compare-And-Swap) semantics. Usually there is no need
   * for this in JS as it is single-threaded, but atomic operations are
   * available for shared buffers.
   * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Atomics
   * @param current the current value
   * @param previous the previous value
   * @param next the next value
   * @returns true on success
   */
  #atomicCompareAndSet(current, previous, next) {
    // atom values should be immutable for this to work as intended; will not
    // detect in-place mutations
    if (current === previous) {
      this.#value = next;
      return true;
    } else {
      return false;
    }
  }
}


const a = new Atom({});

// basic operations
console.log("new atom:", a, ", value:", a.deref());
a.reset({id: 1});
console.log("reset atom:", a.deref());
a.swap((state, y) => ({...state, name: "Joe"}));
console.log("swapped atom:", a.deref());
a.swap((state, y) => ({...state, id: state.id + y}), 12);
console.log("swapped atom:", a.deref());

// validation
a.validator(value => {
  if (!value.id) {
    throw new Error("id is missing");
  }
});
try {
  a.reset(0);
} catch (e) {
  console.log('validation error:', e.message);
}
console.log('validation prevented reset:', a.deref());

// watching
const watcher = (previous, next) => {
  const p = JSON.stringify(previous);
  const n = JSON.stringify(next);
  console.log(`atom state changed: ${p} => ${n}`);
};
a.addWatcher(watcher);
a.swap((state) => ({...state, id: state.id * state.id}));
a.removeWatcher(watcher);
a.swap((state) => ({...state, id: state.id * state.id}));
console.log("state changed without watchers:", a.deref());
