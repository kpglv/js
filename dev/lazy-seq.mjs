import { invokedDirectly } from './helpers.mjs';
import { delay, force, nil } from './lazy.mjs';

// lazy sequences

// a sequence is either
// - an empty sequence (empty)
// - a pair of a value (first) and a sequence (rest)

// a lazy sequence is a sequence where values are
// delayed computations

const empty = [];
const isEmpty = seq => force(seq) === empty;
const cons = (x, seq) => [x, ...seq];
const first = seq => force(seq)[0];
const rest = seq => force(seq)[1];

// create a lazy sequence
const lazy = fn => {
    return (...args) => {
        return delay(fn, ...args);
    };
};

// defining lazy sequences

const repeat = lazy(x => [x, repeat(x)]);

const iterate = lazy((f, x) => [x, iterate(f, f(x))]);

const take = (n, seq) => {
    if (n === 0 || isEmpty(seq)) {
        return empty;
    } else {
        return cons(first(seq), take(n - 1, rest(seq)));
    }
};

// usage examples

if (invokedDirectly(import.meta.url)) {
    const ones = repeat(1);
    console.log('take 0 ones:', take(0, ones));
    console.log('take 3 ones:', take(3, ones));

    // const repeat1 = x => cons(x, repeat1(x));
    // const twos = repeat1(2);
    // console.log('take 0 twos:', take(0, twos));
    // console.log('take 3 twos:', take(3, twos));

    const naturals = iterate(x => x + 1, 0);
    console.log('take 5 naturals:', take(5, naturals));
}
