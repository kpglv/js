import { v4 as uuidv4 } from 'uuid';

// coeffect handler is a function that takes a coeffect map and an event,
// and returns a new coeffect map, usually with some coeffect added to it;
// maybe can side-effect?

export const uuid = (cofx, _) => ({ ...cofx, uuid: uuidv4() });
export const traceEvent = (cofx, event) => {
    console.log('trace: handling event:', event);
    return cofx;
};
export const traceCoeffects = (cofx, _) => {
    console.log('trace: injected coeffects:', cofx);
    return cofx;
};

// usage examples

// console.log('injecting UUID coeffect:', uuid({}));
