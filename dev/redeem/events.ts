// event handler is a function from [coeffects-map event-map] to effects-map

export const hello = ({ uuid }, { message }) => ({
    type: 'effect',
    name: 'log',
    id: uuid,
    message,
});

// usage examples

// console.log('handling the hello event:', hello({}, { message: 'HAYO!' }));
