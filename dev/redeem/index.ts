import { createPipeline, createDispatch } from './core';

// usage examples

import { traceCoeffects, traceEvent, uuid } from './coeffects.js';
import { hello } from './events.js';
import { log } from './effects.js';

const pipeline = createPipeline({
    events: {
        hello: {
            handler: hello,
            coeffects: [traceEvent, uuid, traceCoeffects],
        },
    },
    effects: {
        log,
    },
});

const dispatch = createDispatch(pipeline);

dispatch({ type: 'event', name: 'foo' });
dispatch({ type: 'event', name: 'hello', message: 'Hello redeem!' });
dispatch({ type: 'event', name: 'hello', message: 'Hello again!' });
