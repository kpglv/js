// effect handler is a side-effecting function that takes an effect and
// whose return value is ignored

export const log = ({ id, message }) =>
    console.log(`request ${id}: ${message}`);

// usage examples

// console.log('handling the log effect:');
// log(hello(uuid({}), { message: 'HAYO!' }));
