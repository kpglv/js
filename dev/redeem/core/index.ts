// returns an event/effect/coeffect handling pipeline

export const createPipeline = config => config;

// returns an event dispatcher

export const createDispatch = pipeline => eventMap => {
    if (!pipeline.events[eventMap.name]) {
        console.log('no handler for event:', eventMap.name);
        return;
    }

    // inject coeffects
    const { coeffects } = pipeline.events[eventMap.name];
    const coeffectsMap = coeffects.reduce(
        (acc, coeffect) => coeffect(acc, eventMap),
        {}
    );

    // handle event
    const eventHandler = pipeline.events[eventMap.name].handler;
    const effectMap = eventHandler(coeffectsMap, eventMap);

    // apply effects
    const effectHandler = pipeline.effects[effectMap.name];
    effectHandler(effectMap);
};
