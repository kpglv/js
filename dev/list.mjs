const list = [
    { id: 'a' },
    [{ id: 'b' }, { id: 'c' }],
    [{ id: 'd' }],
    [{ id: 'e' }, [{ id: 'f' }, [{ id: 'g' }, [{ id: 'h' }]]]],
];

const isList = Array.IsArray;
const isAtom = x => !isList(x);
const isEmpty = list => first(list) === nil;
const first = list => list[0];
const rest = list => list.slice(1);
const cons = (element, list) => [element, list];
const nil = Symbol('nil');

console.log('list:', list);
console.log('first:', first(list));
console.log('rest', rest(list));

const find = (list, elementId) => {
    if (isEmpty(list)) {
        return;
    } else if (isAtom(list)) {
    } else if (first(list).id === elementId) {
        return first(list);
    } else {
        return find(rest(list), elementId);
    }
};

console.log('find "g":', find(list, 'g'));
