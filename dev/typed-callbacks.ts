/* concrete callbacks */

const foo = (n: number) => console.log(`n is ${n}`);
interface Obj {
    id: number;
    name: string;
    children?: unknown[];
}
const quux = (o: Obj) => console.log('o is', o);

/* typed functions that accept the callbacks */

// no-no --- it kinda works, but 'Function' is not type-safe; no better than 'any'
const bar = (fn: Function, ...args: unknown[]) => fn(...args);
bar(foo, 1);
bar(quux, { id: 1, name: 'Joe' });

// better; one argument for the callback is enough --- it can always be an object
// (interface) type that can contain arbitrary number of properties of arbitrary
// (different) types, and is easily typed (as compared to usual function's list of
// arguments)
const baz = <T>(fn: (x: T) => void, arg: T) => fn(arg);
baz(foo, 1);
baz(quux, { id: 1, name: 'Joe', children: ['no children'] });
baz(
    (x: unknown) =>
        console.log('any function of one argument will do, arg is:', x),
    'frob'
);

// run the code: bun typed-callbacks.ts
