import { BufferedReader } from './streams.mjs';

export class Token {}

export class Tokenizer {
    #reader;
    #tokens;

    constructor(source) {
        this.#tokens = [];
        this.#reader = new BufferedReader(source);
    }

    get tokens() {
        return this.#tokens;
    }

    async next() {
        const rune = await this.#reader.read();
        return rune;
    }
    async peek() {}

    async tokenize() {}

    addToken(token) {
        this.#tokens.push(token);
    }
}

const r = new BufferedReader('hello');
while (true) {
    const rune = await r.read();
    if (rune == null) {
        break;
    }
    console.log('rune:', rune);
}

// const t = new Tokenizer('𖩉\nhello\nё');
const t = new Tokenizer(process.stdin);

while (true) {
    const rune = await t.next();
    if (rune == null) {
        break;
    }
    t.addToken(rune);
}
console.log(t.tokens);
