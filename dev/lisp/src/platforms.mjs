/**
 * Returns true if the script is running on the client (in a browser).
 * @returns boolean
 */
export const isClient = () => typeof window !== 'undefined';

/**
 * Returns true if the script is running on the server (e.g., Node.js).
 */
export const isServer = () => !isClient();

/**
 * Returns true if the platform the script is running on
 * implements WHATWG streams (web streams).
 */
export const hasWebStreams = () => {
    return typeof ReadableStream !== 'undefined' && typeof WritableStream !== 'undefined';
};
