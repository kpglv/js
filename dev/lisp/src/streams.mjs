import { hasWebStreams } from './platforms.mjs';

export class BufferedReader {
    #reader;
    #buffer;

    constructor(source) {
        this.#reader = this.#toWebReadableStream(source).getReader();
        this.#buffer = [];
    }

    /**
     * Returns a rune (string containing one Unicode character) from the
     * buffer, reading a next chunk of them from the underlying source to
     * the buffer first if needed.
     */
    async read() {
        if (this.#buffer.length === 0) {
            const chunk = await this.#reader.read();
            if (chunk.value == null) {
                return null;
            } else {
                const chunkString = chunk.value.toString();
                this.#buffer = this.#buffer.concat(...chunkString);
            }
        }
        return this.#buffer.shift();
    }

    /**
     * Returns a WHATWG ReadableStream (web stream) created from
     * the given source. If the source is already a ReadableStream,
     * returns it.
     * @see https://streams.spec.whatwg.org/
     * @param source something that may be converted to a readable stream
     *  (Node.js Readable, string)
     * @returns a ReadableStream instance
     */
    #toWebReadableStream(source) {
        if (!hasWebStreams()) {
            throw 'buffered reader: web streams are not supported';
        }

        if (this.#isReadableWebStream(source)) {
            return source;
        } else if (this.#isReadableNodeStream(source)) {
            source.setEncoding('utf-8');
            return ReadableStream.from(source);
        } else if (typeof source === 'string') {
            return ReadableStream.from([source]);
        } else {
            throw 'buffered reader: unknown source type';
        }
    }

    /**
     * Returns true if the object is a ReadableStream.
     * @param {any} object
     */
    #isReadableWebStream(object) {
        return typeof object.getReader === 'function';
    }

    /**
     * Returns true if the object is a Node.js Readable.
     * @param {any} object
     */
    #isReadableNodeStream(object) {
        return typeof object.read === 'function';
    }
}
