:PROPERTIES:
:ID:       57f224a2-d37b-4abf-ba40-4fe789fbad75
:END:
#+title: lisp
#+filetags: :task:

* Tags

  - [[id:64753faf-1bc6-4dd0-8cbc-3df56de6be1e][index]]

* Implement a Lisp interpreter
** Description

   Besides learning and practice, it can be used to implement an extensible
   data format (e.g., JSON with tagged literal support).

   Should run at least on Node and in browsers; should use Web standards
   (e.g., for streams), avoid relying on nonstandard APIs.

   Should reuse data types/structures already available in JS (primitive and
   composite); may add some additional data types (like keywords).

** Problem

   Unmet user objectives and causes of that

** Approach

   The selected approach to the solution

** Log

   The log of the design process --- understanding of the problem, selecting
   an approach; with use cases, decision matrices, diagrams, etc
