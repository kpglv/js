export type Fn<Args extends unknown[], ReturnValue> = (
    ...args: Args
) => ReturnValue;

export type TimeoutType = ReturnType<typeof setTimeout>;
export type Throttled = 'throttled';

/**
 * Returns a function that calls `fn` when invoked, with the same arguments.
 * Invocation attempts will be ignored until `delay` milliseconds passed after the last one.
 * FIXME: it now works quite differently from void-returning version (almost identical to `defer`)
 */
export const debounce = <Args extends unknown[], ReturnValue>(
    fn: Fn<Args, ReturnValue>,
    delay: number
): Fn<Args, Promise<ReturnValue>> => {
    let timer: TimeoutType | undefined;
    return (...args) =>
        new Promise(resolve => {
            clearTimeout(timer);
            timer = setTimeout(() => {
                resolve(fn(...args));
            }, delay);
        });
};

/**
 *
 * Returns a function that calls `fn` with the same arguments when invoked, and returns
 * - its return value, if the call was successful
 * - `throttled`, if the call was throttled.
 * Invocation attempts will be successful only once in `delay` milliseconds.
 */
export const throttle = <Args extends unknown[], ReturnValue>(
    fn: Fn<Args, ReturnValue>,
    delay: number
): Fn<Args, ReturnValue | Throttled> => {
    let timer: TimeoutType | null = null;
    return (...args) => {
        if (timer === null) {
            timer = setTimeout(() => {
                timer = null;
            }, delay);
            return fn(...args);
        } else {
            return 'throttled';
        }
    };
};

/**
 * Returns a promise that will be resolved after `delay` msec with `undefined`.
 */
const delay = (delay: number): Promise<void> =>
    new Promise(resolve => setTimeout(() => resolve(), delay));

/**
 * Returns a function that returns a promise that will be resolved after `delay` msec
 * with return value of the `fn` called with the same arguments.
 */
const defer =
    <Args extends unknown[], ReturnValue>(
        fn: Fn<Args, ReturnValue>,
        delay: number
    ): Fn<Args, Promise<ReturnValue>> =>
    (...args) =>
        new Promise(resolve => setTimeout(() => resolve(fn(...args)), delay));

// usage examples

const say = (
    id: number,
    name: string,
    properties: Record<string, unknown>,
    iteration: number
) =>
    `id ${id}, name ${name}, has ${
        Object.keys(properties).length
    } properties: ${Object.keys(
        properties
    )}, invoked after iteration ${iteration}`;

console.time('delayed say');
await delay(1000);
console.timeEnd('delayed say');
console.log(
    'delayed say:',
    say(1, 'Eddie', { eyes: 'blue', height: 179 }, NaN)
);

const deferredSay = defer(say, 1000);

console.time('deferred say');
const message = await deferredSay(
    1,
    'Eddie',
    { eyes: 'blue', height: 179 },
    NaN
);
console.timeEnd('deferred say');
console.log('deferred say:', message);

const debouncedSay = debounce(say, 300);
const throttledSay = throttle(say, 50);

const iterations = 10;

console.log('calling rate-limited functions %o times:', iterations);
for (let i = 0; i < iterations; i++) {
    const message = await debouncedSay(2, 'Susanna', { age: 36 }, i + 1);
    console.log('debounced say:', message);
    await delay(10);
}

for (let i = 0; i < iterations; i++) {
    const message = throttledSay(
        3,
        'Mary',
        { age: 33, children: 1, cats: 12 },
        i + 1
    );
    await delay(10);
    console.log('throttled say:', message);
}
