const settings = {
    readTimeout: 1000,
};

const setReadTimeout = console.log;
const setConnectTimeout = console.log;

const { readTimeout, connectTimeout } = settings;
readTimeout && setReadTimeout(readTimeout);
connectTimeout && setConnectTimeout(connectTimeout);
