import { multi } from '@kpglv/multimethods';


// TODO:
// - more meaningful error messages
// - protocol/function documentation (latter should be implemented in
//   multimethods)
// - protocol introspection (list of functions, their implementations etc)
// - support ad-hoc hierarchies of dispatch values (maybe should be
//   implemented in multimethods)
// - move to components


/**
 * Returns a protocol --- a set of named functions; the functions are
 * multimethods with common dispatch function.
 */
export const protocol = (dispatch, functionNames) => {
    const protocol = {};
    Object.entries(functionNames).forEach(([name, docstring]) => {
        protocol[name] = multi(dispatch);
        protocol[name].doc = docstring;
    });
    return protocol;
};


/**
 * Provides implementations of the protocol's functions for given dispatch
 * value.
 */
export const implement = (protocolInstance, dispatchValue, implementations) => {
    Object.entries(implementations).forEach(([name, implementation]) => {
        protocolInstance[name].when(dispatchValue, implementation);
    });
};


// examples

const Shape = protocol(x => x.shape, {
    square: "Returns square of the shape.",
    description: "Returns description of the shape."
});
console.log('Shape protocol:', Shape);

implement(Shape, 'rectangle', {
    square: (self) => self.a * self.b,
    description: (self) => `a ${self.a} by ${self.b} rectangle`,
});

implement(Shape, 'circle', {
    square: (self) => 2 * Math.PI * self.r,
    description: (self) => `a circle with radius ${self.r}`,
});

const aRect = { shape: 'rectangle', a: 2, b: 12 };
const aCircle = { shape: 'circle', r: 5 };
const anOctagon = { shape: 'octagon', side: 12 };

// protocol is implemented properly
console.log(
    `rectangle: ${Shape.description(aRect)}, square ${Shape.square(aRect)}`
);
console.log(
    `circle: ${Shape.description(aCircle)}, square ${Shape.square(aCircle)}`
);

// protocol is not implemented
try {
    console.log(
        'octagon:',
        Shape.square(anOctagon),
        Shape.description(anOctagon)
    );
} catch (e) {
    console.log('error:', e.message);
}

// the method is absent in protocol
try {
    console.log(
        'circle:',
        Shape.foo(aCircle)
    );
} catch (e) {
    console.log('error:', e.message);
}

// partially implemented protocol
implement(Shape, 'octagon', {
    description: (self) => `an octagon with side ${self.side}`,
});
console.log('octagon:', Shape.description(anOctagon));
try {
    console.log(`octagon: ${Shape.square(anOctagon)}`);
} catch (e) {
    console.log('error:', e.message);
}
