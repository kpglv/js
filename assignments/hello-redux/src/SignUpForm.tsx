import React from "react";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Tooltip from "@material-ui/core/Tooltip";
import Snackbar from "@material-ui/core/Snackbar";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";

import {
  PasswordHintProps,
  SignUpFormProps,
  SignUpFormState
} from "./modules/types";
import {
  validateEmail,
  validatePassword,
  validatePasswordConfirmation,
  register,
  resetEmail,
  resetPassword,
  resetPasswordConfirmation,
  resetRegistration
} from "./store/actions";
import "./SignUpForm.sass";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      Copyright © {new Date().getFullYear()} Пигалев Константин
    </Typography>
  );
}

const PasswordHint: React.FC<PasswordHintProps> = props => {
  return (
    <Tooltip
      className="sign-up-form__password-tooltip"
      title={<Typography variant="body1">{props.text}</Typography>}
      placement="top-end"
      arrow
      disableFocusListener
    >
      <InputAdornment position="end">
        <HelpOutlineIcon></HelpOutlineIcon>
      </InputAdornment>
    </Tooltip>
  );
};

const SignUpForm: React.FC<SignUpFormProps> = props => {
  function handleInput(event: any) {
    let eventState = {
      key: event.key,
      name: event.target?.name,
      value: event.target?.value
    };
    switch (eventState.name) {
      case "email":
        props.validateEmail(eventState.value);
        break;
      case "password":
        props.validatePassword(eventState.value);
        props.resetPasswordConfirmation();
        break;
      case "password-confirmation":
        props.validatePasswordConfirmation(
          props.state.password.value,
          eventState.value
        );
        break;
      default:
        console.debug("unknown input field, can't validate");
    }
  }

  function handleRegistration() {
    let isEmailValid = props.state.email.valid;
    let isPasswordValid = props.state.password.valid;
    let isPasswordConfirmationValid = props.state.passwordConfirmation.valid;
    console.debug("registering");
    props.register(isEmailValid, isPasswordValid, isPasswordConfirmationValid);
    resetState();
  }

  function isRegistrationPossible() {
    return (
      props.state.email.valid &&
      props.state.password.valid &&
      props.state.passwordConfirmation.valid
    );
  }

  function resetState() {
    props.resetEmail();
    props.resetPassword();
    props.resetPasswordConfirmation();
    setTimeout(props.resetRegistration, 5000);
  }

  return (
    <Container className="sign-up-form" component="main" maxWidth="xs">
      <CssBaseline />
      <div>
        <Typography component="h1" variant="h5">
          Регистрация
        </Typography>
        <form noValidate>
          <TextField
            className="sign-up-form__email"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Эл. почта"
            name="email"
            autoComplete="email"
            autoFocus
            value={props.state.email.value}
            error={!!props.state.email.errorMessage}
            helperText={props.state.email.errorMessage}
            onChange={handleInput}
          />
          <TextField
            className="sign-up-form__password"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Выберите пароль"
            type="password"
            id="password"
            autoComplete="current-password"
            InputProps={{
              endAdornment: (
                <PasswordHint
                  className="sign-up-form__password-tooltip"
                  text={"Пароль должен быть не короче 6 символов"}
                ></PasswordHint>
              )
            }}
            value={props.state.password.value}
            error={!!props.state.password.errorMessage}
            helperText={props.state.password.errorMessage}
            onChange={handleInput}
          />
          <TextField
            className="sign-up-form__password-confirmation"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password-confirmation"
            label="Подтвердите пароль"
            type="password"
            id="password-confirmation"
            autoComplete="current-password"
            InputProps={{
              endAdornment: (
                <PasswordHint
                  text={"Пароль и его подтверждение должны совпадать"}
                ></PasswordHint>
              )
            }}
            value={props.state.passwordConfirmation.value}
            error={!!props.state.passwordConfirmation.errorMessage}
            helperText={props.state.passwordConfirmation.errorMessage}
            onChange={handleInput}
          />
          <Box className="sign-up-form__actions">
            <Button
              className="sign-up-form__submit-button"
              fullWidth
              variant="contained"
              color="primary"
              disabled={!isRegistrationPossible()}
              onClick={handleRegistration}
            >
              Зарегистрироваться
            </Button>
          </Box>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
      <Snackbar
        open={props.state.registration.success}
        message="Вы успешно зарегистрировались! Еще раз?"
        autoHideDuration={6000}
      ></Snackbar>
    </Container>
  );
};

const mapStateToProps = (state: SignUpFormState) => {
  return { state };
};

const mapDispatchToProps = {
  validateEmail,
  validatePassword,
  validatePasswordConfirmation,
  register,
  resetEmail,
  resetPassword,
  resetPasswordConfirmation,
  resetRegistration
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUpForm);
