import * as ActionTypes from "../actions/types";
import { canRegister } from "../../modules/validators";
import { RegistrationState, SignUpAction } from "../../modules/types";

const initialState: RegistrationState = { success: false };

const registration = (
  state = initialState,
  action: SignUpAction
): RegistrationState => {
  switch (action.type) {
    case ActionTypes.REGISTER:
      return {
        ...state,
        success: canRegister(
          action.isEmailValid,
          action.isPasswordValid,
          action.isPasswordConfirmationValid
        )
      };
    case ActionTypes.RESET_REGISTRATION:
      return initialState;
    default:
      return state;
  }
};

export default registration;
