import * as ActionTypes from "../actions/types";
import { validatePassword } from "../../modules/validators";
import { InputFieldState, SignUpAction } from "../../modules/types";

const initialState: InputFieldState = {
  valid: false,
  value: "",
  errorMessage: null
};

const password = (
  state = initialState,
  action: SignUpAction
): InputFieldState => {
  switch (action.type) {
    case ActionTypes.VALIDATE_PASSWORD:
      let valid = validatePassword(action.password);
      let errorMessage = valid ? "" : "Пароль не соответствует требованиям";
      return {
        ...state,
        valid: valid,
        value: action.password,
        errorMessage: errorMessage
      };
    case ActionTypes.RESET_PASSWORD:
      return initialState;
    default:
      return state;
  }
};

export default password;
