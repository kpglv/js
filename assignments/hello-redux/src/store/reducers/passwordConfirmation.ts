import * as ActionTypes from "../actions/types";
import { validatePasswordConfirmation } from "../../modules/validators";
import { InputFieldState, SignUpAction } from "../../modules/types";

const initialState: InputFieldState = {
  valid: false,
  value: "",
  errorMessage: null
};

const passwordConfirmation = (
  state = initialState,
  action: SignUpAction
): InputFieldState => {
  switch (action.type) {
    case ActionTypes.VALIDATE_PASSWORD_CONFIRMATION:
      let valid = validatePasswordConfirmation(
        action.password,
        action.passwordConfirmation
      );
      let errorMessage = valid ? "" : "Пароли не совпадают";
      return {
        ...state,
        valid: valid,
        value: action.passwordConfirmation,
        errorMessage: errorMessage
      };
    case ActionTypes.RESET_PASSWORD_CONFIRMATION:
      return initialState;
    default:
      return state;
  }
};

export default passwordConfirmation;
