import { combineReducers } from "redux";
import email from "./email";
import password from "./password";
import passwordConfirmation from "./passwordConfirmation";
import registration from "./registration";

export default combineReducers({
  email,
  password,
  passwordConfirmation,
  registration
});
