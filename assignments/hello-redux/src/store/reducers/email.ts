import * as ActionTypes from "../actions/types";
import { validateEmail } from "../../modules/validators";
import { InputFieldState, SignUpAction } from "../../modules/types";

const initialState: InputFieldState = {
  valid: false,
  value: "",
  errorMessage: null
};

const email = (state = initialState, action: SignUpAction): InputFieldState => {
  switch (action.type) {
    case ActionTypes.VALIDATE_EMAIL:
      let valid = validateEmail(action.email);
      let errorMessage = valid ? "" : "Неверный эл. адрес";
      return {
        ...state,
        valid: valid,
        value: action.email,
        errorMessage: errorMessage
      };
    case ActionTypes.RESET_EMAIL:
      return initialState;
    default:
      return state;
  }
};

export default email;
