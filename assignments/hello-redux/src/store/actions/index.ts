import * as ActionTypes from "../actions/types";
import { SignUpAction } from "../../modules/types";

export const validateEmail = (email: string): SignUpAction => ({
  type: ActionTypes.VALIDATE_EMAIL,
  email: email
});

export const validatePassword = (password: string): SignUpAction => ({
  type: ActionTypes.VALIDATE_PASSWORD,
  password: password
});

export const validatePasswordConfirmation = (
  password: string,
  passwordConfirmation: string
): SignUpAction => ({
  type: ActionTypes.VALIDATE_PASSWORD_CONFIRMATION,
  password: password,
  passwordConfirmation: passwordConfirmation
});

export const register = (
  isEmailValid: boolean,
  isPasswordValid: boolean,
  isPasswordConfirmationValid: boolean
): SignUpAction => ({
  type: ActionTypes.REGISTER,
  isEmailValid: isEmailValid,
  isPasswordValid: isPasswordValid,
  isPasswordConfirmationValid: isPasswordConfirmationValid
});

export const resetEmail = (): SignUpAction => ({
  type: ActionTypes.RESET_EMAIL
});

export const resetPassword = (): SignUpAction => ({
  type: ActionTypes.RESET_PASSWORD
});

export const resetPasswordConfirmation = (): SignUpAction => ({
  type: ActionTypes.RESET_PASSWORD_CONFIRMATION
});

export const resetRegistration = (): SignUpAction => ({
  type: ActionTypes.RESET_REGISTRATION
});
