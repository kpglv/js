type Nullable<T> = T | null;

export interface PasswordHintProps {
  text: string;
  [propName: string]: any;
}

export interface SignUpFormProps {
  [propName: string]: any;
}

export interface InputFieldState {
  valid: boolean;
  value: string;
  errorMessage: Nullable<string>;
}

export interface RegistrationState {
  success: boolean;
}

export interface SignUpFormState {
  email: InputFieldState;
  password: InputFieldState;
  passwordConfirmation: InputFieldState;
  form: RegistrationState;
}

export interface SignUpAction {
  type: string;
  [propName: string]: any;
}
