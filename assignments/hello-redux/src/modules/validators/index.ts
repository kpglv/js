export const validateEmail = (email: string): boolean => {
  // https://stackoverflow.com/questions/46155/how-to-validate-an-email-address-in-javascript
  let regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
};

export const validatePassword = (password: string): boolean => {
  return password.length > 5;
};

export const validatePasswordConfirmation = (
  password: string,
  passwordConfirmation: string
): boolean => {
  return password === passwordConfirmation;
};

export const canRegister = (
  isEmailValid: boolean,
  isPasswordValid: boolean,
  isPasswordConfirmationValid: boolean
): boolean => {
  return isEmailValid && isPasswordValid && isPasswordConfirmationValid;
};
