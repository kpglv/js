import React from "react";
import { render } from "@testing-library/react";
import App from "../App";

import { Provider } from "react-redux";
import { createStore } from "redux";
import rootReducer from "../store/reducers";

const store = createStore(rootReducer);

test("renders learn react link", () => {
  const { getByText } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );
  const linkElement = getByText(/Регистрация/i);
  expect(linkElement).toBeInTheDocument();
});
