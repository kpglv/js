import * as Actions from "../store/actions";
import email from "../store/reducers/email";
import password from "../store/reducers/password";
import passwordConfirmation from "../store/reducers/passwordConfirmation";
import registration from "../store/reducers/registration";
import rootReducer from "../store/reducers";

// root reducer

test("empty action on uninitialized state produces valid initial state", () => {
  let state = rootReducer(undefined, { type: "" });
  expect(state).toEqual({
    email: { valid: false, value: "", errorMessage: null },
    password: { valid: false, value: "", errorMessage: null },
    passwordConfirmation: { valid: false, value: "", errorMessage: null },
    registration: { success: false }
  });
});

// email reducer

test("numeric email saved to store as invalid", () => {
  const state = { valid: false, value: "", errorMessage: null };
  const action = Actions.validateEmail("12345");
  const newState = email(state, action);
  expect(newState.valid).toBe(false);
  expect(newState.value).toBe("12345");
});

// password reducer

test("empty password saved to store as invalid", () => {
  const state = { valid: false, value: "", errorMessage: null };
  const action = Actions.validatePassword("");
  const newState = password(state, action);
  expect(newState.valid).toBe(false);
  expect(newState.value).toBe("");
});

// passwordConfirmation reducer

test("non-matching password confirmation saved to store as invalid", () => {
  const state = { valid: false, value: "", errorMessage: null };
  const action = Actions.validatePasswordConfirmation("12345", "");
  const newState = passwordConfirmation(state, action);
  expect(newState.valid).toBe(false);
  expect(newState.value).toBe("");
});

// registration reducer

test("form with invalid email saved to store as invalid", () => {
  const state = { success: false };
  const action = Actions.register(false, true, true);
  const newState = registration(state, action);
  expect(newState.success).toBe(false);
});

test("form with invalid password saved to store as invalid", () => {
  const state = { success: false };
  const action = Actions.register(true, false, true);
  const newState = registration(state, action);
  expect(newState.success).toBe(false);
});

test("form with invalid password confirmation saved to store as invalid", () => {
  const state = { success: false };
  const action = Actions.register(true, true, false);
  const newState = registration(state, action);
  expect(newState.success).toBe(false);
});

test("form with all valid fields saved to store as valid", () => {
  const state = { success: false };
  const action = Actions.register(true, true, true);
  const newState = registration(state, action);
  expect(newState.success).toBe(true);
});
