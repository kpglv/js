import { resetPasswordConfirmation } from "./../store/actions/index";
import * as Validators from "../modules/validators";
import passwordConfirmation from "../store/reducers/passwordConfirmation";

// email

test("empty email is invalid", () => {
  const email = "";
  const isEmailValid = Validators.validateEmail(email);
  expect(isEmailValid).toBe(false);
});

test("numeric email is invalid", () => {
  const email = "12345";
  const isEmailValid = Validators.validateEmail(email);
  expect(isEmailValid).toBe(false);
});

test("email without @ is invalid", () => {
  const email = "asd.ru";
  const isEmailValid = Validators.validateEmail(email);
  expect(isEmailValid).toBe(false);
});

test("email with TLD is invalid", () => {
  const email = "asd@cdf";
  const isEmailValid = Validators.validateEmail(email);
  expect(isEmailValid).toBe(false);
});

test("email without user name is invalid", () => {
  const email = "@mail.ru";
  const isEmailValid = Validators.validateEmail(email);
  expect(isEmailValid).toBe(false);
});

test("email with non-ASCII symbols in domain is invalid", () => {
  const email = "user@майл.ру";
  const isEmailValid = Validators.validateEmail(email);
  expect(isEmailValid).toBe(false);
});

test("email with 4th level domain is valid", () => {
  const email = "user@domain.org.com.uk";
  const isEmailValid = Validators.validateEmail(email);
  expect(isEmailValid).toBe(true);
});

test("my email is valid", () => {
  const email = "pigalev.k@gmail.com";
  const isEmailValid = Validators.validateEmail(email);
  expect(isEmailValid).toBe(true);
});

// password

test("empty password is invalid", () => {
  const password = "";
  const isPasswordValid = Validators.validatePassword(password);
  expect(isPasswordValid).toBe(false);
});

test("3-symbol password is invalid", () => {
  const password = "123";
  const isPasswordValid = Validators.validatePassword(password);
  expect(isPasswordValid).toBe(false);
});

test("12-symbol password is valid", () => {
  const password = "123ABDzxc@,.~{}!";
  const isPasswordValid = Validators.validatePassword(password);
  expect(isPasswordValid).toBe(true);
});

// password confirmation

test("matching password confirmation is valid", () => {
  const password = "123";
  const passwordConfirmation = "123";
  const isPasswordConfirmationValid = Validators.validatePasswordConfirmation(
    password,
    passwordConfirmation
  );
  expect(isPasswordConfirmationValid).toBe(true);
});

test("non-matching password confirmation is invalid", () => {
  const password = "123";
  const passwordConfirmation = "1234";
  const isPasswordConfirmationValid = Validators.validatePasswordConfirmation(
    password,
    passwordConfirmation
  );
  expect(isPasswordConfirmationValid).toBe(false);
});

// registration capability

test("can't register if email is invalid", () => {
  const isEmailValid = false;
  const isPasswordValid = true;
  const isPasswordConfirmationValid = true;
  const canRegister = Validators.canRegister(
    isEmailValid,
    isPasswordValid,
    isPasswordConfirmationValid
  );
  expect(canRegister).toBe(false);
});

test("can't register if password is invalid", () => {
  const isEmailValid = true;
  const isPasswordValid = false;
  const isPasswordConfirmationValid = true;
  const canRegister = Validators.canRegister(
    isEmailValid,
    isPasswordValid,
    isPasswordConfirmationValid
  );
  expect(canRegister).toBe(false);
});

test("can't register if password confirmation is invalid", () => {
  const isEmailValid = true;
  const isPasswordValid = true;
  const isPasswordConfirmationValid = false;
  const canRegister = Validators.canRegister(
    isEmailValid,
    isPasswordValid,
    isPasswordConfirmationValid
  );
  expect(canRegister).toBe(false);
});

test("can register if all 3 fields are valid", () => {
  const isEmailValid = true;
  const isPasswordValid = true;
  const isPasswordConfirmationValid = true;
  const canRegister = Validators.canRegister(
    isEmailValid,
    isPasswordValid,
    isPasswordConfirmationValid
  );
  expect(canRegister).toBe(true);
});
