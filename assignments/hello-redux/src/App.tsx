import React from "react";
import "./App.sass";
import SignUpForm from "./SignUpForm";

const App: React.FC = () => {
  return (
    <div className="app">
      <header className="header app__header">
        <span className="header__title header__title_bold">
          {process.env.REACT_APP_TITLE}
        </span>
      </header>
      <main className="main app__main">
        <SignUpForm className="app__sign-up-form"></SignUpForm>
      </main>
      <footer className="footer app__footer"></footer>
    </div>
  );
};

export default App;
