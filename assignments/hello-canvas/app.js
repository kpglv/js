const HelloCanvasApp = (function() {
  let app = document.querySelector(".app");

  let brushColorControl = app.querySelector(".tool__brush-color");
  let brushWidthControl = app.querySelector(".tool__brush-width");
  let saveButtons = app.querySelectorAll(".app__save-button");
  let clearButtons = app.querySelectorAll(".app__clear-button");
  let clearAllButtons = app.querySelectorAll(".app__clear-all-button");
  let canvas = app.querySelector(".app__canvas");

  let ctx = canvas.getContext("2d");
  let SHARED_COMMANDS_KEY = "hello-canvas-shared-commands";
  let MOUSE_BUTTON_LEFT = 0;
  let previousMouseCoordinates = { x: 0, y: 0 };
  let localCommands = [];

  window.onload = onLoad;

  function initialize() {
    if (!localStorage.getItem(SHARED_COMMANDS_KEY)) {
      localStorage.setItem(SHARED_COMMANDS_KEY, "[]");
    }

    ctx.lineCap = "round";
    ctx.lineJoin = "round";

    brushColorControl.onchange = onBrushColorChange;
    brushWidthControl.onchange = onBrushWidthChange;
    canvas.onmousedown = onMouseDown;
    canvas.ontouchstart = onTouchStart;
    canvas.onmouseup = onMouseUp;
    canvas.ontouchend = onTouchEnd;
    saveButtons.forEach(button => (button.onclick = onSaveButtonClick));
    clearButtons.forEach(button => (button.onclick = onClearButtonClick));
    clearAllButtons.forEach(button => (button.onclick = onClearAllButtonClick));
    window.onstorage = onStorage;

    onBrushColorChange();
    onBrushWidthChange();
  }

  // local commands

  function startRecordingCommands(mouseEvent) {
    previousMouseCoordinates.x = mouseEvent.pageX - canvas.offsetLeft;
    previousMouseCoordinates.y = mouseEvent.pageY - canvas.offsetTop;
    canvas.onmousemove = onMouseMove;
    canvas.ontouchmove = onTouchMove;
  }

  function recordCommand(mouseEvent) {
    let { x: previousX, y: previousY } = previousMouseCoordinates;
    let currentX = mouseEvent.pageX - canvas.offsetLeft;
    let currentY = mouseEvent.pageY - canvas.offsetTop;

    let command = {
      strokeStyle: ctx.strokeStyle,
      lineWidth: ctx.lineWidth,
      path: `M ${previousX} ${previousY} L ${currentX} ${currentY}`
    };

    previousMouseCoordinates.x = currentX;
    previousMouseCoordinates.y = currentY;

    return command;
  }

  function stopRecordingCommands() {
    canvas.onmousemove = null;
    canvas.ontouchmove = null;
  }

  function clearLocalCommands() {
    localCommands = [];
  }

  function shareLocalCommands() {
    let sharedCommands = loadSharedCommands();
    localStorage.setItem(
      SHARED_COMMANDS_KEY,
      JSON.stringify(sharedCommands.concat(localCommands))
    );
  }

  // shared commands

  function loadSharedCommands() {
    return JSON.parse(localStorage.getItem(SHARED_COMMANDS_KEY)) || [];
  }

  function clearSharedCommands() {
    localStorage.setItem(SHARED_COMMANDS_KEY, "[]");
  }

  // rendering

  function renderCommand(command) {
    ctx.strokeStyle = command.strokeStyle;
    ctx.lineWidth = command.lineWidth;
    ctx.stroke(new Path2D(command.path));
  }

  function renderCommands(commandList) {
    ctx.save();
    commandList.forEach(renderCommand);
    ctx.restore();
  }

  function renderLocalCommands() {
    renderCommands(localCommands);
  }

  function renderSharedCommands() {
    renderCommands(loadSharedCommands());
  }

  function renderAllCommands() {
    renderSharedCommands();
    renderLocalCommands();
  }

  function clearCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }

  // event handlers

  function onLoad() {
    initialize();
    renderSharedCommands();
  }

  function onMouseDown(mouseEvent) {
    if (mouseEvent.button == MOUSE_BUTTON_LEFT) {
      startRecordingCommands(mouseEvent);
    }
  }

  function onMouseUp() {
    stopRecordingCommands();
  }

  function onMouseMove(mouseEvent) {
    let command = recordCommand(mouseEvent);
    localCommands.push(command);
    renderCommand(command);
  }

  function onTouchStart(touchEvent) {
    var touch = touchEvent.touches[0];
    var mouseEvent = new MouseEvent("mousedown", {
      clientX: touch.clientX,
      clientY: touch.clientY
    });
    canvas.dispatchEvent(mouseEvent);
  }

  function onTouchEnd() {
    var mouseEvent = new MouseEvent("mouseup", {});
    canvas.dispatchEvent(mouseEvent);
  }

  function onTouchMove(touchEvent) {
    var touch = touchEvent.touches[0];
    var mouseEvent = new MouseEvent("mousemove", {
      clientX: touch.clientX,
      clientY: touch.clientY
    });
    canvas.dispatchEvent(mouseEvent);
  }

  function onBrushColorChange() {
    ctx.strokeStyle = brushColorControl.value;
  }

  function onBrushWidthChange() {
    ctx.lineWidth = parseInt(brushWidthControl.value);
  }

  function onSaveButtonClick() {
    shareLocalCommands();
    clearCanvas();
    renderAllCommands();
  }

  function onClearButtonClick() {
    clearLocalCommands();
    clearCanvas();
    renderSharedCommands();
  }

  function onClearAllButtonClick() {
    if (confirm("Очистить сохраненный рисунок?")) {
      clearSharedCommands();
      clearLocalCommands();
      clearCanvas();
    }
  }

  function onStorage(event) {
    if (event.key == SHARED_COMMANDS_KEY) {
      clearCanvas();
      renderAllCommands();
    }
  }

  return {
    getDrawingContext: () => {
      return ctx;
    },
    getLocalDrawing: () => {
      return localCommands;
    },
    getSharedDrawing: loadSharedCommands,
    save: onSaveButtonClick,
    clearUnsaved: onClearButtonClick,
    clearAll: onClearAllButtonClick
  };
})();
