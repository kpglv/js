import type { Config } from 'tailwindcss';
import tailwindFormsPlugin from '@tailwindcss/forms';

export default {
    content: ['./src/**/*.{js,ts,jsx,tsx}'],
    darkMode: ['class'],
    theme: {
        extend: {},
    },
    plugins: [tailwindFormsPlugin],
} satisfies Config;
