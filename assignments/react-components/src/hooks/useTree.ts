import { useReducer } from 'react';
// TODO: доработать типизацию мультиметодов - добавить поддержку
// произвольного количества типизированных аргументов (сейчас
// в типизированной версии поддерживается только один аргумент)
// и использовать типизированную версию
// @ts-expect-error not (properly) typed yet
import { multi } from '@kpglv/multimethods';
import type { TreeNode } from '../domain/tree';
import { find, ancestors } from '../domain/tree/queries';
import {
    setSelection,
    toggleSelection,
    toggleCheck,
    toggleCollapse,
    expand,
    expandAll,
    collapseAll,
    checkAll,
    uncheckAll,
} from '../domain/tree/mutations';

/** Произвольное действие */
interface Action<T> {
    type: T;
}

/** Действие с полезной нагрузкой */
interface PayloadAction<T, P> extends Action<T> {
    payload: P;
}

/** Тип, перечисляющий виды действий над деревом */
type TreeActionType =
    | 'setSelection'
    | 'toggleSelection'
    | 'toggleCheck'
    | 'toggleCollapse'
    | 'expandAll'
    | 'collapseAll'
    | 'checkAll'
    | 'uncheckAll';

/** Произвольное действие над деревом */
type TreeAction = Action<TreeActionType>;

/** Действие над деревом, содержащее полезную нагрузку */
type TreeNodePayloadAction = PayloadAction<TreeActionType, TreeNode>;

/** Состояние дерева (хранится непосредственно в самом дереве) */
type State = TreeNode;

/** Редьюсер для действий дерева */
type TreeReducer = (state: State, action: TreeAction) => State;

/**
 * Возвращает новое состояние (дерево), полученное применением выбранного
 * метода к текущему состоянию. Выбирает метод по полю 'type' указанного действия.
 * Возвращает текущее состояние для неизвестных действий.
 */
const treeReducer: TreeReducer = multi(
    (_: State, action: TreeAction) => action.type
)
    .default((state: State, action: TreeAction) => {
        console.warn('Неизвестное действие: %o', action);
        return state;
    })
    .when('expandAll', (state: State) => expandAll(state))
    .when('collapseAll', (state: State) => collapseAll(state))
    .when('checkAll', (state: State) => checkAll(state))
    .when('uncheckAll', (state: State) => uncheckAll(state))
    .when(
        'toggleSelection',
        (state: State, { payload: node }: TreeNodePayloadAction) =>
            toggleSelection(state, node)
    )
    .when(
        'toggleCheck',
        (state: State, { payload: node }: TreeNodePayloadAction) =>
            toggleCheck(state, node)
    )
    .when(
        'toggleCollapse',
        (state: State, { payload: node }: TreeNodePayloadAction) =>
            toggleCollapse(state, node)
    )
    .when(
        'setSelection',
        (state: State, { payload: node }: TreeNodePayloadAction) =>
            setSelection(expand(state, ancestors(state, node)), node)
    );

/**
 * Возвращает дерево и набор функций для работы с ним:
 * - модификации
 * - поиска узлов
 *
 * Пользовательский хук.
 *
 * @param tree исходное дерево.
 */
export const useTree = (tree: TreeNode) => {
    const [state, dispatch] = useReducer(treeReducer, tree);

    const expandAll = () => dispatch({ type: 'expandAll' });
    const collapseAll = () => dispatch({ type: 'collapseAll' });
    const checkAll = () => dispatch({ type: 'checkAll' });
    const uncheckAll = () => dispatch({ type: 'uncheckAll' });

    const setSelection = (node: TreeNode) =>
        dispatch({
            type: 'setSelection',
            payload: node,
        } as TreeNodePayloadAction);

    const toggleSelection = (node: TreeNode) =>
        dispatch({
            type: 'toggleSelection',
            payload: node,
        } as TreeNodePayloadAction);

    const toggleCheck = (node: TreeNode) =>
        dispatch({
            type: 'toggleCheck',
            payload: node,
        } as TreeNodePayloadAction);

    const toggleCollapse = (node: TreeNode) =>
        dispatch({
            type: 'toggleCollapse',
            payload: node,
        } as TreeNodePayloadAction);

    return {
        /** Состояние дерева */
        state,
        /** Выбирает узел в дереве  */
        setSelection,
        /** Переключает выбор узла в дереве */
        toggleSelection,
        /** Переключает отметку узла в дереве */
        toggleCheck,
        /** Переключает сворачивание узла в дереве */
        toggleCollapse,
        /** Разворачивает все узлы дерева */
        expandAll,
        /** Сворачивает все узлы дерева */
        collapseAll,
        /** Отмечает все узлы дерева */
        checkAll,
        /** Снимает отметку со всех узлов дерева */
        uncheckAll,
        /** Ищет узлы в дереве по заданным критериям */
        find,
    };
};
