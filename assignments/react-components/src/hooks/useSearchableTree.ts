import { useRef, useState } from 'react';
import { CyclingIndexGenerator } from '../domain/search';
import { TreeNode } from '../domain/tree';
import { useTree } from './useTree';

/**
 * Возвращает дерево и набор функций для работы с ним:
 * - модификации
 * - поиска узлов
 * - последовательного поиска узлов по неизменной строке
 *
 * Пользовательский хук.
 *
 * @param tree исходное дерево.
 */
export const useSearchableTree = (tree: TreeNode) => {
    const treeStateAndActions = useTree(tree);
    const { setSelection, find } = treeStateAndActions;

    const [searchQuery, setSearchQuery] = useState('');
    const [prevSearchQuery, setPrevSearchQuery] = useState('');

    const [nodesCount, setNodesCount] = useState(0);
    const [nodeIndex, setNodeIndex] = useState(0);
    const indexGenerator = useRef<Generator | null>(null);

    /**
     * Следует ли считать запрос новым (отличающимся от предыдущего).
     * @param query строка запроса
     * @param count количество результатов запроса
     */
    const isNewQuery = (query: string, count: number) =>
        !indexGenerator || query !== prevSearchQuery || count !== nodesCount;

    /**
     * Начать новый повторяющийся запрос (инициализировать новый генератор
     * циклических индексов для результата последовательных запросов
     * с той же строкой поиска).
     * @param length длина массива, для которого генерируются индексы
     */
    const startRepeatableQuery = (length: number) => {
        indexGenerator.current = CyclingIndexGenerator(length);
    };

    /**
     * Закончить повторяющийся запрос (сбросить генератор циклических индексов
     * для результата последовательных запросов с той же строкой поиска).
     */
    const endRepeatableQuery = () => {
        indexGenerator.current = null;
        setNodesCount(0);
        setNodeIndex(0);
    };

    /**
     * Найти и выбрать следующий узел, соответствующий строке запроса, если
     * запрос тот же самый, иначе начать новый запрос.
     * @param query строка запроса
     */
    const findNext = (query: string) => {
        const nodeIds = find(tree, { query, sort: 'natural' });
        const count = nodeIds.length;
        setNodesCount(count);

        if (!count) return;

        if (isNewQuery(query, count)) {
            startRepeatableQuery(count);
            const index = indexGenerator.current?.next().value;
            setNodeIndex(index);
            setSelection({
                id: nodeIds[index],
            });
            setPrevSearchQuery(query);
        } else {
            const index = indexGenerator.current?.next().value;
            setNodeIndex(index);
            setSelection({
                id: nodeIds[index],
            });
        }
        setPrevSearchQuery(query);
    };

    const setQuery = (query: string) => {
        endRepeatableQuery();
        setSearchQuery(query);
    };

    return {
        /** Состояние дерева и действия над ним */
        ...treeStateAndActions,
        /** Текущая строка поиска */
        query: searchQuery,
        /** Установить текущую строку поиска (начать новый поиск) */
        setQuery,
        /** Найти следующий узел по текущей строке поиска */
        findNext,
        /** Индекс текущего выбранного узла дерева
            в коллекции найденных по текущей строке поиска */
        nodeIndex,
        /** Количество узлов дерева, найденных по текущей строке поиска */
        nodesCount,
    };
};
