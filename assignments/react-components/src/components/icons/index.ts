import { ComponentType } from 'react';
import {
    BanknotesIcon,
    Bars3BottomLeftIcon,
    BuildingOffice2Icon,
    Cog6ToothIcon,
    DocumentIcon,
    DocumentDuplicateIcon,
    InformationCircleIcon,
    ListBulletIcon,
    LockClosedIcon,
    SignalIcon,
    UsersIcon,
    UserIcon,
    UserPlusIcon,
} from '@heroicons/react/24/outline';

/**
 * Иконка (компонент, возвращающий SVG).
 */
export type IconComponent = ComponentType<{ className?: string }>;

/**
 * Отображение строковых идентификаторов в иконки.
 */
export interface IconMapping {
    [name: string]: IconComponent;
}

const icons: IconMapping = {
    companies: BuildingOffice2Icon,
    userGroups: UsersIcon,
    users: UserIcon,
    permissions: LockClosedIcon,
    accounts: DocumentIcon,
    accountGroups: DocumentDuplicateIcon,
    tariffs: BanknotesIcon,
    info: InformationCircleIcon,
    config: Cog6ToothIcon,
    connectedUsers: UserPlusIcon,
    relays: SignalIcon,
    systemLogs: Bars3BottomLeftIcon,
    userLogs: ListBulletIcon,
};

export default icons;
