import { PropsWithChildren } from 'react';
import type { IconComponent } from './icons';

interface ButtonProps extends PropsWithChildren {
    /** Всплывающая подсказка */
    title?: string;
    /** Иконка */
    Icon?: IconComponent;
    /** Обработчик клика по кнопке */
    onClick?: () => void;
}

/**
 * Компонент кнопки.
 *
 * Презентационный (без состояния).
 */
export const Button = ({ title, Icon, onClick, children }: ButtonProps) => {
    return (
        <button className={styles.button} title={title} onClick={onClick}>
            {Icon ? <Icon className={styles.icon} /> : null}
            {children}
        </button>
    );
};

const styles = {
    button: [
        'h-8 px-1 rounded',
        'flex items-center justify-center',
        'text-gray-900 dark:text-gray-200',
        'bg-gray-200 dark:bg-gray-700',
        'hover:bg-gray-300 dark:hover:bg-gray-600',
        'active:bg-gray-400 dark:active:bg-gray-500',
        'transition-all',
    ].join(' '),
    icon: ['w6 h-6'].join(' '),
};
