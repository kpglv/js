import type { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Button } from './Button';
import { BuildingLibraryIcon } from '@heroicons/react/24/outline';

const meta = {
    title: 'Общие/Кнопка',
    component: Button,
    tags: ['autodocs'],
    parameters: {
        layout: 'centered',
    },
    args: {
        title: 'Пример кнопки',
        onClick: action('on-click'),
    },
    argTypes: {},
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof meta>;

/**
 * Самый простой и гибкий способ отображать произвольный контент
 * (иконки, текст, ...) в кнопке --- это передавать его как дочерние
 * элементы (children); но тогда ответственность за внешний вид контента
 * полностью лежит на компоненте, использующем кнопку (может привести к
 * несогласованности внешнего вида кнопок, используемых в разных компонентах).
 *
 * Передача некоторых предопределённых видов контента (например, иконок)
 * отдельным входным параметром позволяет увязать внешний вид контента
 * (например, размер иконки) с внешним видом кнопки и упростить использование
 * (за счёт некоторой потери гибкости).
 */

export const Text: Story = {
    name: 'Текст',
    args: {
        children: 'Нажми меня',
    },
};

export const Icon: Story = {
    name: 'Иконка',
    args: {
        Icon: BuildingLibraryIcon,
    },
};

export const TextAndIcon: Story = {
    name: 'Текст и иконка',
    args: {
        Icon: BuildingLibraryIcon,
        children: 'Нажми меня!',
    },
};

export const CustomTextAndIcon: Story = {
    name: 'Стилизованный текст и иконка',
    args: {
        Icon: BuildingLibraryIcon,
        children: <div className="font-bold uppercase">Нажми меня</div>,
    },
};
