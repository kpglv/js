import type { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Search } from './Search';

const meta = {
    title: 'Общие/Поиск',
    component: Search,
    tags: ['autodocs'],
    parameters: {
        layout: 'centered',
    },
    args: {
        onEscape: action('on-escape'),
        onEnter: action('on-enter'),
        onChange: action('on-change'),
    },
    argTypes: {},
} satisfies Meta<typeof Search>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
    name: 'Базовый',
    args: {
        searchQuery: 'hello',
    },
};
