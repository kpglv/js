import type { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { TableRow } from './Table';

import exampleColumns from '../../../test/data/table/columns.json';
import exampleRows from '../../../test/data/table/rows-10.json';

const meta = {
    title: 'Задание 2 (WIP)/Строка таблицы',
    component: TableRow,
    tags: ['autodocs'],
    parameters: {
        layout: 'centered',
    },
    args: {
        onRowClick: action('on-row-click'),
        onSelect: action('on-select'),
    },
    argTypes: {},
} satisfies Meta<typeof TableRow>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
    name: 'По умолчанию',
    args: {
        columns: exampleColumns,
        row: exampleRows[0],
        isSelected: false,
    },
};
