import type { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { TableHeader } from './Table';

import exampleColumns from '../../../test/data/table/columns.json';

const meta = {
    title: 'Задание 2 (WIP)/Заголовок таблицы',
    component: TableHeader,
    tags: ['autodocs'],
    parameters: {
        layout: 'centered',
    },
    args: {
        onSort: action('on-sort'),
        onSelect: action('on-select'),
    },
    argTypes: {},
} satisfies Meta<typeof TableHeader>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
    name: 'По умолчанию',
    render: args => (
        <table>
            <thead>
                <TableHeader {...args} />
            </thead>
        </table>
    ),
    args: {
        columns: exampleColumns,
        isSelected: false,
        sort: { column: 'name', order: 'asc' },
    },
};
