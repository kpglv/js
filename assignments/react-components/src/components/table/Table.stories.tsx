import type { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Table } from './Table';

import exampleColumns from '../../../test/data/table/columns.json';
import exampleRows from '../../../test/data/table/rows-10.json';

const meta = {
    title: 'Задание 2 (WIP)/Таблица',
    component: Table,
    tags: ['autodocs'],
    parameters: {
        layout: 'centered',
    },
    args: {
        onRowClick: action('on-row-click'),
        onSelect: action('on-select'),
        onSort: action('on-sort'),
    },
    argTypes: {},
} satisfies Meta<typeof Table>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
    name: 'По умолчанию',
    args: {
        columns: exampleColumns,
        rows: exampleRows,
        initialSort: { column: 'name', order: 'asc' },
        initialSelection: [],
    },
};
