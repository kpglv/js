import { ColumnDescription, RowData, SortSpec } from '../../domain/table';
import {
    ArrowLongUpIcon,
    ArrowLongDownIcon,
} from '@heroicons/react/24/outline';

interface TableHeaderProps {
    /** Описания столбцов */
    columns: ColumnDescription[];
    /** Столбец и направление сортировки */
    sort: SortSpec;
    /** Выбраны все строки? */
    isSelected: boolean;
    /** Обработчик изменения параметров сортировки @param sort параметры сортировки */
    onSort: (sort: SortSpec) => void;
    /** Обработчик выбора/снятия выбора всех строк
        @param isSelected выбраны/не выбраны
        @param row константа 'all' */
    onSelect: (isSelected: boolean, row: 'all') => void;
}

/**
 * Компонент для отображения заголовка таблицы.
 *
 * Презентационный (без состояния)
 */
export const TableHeader = ({
    columns,
    sort,
    isSelected,
    onSelect,
    onSort,
}: TableHeaderProps) => {
    return (
        <tr className={styles.header.row}>
            <th>
                <input
                    type="checkbox"
                    checked={isSelected}
                    onChange={() => onSelect(!isSelected, 'all')}
                />
            </th>
            {columns.map(({ name, title }) => (
                <th key={name} onClick={() => onSort(sort)}>
                    <div className={styles.header.cell}>
                        {title}
                        {name === sort.column ? (
                            sort.order === 'asc' ? (
                                <ArrowLongDownIcon className="w-4 h-4" />
                            ) : sort.order === 'desc' ? (
                                <ArrowLongUpIcon className="w-4 h-4" />
                            ) : null
                        ) : null}
                    </div>
                </th>
            ))}
        </tr>
    );
};

interface TableRowProps {
    /** Описания столбцов */
    columns: ColumnDescription[];
    /** Данные для отображения в строке */
    row: RowData;
    /** Строка выбрана? */
    isSelected: boolean;
    /** Обработчик выбора/снятия выбора строки
        @param isSelected выбрана/не выбрана
        @param row данные строки */
    onSelect: (isSelected: boolean, row: RowData) => void;
    /** Обработчик клика на строке @param row данные строки */
    onRowClick?: (row: RowData) => void;
}

/**
 * Компонент для отображения строки таблицы.
 *
 * Презентационный (без состояния).
 */
export const TableRow = ({
    columns,
    row,
    isSelected,
    onSelect,
    onRowClick,
}: TableRowProps) => {
    return (
        <tr
            className={styles.row.row}
            onClick={onRowClick ? () => onRowClick(row) : undefined}
        >
            <input
                type="checkbox"
                checked={isSelected}
                onChange={() => onSelect(!isSelected, row)}
            />
            {columns.map(({ name }) => (
                <td key={name} className={styles.row.cell}>
                    {row[name] != null ? (row[name] as string) : '-'}
                </td>
            ))}
        </tr>
    );
};

interface TableProps {
    /** Описания столбцов */
    columns: ColumnDescription[];
    /** Данные для отображения в строках */
    rows: RowData[];
    /** Столбец и направление сортировки */
    initialSort: SortSpec;
    /** Начальное состояние выбора */
    initialSelection: RowData[] | 'all';
    /** Обработчик выбора/снятия выбора строк
        @param isSelected выбрана/не выбрана */
    onSelect: (isSelected: boolean) => void;
    /** Обработчик изменения параметров сортировки
        @param sort параметры сортировки */
    onSort: (sort: SortSpec) => void;
    /** Обработчик клика на строке
        @param row данные строки */
    onRowClick?: (row: RowData) => void;
}

/**
 * Компонент для отображения набора табличных данных.
 *
 * Презентационный (без состояния).
 */
export const Table = ({
    columns,
    rows,
    initialSort,
    initialSelection,
    onSelect,
    onSort,
    onRowClick,
}: TableProps) => {
    return (
        <table>
            <thead>
                <TableHeader
                    columns={columns}
                    sort={initialSort}
                    isSelected={initialSelection === 'all'}
                    onSort={onSort}
                    onSelect={onSelect}
                />
            </thead>
            <tbody>
                {rows.map(row => (
                    <TableRow
                        key={row.id}
                        columns={columns}
                        row={row}
                        isSelected={!!row.isSelected}
                        onSelect={onSelect}
                        onRowClick={onRowClick}
                    />
                ))}
            </tbody>
        </table>
    );
};

const styles = {
    header: {
        row: [].join(' '),
        cell: ['flex items-center gap-1'].join(' '),
    },
    row: {
        row: [].join(' '),
        cell: [].join(' '),
    },
};
