import type { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { List } from './List';
import exampleListData from '../../../test/data/list/example.json';
import icons from '../icons';

const meta = {
    title: 'Задание 1/Список (результат)',
    component: List,
    parameters: {
        layout: 'centered',
    },
    tags: ['autodocs'],
    args: {
        onItemClick: action('on-item-click'),
    },
    argTypes: {},
} satisfies Meta<typeof List>;
export default meta;

type Story = StoryObj<typeof meta>;

export const Default: Story = {
    name: 'По умолчанию',
    args: {
        sections: exampleListData,
        icons,
    },
};
