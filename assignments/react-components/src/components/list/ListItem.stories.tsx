import type { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { ListItem } from './ListItem';
import { BeakerIcon } from '@heroicons/react/24/solid';

const meta = {
    title: 'Задание 1/Элемент списка',
    component: ListItem,
    parameters: {
        layout: 'centered',
    },
    tags: ['autodocs'],
    argTypes: {},
    args: {
        onClick: action('on-click'),
    },
} satisfies Meta<typeof ListItem>;
export default meta;

type Story = StoryObj<typeof meta>;

export const Default: Story = {
    name: 'По умолчанию',
    args: {
        id: 'research',
        title: 'Исследования',
    },
};

export const Icon: Story = {
    name: 'С иконкой',
    args: {
        ...meta.args,
        id: 'research',
        title: 'Исследования',
    },
    render: ({ id, title, onClick }) => (
        <ListItem id={id} title={title} Icon={BeakerIcon} onClick={onClick} />
    ),
};
