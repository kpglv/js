import type { Section } from '../../domain/list';
import type { IconMapping } from '../icons';
import { ListItem } from './ListItem';

export interface ListProps {
    /** Секции с дочерними элементами */
    sections: Section[];
    /** Отображение идентификаторов в иконки (компоненты) */
    icons: IconMapping;
    /** Обработчик клика на элементе секции */
    onItemClick?: (itemId: string) => void;
}

/**
 * Компонент для отображения списка --- последовательности элементов,
 * разбитой на секции.
 *
 * Презентационный (без состояния).
 */
export const List = ({ sections, icons, onItemClick }: ListProps) => {
    return (
        <div className={styles.list}>
            {sections.map(({ title, children = [] }) => (
                <div>
                    <h1 className={styles.title}>{title}</h1>
                    {children.map(({ id, title }) => (
                        <ListItem
                            key={id}
                            id={id}
                            title={title}
                            Icon={icons[id]}
                            onClick={onItemClick}
                        />
                    ))}
                </div>
            ))}
        </div>
    );
};

const styles = {
    list: [
        'flex flex-col gap-2',
        'p-2',
        'cursor-default',
        'text-gray-900 bg-gray-50',
        'dark:text-gray-200 dark:bg-gray-800',
    ].join(' '),
    title: ['p-2', 'text-xs font-bold uppercase'].join(' '),
    item: [].join(' '),
};
