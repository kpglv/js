import type { IconComponent } from '../icons';

export interface ListItemProps {
    /** Идентификатор элемента списка */
    id: string;
    /** Название элемента списка */
    title: string;
    /** Иконка элемента списка */
    Icon?: IconComponent;
    /** Обработчик клика по элементу списка @param id идентификатор элемента списка */
    onClick?: (id: string) => void;
}

/**
 * Компонент для отображения элемента списка.
 *
 * Презентационный (без состояния).
 */
export const ListItem = ({ id, title, Icon, onClick }: ListItemProps) => {
    return (
        <div className={styles.item} onClick={() => onClick && onClick(id)}>
            {Icon ? <Icon className={styles.icon} /> : null}
            <span>{title}</span>
        </div>
    );
};

const styles = {
    item: [
        'flex items-center gap-2',
        'px-2 py-1 rounded',
        'cursor-default',
        'text-sm',
        'text-gray-900 bg-gray-50 hover:bg-gray-200 active:bg-gray-300',
        'dark:text-gray-200 dark:bg-gray-800 dark:hover:bg-gray-700 dark:active:bg-gray-600',
    ].join(' '),
    icon: ['w-4 h-4'].join(' '),
};
