import type { TreeNode } from '../../domain/tree';
import { TreeNodeItem } from './TreeNodeItem';

interface TreeProps {
    /** Корневой узел дерева */
    node: TreeNode;
    /** Обработчик события сворачивания/разворачивания узла дерева */
    onToggle: (node: TreeNode) => void;
    /** Обработчик события выбора узла дерева */
    onSelect: (node: TreeNode) => void;
    /** Обработчик события отметки узла дерева */
    onCheck: (node: TreeNode) => void;
}

/**
 * Компонент для отображения дерева.
 *
 * Презентационный (без состояния).
 */
export const Tree = ({ node, onToggle, onSelect, onCheck }: TreeProps) => {
    return (
        <div className={styles.tree}>
            <TreeNodeItem
                node={node}
                onToggle={onToggle}
                onSelect={onSelect}
                onCheck={onCheck}
            />
            <div className={styles.children(!!node.collapsed)}>
                {node?.children?.map(child => (
                    <Tree
                        key={child.id}
                        node={child}
                        onToggle={onToggle}
                        onSelect={onSelect}
                        onCheck={onCheck}
                    />
                ))}
            </div>
        </div>
    );
};

const styles = {
    tree: [
        'w-full',
        'text-sm text-gray-900 bg-gray-50',
        'dark:text-gray-200 dark:bg-gray-800',
    ].join(' '),
    children: (isCollapsed: boolean) =>
        ['pl-5', isCollapsed ? 'hidden' : ''].join(' '),
};
