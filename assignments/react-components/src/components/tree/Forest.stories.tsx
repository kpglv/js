import type { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Forest } from './Forest';

import nodes from '../../../test/data/list/example.json';

const meta = {
    title: 'Задание 3/Лес',
    component: Forest,
    tags: ['autodocs'],
    parameters: {
        layout: 'centered',
    },
    args: {
        onToggle: action('on-toggle'),
        onSelect: action('on-select'),
        onCheck: action('on-check'),
    },
    argTypes: {},
} satisfies Meta<typeof Forest>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
    name: 'По умолчанию',
    args: {
        nodes,
    },
};
