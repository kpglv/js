import { TreeNode } from '../../domain/tree';

interface TreeNodeCheckboxProps {
    /** Узел дерева */
    node: TreeNode;
    /** Состояние отметки узла дерева */
    isChecked: boolean;
    /** Обработчик события отметки узла дерева */
    onCheck: (node: TreeNode) => void;
}

/**
 * Компонент для переключения состояния отметки узла дерева.
 *
 * Презентационный (без состояния).
 */
export const TreeNodeCheckbox = ({
    node,
    isChecked,
    onCheck,
}: TreeNodeCheckboxProps) => {
    return (
        <div className={styles.checkbox}>
            <input
                className={styles.checkboxInput}
                type="checkbox"
                checked={isChecked}
                onChange={() => onCheck(node)}
            />
        </div>
    );
};

const styles = {
    checkbox: ['w-4 h-4', 'flex items-center justify-center'].join(' '),
    checkboxInput: [
        'text-gray-500 bg-gray-50',
        'dark:text-gray-700 dark:bg-gray-700',
        '!ring-offset-gray-50',
        'dark:!ring-offset-gray-800',
    ].join(' '),
};
