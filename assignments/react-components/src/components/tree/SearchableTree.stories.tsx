import type { Meta, StoryObj } from '@storybook/react';

import SearchableTree from './SearchableTree';

import mediumTree from '../../../test/data/tree/medium.json';

const meta = {
    title: 'Задание 3/Дерево с поиском (результат)',
    component: SearchableTree,
    tags: ['autodocs'],
    parameters: {
        layout: 'centered',
    },
    args: {},
    argTypes: {},
} satisfies Meta<typeof SearchableTree>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Medium: Story = {
    name: 'Среднее дерево',
    args: {
        tree: mediumTree,
    },
};
