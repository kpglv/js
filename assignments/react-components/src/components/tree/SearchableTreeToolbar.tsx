import {
    BarsArrowDownIcon,
    BarsArrowUpIcon,
    CheckIcon,
    XMarkIcon,
} from '@heroicons/react/24/outline';
import { Button } from '../Button';
import { Search } from '../Search';
import { SearchStats } from '../../domain/search';

export interface SearchableTreeToolbarProps {
    /** Строка поиска */
    searchQuery: string;
    /** Статистика поиска */
    searchStats?: SearchStats;
    /** Обработчик сворачивания всех узлов */
    onQueryChange?: (query: string) => void;
    /** Обработчик нажатия Enter в строке поиска */
    onQueryEnter?: () => void;
    /** Обработчик нажатия Escape в строке поиска */
    onQueryEscape?: () => void;
    /** Обработчик сворачивания всех узлов */
    onCollapseAll?: () => void;
    /** Обработчик разворачивания всех узлов */
    onExpandAll?: () => void;
    /** Обработчик отметки всех узлов */
    onCheckAll?: () => void;
    /** Обработчик снятия отметки со всех узлов */
    onUncheckAll?: () => void;
}

/**
 * Компонент панели инструментов для дерева с поиском.
 *
 * Презентационный (без состояния).
 */
export const SearchableTreeToolbar = ({
    searchQuery,
    searchStats,
    onQueryChange,
    onQueryEnter,
    onQueryEscape,
    onCollapseAll,
    onExpandAll,
    onCheckAll,
    onUncheckAll,
}: SearchableTreeToolbarProps) => {
    return (
        <div className={styles.toolbar}>
            <Button
                title="Свернуть все"
                Icon={BarsArrowUpIcon}
                onClick={onCollapseAll}
            />
            <Button
                title="Развернуть все"
                Icon={BarsArrowDownIcon}
                onClick={onExpandAll}
            />
            <Button
                title="Отметить все"
                Icon={CheckIcon}
                onClick={onCheckAll}
            />
            <Button
                title="Снять отметку со всех"
                Icon={XMarkIcon}
                onClick={onUncheckAll}
            />
            <Search
                searchQuery={searchQuery}
                searchStats={searchStats}
                onEnter={onQueryEnter}
                onEscape={onQueryEscape}
                onChange={onQueryChange}
            />
        </div>
    );
};

const styles = {
    toolbar: ['flex items-center gap-2'].join(' '),
};
