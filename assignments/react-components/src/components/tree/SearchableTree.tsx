import { type TreeNode } from '../../domain/tree';

import { Tree } from './Tree';
import { useSearchableTree } from '../../hooks/useSearchableTree';
import { SearchableTreeToolbar } from './SearchableTreeToolbar';
import { useMemo } from 'react';

interface SearchableTreeProps {
    tree: TreeNode;
}

const styles = {
    searchableTree: [
        'h-96 min-w-64 p-4',
        'flex flex-col gap-2',
        'text-sm text-gray-900 bg-gray-50',
        'dark:text-gray-200 dark:bg-gray-800',
    ].join(' '),
    toolbar: ['flex items-center gap-2'].join(' '),
    search: [].join(' '),
    scroll: ['py-2 overflow-scroll'].join(' '),
    tree: [].join(' '),
};

/**
 * Компонент для отображения дерева с выбором, отметкой, сворачиванием
 * и последовательным поиском узлов.
 *
 * Контейнер (с состоянием).
 *
 * ### Cворачивание
 *
 * Узлы сворачиваются и разворачиваются независимо друг от друга.
 *
 * ### Выбор и отметка
 *
 * - Выбрать можно только один узел одновременно, при выборе нового узла выбор
 *   с предыдущего узла снимается
 * - Отметить можно любое количество узлов одновременно (но отмеченные узлы не
 *   должны иметь неотмеченных потомков; семантика отметки --- выбрать узел и
 *   всех его потомков)
 * - Отметка не влечёт за собой выбор и наоборот (независимые состояния)
 *
 * ### Поиск
 *
 * По нажатию Enter в поле ввода идёт поиск узлов, названия которых содержат
 * введённую подстроку (регистронезависимо). Если строка поиска не меняется,
 * последующие нажатия Enter выбирают следующий узел из списка найденных;
 * свёрнутые родительские узлы разворачиваются, узел прокручивается в
 * область видимости.
 *
 * По нажатию Escape поле ввода очищается.
 */
const SearchableTree = ({ tree }: SearchableTreeProps) => {
    const {
        // состояние дерева и функции для работы с ним
        state,
        toggleSelection,
        toggleCheck,
        toggleCollapse,
        expandAll,
        collapseAll,
        checkAll,
        uncheckAll,
        // последовательный поиск узлов дерева по строке
        query,
        setQuery,
        findNext,
        nodeIndex,
        nodesCount,
    } = useSearchableTree(tree);

    // TODO: внести статистику поиска в хук?
    const searchStats = useMemo(
        () => ({
            current: nodeIndex + 1,
            total: nodesCount,
        }),
        [nodeIndex, nodesCount]
    );

    return (
        <div className={styles.searchableTree}>
            {/* TODO: разделить панель инструментов на две (работа с деревом,
                последовательный поиск в дереве)? */}
            <SearchableTreeToolbar
                searchQuery={query}
                searchStats={searchStats}
                onQueryEnter={() => findNext(query)}
                onQueryEscape={() => setQuery('')}
                onQueryChange={(query: string) => setQuery(query)}
                onCollapseAll={collapseAll}
                onExpandAll={expandAll}
                onCheckAll={checkAll}
                onUncheckAll={uncheckAll}
            />
            <div className={styles.scroll}>
                <Tree
                    node={state}
                    onSelect={toggleSelection}
                    onCheck={toggleCheck}
                    onToggle={toggleCollapse}
                />
            </div>
        </div>
    );
};

export default SearchableTree;
