import { FolderIcon, DocumentIcon } from '@heroicons/react/24/outline';

import { useEffect, useRef } from 'react';
import { TreeNode } from '../../domain/tree';
import { TreeNodeCheckbox } from './TreeNodeCheckbox';
import { TreeNodeCollapseToggle } from './TreeNodeCollapseToggle';

interface TreeNodeItemProps {
    node: TreeNode;
    onToggle: (node: TreeNode) => void;
    onSelect: (node: TreeNode) => void;
    onCheck: (node: TreeNode) => void;
}

/**
 * Компонент для отображения узла дерева.
 *
 * Презентационный (без состояния).
 */
export const TreeNodeItem = ({
    node,
    onToggle,
    onSelect,
    onCheck,
}: TreeNodeItemProps) => {
    const treeNodeItemElement = useRef<HTMLDivElement>(null);

    useEffect(() => {
        if (node.selected) {
            treeNodeItemElement.current?.scrollIntoView({
                behavior: 'smooth',
                block: 'nearest',
                inline: 'center',
            });
        }
    }, [node.selected]);

    return (
        <div
            className={styles.treeNodeItem(!!node.selected)}
            ref={treeNodeItemElement}
        >
            <div className={styles.prefix}>
                {node.children ? (
                    <TreeNodeCollapseToggle
                        node={node}
                        onToggle={() => onToggle(node)}
                    />
                ) : (
                    <div className={styles.icon}></div>
                )}
                <TreeNodeCheckbox
                    node={node}
                    isChecked={!!node.checked}
                    onCheck={onCheck}
                />
            </div>
            <div className={styles.title} onClick={() => onSelect(node)}>
                {node.children ? (
                    <FolderIcon className={styles.icon} />
                ) : (
                    <DocumentIcon className={styles.icon} />
                )}
                {node.title || node.id}
            </div>
        </div>
    );
};

const styles = {
    treeNodeItem: (isSelected: boolean) =>
        [
            'p-1 w-full',
            'flex items-center gap-1',
            'rounded',
            'transition-all',
            isSelected ? 'bg-gray-200 dark:bg-gray-700' : '',
        ].join(' '),
    prefix: ['flex items-center gap-1'].join(' '),
    title: ['flex items-center gap-0.5', 'w-full', 'cursor-default'].join(
        ' '
    ),
    icon: ['w-4 h-4'].join(' '),
};
