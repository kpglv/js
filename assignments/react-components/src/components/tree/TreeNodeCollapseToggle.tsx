import {
    ChevronDownIcon,
    ChevronRightIcon,
} from '@heroicons/react/24/outline';
import { TreeNode } from '../../domain/tree';

interface TreeNodeCollapseToggleProps {
    /** Узел дерева */
    node: TreeNode;
    /** Обработчик события сворачивания/разворачивания узла дерева */
    onToggle: (node: TreeNode) => void;
}

/**
 * Компонент для переключения состояния сворачивания узла дерева.
 *
 * Презентационный (без состояния).
 */
export const TreeNodeCollapseToggle = ({
    node,
    onToggle,
}: TreeNodeCollapseToggleProps) => {
    return (
        <div onClick={() => onToggle(node)}>
            {node.collapsed ? (
                <ChevronRightIcon className={styles.icon} />
            ) : (
                <ChevronDownIcon className={styles.icon} />
            )}
        </div>
    );
};

const styles = {
    icon: ['w-4 h-4'].join(' '),
};
