import type { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { Tree } from './Tree';

import smallTree from '../../../test/data/tree/small.json';
import mediumTree from '../../../test/data/tree/medium.json';

const meta = {
    title: 'Задание 3/Дерево',
    component: Tree,
    tags: ['autodocs'],
    parameters: {
        layout: 'centered',
    },
    args: {
        onToggle: action('on-toggle'),
        onSelect: action('on-select'),
        onCheck: action('on-check'),
    },
    argTypes: {},
} satisfies Meta<typeof Tree>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Small: Story = {
    name: 'Небольшое дерево',
    args: {
        node: smallTree,
    },
};

export const Medium: Story = {
    name: 'Среднее дерево',
    args: {
        node: mediumTree,
    },
};
