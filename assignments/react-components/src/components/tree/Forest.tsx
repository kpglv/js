import type { TreeNode } from '../../domain/tree';
import { Tree } from './Tree';

interface ForestProps {
    /** Корневые узлы отдельных деревьев */
    nodes: TreeNode[];
    /** Обработчик события сворачивания/разворачивания узла дерева */
    onToggle: (node: TreeNode) => void;
    /** Обработчик события выбора узла дерева */
    onSelect: (node: TreeNode) => void;
    /** Обработчик события отметки узла дерева */
    onCheck: (node: TreeNode) => void;
}

/**
 * Компонент для отображения множества отдельных (не связанных) деревьев.
 *
 * Презентационный (без состояния).
 */
export const Forest = ({ nodes, ...rest }: ForestProps) => {
    return (
        <div className={styles.forest}>
            {nodes.map(node => (
                <Tree key={node.id} node={node} {...rest} />
            ))}
        </div>
    );
};

const styles = {
    forest: [
        'p-2',
        'text-sm text-gray-900 bg-gray-50',
        'dark:text-gray-200 dark:bg-gray-800',
    ].join(' '),
};
