import { KeyboardEvent, useMemo } from 'react';
import { SearchStats } from '../domain/search';

export interface SearchProps {
    /** Строка запроса */
    searchQuery: string;
    /** Статистика результатов поиска */
    searchStats?: SearchStats;
    /** Обработчик нажатия клавиши Enter
        @param text введённый текст */
    onEnter?: (text: string) => void;
    /** Обработчик нажатия клавиши Escape */
    onEscape?: () => void;
    /** Обработчик ввода символа
        @param text введённый текст */
    onChange?: (text: string) => void;
}

/**
 * Компонент для отображения поля ввода текста (для поиска, фильтрации, ...).
 *
 * Презентационный (без состояния).
 */
export const Search = ({
    searchQuery,
    searchStats = { current: 0, total: 0 },
    onChange,
    onEnter,
    onEscape,
}: SearchProps) => {
    const { current, total } = searchStats;

    /**
     * Нужно ли показывать элемент со статистикой повторяющегося
     * запроса (текущий элемент/всего элементов).
     */
    const shouldShowSearchStats = useMemo(() => !!total, [total]);

    const onKeyDown = (event: KeyboardEvent<HTMLInputElement>) => {
        switch (event.key) {
            case 'Enter':
                onEnter && onEnter((event.target as HTMLInputElement).value);
                break;
            case 'Escape':
                onEscape && onEscape();
                break;
        }
    };
    return (
        <div className={styles.search}>
            <input
                className={styles.input}
                type="search"
                value={searchQuery}
                onKeyDown={onKeyDown}
                onChange={event => onChange && onChange(event.target.value)}
            />
            {searchStats ? (
                <div
                    className={styles.searchStats(shouldShowSearchStats)}
                    onClick={onEscape}
                >
                    {current || 0}/{total}
                </div>
            ) : null}
        </div>
    );
};

const styles = {
    search: ['relative', 'text-sm'].join(' '),
    input: [
        'text-sm h-8 rounded border-none',
        'text-gray-900 bg-gray-200',
        'dark:text-gray-200 dark:bg-gray-700',
    ].join(' '),
    searchStats: (visible: boolean) =>
        ['absolute right-3 top-1.5', !visible ? 'hidden' : ''].join(' '),
};
