/**
 * Описание столбцов таблицы.
 */
export interface ColumnDescription {
    name: string;
    title: string;
}

/**
 * Запись таблицы.
 */
export interface RowData {
    id: string | number;
    [key: string]: unknown;
}

/**
 * Порядок сортировки.
 */
export type SortingOrder = 'asc' | 'desc' | undefined;

/**
 * Параметры сортировки.
 */
export interface SortSpec {
    column: string;
    order?: SortingOrder;
}

/**
 * Возвращает следующий порядок сортировки.
 * @param order текущий порядок сортировки.
 * @returns
 */
export const nextOrder = (order?: SortingOrder) => {
    switch (order) {
        case undefined:
            return 'asc';
        case 'asc':
            return 'desc';
        default:
            return;
    }
};
