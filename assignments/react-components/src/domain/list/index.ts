/**
 * Элемент списка.
 */
export interface Item {
    /** Идентификатор элемента */
    id: string;
    /** Название элемента */
    title: string;
}

/**
 * Секция списка.
 */
export interface Section {
    /** Идентификатор секции */
    id: string;
    /** Название секции */
    title: string;
    /** Элементы секции */
    children?: Item[];
}
