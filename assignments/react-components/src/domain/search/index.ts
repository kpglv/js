export interface SearchStats {
    /** Индекс текущего выбранного элемента в коллекции найденных элементов */
    current?: number;
    /** Количество найденных элементов */
    total?: number;
}

/**
 * Возвращает генератор, порождающий бесконечно повторяющуюся
 * последовательность целых чисел в диапазоне от нуля (включая)
 * до конечного числа (исключая).
 * @param end конечное число
 */
export const CyclingIndexGenerator = function* (end: number) {
    let counter = 0;
    while (true) {
        yield counter;
        if (end === 0) continue;
        counter = (counter + 1) % end;
    }
};
