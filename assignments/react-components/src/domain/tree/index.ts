/**
 * Узел дерева.
 */
export interface TreeNode {
    id: string;
    title?: string;
    children?: TreeNode[];
    collapsed?: boolean;
    selected?: boolean;
    checked?: boolean;
}

/**
 * Параметры поиска узлов дерева.
 */
export interface TreeSearchSpec {
    query: string;
    pred?: (node: TreeNode, query: string) => boolean;
    sort?: 'topological' | 'natural';
}
