import type { TreeNode, TreeSearchSpec } from '.';
// @ts-expect-error not typed yet
import { postwalk, isObject, isArray } from '@kpglv/tree-walk';
import {
    graph,
    depend,
    dependants,
    dependencies,
    topologicalSort,
    // @ts-expect-error not (properly) typed yet
} from '@kpglv/dependency-graph';

/**
 * Возвращает истину, если указанное значение является узлом дерева.
 * @param x любое значение
 */
export const isTreeNode = (x: unknown): x is TreeNode => {
    return isObject(x) && !isArray(x) && Object.hasOwn(x as object, 'id');
};

// TODO: исправить типизацию графа зависимостей
/**
 * Возвращает новый граф зависимостей между узлами дерева (для более эффективной
 * навигации/поиска в дереве).
 * @param tree дерево
 */
export const createDependencyGraph = (tree: TreeNode) => {
    const dependencyGraph = graph();

    postwalk((value: unknown) => {
        if (!isTreeNode(value)) return value;

        if (value.children) {
            value.children.forEach(child => {
                if (isTreeNode(child)) {
                    depend(dependencyGraph, value.id, child.id);
                }
            });
        }
        return value;
    }, tree);

    return dependencyGraph;
};

/**
 * Возвращает множество идентификаторов предков узла в дереве.
 * @param tree дерево
 * @param node узел
 * @param depGraph граф зависимостей между узлами дерева
 */
export const ancestors = (
    tree: TreeNode,
    node: TreeNode,
    depGraph?: object
): string[] => {
    const dependencyGraph = depGraph || createDependencyGraph(tree);
    return Array.from(dependants(dependencyGraph, node.id));
};

/**
 * Возвращает множество идентификаторов потомков узла в дереве.
 * @param tree дерево
 * @param node узел
 * @param depGraph граф зависимостей между узлами дерева
 */
export const descendants = (
    tree: TreeNode,
    node: TreeNode,
    depGraph?: object
): string[] => {
    const dependencyGraph = depGraph || createDependencyGraph(tree);
    return Array.from(dependencies(dependencyGraph, node.id));
};

/**
 * Возвращает множество идентификаторов узлов дерева, удовлетворяющих
 * предикату. Результат может быть отсортирован топологически (от корня
 * к листьям --- корень, затем узлы первого уровня, затем узлы второго
 * уровня и т.д.), натурально (узел, затем его потомки по порядку;
 * после каждого потомка его потомки) или возвращён без сортировки,
 * в порядке обхода дерева (depth-first); по умолчанию без сортировки.
 * Предикат принимает узел дерева и строку запроса, возвращает истину,
 * если узел соответствует строке запроса.
 * @param tree дерево
 * @param param1.query строка запроса
 * @param param1.pred предикат
 * @param param1.sort способ сортировки
 */
export const find = (
    tree: TreeNode,
    {
        query,
        pred = (node: TreeNode, query: string) =>
            !!node?.title?.toLowerCase().includes(query.toLowerCase()),
        sort,
    }: TreeSearchSpec
): string[] => {
    switch (sort) {
        case 'natural': {
            return naturalSort(tree)
                .filter(node => pred(node, query))
                .map(node => node.id);
        }
        case 'topological': {
            // FIXME: можно было бы реализовать топологическую сортировку
            // без отдельного шага с дополнительными обходами дерева,
            // если бы граф зависимостей поддерживал произвольные объекты
            // в качестве узлов, а не только строки / примитивы, так как
            // одних идентификаторов узлов дерева недостаточно для их
            // фильтрации; искать каждый узел по идентификатору и делать
            // алгоритм O(n^2) вместо O(n) тоже не хотелось бы;
            // O(2-4n) ~ O(n) выглядит как приемлемый (временный) компромисс
            const foundNodeIds: Set<string> = new Set();

            postwalk((value: unknown) => {
                if (!isTreeNode(value)) return value;

                if (pred(value, query)) {
                    foundNodeIds.add(value.id);
                }
                return value;
            }, tree);

            return topologicalSort(createDependencyGraph(tree)).filter(
                (id: string) => foundNodeIds.has(id)
            );
        }
        default: {
            const foundNodeIds: string[] = [];

            postwalk((value: unknown) => {
                if (!isTreeNode(value)) return value;

                if (pred(value, query)) {
                    foundNodeIds.push(value.id);
                }
                return value;
            }, tree);

            return foundNodeIds;
        }
    }
};

/**
 * Возвращает последовательность всех узлов в дереве, отсортированных
 * в порядке натурального обхода (узел, затем его прямые потомки по порядку).
 * @param tree дерево
 */
export const naturalSort = (tree: TreeNode): TreeNode[] => {
    return [tree, ...(tree.children || []).flatMap(naturalSort)];
};

/**
 * Возвращает узел дерева с указанным идентификатором, или undefined.
 * @param tree дерево
 * @param nodeId идентификатор узла
 */
export const findNodeById = (
    tree: TreeNode,
    nodeId: string
): TreeNode | undefined => naturalSort(tree).find(node => node.id === nodeId);

/**
 * Возвращает узлы дерева с указанными идентификаторами.
 * @param tree дерево
 * @param nodeIds идентификаторы узлов
 */
export const findNodesByIds = (
    tree: TreeNode,
    nodeIds: string[]
): TreeNode[] => {
    const ids = new Set(nodeIds);
    return naturalSort(tree).filter(node => ids.has(node.id));
};
