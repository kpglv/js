import type { TreeNode } from '.';
// @ts-expect-error not typed yet
import { postwalk } from '@kpglv/tree-walk';
import {
    isTreeNode,
    ancestors,
    descendants,
    createDependencyGraph,
} from './queries';

/**
 * Возвращает новое дерево, в котором указанный узел выбран. Сбрасывает состояние
 * выбора всех остальных узлов дерева (только один узел может быть выбран).
 * @param tree дерево
 * @param param1.node узел
 * @returns
 */
export const setSelection = (tree: TreeNode, node: TreeNode): TreeNode => {
    return postwalk((value: unknown) => {
        if (!isTreeNode(value)) return value;

        const { selected: _, ...rest } = value;
        if (value.id === node.id) {
            return { ...rest, selected: true };
        } else {
            return rest;
        }
    }, tree);
};

/**
 * Возвращает новое дерево, в котором состояние выбора
 * указанного узла переключено на обратное. Сбрасывает состояние
 * выбора всех остальных узлов дерева (только один узел может быть выбран).
 * @param tree дерево
 * @param param1.node узел
 * @returns
 */
export const toggleSelection = (tree: TreeNode, node: TreeNode): TreeNode => {
    return postwalk((value: unknown) => {
        if (!isTreeNode(value)) return value;

        const { selected, ...rest } = value;
        if (value.id === node.id && !selected) {
            return { ...rest, selected: true };
        } else {
            return rest;
        }
    }, tree);
};

/**
 * Возвращает новое дерево, в котором состояние отметки
 * указанного узла переключено на обратное. Переключает также отметку
 * всех его родителей и потомков в соответствии с новым состоянием отметки
 * (отмеченные узлы не должны иметь неотмеченных потомков).
 * @param tree дерево
 * @param param1.node узел
 * @returns
 */
export const toggleCheck = (tree: TreeNode, node: TreeNode): TreeNode => {
    const dependencyGraph = createDependencyGraph(tree);
    const nodeAncestors = new Set(ancestors(tree, node, dependencyGraph));
    const nodeDescendants = new Set(descendants(tree, node, dependencyGraph));
    // оптимизация - избежать лишнего обхода дерева для определения состояния
    // отметки целевого узла; не будет работать правильно, если целевой узел
    // не содержит актуальной информации об отметке (например, не взят из
    // дерева, а создан заново с тем же идентификатором)
    const targetChecked = node.checked;

    return postwalk((value: unknown) => {
        if (!isTreeNode(value)) return value;

        const { checked: _, ...rest } = value;
        if (value.id === node.id || nodeDescendants.has(value.id)) {
            return targetChecked ? rest : { ...rest, checked: true };
        } else if (nodeAncestors.has(value.id)) {
            return rest;
        } else {
            return value;
        }
    }, tree);
};

/**
 * Возвращает новое дерево, в котором состояние скрытия
 * потомков указанного узла переключено на обратное.
 * @param tree дерево
 * @param param1.node узел
 * @returns
 */
export const toggleCollapse = (tree: TreeNode, node: TreeNode): TreeNode => {
    return postwalk((value: unknown) => {
        if (!isTreeNode(value)) return value;

        const { collapsed, ...rest } = value;
        if (value.id === node.id) {
            if (!collapsed) {
                return { ...rest, collapsed: true };
            } else {
                return rest;
            }
        } else {
            return value;
        }
    }, tree);
};

/**
 * Возвращает новое дерево, в котором состояние скрытия потомков
 * сброшено для указанных узлов.
 * @param tree дерево
 * @param nodeIds идентификаторы узлов
 * @returns
 */
export const expand = (tree: TreeNode, nodeIds: string[]): TreeNode => {
    const toExpand = new Set(nodeIds);
    return postwalk((value: unknown) => {
        if (!isTreeNode(value)) return value;

        const { collapsed: _, ...rest } = value;
        if (toExpand.has(value.id)) {
            return rest;
        }
        return value;
    }, tree);
};

/**
 * Возвращает новое дерево, в котором состояние скрытия потомков
 * установлено для указанных узлов.
 * @param tree дерево
 * @returns
 */
export const collapse = (tree: TreeNode, nodeIds: string[]): TreeNode => {
    const toCollapse = new Set(nodeIds);
    return postwalk((value: unknown) => {
        if (!isTreeNode(value)) return value;

        const { collapsed: _, ...rest } = value;
        if (toCollapse.has(value.id)) {
            return { ...rest, collapsed: true };
        }
        return value;
    }, tree);
};

/**
 * Возвращает новое дерево, в котором состояние скрытия потомков
 * сброшено для всех узлов.
 * @param tree дерево
 * @returns
 */
export const expandAll = (tree: TreeNode): TreeNode => {
    return postwalk((value: unknown) => {
        if (!isTreeNode(value)) return value;
        const { collapsed: _, ...rest } = value;
        return rest;
    }, tree);
};

/**
 * Возвращает новое дерево, в котором состояние скрытия потомков
 * установлено для всех узлов.
 * @param tree дерево
 * @returns
 */
export const collapseAll = (tree: TreeNode): TreeNode => {
    return postwalk((value: unknown) => {
        if (!isTreeNode(value)) return value;
        return { ...value, collapsed: true };
    }, tree);
};

/**
 * Возвращает новое дерево, в котором все узлы отмечены.
 * @param tree дерево
 * @returns
 */
export const checkAll = (tree: TreeNode): TreeNode => {
    return postwalk((value: unknown) => {
        if (!isTreeNode(value)) return value;
        return { ...value, checked: true };
    }, tree);
};

/**
 * Возвращает новое дерево, в котором отметка снята со всех узлов.
 * @param tree дерево
 * @returns
 */
export const uncheckAll = (tree: TreeNode): TreeNode => {
    return postwalk((value: unknown) => {
        if (!isTreeNode(value)) return value;
        const { checked: _, ...rest } = value;
        return rest;
    }, tree);
};
