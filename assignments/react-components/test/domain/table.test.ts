import { describe, expect, it } from 'vitest';
import fc from 'fast-check';

import { nextOrder } from '../../src/domain/table';

const validOrders = new Set([undefined, 'asc', 'desc']);

describe('nextOrder', () => {
    it('всегда возвращает валидный порядок сортировки', () => {
        fc.assert(
            fc.property(fc.anything(), order => {
                // @ts-expect-error unknown (possibly invalid) sorting order
                expect(validOrders.has(nextOrder(order))).toBe(true);
            })
        );
    });

    it('корректно возвращает следующий порядок сортировки', () => {
        expect(nextOrder()).toBe('asc');
        expect(nextOrder('asc')).toBe('desc');
        expect(nextOrder('desc')).toBeUndefined();
        // @ts-expect-error invalid sorting order
        expect(nextOrder('foo')).toBeUndefined();
    });
});
