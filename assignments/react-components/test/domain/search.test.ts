import { describe, expect, it } from 'vitest';
import { CyclingIndexGenerator } from '../../src/domain/search';

describe('CyclingIndexGenerator', () => {
    it('возвращает генератор', () => {
        expect(CyclingIndexGenerator(12)).toHaveProperty('next');
    });
});

describe('генератор индексов', () => {
    it('возвращает циклическую последовательность целых чисел', () => {
        const gen = CyclingIndexGenerator(3);
        expect(gen.next().value).toBe(0);
        expect(gen.next().value).toBe(1);
        expect(gen.next().value).toBe(2);
        expect(gen.next().value).toBe(0);
        expect(gen.next().value).toBe(1);
        expect(gen.next().value).toBe(2);
        expect(gen.next().value).toBe(0);
    });
});
