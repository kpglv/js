import { describe, expect, it } from 'vitest';
import {
    isTreeNode,
    find,
    ancestors,
    descendants,
    createDependencyGraph,
    naturalSort,
    findNodeById,
    findNodesByIds,
} from '../../src/domain/tree/queries';
import {
    setSelection,
    toggleSelection,
    toggleCheck,
    toggleCollapse,
    expand,
    collapse,
    expandAll,
    collapseAll,
    checkAll,
    uncheckAll,
} from '../../src/domain/tree/mutations';

import smallTree from '../data/tree/small.json';
import mediumTree from '../data/tree/medium.json';
import { TreeNode } from '../../src/domain/tree';

// TODO: более тщательное тестирование

/* queries */

describe('isTreeNode', () => {
    it('возвращает истину на валидных узлах', () => {
        expect(isTreeNode(smallTree)).toBe(true);
        expect(isTreeNode(mediumTree)).toBe(true);
    });
    it('возвращает ложь на невалидных узлах', () => {
        expect(isTreeNode({})).toBe(false);
        expect(isTreeNode([])).toBe(false);
        expect(isTreeNode(1)).toBe(false);
        expect(isTreeNode('foo')).toBe(false);
        expect(isTreeNode(isTreeNode)).toBe(false);
    });
});

describe('find', () => {
    it('находит существующие узлы', () => {
        expect(find(smallTree, { query: 'cde' })).toStrictEqual(['cde']);
    });
    it('не находит несуществующих узлов', () => {
        expect(find(smallTree, { query: 'cdef' })).toStrictEqual([]);
    });
});

describe('ancestors', () => {
    it('возвращает всех предков существующего узла', () => {
        expect(ancestors(smallTree, { id: 'cde' })).toStrictEqual([
            'cd',
            'c',
            'root',
        ]);
    });
    it('возвращает пустой массив для несуществующего узла', () => {
        expect(ancestors(smallTree, { id: 'nope' })).toStrictEqual([]);
    });
});

describe('descendants', () => {
    it('возвращает идентификаторы всех потомков существующего узла', () => {
        expect(new Set(descendants(smallTree, { id: 'root' }))).toStrictEqual(
            new Set(['a', 'b', 'c', 'ab', 'cd', 'cde'])
        );
    });
    it('возвращает пустой массив для несуществующего узла', () => {
        expect(descendants(smallTree, { id: 'nope' })).toStrictEqual([]);
    });
});

describe('createDependencyGraph', () => {
    it('возвращает граф зависимостей для валидного дерева', () => {
        expect(createDependencyGraph(smallTree).dependants).toBeInstanceOf(
            Map
        );
        expect(createDependencyGraph(smallTree).dependencies).toBeInstanceOf(
            Map
        );
    });
});

describe('naturalSort', () => {
    it('находит все узлы в дереве', () => {
        expect(naturalSort({ id: 'a' })).toStrictEqual([{ id: 'a' }]);
        expect(naturalSort(smallTree).length).toBe(7);
    });
});

describe('findNodeById', () => {
    it('находит существующий узел', () => {
        expect(findNodeById(smallTree, 'cd')?.id).toBe('cd');
    });
    it('не находит несуществующих узлов', () => {
        expect(findNodeById(smallTree, 'nope')).toBeUndefined();
    });
});

describe('findNodesByIds', () => {
    it('находит существующие узлы', () => {
        expect(findNodesByIds(smallTree, ['cd', 'ab']).length).toBe(2);
    });
    it('не находит несуществующих узлов', () => {
        expect(findNodesByIds(smallTree, ['nope', 'no']).length).toBe(0);
        expect(findNodesByIds(smallTree, ['nope', 'cde']).length).toBe(1);
    });
});

/* mutations */

describe('setSelection', () => {
    it('возвращает ранее не выбранный узел выбранным', () => {
        expect(
            findNodeById(setSelection(smallTree, { id: 'b' }), 'b')?.selected
        ).toBe(true);
    });
    it('возвращает ранее выбранный узел выбранным', () => {
        expect(
            findNodeById(setSelection(smallTree, { id: 'a' }), 'a')?.selected
        ).toBe(true);
    });
});

describe('toggleSelection', () => {
    it('возвращает ранее не выбранный узел выбранным', () => {
        expect(
            findNodeById(toggleSelection(smallTree, { id: 'b' }), 'b')
                ?.selected
        ).toBe(true);
    });
    it('возвращает ранее выбранный узел не выбранным', () => {
        expect(
            findNodeById(toggleSelection(smallTree, { id: 'a' }), 'a')
                ?.selected
        ).toBeUndefined();
    });
});

// FIXME: узел должен быть обязательно взят из дерева,
// не создан заново
describe('toggleCheck', () => {
    it('возвращает ранее не отмеченный узел отмеченным', () => {
        const node = findNodeById(smallTree, 'b') as TreeNode;
        expect(findNodeById(toggleCheck(smallTree, node), 'b')?.checked).toBe(
            true
        );
    });
    it('возвращает ранее отмеченный узел не отмеченным', () => {
        const node = findNodeById(smallTree, 'a') as TreeNode;
        expect(
            findNodeById(toggleCheck(smallTree, node), 'a')?.checked
        ).toBeUndefined();
    });
    it('возвращает всех потомков заново отмечeнного узла отмеченными', () => {
        const node = findNodeById(smallTree, 'root') as TreeNode;
        const newTree = toggleCheck(smallTree, node);
        const descendantNodes = findNodesByIds(
            newTree,
            descendants(newTree, node)
        );
        expect(descendantNodes.every(node => node.checked)).toBe(true);
    });
    it('возвращает всех потомков заново неотмечeнного узла неотмеченными', () => {
        const node = findNodeById(smallTree, 'c') as TreeNode;
        const newTree = toggleCheck(smallTree, node);
        const descendantNodes = findNodesByIds(
            newTree,
            descendants(newTree, node)
        );
        expect(descendantNodes.every(node => !node.checked)).toBe(true);
    });
    it('возвращает всех родителей заново неотмечeнного узла неотмеченными', () => {
        const node = findNodeById(smallTree, 'cde') as TreeNode;
        const newTree = toggleCheck(smallTree, node);
        const ancestorNodes = findNodesByIds(
            newTree,
            ancestors(newTree, node)
        );
        expect(ancestorNodes.every(node => !node.checked)).toBe(true);
    });
});

describe('toggleCollapse', () => {
    it('возвращает ранее не свёрнутый узел свёрнутым', () => {
        expect(
            findNodeById(toggleCollapse(smallTree, { id: 'b' }), 'b')
                ?.collapsed
        ).toBe(true);
    });
    it('возвращает ранее свёрнутый узел не свёрнутым', () => {
        expect(
            findNodeById(toggleCollapse(smallTree, { id: 'cd' }), 'cd')
                ?.collapsed
        ).toBeUndefined();
    });
});

describe('expand', () => {
    it('возвращает ранее свёрнутыe узлы не свёрнутыми', () => {
        expect(
            findNodesByIds(expand(smallTree, ['cd']), ['cd']).every(
                node => !node.collapsed
            )
        ).toBe(true);
    });
    it('возвращает ранее не свёрнутые узлы не свёрнутыми', () => {
        expect(
            findNodesByIds(expand(smallTree, ['a', 'b']), ['a', 'b']).every(
                node => !node.collapsed
            )
        ).toBe(true);
    });
});

describe('collapse', () => {
    it('возвращает ранее свёрнутыe узлы свёрнутыми', () => {
        expect(
            findNodesByIds(collapse(smallTree, ['cd']), ['cd']).every(
                node => node.collapsed
            )
        ).toBe(true);
    });
    it('возвращает ранее не свёрнутые узлы свёрнутыми', () => {
        expect(
            findNodesByIds(collapse(smallTree, ['a', 'b']), ['a', 'b']).every(
                node => node.collapsed
            )
        ).toBe(true);
    });
});

describe('expandAll', () => {
    it('возвращает все узлы дерева не свёрнутыми', () => {
        expect(
            naturalSort(expandAll(smallTree)).every(node => !node.collapsed)
        ).toBe(true);
    });
});

describe('collapseAll', () => {
    it('возвращает все узлы дерева свёрнутыми', () => {
        expect(
            naturalSort(collapseAll(smallTree)).every(node => node.collapsed)
        ).toBe(true);
    });
});

describe('checkAll', () => {
    it('возвращает все узлы дерева отмеченными', () => {
        expect(
            naturalSort(checkAll(smallTree)).every(node => node.checked)
        ).toBe(true);
    });
});

describe('uncheckAll', () => {
    it('возвращает все узлы дерева не отмеченными', () => {
        expect(
            naturalSort(uncheckAll(smallTree)).every(node => !node.checked)
        ).toBe(true);
    });
});
