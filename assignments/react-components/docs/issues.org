* DONE Ошибка сборки: =require() of ES Module ... not supported=

  Известная проблема с Yarn 1, решение:
  https://github.com/storybookjs/storybook/issues/22431#issuecomment-1630086092

* INPROGRESS Ошибка запуска Storybook: =ENOSPC: System limit for number of file watchers reached=
  :LOGBOOK:
  CLOCK: [2024-03-08 Fri 19:20]--[2024-03-08 Fri 19:33] =>  0:13
  :END:

  Причина: превышен лимит количества файлов, отслеживаемых с помощью inotify:
  ~cat /proc/sys/fs/inotify/max_user_watches~;
  https://stackoverflow.com/a/55763478. Можно или увеличить лимит, или
  ограничить отслеживание файлов (Vite server, VSCode).

  Vite не отслеживает изменения в =.git/= и =node_modules/= по умолчанию:
  https://v2.vitejs.dev/config/#server-watch

  Отслеживание VSCode изменений в файлах может быть проблемой для больших
  воркспейсов (например, монорепозиториев). Возможное решение: исключить
  некоторые пути в настройках отслеживания (некоторые поддиректории =.git/= и
  =node_modules/= не отслеживаются по умолчанию; не помогло).
  https://code.visualstudio.com/docs/setup/linux#_visual-studio-code-is-unable-to-watch-for-file-changes-in-this-large-workspace-error-enospc

  Решение: увеличить лимит: ~fs.inotify.max_user_watches=524288~ в
  =/etc/sysctl.d/local.conf= и ~sudo service procps force-reload~. (По
  какой-то причине сбрасывается, после перезагрузки? гибернации?)
