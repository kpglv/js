import { mount } from "@vue/test-utils";
import Followings from "@/components/Followings.vue";
import User from "@/components/User.vue";

describe("Repositories.vue", () => {
  it("renders list of followings", () => {
    const login1 = "testUser1";
    const login2 = "testUser2";
    const followings = [{ login: login1 }, { login: login2 }];
    const wrapper = mount(Followings, {
      propsData: { followings },
      stubs: {
        User: `<div>{{ user.login }}</div>`
      }
    });
    expect(wrapper.contains(User)).toBe(true);
    expect(wrapper.text()).toMatch(login1);
    expect(wrapper.text()).toMatch(login2);
  });
});
