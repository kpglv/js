import { mount } from "@vue/test-utils";
import Repositories from "@/components/Repositories.vue";
import Repository from "@/components/Repository.vue";

describe("Repositories.vue", () => {
  it("renders list of repositories", () => {
    const name1 = "testRepo1";
    const name2 = "testRepo2";
    const repositories = [{ name: name1 }, { name: name2 }];
    const wrapper = mount(Repositories, {
      propsData: { repositories },
      stubs: {
        Repository: `<div>{{ repository.name }}</div>`
      }
    });
    expect(wrapper.contains(Repository)).toBe(true);
    expect(wrapper.text()).toMatch(name1);
    expect(wrapper.text()).toMatch(name2);
  });
});
