import { shallowMount } from "@vue/test-utils";
import UserProfile from "@/components/UserProfile.vue";

describe("UserProfile.vue", () => {
  it("renders login of the user", () => {
    const login = "testUser";
    const userProfile = { login: login };
    const wrapper = shallowMount(UserProfile, {
      propsData: { userProfile }
    });
    expect(wrapper.text()).toMatch(login);
  });
});
