import { shallowMount } from "@vue/test-utils";
import TeamCandidate from "@/components/TeamCandidate.vue";
import User from "@/components/User.vue";

describe("TeamCandidate.vue", () => {
  it("renders login of the team candidate", () => {
    const login = "testUser";
    const teamCandidate = { login: login };
    const wrapper = shallowMount(TeamCandidate, {
      propsData: { teamCandidate },
      stubs: {
        User: `<div>{{ user.login }}</div>`
      }
    });

    expect(wrapper.contains(User)).toBe(true);
    expect(wrapper.text()).toMatch(login);
  });
});
