import { mount } from "@vue/test-utils";
import TeamCandidates from "@/components/TeamCandidates.vue";
import TeamCandidate from "@/components/TeamCandidate.vue";

describe("TeamCandidates.vue", () => {
  it("renders list of team candidates", () => {
    const login1 = "testUser1";
    const login2 = "testUser2";
    const teamCandidates = [{ login: login1 }, { login: login2 }];
    const wrapper = mount(TeamCandidates, {
      propsData: { teamCandidates },
      stubs: {
        TeamCandidate: `<div>{{ teamCandidate.login }}</div>`
      }
    });
    expect(wrapper.contains(TeamCandidate)).toBe(true);
    expect(wrapper.text()).toMatch(login1);
    expect(wrapper.text()).toMatch(login2);
  });
});
