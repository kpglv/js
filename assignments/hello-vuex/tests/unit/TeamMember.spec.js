import { shallowMount } from "@vue/test-utils";
import TeamMember from "@/components/TeamMember.vue";
import User from "@/components/User.vue";

describe("TeamMember.vue", () => {
  it("renders login of the team member", () => {
    const login = "testUser";
    const teamMember = { login: login };
    const wrapper = shallowMount(TeamMember, {
      propsData: { teamMember },
      stubs: {
        User: `<div>{{ user.login }}</div>`
      }
    });

    expect(wrapper.contains(User)).toBe(true);
    expect(wrapper.text()).toMatch(login);
  });
});
