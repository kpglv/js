import { shallowMount } from "@vue/test-utils";
import User from "@/components/User.vue";

describe("User.vue", () => {
  it("renders login of the user", () => {
    const login = "testUser";
    const user = { login: login };
    const wrapper = shallowMount(User, {
      propsData: { user }
    });
    expect(wrapper.text()).toMatch(login);
  });
});
