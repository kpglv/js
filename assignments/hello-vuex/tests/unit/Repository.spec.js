import { shallowMount } from "@vue/test-utils";
import Repository from "@/components/Repository.vue";

describe("Repository.vue", () => {
  it("renders repository name", () => {
    const name = "testRepo";
    const repository = { name: name };
    const wrapper = shallowMount(Repository, {
      propsData: { repository },
      methods: {
        getLanguages() {}
      }
    });
    expect(wrapper.text()).toMatch(name);
  });
});
