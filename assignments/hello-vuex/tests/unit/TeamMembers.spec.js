import { mount } from "@vue/test-utils";
import TeamMembers from "@/components/TeamMembers.vue";
import TeamMember from "@/components/TeamMember.vue";

describe("TeamMember.vue", () => {
  it("renders list of team members", () => {
    const login1 = "testUser1";
    const login2 = "testUser2";
    const teamMembers = [{ login: login1 }, { login: login2 }];
    const wrapper = mount(TeamMembers, {
      propsData: { teamMembers },
      stubs: {
        TeamMember: `<div>{{ teamMember.login }}</div>`
      }
    });
    expect(wrapper.contains(TeamMember)).toBe(true);
    expect(wrapper.text()).toMatch(login1);
    expect(wrapper.text()).toMatch(login2);
  });
});
