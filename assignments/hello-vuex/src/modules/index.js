export function formatLocalDate(isoDateString) {
    return (new Date(isoDateString)).toLocaleDateString()
}