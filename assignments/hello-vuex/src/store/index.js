import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";
import UriTemplates from "uri-templates";

let authorizationToken = process.env.VUE_APP_GITHUB_TOKEN;
let mode = process.env.NODE_ENV;
if (authorizationToken && mode === "development") {
  Axios.defaults.headers.common[
    "Authorization"
  ] = `token ${authorizationToken}`;
}

export const getterTypes = {
  USER_PROFILE_URL: "USER_PROFILE_URL",
  USER_SEARCH_URL: "USER_SEARCH_URL",
  REPOSITORIES_URL: "REPOSITORIES_URL",
  FOLLOWING_URL: "FOLLOWING_URL",
  USER_PROFILE: "USER_PROFILE",
  REPOSITORIES: "REPOSITORIES",
  FOLLOWINGS: "FOLLOWINGS",
  TEAM_CANDIDATES: "TEAM_CANDIDATES",
  TEAM_MEMBERS: "TEAM_MEMBERS"
};
export const mutationTypes = {
  SET_USER_PROFILE: "SET_USER_PROFILE",
  SET_REPOSITORIES: "SET_REPOSITORIES",
  SET_FOLLOWINGS: "SET_FOLLOWINGS",
  SET_TEAM_CANDIDATES: "SET_TEAM_CANDIDATES",
  SET_TEAM_MEMBERS: "SET_TEAM_MEMBERS",
  ADD_TEAM_MEMBER: "ADD_TEAM_MEMBER",
  REMOVE_TEAM_MEMBER: "REMOVE_TEAM_MEMBER"
};
export const actionTypes = {
  SET_USER_PROFILE: "SET_USER_PROFILE",
  SET_TEAM_CANDIDATES: "SET_TEAM_CANDIDATES",
  ADD_TEAM_MEMBER: "ADD_TEAM_MEMBER",
  REMOVE_TEAM_MEMBER: "REMOVE_TEAM_MEMBER",
  SET_REPOSITORIES: "SET_REPOSITORIES",
  SET_FOLLOWINGS: "SET_FOLLOWINGS"
};

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userProfile: {},
    repositories: [],
    followings: [],
    teamCandidates: [],
    teamMembers: [],
    urls: {
      userProfileUrl: process.env.VUE_APP_GITHUB_USER_PROFILE_URL,
      teamCandidatesUrl: process.env.VUE_APP_GITHUB_TEAM_CANDIDATES_URL,
      userSearchUrl: process.env.VUE_APP_GITHUB_USER_SEARCH_URL
    }
  },
  getters: {
    [getterTypes.USER_PROFILE_URL]: state => state.urls.userProfileUrl,
    [getterTypes.USER_SEARCH_URL]: state => state.userSearchUrl,
    [getterTypes.REPOSITORIES_URL]: state => state.userProfile?.repos_url,
    [getterTypes.FOLLOWING_URL]: state => state.userProfile?.following_url,
    [getterTypes.USER_PROFILE]: state => state.userProfile,
    [getterTypes.REPOSITORIES]: state => state.repositories,
    [getterTypes.FOLLOWINGS]: state => state.followings,
    [getterTypes.TEAM_CANDIDATES]: state => state.teamCandidates,
    [getterTypes.TEAM_MEMBERS]: state => state.teamMembers
  },
  mutations: {
    [mutationTypes.SET_USER_PROFILE]: (state, payload) =>
      (state.userProfile = payload),
    [mutationTypes.SET_REPOSITORIES]: (state, payload) =>
      (state.repositories = payload),
    [mutationTypes.SET_FOLLOWINGS]: (state, payload) =>
      (state.followings = payload),
    [mutationTypes.SET_TEAM_CANDIDATES]: (state, payload) =>
      (state.teamCandidates = payload),
    [mutationTypes.SET_TEAM_MEMBERS]: (state, payload) =>
      (state.teamMembers = payload),
    [mutationTypes.ADD_TEAM_MEMBER]: (state, candidate) => {
      if (!state.teamMembers.find(member => member.id === candidate.id)) {
        state.teamMembers.push(candidate);
      }
    },
    [mutationTypes.REMOVE_TEAM_MEMBER]: (state, memberId) => {
      state.teamMembers = state.teamMembers.filter(
        member => member.id !== memberId
      );
    }
  },
  actions: {
    [actionTypes.SET_USER_PROFILE]: async context => {
      let { data } = await Axios.get(context.state.urls.userProfileUrl);
      context.commit(mutationTypes.SET_USER_PROFILE, data);
    },
    [actionTypes.SET_TEAM_CANDIDATES]: async (context, query) => {
      let url = query
        ? UriTemplates(context.state.urls.userSearchUrl).fill({
            q: query,
            per_page: 5
          })
        : UriTemplates(context.state.urls.teamCandidatesUrl).fill({
            since: 50000000,
            per_page: 5
          });
      let { data } = await Axios.get(url);
      context.commit(
        mutationTypes.SET_TEAM_CANDIDATES,
        data.items ? data.items : data
      );
    },
    [actionTypes.ADD_TEAM_MEMBER]: (context, candidate) => {
      context.commit(mutationTypes.ADD_TEAM_MEMBER, candidate);
    },
    [actionTypes.REMOVE_TEAM_MEMBER]: (context, memberId) => {
      context.commit(mutationTypes.REMOVE_TEAM_MEMBER, memberId);
    },
    [actionTypes.SET_REPOSITORIES]: async context => {
      let repositoriesUrl = context.state.userProfile?.repos_url;
      if (repositoriesUrl) {
        let { data } = await Axios.get(repositoriesUrl);
        context.commit(mutationTypes.SET_REPOSITORIES, data);
      }
    },
    [actionTypes.SET_FOLLOWINGS]: async context => {
      let followingsUrl = UriTemplates(
        context.state.userProfile?.following_url
      ).fill({ "/other_user": "" });
      if (followingsUrl) {
        let { data } = await Axios.get(followingsUrl);
        context.commit(mutationTypes.SET_FOLLOWINGS, data);
      }
    }
  }
});
