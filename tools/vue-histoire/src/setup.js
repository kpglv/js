import { createPinia } from 'pinia';
import { defineSetupVue3 } from '@histoire/plugin-vue';
import router from 'wise-cats-spa/src/router';
import { i18n } from 'wise-cats-spa/src/i18n';

// import { useWiseCatsStore } from './stores/wise-cats';
// import { someWiseCats } from '../test/data';

import Wrapper from './Wrapper.vue';
import './tailwind.css';

export const setupVue3 = defineSetupVue3(
    ({ app, story, variant, addWrapper }) => {
        // add initializations
        addWrapper(Wrapper);

        app.use(createPinia());
        app.use(router);
        app.use(i18n);

        // add test data to the wise cats store
        // const store = useWiseCatsStore();
        // store.setWiseCats(someWiseCats);

        // navigate manually to the home page
        // router.replace('/');
    }
);
