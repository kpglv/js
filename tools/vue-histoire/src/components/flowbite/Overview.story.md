---
group: 'flowbite'
icon: 'carbon:bookmark'
---

# Flowbite Vue 3

Vue component library based on Tailwind CSS.

-   https://flowbite-vue.com/
