---
group: 'app'
icon: 'carbon:bookmark'
---

# Wise Cats App

An example app that uses Vue 3 and Tailwind CSS.
