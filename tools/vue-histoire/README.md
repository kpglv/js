# vue-histoire

An environment for isolated development and visual testing
for Vue components (like Storybook).

This base contains stories for components and pages of a simple Vue 3 SPA
(Wise Cats).

-   Histoire: A new way to write stories https://histoire.dev/
-   Component Driven User Interfaces https://www.componentdriven.org/

Also contains stories for components from some third-party
component libraries like Flowbite and Headless UI.

-   Flowbite Vue 3: Vue component library based on Tailwind CSS
    https://flowbite-vue.com/
-   Completely unstyled, fully accessible UI components, designed
-   to integrate beautifully with Tailwind CSS. https://headlessui.com/

## Demo

Deployed on Vercel: https://js-vue-histoire.vercel.app/

## Getting Started

Start the development server

```
yarn story:dev
```

Generate a static production build

```
yarn story:build
```

Serve the production build for preview (must be built first)

```
yarn story:preview
```
