import FlowbitePlugin from 'flowbite/plugin';
import typography from '@tailwindcss/typography';

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './src/**/*.vue',
        '../../node_modules/flowbite-vue/**/*.{js,jsx,ts,tsx,vue}',
        '../../node_modules/flowbite/**/*.{js,jsx,ts,tsx}',
        '../wise-cats-spa/src/**/*.{js,jsx,ts,tsx,vue}',
        // for builds on Vercel (that does not use shared node_modules)
        './node_modules/flowbite-vue/**/*.{js,jsx,ts,tsx,vue}',
        './node_modules/flowbite/**/*.{js,jsx,ts,tsx}',
    ],
    theme: {
        extend: {},
    },
    plugins: [FlowbitePlugin, typography],
};
