import { defineConfig, defaultColors } from 'histoire';
import { HstVue } from '@histoire/plugin-vue';

export default defineConfig({
    setupFile: '/src/setup.js',
    tree: {
        groups: [
            {
                id: 'top',
                title: '',
            },
            {
                id: 'flowbite',
                title: 'Flowbite',
            },
            { id: 'headlessui', title: 'Headless UI' },
            { id: 'app', title: 'Wise Cats App' },
        ],
        order: (a, b) => {
            if (/Overview|Welcome/i.test(a)) {
                return -1;
            }
            return a.localeCompare(b);
        },
    },
    theme: {
        colors: {
            primary: defaultColors.green,
        },
    },
    plugins: [HstVue()],
});
