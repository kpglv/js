import { resolve } from 'path';
import { defineConfig } from 'vite';
import { viteStaticCopy } from 'vite-plugin-static-copy';
import { vitePluginVersionMark } from 'vite-plugin-version-mark';

export default defineConfig({
    appType: 'mpa',
    build: {
        rollupOptions: {
            input: {
                main: resolve(__dirname, 'index.html'),
                example: resolve(__dirname, 'src/example/index.html'),
                usingMonorepos: resolve(
                    __dirname,
                    'src/using-monorepos/index.html'
                ),
                composingReactComponents: resolve(
                    __dirname,
                    'src/composing-react-components/index.html'
                ),
                themingReactComponents: resolve(
                    __dirname,
                    'src/theming-react-components/index.html'
                ),
            },
        },
    },
    plugins: [
        viteStaticCopy({
            targets: [
                {
                    src: 'src/**/*.md',
                    dest: '/',
                },
            ],
            structured: true,
        }),
        vitePluginVersionMark({
            // command: 'git describe --tags',
            command:
                "git log -1 --format='%cd (%h)' --date=format:'%Y-%m-%d'",
            ifShortSHA: true,
            ifMeta: true,
            ifLog: true,
            ifGlobal: true,
        }),
    ],
});
