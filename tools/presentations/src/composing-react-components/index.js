import Reveal from 'reveal.js';
import 'reveal.js/dist/reveal.css';
import 'reveal.js/dist/theme/blood.css';

import Highlight from 'reveal.js/plugin/highlight/highlight.esm.js';
import 'reveal.js/plugin/highlight/monokai.css';
import js from 'highlight.js/lib/languages/javascript';
// import 'highlight.js/styles/github.css';

import Markdown from 'reveal.js/plugin/markdown/markdown.esm.js';

import './index.css';

const deck = new Reveal({ plugins: [Highlight, Markdown] });
deck.initialize({
    hash: true,
    slideNumber: true,
    highlight: {
        beforeHighlight: hljs => hljs.registerLanguage('js', js),
    },
});
