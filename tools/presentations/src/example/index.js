import Reveal from 'reveal.js';
import Highlight from 'reveal.js/plugin/highlight/highlight.esm.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.esm.js';
import Math from 'reveal.js/plugin/math/math.esm.js';
import 'reveal.js/dist/reveal.css';
import 'reveal.js/dist/theme/blood.css';
import 'reveal.js/plugin/highlight/zenburn.css';

const deck = new Reveal({ plugins: [Highlight, Markdown, Math] });
deck.initialize({ hash: true, slideNumber: true });
