# presentations

Presentations using Reveal.js.

## Topics

-   Using Monorepos
-   Composing React Components
-   Theming React Components

TODO:

-   Customizing Data Processing Pipelines: Middleware, Interceptors
-   Polylith in Depth
-   History of Clojure https://dl.acm.org/doi/pdf/10.1145/3386321
    -   Information systems vs artificial systems: irregularity
        and constant change
    -   Data-orientation, programming to abstractions,
        laziness, functional programming (immutability, side causes/effects),
        state management (refs, STM, MVCC), concurrency
-   JS vs lisps
-   Of Coupling and Cohesion
    -   The cost of software is dominated by the cost of maintenance,
        the cost of maintenance is dominated by the cost of changes that
        ripple through the system, and effective software design
        minimizes the chance of changes propagating
        (https://tidyfirst.substack.com/p/coupling-and-cohesion)
-   TypeScript configuration, sensible defaults:
    https://www.typescriptlang.org/tsconfig/
    -   module: esnext, moduleResolution: bundler

## Getting Started

Develop & view

```
yarn dev
```

Build

```
yarn build
```
