import type { Meta, StoryObj } from '@storybook/react';
import { Card } from 'flowbite-react';

// component description

const Description = {
    title: 'Flowbite/Card',
    component: Card,
    args: {},
    tags: ['autodocs'],
} satisfies Meta<typeof Card>;
export default Description;

// stories

type Story = StoryObj<typeof Description>;

export const Default: Story = {};
