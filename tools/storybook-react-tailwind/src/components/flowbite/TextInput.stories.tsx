import type { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { TextInput } from 'flowbite-react';
import { HiMail } from 'react-icons/hi';

const meta = {
    title: 'Flowbite/TextInput',
    component: TextInput,
    parameters: {
        layout: 'centered',
    },
    args: {
        onClick: action('on-click'),
    },
    argTypes: {},
    tags: ['autodocs'],
} satisfies Meta<typeof TextInput>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
    name: 'Default',
    args: {
        placeholder: 'name@flowbite.com',
        helperText: (
            <>
                All your personal data will be submitted to{' '}
                <a
                    className="text-cyan-500"
                    href="https://example.com"
                    target="_blank"
                >
                    Example Corp.
                </a>
            </>
        ),
    },
};

export const Icon: Story = {
    name: 'Icon',
    args: {
        icon: HiMail,
    },
};

export const Addon: Story = {
    name: 'Addon',
    args: {
        addon: '@',
    },
};

export const Shadow: Story = {
    name: 'Shadow',
    args: {
        shadow: true,
    },
};
