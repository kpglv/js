import type { Meta, StoryObj } from '@storybook/react';
import { Dropdown } from 'flowbite-react';

// component description

const Description = {
    title: 'Flowbite/Dropdown',
    component: Dropdown,
    argTypes: {
        placement: {
            control: 'inline-radio',
            options: ['top', 'right', 'bottom', 'left'],
        },
    },
    args: { label: 'Dropdown' },
    tags: ['autodocs'],
} satisfies Meta<typeof Dropdown>;
export default Description;

// stories

type Story = StoryObj<typeof Description>;

export const Default: Story = {
    args: {},
};
