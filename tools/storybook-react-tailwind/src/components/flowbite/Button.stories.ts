import type { Meta, StoryObj } from '@storybook/react';
import { Button } from 'flowbite-react';

// component description

const Description = {
    title: 'Flowbite/Button',
    component: Button,
    argTypes: {
        href: { control: 'text' },
        tag: { control: 'text' },
        color: { control: 'text' },
        gradientMonochrome: { control: 'text' },
        shadow: { control: 'text' },
        size: { control: 'select', options: ['xs', 'sm', 'md', 'lg', 'xl'] },
        outline: { control: 'boolean' },
        pill: { control: 'boolean' },
        square: { control: 'boolean' },
        isProcessing: { control: 'boolean' },
        disabled: { control: 'boolean' },
        prefix: { control: 'text' },
        suffix: { control: 'text' },
        children: { control: 'text' },
    },
    tags: ['autodocs'],
} satisfies Meta<typeof Button>;
export default Description;

// stories

type Story = StoryObj<typeof Description>;

export const Default: Story = {
    args: {
        children: 'Click me',
        size: 'md',
    },
};
