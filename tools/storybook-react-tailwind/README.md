# storybook-react-tailwind

An environment for isolated development and visual testing for React components.

Uses

-   Tailwind CSS styling framework
-   Flowbite component library (React version)

## Getting Started

```
yarn storybook
```
