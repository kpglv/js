import type { Meta, StoryObj } from '@storybook/vue3';
import { FwbCard } from 'flowbite-vue';
import CardHtml from './CardHtml.vue';

// component description

const Description = {
    title: 'Flowbite/Card',
    component: FwbCard,
    argTypes: {
        default: {
            control: 'select',
            options: ['empty', 'image', 'text', 'html'],
            mapping: {
                empty: '',
                text: 'A card',
                html: CardHtml,
                image: '',
            },
        },
    },
    args: {
        default: 'text',
    },
    tags: ['autodocs'],
} satisfies Meta<typeof FwbCard>;
export default Description;

// stories

type Story = StoryObj<typeof Description>;

export const Default: Story = {
    render: args => ({
        components: { FwbCard, CardHtml },
        setup(props, { attrs, slots }) {
            return { args, props, attrs, slots };
        },
        template: `
            <fwb-card v-bind="args">
                <CardHtml />
            </fwb-card>
        `,
    }),
};
