import type { Meta, StoryObj } from '@storybook/vue3';
import { FwbDropdown } from 'flowbite-vue';

// component description

const Description = {
    title: 'Flowbite/Dropdown',
    component: FwbDropdown,
    argTypes: {
        text: { control: 'text' },
        placement: {
            control: 'inline-radio',
            options: ['top', 'right', 'bottom', 'left'],
        },
        default: {
            control: 'text',
            description: 'Slot content',
        },
    },
    args: {
        text: 'Dropdown',
        default: 'Dropdown content',
    },
    tags: ['autodocs'],
} satisfies Meta<typeof FwbDropdown>;
export default Description;

// stories

type Story = StoryObj<typeof Description>;

export const Default: Story = {
    render: args => ({
        components: { FwbDropdown },
        setup(props, { attrs, slots }) {
            return { args, props, attrs, slots };
        },
        template: `
            <fwb-dropdown v-bind="args">
                {{args.default}}
            </fwb-dropdown>
        `,
    }),
};
