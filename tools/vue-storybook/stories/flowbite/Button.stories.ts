import type { Meta, StoryObj } from '@storybook/vue3';
import { FwbButton } from 'flowbite-vue';

// component description

const Description = {
    title: 'Flowbite/Button',
    component: FwbButton,
    argTypes: {
        href: { control: 'text' },
        tag: { control: 'text' },
        color: { control: 'text' },
        gradient: { control: 'text' },
        shadow: { control: 'text' },
        size: { control: 'select', options: ['xs', 'sm', 'md', 'lg', 'xl'] },
        outline: { control: 'boolean' },
        pill: { control: 'boolean' },
        square: { control: 'boolean' },
        loading: { control: 'boolean' },
        disabled: { control: 'boolean' },
        default: {
            control: 'text',
            description: 'Slot content',
        },
        prefix: { control: 'text' },
        suffix: { control: 'text' },
    },
    tags: ['autodocs'],
} satisfies Meta<typeof FwbButton>;
export default Description;

// stories

type Story = StoryObj<typeof Description>;

export const Default: Story = {
    render: args => ({
        components: { FwbButton },
        setup(props, { attrs, slots }) {
            return { args, props, attrs, slots };
        },
        template: `
            <fwb-button>
                <template #prefix>{{args.prefix}}</template>
                    {{args.default}}
                <template #suffix>{{args.suffix}}</template>
            </fwb-button>
        `,
    }),
    args: {
        default: 'Click me',
        size: 'md',
    },
};
