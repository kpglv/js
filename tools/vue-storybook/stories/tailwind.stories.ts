import type { Meta, StoryObj } from '@storybook/vue3';
import MyButton from '@kpglv/vue-components/tailwind/input/Button.vue';

// more on how to set up stories at: https://storybook.js.org/docs/writing-stories

// component description

const Description = {
    // the component of interest
    component: MyButton,
    // component's name displayed in navigation, should be unique project-wise
    title: 'Tailwind/Button',
    // component's arguments and their controls
    argTypes: {
        onClick: { action: 'clicked' },
        kind: { control: 'select', options: ['default', 'text', 'outline'] },
        role: {
            control: 'select',
            options: [
                'primary',
                'secondary',
                'success',
                'error',
                'warning',
                'info',
            ],
        },
        size: { control: 'select', options: ['small', 'medium', 'large'] },
        disabled: { control: 'boolean' },
        default: {
            control: 'text',
            description: 'Slot content',
        },
    },
    // arguments' default values
    args: {
        onClick: () => console.log('clicked!'),
        role: 'primary',
        size: 'medium',
        disabled: false,
        default: 'Click me',
    },
    // default display parameters for stories (background, grid, outlines, etc)
    parameters: {
        backgrounds: {
            default: 'light',
        },
    },
    // wrappers for stories
    decorators: [
        () => ({
            template:
                '<div class="padding-decorator" style="margin: 3em;"><story /></div>',
        }),
    ],
    // this component will have an automatically generated docsPage entry:
    // https://storybook.js.org/docs/writing-docs/autodocs
    tags: ['autodocs'],
} satisfies Meta<typeof MyButton>;
export default Description;

// stories

type Story = StoryObj<typeof Description>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */

export const Default: Story = {
    args: {},
};

export const Text: Story = {
    args: {
        kind: 'text',
    },
};

export const Outline: Story = {
    args: {
        kind: 'outline',
    },
};

export const Primary: Story = {
    args: {},
};

export const Secondary: Story = {
    args: {
        role: 'secondary',
    },
};

export const Small: Story = {
    args: {
        size: 'small',
    },
};

export const Medium: Story = {
    args: {},
};

export const Large: Story = {
    args: {
        size: 'large',
    },
};

export const Disabled: Story = {
    args: {
        disabled: true,
    },
};

export const Clickable: Story = {
    name: 'Custom action on click',
    args: {
        onClick: () => console.log('clicked some more'),
    },
};
