import type { Meta, StoryObj } from '@storybook/vue3';

import MyButton from '@kpglv/vue-components/unstyled/input/Button.vue';

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
const meta = {
    title: 'Unstyled/Button',
    component: MyButton,
    // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/writing-docs/autodocs
    tags: ['autodocs'],
    argTypes: {
        onClick: { action: 'clicked' },
    },
    args: {}, // default value
} satisfies Meta<typeof MyButton>;

export default meta;
type Story = StoryObj<typeof meta>;
