import type { Meta, StoryObj } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import { App, Button } from 'framework7-react';

const meta = {
    title: 'Framework7/Button',
    component: Button,
    decorators: [
        Story => (
            <App theme="auto" name="My App">
                <div style={{ padding: '1rem' }}>
                    <Story />
                </div>
            </App>
        ),
    ],
    parameters: {
        layout: 'centered',
    },
    args: {
        onClick: action('on-click'),
        children: 'Click me',
        href: false,
    },
    argTypes: {
        tonal: { type: 'boolean' },
        fill: { type: 'boolean' },
        outline: { type: 'boolean' },
        small: { type: 'boolean' },
        large: { type: 'boolean' },
    },
    tags: ['autodocs'],
} satisfies Meta<typeof Button>;

export default meta;
type Story = StoryObj<typeof meta>;

const exampleStyle = {
    display: 'flex',
    gap: '1rem',
    'align-items': 'center',
    padding: '1rem',
};

/**
 * Represent an action, handles clicks
 */
export const Default: Story = {
    args: {},
};

/**
 * Square and round
 */
export const Usual: Story = {
    render: args => (
        <div style={exampleStyle}>
            <Button {...args} />
            <Button {...{ ...args, round: true }} />
        </div>
    ),
};

/**
 * Square and round
 */
export const Tonal: Story = {
    render: args => (
        <div style={exampleStyle}>
            <Button {...args} />
            <Button {...{ ...args, round: true }} />
        </div>
    ),
    args: {
        tonal: true,
    },
};

/**
 * Square and round
 */
export const Fill: Story = {
    render: args => (
        <div style={exampleStyle}>
            <Button {...args} />
            <Button {...{ ...args, round: true }} />
        </div>
    ),
    args: {
        fill: true,
    },
};

/**
 * Square and round
 */
export const Outline: Story = {
    render: args => (
        <div style={exampleStyle}>
            <Button {...args} />
            <Button {...{ ...args, round: true }} />
        </div>
    ),
    args: {
        outline: true,
    },
};

/**
 * Square and round
 */
export const Raised: Story = {
    render: args => (
        <div style={exampleStyle}>
            <Button {...args} />
            <Button {...{ ...args, round: true }} />
        </div>
    ),
    args: {
        raised: true,
    },
};

/**
 * Small and large
 */
export const Size: Story = {
    render: args => (
        <div style={exampleStyle}>
            <Button {...{ ...args, small: true }} />
            <Button {...{ ...args, small: true, round: true }} />
            <Button {...{ ...args, large: true }} />
            <Button {...{ ...args, large: true, round: true }} />
        </div>
    ),
    args: {
        fill: true,
        raised: true,
    },
};

/**
 * Small and large, round and square
 */
export const Loading: Story = {
    render: args => (
        <div style={exampleStyle}>
            <Button {...{ ...args, small: true }} />
            <Button {...{ ...args, small: true, round: true }} />
            <Button {...{ ...args, large: true }} />
            <Button {...{ ...args, large: true, round: true }} />
        </div>
    ),
    args: {
        preloader: true,
        loading: true,
        fill: true,
        raised: true,
    },
};

/**
 * Usual, tonal, fill, outline and raised
 */
export const Disabled: Story = {
    render: args => (
        <div style={exampleStyle}>
            <Button {...{ ...args }} />
            <Button {...{ ...args, tonal: true }} />
            <Button {...{ ...args, fill: true }} />
            <Button {...{ ...args, outline: true }} />
            <Button {...{ ...args, fill: true, raised: true }} />
        </div>
    ),
    args: {
        disabled: true,
    },
};
