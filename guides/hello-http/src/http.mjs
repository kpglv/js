// https://nodejs.org/api/http.html
// https://www.digitalocean.com/community/tutorials/how-to-create-a-web-server-in-node-js-with-the-http-module


import http from 'http';
import fs from 'fs';

// request handling

const home = (_, res) => {
  res.setHeader("Content-Type", "text/html");
  res.writeHead(200);
  res.end('<html><body><h1>Hello, Node.js http module!</h1></body></html>');
};

const json = (_, res) => {
  res.setHeader("Content-Type", "application/json");
  res.writeHead(200);
  res.end(JSON.stringify({message: 'Hello, Node.js http module!'}));
};

const csv = (_, res) => {
  res.setHeader("Content-Type", "text/csv");
  // as a file to download
  res.setHeader("Content-Disposition", "attachment;filename=oceanpals.csv");
  res.writeHead(200);
  res.end('id,name,email\n1,Sammy Shark,shark@ocean.com');
};

const file = (_, res) => {
  // file contents can be read to a variable ahead of time instead of reading
  // it on each request
  fs.promises.readFile(new URL('../resources/hello.html', import.meta.url))
    .then(contents => {
      res.setHeader("Content-Type", "text/html");
      res.writeHead(200);
      res.end(contents);
    })
    .catch(error => {
      res.writeHead(500);
      res.end(error.message);
      return;
    });
};

const notFound = (_, res) => {
  res.setHeader("Content-Type", "application/json");
  res.writeHead(404);
  res.end(JSON.stringify({message: 'requested resource is not found'}));
};

// routing

const routes = {
  "/": home,
  "/json": json,
  "/csv": csv,
  "/file": file,
};

const requestListener = (req, res) => {
  const handler = routes[req.url] || notFound;
  handler(req, res);
};

// server

const host = 'localhost';
const port = 8000;
const server = http.createServer(requestListener);

server.listen(port, host, () => {
  console.log(`HTTP server is running on http://${host}:${port}`);
});
