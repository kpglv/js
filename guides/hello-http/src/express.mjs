// https://expressjs.com/en/starter/hello-world.html

import express from 'express';

const app = express();
const port = 8000;

// routing and request handling

// curl http://localhost:8000; set 'Accept' header to get different response
// formats
app.get('/', (req, res) => {
  res.format({
    text: () => {
      res.send('Hello, Express!');
    },
    html: function(){
      res.send('<html><body><h1>Hello, Express!</h1></body></html>');
    },
    json: () => {
      res.json({ message: 'Hello, Express!' });
    },
  });
});

// curl http://localhost:8000/hello.html
app.use(express.static('resources'));

// server

app.listen(port, () => {
  console.log(`Express HTTP server listening on port ${port}`);
});
