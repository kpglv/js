# hello-http

Exploring libraries and frameworks that provide HTTP server/client
functionality.

- Node.js
  - HTTP module https://nodejs.org/api/http.html
  - Express web framework https://expressjs.com/

## Getting Started

Explore the code in the `src` directory.

Run code examples with node, e.g.

```
node src/express.mjs
```
