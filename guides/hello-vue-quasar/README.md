# hello-vue-quasar

An example SPA (application shell) built with Vue3 and Quasar framework.

## Getting Started

Install dependencies

```
yarn
```

Install Quasar CLI globally

```
yarn global add @quasar/cli
```

## Developing

Build and serve in development mode

```
quasar dev
```

## Building

Run linter

```
yarn run lint
```

Build for production

```
quasar build
```

## Testing

Unit tests

```
yarn test:unit
```

Unit tests, brief report

```
yarn test:unit:ci
```

e2e tests: start a build to test and a browser to run tests

```
yarn test:e2e
```

e2e tests: start a build to test and a headless browser to run tests

```
yarn test e2e:ci
```

## Updating project dependencies

```
yarn upgrade
```

Update testing tools

```
quasar ext add @quasar/testing-unit-jest@next
quasar ext add @quasar/testing-e2e-cypress@next
```
