export default {
    app: {
        title: 'Пример приложения',
        sections: {
            recent: {
                title: 'Недавние',
            },
            favourites: {
                title: 'Избранное',
            },
            public: {
                title: 'Общие документы',
            },
            private: {
                title: 'Мои документы',
            },
            trash: {
                title: 'Корзина',
            },
            settings: {
                title: 'Настройки',
            },
            help: {
                title: 'Помощь',
            },
        },
        pages: {
            index: {
                title: 'Главная страница',
            },
            settings: {
                title: 'Настройки',
            },
            resources: {
                title: 'Ресурсы',
            },
            resource: {
                title: 'Ресурс',
            },
            searchResults: {
                title: 'Результаты поиска',
            },
            error404: {
                title: 'Страница не найдена',
            },
        },
        menu: {
            appSettings: {
                label: 'Настройки',
            },
            login: {
                label: 'Войти',
            },
            logout: {
                label: 'Выйти',
            },
        },
        components: {
            resourcesList: {
                empty: 'Здесь пока ничего нет.',
            },
            messageList: {
                title: 'Сообщения',
                empty: 'Здесь пока ничего нет.',
                message: {
                    key: 'Ключ',
                    value: 'Значение',
                },
            },
            resourceCard: {
                title: 'Данные',
                message: 'Сообщение',
                status: 'Статус',
            },
        },
        navigation: {
            goHome: 'На главную',
            login: 'Войти',
        },
        search: {
            placeholder: 'Искать',
        },
    },
    actions: {
        resource: {
            folder: {
                label: 'Папка',
            },
            document: {
                label: 'Документ',
            },
        },
        item: {
            create: {
                folder: 'Создать папку',
                user: 'Создать пользователя',
                group: 'Создать группу',
            },
            open: 'Открыть',
            select: 'Выбрать',
            copy: 'Копировать',
            cut: 'Вырезать',
            rename: 'Переименовать',
            delete: 'Удалить',
            properties: 'Свойства',
            permissions: 'Разрешения',
            addToFavourites: 'Добавить в избранное',
        },
        list: {
            selectAll: 'Выбрать все',
            copy: 'Копировать',
            cut: 'Вырезать',
            delete: 'Удалить',
        },
    },
    notifications: {
        folderCreated: 'Папка создана',
        accessDenied: 'Доступ запрещён',
        unknownError: 'Неизвестная ошибка',
        unknownNetworkError: 'Неизвестная ошибка сети',
        authorizationError: 'Ошибка авторизации',
    },
    validation: {
        email: 'Пожалуйста, введите валидный email',
    },
    settings: {
        language: 'Язык',
        english: 'English',
        russian: 'Русский',
    },
    submit: 'Отправить',
    reset: 'Сбросить',
    notImplemented: 'Не реализовано',
};
