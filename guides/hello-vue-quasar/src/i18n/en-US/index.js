export default {
    app: {
        title: 'Example App',
        sections: {
            recent: {
                title: 'Recent',
            },
            favourites: {
                title: 'Favourites',
            },
            public: {
                title: 'Public Documents',
            },
            private: {
                title: 'Private Documents',
            },
            trash: {
                title: 'Trash',
            },
            settings: {
                title: 'Settings',
            },
            help: {
                title: 'Help and Support',
            },
        },
        pages: {
            index: {
                title: 'Main page',
            },
            settings: {
                title: 'Settings',
            },
            resources: {
                title: 'Resources',
            },
            resource: {
                title: 'Resource',
            },
            searchResults: {
                title: 'Search results',
            },
            error404: {
                title: 'Page not found',
            },
        },
        menu: {
            appSettings: {
                label: 'Settings',
            },
            login: {
                label: 'Log In',
            },
            logout: {
                label: 'Log Out',
            },
        },
        components: {
            resourcesList: {
                empty: 'No resources.',
            },
            messageList: {
                title: 'Messages',
                empty: 'Nothing to see there... yet.',
                message: {
                    key: 'Key',
                    value: 'Value',
                },
            },
            resourceCard: {
                title: 'Data',
                message: 'Message',
                status: 'Status',
            },
        },
        navigation: {
            goHome: 'Go home',
            login: 'Log in',
        },
        search: {
            placeholder: 'Search',
        },
    },
    actions: {
        resource: {
            folder: {
                label: 'Folder',
            },
            document: {
                label: 'Document',
            },
        },
        item: {
            create: {
                folder: 'Create folder',
                user: 'Create user',
                group: 'Create group',
            },
            open: 'Open',
            select: 'Select',
            copy: 'Copy',
            cut: 'Cut',
            rename: 'Rename',
            delete: 'Delete',
            properties: 'Properties',
            permissions: 'Permissions',
            addToFavourites: 'Add to favourites',
        },
        list: {
            selectAll: 'Select all',
            copy: 'Copy',
            cut: 'Cut',
            delete: 'Delete',
        },
    },
    notifications: {
        folderCreated: 'Folder created',
        accessDenied: 'Access denied',
        unknownError: 'Unknown error',
        unknownNetworkError: 'Unknown network error',
        authorizationError: 'Authorization error',
    },
    validation: {
        email: 'Please enter valid email',
    },
    settings: {
        language: 'Language',
        english: 'English',
        russian: 'Русский',
    },
    submit: 'Submit',
    reset: 'Reset',
    notImplemented: 'Not implemented',
};
