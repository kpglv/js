/* eslint-disable */
import enUS from 'src/i18n/en-US';
import ru from 'src/i18n/ru';

export default {
    'en-US': enUS,
    ru,
};
