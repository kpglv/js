import MainLayout from 'layouts/MainLayout.vue';

const routes = [
    {
        path: '',
        component: MainLayout,
        children: [
            {
                name: 'Index',
                path: '',
                component: () => import('pages/Index.vue'),
            },

            {
                name: 'Resources',
                path: '/sections/:sectionId/:resourceId?',
                component: () => import('pages/Resources.vue'),
                props: ({ params }) => ({
                    sectionId: params.sectionId,
                    resourceId: Number.parseInt(params.resourceId, 10),
                }),
            },

            {
                name: 'Settings',
                path: '/sections/settings',
                component: () => import('pages/Settings.vue'),
            },

            {
                name: 'Search',
                path: '/search',
                component: () => import('pages/SearchResults.vue'),
            },
        ],
    },

    {
        name: 'NotFound',
        path: '/:catchAll(.*)*',
        component: () => import('layouts/FullscreenLayout.vue'),
        children: [
            {
                path: '',
                component: () => import('pages/Error404.vue'),
            },
        ],
    },
];

export default routes;
