/**
 * Помощник работы с api-клиентом
 * @module src/composables/useApiHelper
 */

import { exampleApi } from 'boot/axios';

const apiHelper = {
    /**
     * Add headers with access token on each request.
     * @param {string} accessToken
     */
    setAccessToken(accessToken) {
        exampleApi.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
    },

    /**
     * Remove all predefined per-request headers.
     * @param {string} accessToken
     */
    resetAccessToken() {
        exampleApi.defaults.headers.common = {};
    },
};

export default () => apiHelper;
