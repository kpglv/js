/**
 * Всплывающие уведомления.
 * @module src/composables/useAlert
 */

import { Notify } from 'quasar';

const alert = {
    /**
     * An error.
     * @param {string} message
     */
    error(message) {
        Notify.create({
            type: 'negative',
            message,
        });
    },

    /**
     * Success.
     * @param {string} message
     */
    success(message) {
        Notify.create({
            type: 'positive',
            message,
        });
    },

    /**
     * Information message.
     * @param {string} message
     */
    info(message) {
        Notify.create({
            type: 'info',
            message,
        });
    },

    /**
     * A warning.
     * @param {string} message
     */
    warning(message) {
        Notify.create({
            type: 'warning',
            message,
        });
    },
};

export default () => alert;
