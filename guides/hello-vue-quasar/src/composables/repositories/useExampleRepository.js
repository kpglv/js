/* eslint-disable no-console */
/**
 * Resource repository example.
 * @module src/composables/repositories/useExampleRepository
 */

import { exampleApi } from 'boot/axios';

const exampleRepository = {
    /**
     * Get public resource.
     * @returns {object}
     */
    async getPublic() {
        try {
            const { data } = await exampleApi.get('/hello');
            return data;
        } catch (error) {
            return {
                status: 'error',
                message: error.message,
            };
        }
    },

    /**
     * Get protected resource (requires authentication).
     * @returns {object}
     */
    async getRestricted() {
        try {
            const { data } = await exampleApi.get('/restricted');
            return data;
        } catch (error) {
            return {
                status: 'error',
                message: error.message,
            };
        }
    },
};

export default () => exampleRepository;
