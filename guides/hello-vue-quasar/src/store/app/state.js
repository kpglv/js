export default () => ({
    // app info
    app: {},
    // current user info ('guest' if not authenticated)
    user: 'guest',
    // user ID token
    idToken: '',
    // app access token
    accessToken: '',
    // app refresh token
    refreshToken: '',
    // app settings (separate for each user)
    settings: {
        guest: {
            language: 'ru',
        },
    },
    // available app settings
    availableSettings: {
        language: {
            name: 'language',
            type: 'select',
            default: 'ru',
            options: [
                { value: 'en-US', label: 'English' },
                { value: 'ru', label: 'Русский' },
            ],
        },
    },
});
