/**
 * Set auth info.
 * @param {object} state
 * @param {string} refreshToken
 */
export const setAuthInfo = (
    state,
    { idToken, accessToken, refreshToken, user, app }
) => {
    state.idToken = idToken;
    state.accessToken = accessToken;
    state.refreshToken = refreshToken;
    state.user = user;
    state.app = app;
};

/**
 * Set app settings for current user.
 * @param {object} state
 * @param {string} userId ('guest' if unauthenticated)
 * @param {array} settings
 */
export const setSettings = (state, { userId, settings }) => {
    state.settings[userId] = settings;
};
