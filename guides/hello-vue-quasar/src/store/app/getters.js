/**
 * Get app info.
 * @param {object} state
 * @returns {object}
 */
export const app = (state) => state.app;

/**
 * Get current user info.
 * @param {object} state
 * @returns {object}
 */
export const user = (state) => state.user;

/**
 * Get current user's ID token.
 * @param {object} state
 * @returns {string}
 */
export const idToken = (state) => state.idToken;

/**
 * Get app access token.
 * @param {object} state
 * @returns {string}
 */
export const accessToken = (state) => state.accessToken;

/**
 * Get app refresh token.
 * @param {object} state
 * @returns {string}
 */
export const refreshToken = (state) => state.refreshToken;

/**
 * Get app settings for the current user.
 * @param {object} state
 * @param {object} getters
 * @returns {object}
 */
export const settings = (state, getters) =>
    state.settings[getters.user?.sub || 'guest'];

/**
 * Get all available app settings.
 * @param {object} state
 * @returns {object}
 */
export const availableSettings = (state) => state.availableSettings;

/**
 * Returns true if the current user is authenticated.
 * @param {object} state
 * @param {object} getters
 * @returns {boolean}
 */
export const isAuthenticated = (state, getters) => !!getters.idToken;
