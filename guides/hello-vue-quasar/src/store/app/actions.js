import { i18n } from 'boot/i18n';
import { auth } from 'boot/auth';
import useApiHelper from 'composables/useApiHelper';

const apiHelper = useApiHelper();

/* Authentification */

/**
 * Authenticate the current user.
 */
export const login = () => {
    auth.login();
};

/**
 * Log out the current user.
 */
export const logout = () => {
    auth.logout();
};

/**
 * Set application and current user info.
 * - current user's ID token
 * - current user info
 * - app access token (on behalf of the current user)
 * - app description
 * - app refresh token
 * @param {function} param0.commit
 * @param {function} param0.dispatch
 */
export const setAuthInfo = ({ commit, dispatch }) => {
    const user = auth.idTokenParsed ? { ...auth.idTokenParsed } : 'guest';
    const app = auth.tokenParsed;
    commit('setAuthInfo', {
        idToken: auth.idToken,
        accessToken: auth.token,
        refreshToken: auth.refreshToken,
        user,
        app,
    });
    dispatch('setApiAccessToken');
};

/**
 * Set or remove access token for API client.
 * @param {object} getters
 */
export const setApiAccessToken = ({ getters }) => {
    if (getters.accessToken) {
        apiHelper.setAccessToken(getters.accessToken);
    } else {
        apiHelper.resetAccessToken();
    }
};

/**
 * Update user-specific application settings.
 * @param {function} commit
 * @param {object} param0.getters
 * @param {object} settings application settings
 */
export const updateSettings = ({ commit, getters }, { settings }) => {
    const userId = getters.user?.sub || 'guest';
    commit('setSettings', { userId, settings });
};

/**
 * Apply user settings.
 * @param {object} getters
 */
export const applySettings = ({ getters }) => {
    i18n.global.locale =
        getters.settings?.language ||
        getters.availableSettings.language.default;
};
