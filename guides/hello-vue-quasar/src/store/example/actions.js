import useExampleRepository from 'composables/repositories/useExampleRepository';

const exampleRepository = useExampleRepository();

/**
 * Get public endpoint data.
 * @param {function} param0.commit
 */
export const getPublicResource = async ({ commit }) => {
    const data = await exampleRepository.getPublic();
    commit('setPublicResource', { publicResource: data });
};

/**
 * Get protected endpoint data (requiring auth).
 * @param {function} param0.commit
 */
export const getRestrictedResource = async ({ commit }) => {
    const data = await exampleRepository.getRestricted();
    commit('setRestrictedResource', { restrictedResource: data });
};

/**
 * Add message to the list.
 * @param {function} param0.commit
 * @param {object} param0.getters
 * @param {array} param1.message
 */
export const addMessage = ({ commit, getters }, { message }) => {
    const { messages } = getters;
    commit('setMessages', { messages: messages.concat(message) });
};

/**
 * Remove message from the list.
 * @param {function} param0.commit
 * @param {object} param0.getters
 * @param {array} param1.correlationId
 */
export const removeMessage = ({ commit, getters }, { correlationId }) => {
    const messages = getters.messages.filter(
        (message) => message.correlationId !== correlationId
    );
    commit('setMessages', { messages });
};
