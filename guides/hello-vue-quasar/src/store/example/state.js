export default () => ({
    // app sections
    sections: [
        {
            id: 'recent',
            name: 'app.sections.recent.title',
            icon: 'history',
            separator: false,
        },
        {
            id: 'favourites',
            name: 'app.sections.favourites.title',
            icon: 'star',
            separator: true,
            requireAuth: true,
        },
        {
            id: 'public',
            name: 'app.sections.public.title',
            icon: 'public',
            separator: false,
        },
        {
            id: 'private',
            name: 'app.sections.private.title',
            icon: 'security',
            separator: false,
            requireAuth: true,
        },
        {
            id: 'trash',
            name: 'app.sections.trash.title',
            icon: 'delete',
            separator: true,
            requireAuth: true,
        },
        {
            id: 'help',
            name: 'app.sections.help.title',
            icon: 'help',
            separator: false,
        },
        {
            id: 'settings',
            name: 'app.sections.settings.title',
            icon: 'settings',
            separator: false,
        },
    ],
    // messages received via WebSocket
    messages: [],
    // public resources from API
    publicResource: null,
    // protected resources from API
    restrictedResource: null,
});
