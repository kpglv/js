/**
 * Set public data.
 * @param {object} state
 * @param {object} param1.publicResource
 */
export const setPublicResource = (state, { publicResource }) => {
    state.publicResource = publicResource;
};

/**
 * Set protected data.
 * @param {object} state
 * @param {object} param1.restrictedResource
 */
export const setRestrictedResource = (state, { restrictedResource }) => {
    state.restrictedResource = restrictedResource;
};

/**
 * Set message list.
 * @param {object} state
 * @param {object} param1.messages
 */
export const setMessages = (state, { messages }) => {
    state.messages = messages;
};
