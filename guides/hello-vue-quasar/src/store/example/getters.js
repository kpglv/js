/**
 * Get app sections list.
 * @param {object} state
 * @returns {array}
 */
export const sections = (state) => state.sections;

/**
 * Get public data.
 * @param {string} state
 * @returns {object}
 */
export const publicResource = (state) => state.publicResource;

/**
 * Get protected data.
 * @param {string} state
 * @returns {object}
 */
export const restrictedResource = (state) => state.restrictedResource;

/**
 * Get message list.
 * @param {string} state
 * @returns {object}
 */
export const messages = (state) => state.messages;
