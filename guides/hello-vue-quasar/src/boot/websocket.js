/* eslint-disable no-underscore-dangle */
/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
import { boot } from 'quasar/wrappers';

const Nes = require('@hapi/nes/lib/client');

/**
 * Transform HTTP(S) URL into WS(S).
 * @param {string} url
 * @returns {string}
 */
const httpToWsUrl = (url) => url.replace(/(http)(s)?:\/\//, 'ws$2://');
const websocketServiceUrl = httpToWsUrl(process.env.SERVICE_URL);

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ store /* , app, router, ... */ }) => {
    const client = new Nes.Client(websocketServiceUrl);

    /**
     * Handle the message received via WebSocket.
     * @param {object} message
     * @param {object} flags additional parameters
     */
    const onMessage = (message, flags) => {
        store.dispatch('example/addMessage', { message });
    };

    /**
     * Handle 'connection established' event.
     */
    const onConnect = async () => {
        console.info('WebSocket: connection established');
        await client.subscribe('/message', onMessage);
    };

    /**
     * Handle 'connection lost' event.
     * @param {boolean} willReconnect whether to reconnect
     * @param {object} message disconnect cause description (code, message)
     */
    const onDisconnect = async (willReconnect, message) => {
        console.info(
            `WebSocket: connection lost, ${
                willReconnect
                    ? 'will be reestablished'
                    : 'will not be reestablished'
            }, cause: ${message.explanation}`
        );
        await client.unsubscribe('/message', onMessage);
    };

    /**
     * Handle general WebSocket error.
     * @param {object} error
     */
    const onError = (error) => {
        console.error('WebSocket: ', error.message);
    };

    client.onConnect = onConnect;
    client.onDisconnect = onDisconnect;
    client.onError = onError;

    try {
        // await client.connect();
    } catch (error) {
        console.error('WebSocket: ', error.message);
    }
});
