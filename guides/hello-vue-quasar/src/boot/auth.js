/* eslint-disable no-console */
import { boot } from 'quasar/wrappers';
import Keycloak from 'keycloak-js';
import KeycloakAuthorization from 'keycloak-js/dist/keycloak-authz';

const authConfig = {
    realm: process.env.AUTH_SERVICE_REALM,
    url: process.env.AUTH_SERVICE_URL,
    clientId: process.env.APP_CLIENTID,
};
export const auth = new Keycloak(authConfig);

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(async ({ app, store } /* { , router, ... } */) => {
    // FIXME: requires configured Keycloak
    // await initAuth(app, store);
});

async function initAuth(app, store) {
    try {
        app.config.globalProperties.$auth = auth;

        auth.onReady = (authenticated) => {
            console.info(
                `Keycloak: initialized (user ${authenticated ? 'authenticated' : 'not authenticated'})`
            );
            store.dispatch('app/setAuthInfo');
        };

        auth.onAuthSuccess = () => {
            console.info('Keycloak: user successfully authenticated');
            store.dispatch('app/setAuthInfo');
        };
        auth.onAuthLogout = () => {
            console.info('Keycloak: user successfully logged out');
            store.dispatch('app/setAuthInfo');
        };
        auth.onAuthError = () => {
            console.error('Keycloak: failed to authenticate user');
        };

        auth.onTokenExpired = () => {
            console.info('Keycloak: tokens have been expired');
            auth.updateToken(70)
                .then((refreshed) => {
                    if (refreshed) {
                        console.info('Keycloak: tokens have been refreshed');
                    } else {
                        console.info(
                            `Keycloak: tokens are not refreshed, valid for ${Math.round(
                                auth.tokenParsed.exp +
                                auth.timeSkew -
                                new Date().getTime() / 1000
                        )} s`);
                    }
                })
                .catch(() => {
                    console.error('Keycloak: failed to refresh tokens');
                });
        };
        auth.onAuthRefreshSuccess = () => {
            console.info('Keycloak: user session was updated');
            store.dispatch('app/setAuthInfo');
        };
        auth.onAuthRefreshError = () => {
            console.error(
                'Keycloak: failed to update user session'
            );
        };

        await auth.init({
            onLoad: 'check-sso',
        });

        const authz = new KeycloakAuthorization(auth);

        // FIXME:
        console.info('Keycloak auth:', auth);
        console.info('Keycloak authz:', authz);
    } catch (error) {
        console.error('Keycloak:', error);
    }
}
