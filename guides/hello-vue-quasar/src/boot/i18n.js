/* eslint-disable */
import { boot } from 'quasar/wrappers';
import { createI18n } from 'vue-i18n';
import messages from 'src/i18n';

export const i18n = createI18n({
    locale: 'ru',
    fallbackLocale: 'ru',
    messages,
});

export default boot(({ app, store }) => {
    app.use(i18n);
    store.dispatch('app/applySettings');
});
