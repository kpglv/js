import { afterAll, afterEach, jest, describe, it, expect } from '@jest/globals';
import { getResource, getRestrictedResource } from 'store/example/actions';
import { sections, current } from 'store/example/getters';
import { setCurrent } from 'store/example/mutations';
import useExampleRepository from 'src/composables/repositories/useExampleRepository';

const exampleRepository = useExampleRepository();

describe('store "example"', () => {
    describe('Действия', () => {
        let exampleRepositorySpy;
        const commit = jest.fn();

        afterEach(() => {
            exampleRepositorySpy.mockReset();
        });

        afterAll(() => {
            exampleRepositorySpy.mockRestore();
        });

        it('getResource', async () => {
            exampleRepositorySpy = jest
                .spyOn(exampleRepository, 'getPublic')
                .mockResolvedValue({ result: true });

            await getResource({ commit });

            expect(exampleRepositorySpy).toHaveBeenCalled();
            expect(commit).toHaveBeenCalledWith('setCurrent', {
                current: { result: true },
            });
        });

        it('getRestrictedResource', async () => {
            exampleRepositorySpy = jest
                .spyOn(exampleRepository, 'getRestricted')
                .mockResolvedValue({ result: true });

            await getRestrictedResource({ commit });

            expect(exampleRepositorySpy).toHaveBeenCalled();
            expect(commit).toHaveBeenCalledWith('setCurrent', {
                current: { result: true },
            });
        });
    });

    describe('getters', () => {
        it('sections', () => {
            const state = { sections: [] };
            expect(sections(state)).toStrictEqual([]);
        });

        it('current', () => {
            const state = { current: { message: 'Hello!' } };
            expect(current(state)).toStrictEqual({ message: 'Hello!' });
        });
    });

    describe('mutators', () => {
        it('setCurrent', () => {
            const state = {};
            setCurrent(state, { current: { message: 'Hello!' } });
            expect(state.current).toStrictEqual({ message: 'Hello!' });
        });
    });
});
