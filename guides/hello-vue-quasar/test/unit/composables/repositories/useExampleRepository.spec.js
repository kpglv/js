import { afterEach, afterAll, jest, describe, it, expect } from '@jest/globals';
import { exampleApi } from 'boot/axios';
import useExampleRepository from 'src/composables/repositories/useExampleRepository';

jest.mock('boot/axios');

describe('Пример репозитория', () => {
    const exampleRepository = useExampleRepository();
    const data = [{ anyKey: 'anyValue' }];

    afterEach(() => {
        jest.resetAllMocks();
    });

    afterAll(() => {
        jest.restoreAllMocks();
    });

    it('getPublic', () => {
        exampleApi.get.mockResolvedValue({ data });
        return exampleRepository
            .getPublic()
            .then((response) => expect(response).toEqual(data));
    });

    it('getRestricted', () => {
        exampleApi.get.mockResolvedValue({ data });
        return exampleRepository
            .getRestricted(1)
            .then((response) => expect(response).toEqual(data));
    });
});
