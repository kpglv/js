/* eslint-env node */
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

// e2e/plugins/index.js

require("dotenv").config();

module.exports = (on, config) => {
  // установить порт для baseUrl из файла .env
  config.baseUrl = `${process.env.APP_HOST}:${process.env.APP_PORT}${process.env.APP_RELATIVE_PATH}`;
  return config;
};
