/*
 * This file runs in a Node context (it's NOT transpiled by Babel), so use only
 * the ES6 features that are supported by your Node version. https://node.green/
 */

// Configuration for your app
// https://quasar.dev/quasar-cli/quasar-conf-js

/* eslint-disable no-console */
/* eslint-env node */
const ESLintPlugin = require('eslint-webpack-plugin');
const NodePolyfillPlugin = require('node-polyfill-webpack-plugin');
/* eslint func-names: 0 */
/* eslint global-require: 0 */
const { configure } = require('quasar/wrappers');
const path = require('path');

module.exports = configure((ctx) => ({
    // https://quasar.dev/quasar-cli/supporting-ts
    supportTS: false,

    // https://quasar.dev/quasar-cli/prefetch-feature
    // preFetch: true,

    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    // https://quasar.dev/quasar-cli/boot-files
    boot: ['auth', 'axios', 'i18n', 'websocket'],

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-css
    css: ['app.scss'],

    // https://github.com/quasarframework/quasar/tree/dev/extras
    extras: [
        // 'ionicons-v4',
        // 'mdi-v5',
        // 'fontawesome-v5',
        // 'eva-icons',
        // 'themify',
        // 'line-awesome',
        // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

        'roboto-font', // optional, you are not bound to it
        'material-icons', // optional, you are not bound to it
    ],

    // Full list of options: https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-build
    build: {
        env: {
            ...require('dotenv').config().parsed,
            ...process.env,
        },
        vueRouterMode: 'history', // available values: 'hash', 'history'

        // transpile: false,
        publicPath: process.env.APP_RELATIVE_PATH || '',

        // Add dependencies for transpiling with Babel (Array of string/regex)
        // (from node_modules, which are by default not transpiled).
        // Applies only if "transpile" is set to true.
        // transpileDependencies: [],

        // rtl: true, // https://quasar.dev/options/rtl-support
        // preloadChunks: true,
        // showProgress: false,
        // gzip: true,
        // analyze: true,

        // Options below are automatically set depending on the env, set them if
        // you want to override
        // extractCSS: false,

        // https://quasar.dev/quasar-cli/handling-webpack
        // "chain" is a webpack-chain object https://github.com/neutrinojs/webpack-chain
        chainWebpack(chain) {
            chain
                .plugin('eslint-webpack-plugin')
                .use(ESLintPlugin, [{ extensions: ['js', 'vue'] }]);

            chain
                .plugin('node-polyfill-webpack-plugin')
                .use(NodePolyfillPlugin);

            // добавить алиасы стилей и модулей (composables)
            const stylesDefaultSearchDir = path.resolve(__dirname, './src/css');
            const composablesDefaultSearchDir = path.resolve(
                __dirname,
                './src/composables'
            );
            chain.resolve.alias.set('css', stylesDefaultSearchDir);
            chain.resolve.alias.set('composables', composablesDefaultSearchDir);
        },
    },

    // Full list of options: https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-devServer
    devServer: {
        https: false,
        port: process.env.APP_PORT || 4444,
        open: true, // opens browser window automatically
    },

    // https://quasar.dev/quasar-cli/quasar-conf-js#Property%3A-framework
    framework: {
        config: {},

        // iconSet: 'material-icons', // Quasar icon set
        lang: 'ru', // Quasar language pack

        // For special cases outside of where the auto-import strategy can have an impact
        // (like functional components as one of the examples),
        // you can manually specify Quasar components/directives to be available everywhere:
        //
        // components: [],
        // directives: [],

        // Quasar plugins
        plugins: ['Notify'],
    },

    // animations: 'all', // --- includes all animations
    // https://quasar.dev/options/animations
    animations: [],

    // https://quasar.dev/quasar-cli/developing-ssr/configuring-ssr
    ssr: {
        pwa: false,

        // manualStoreHydration: true,
        // manualPostHydrationTrigger: true,

        prodPort: 3000, // The default port that the production server should use
        // (gets superseded if process.env.PORT is specified at runtime)

        maxAge: 1000 * 60 * 60 * 24 * 30,
        // Tell browser when a file from the server should expire from cache (in ms)

        chainWebpackWebserver(chain) {
            chain
                .plugin('eslint-webpack-plugin')
                .use(ESLintPlugin, [{ extensions: ['js'] }]);
        },

        middlewares: [
            ctx.prod ? 'compression' : '',
            'render', // keep this as last one
        ],
    },

    // https://quasar.dev/quasar-cli/developing-pwa/configuring-pwa
    pwa: {
        workboxPluginMode: 'GenerateSW', // 'GenerateSW' or 'InjectManifest'
        workboxOptions: {}, // only for GenerateSW

        // for the custom service worker ONLY (/src-pwa/custom-service-worker.[js|ts])
        // if using workbox in InjectManifest mode
        chainWebpackCustomSW(chain) {
            chain
                .plugin('eslint-webpack-plugin')
                .use(ESLintPlugin, [{ extensions: ['js'] }]);
        },

        manifest: {
            name: 'hello-vue-quasar',
            short_name: 'hello-vue-quasar',
            description: 'Example App',
            display: 'standalone',
            orientation: 'portrait',
            background_color: '#ffffff',
            theme_color: '#027be3',
            icons: [],
        },
    },

    // Full list of options:
    // https://quasar.dev/quasar-cli/developing-cordova-apps/configuring-cordova
    cordova: {
        // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    // Full list of options:
    // https://quasar.dev/quasar-cli/developing-capacitor-apps/configuring-capacitor
    capacitor: {
        hideSplashscreen: true,
    },

    // Full list of options:
    // https://quasar.dev/quasar-cli/developing-electron-apps/configuring-electron
    electron: {
        bundler: 'packager', // 'packager' or 'builder'

        packager: {
            // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options
            // OS X / Mac App Store
            // appBundleId: '',
            // appCategoryType: '',
            // osxSign: '',
            // protocol: 'myapp://path',
            // Windows only
            // win32metadata: { ... }
        },

        builder: {
            // https://www.electron.build/configuration/configuration

            appId: 'example-app',
        },

        // "chain" is a webpack-chain object https://github.com/neutrinojs/webpack-chain
        chainWebpackMain(chain) {
            chain
                .plugin('eslint-webpack-plugin')
                .use(ESLintPlugin, [{ extensions: ['js'] }]);
        },

        // "chain" is a webpack-chain object https://github.com/neutrinojs/webpack-chain
        chainWebpackPreload(chain) {
            chain
                .plugin('eslint-webpack-plugin')
                .use(ESLintPlugin, [{ extensions: ['js'] }]);
        },
    },
}));
