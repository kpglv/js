# hello-db

Exploring database connectivity means available in JS.

- Relational databases
  - Collection of node.js modules for interfacing with PostgreSQL
    https://node-postgres.com/
- NoSQL databases
  - PouchDB (IndexedDB in browser/LevelDB in Node) https://pouchdb.com/

## Getting Started

See the code in the `src` directory.

Prepare environment variables by creating `.env` file (see `example.env`) and
start a database instance if example requires it, e.g.

```
podman-compose up -d
```
