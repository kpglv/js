// https://pouchdb.com/getting-started.html


import PouchDB from 'pouchdb';


// creating a database

const db = new PouchDB('todos');

// defining functions that operate on the database

const idSequence = (i) => {
  return () => {
    i++;
    return i.toString();
  };
};
const nextId = idSequence(0);

const addTodo = (db, text) => {
  const todo = {
    _id: nextId(),
    title: text,
    completed: false,
  };
  return db.put(todo);
};

const updateTodo = (db, updatedTodo) => {
    return db.put(updatedTodo);
};

const deleteTodo = (db, todo) => {
  return db.remove(todo);
};

const showTodos = (db) => {
  return db.allDocs({ include_docs: true, descending: true }, (err, doc) => {
    const texts = doc.rows.map(row => row.doc.title);
    console.log('all todos retrieved:', texts);
  });
};

// watching for changes in the database

// db.changes({
//   since: 'now',
//   live: true,
// }).on('change', () => {
//   console.log('db changed');
//   showTodos(db);
// });

// syncing between several databases: TODO


// testing

// create todos
addTodo(db, 'get up in the morning')
  .then(() => addTodo(db, 'fight the existential horror'))
  .then(() => addTodo(db, 'enjoy the life'))
  .then(() => addTodo(db, 'go to bed'))

// update todo
  .then(() => {
    return db.get("3")
      .then((todo) => {
        return updateTodo(db, {
          _id: "3",
          _rev: todo._rev,
          title: todo.title + " HARD!",
        });
      });
  })
// delete todo
  .then(() => {
    return db.get("2")
      .then((todo) => deleteTodo(db, todo));
  })
// retrieve all todos
  .then(() => showTodos(db))
  .catch((err) => console.error('db error:', err.message));
