import pg from 'pg';
import dotenv from 'dotenv';

dotenv.config();
const client = new pg.Client({
  host: process.env.DATABASE_HOST,
  port: process.env.DATABASE_PORT,
  database: process.env.DATABASE_NAME,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
});
await client.connect();

const res = await client.query('SELECT $1::text as message', ['Hello world!']);
console.log(res.rows[0].message);

const albums = await client.query('SELECT * FROM album');
console.log(albums.rows);

await client.end();
