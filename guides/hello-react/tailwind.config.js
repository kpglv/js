/** @type {import('tailwindcss').Config} */

const defaultTheme = require('tailwindcss/defaultTheme');
const colors = require('tailwindcss/colors');

module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ["Fira Sans", ...defaultTheme.fontFamily.sans],
      },
      colors: {
        primary: {
          DEFAULT: '#678fca',
          '50': '#f3f6fb',
          '100': '#e3ebf6',
          '200': '#cedeef',
          '300': '#acc7e4',
          '400': '#85abd5',
          '500': '#678fca',
          '600': '#5e7fc0',
          '700': '#4965ac',
          '800': '#40548d',
          '900': '#384770',
          '950': '#252e46',
        },
        secondary: {
          DEFAULT: '#7ab131',
          '50': '#f5fbea',
          '100': '#e8f5d2',
          '200': '#d3ebab',
          '300': '#b5dd79',
          '400': '#96ca4b',
          '500': '#7ab131',
          '600': '#5e8c24',
          '700': '#496c1f',
          '800': '#3b561e',
          '900': '#344a1d',
          '950': '#19280b',
        },
        error: colors.red,
        warning: colors.yellow,
        success: colors.green,
        info: colors.blue,
      }
    },
  },
  plugins: [],
}
