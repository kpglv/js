# hello-react

A tic-tac-toe game.

Demonstrates basics of React --- components, JSX, hooks.

- added TailwindCSS configuration

## Getting Started

Start development server (uses `react-scripts`).

```
yarn start
```
