// runs on the client

import { createApp } from './app.js';

createApp().mount('#app');
