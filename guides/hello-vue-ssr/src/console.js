// runs on the server

import { renderToString } from 'vue/server-renderer';
import { createApp } from './app.js';

const app = createApp();

renderToString(app).then(html => {
    console.log(html);
});
