# hello-vue-ssr

Exploring basic Vue APIs for SSR.

- Server-Side Rendering (SSR) https://vuejs.org/guide/scaling-up/ssr.html

## Getting Started

Explore and run example code.

Printing the results of server-side rendering to console

```
node src/console.js
```

Serving the results of server-side rendering over HTTP

```
node src/server.js
```