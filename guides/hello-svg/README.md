# hello-svg

Exploring basics of SVG and its support in modern browsers.

- Introducing SVG from scratch
  https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Introduction
- SVG animation example
  https://notebook.lachlanjc.com/2022-11-30_animating_an_icon_with_tailwind_css

## Getting started

Open `index.html` file in browser to see working examples.
