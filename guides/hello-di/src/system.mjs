import dotenv from 'dotenv';
import { readFileSync } from 'fs';
import { init, halt, initKey, haltKey } from '@kpglv/dependency-injection';
import { startHttpServer, stopHttpServer } from './http.mjs';

dotenv.config();

// defining component initializers

initKey.when('version', (_, version) => version);

initKey.when('webserver', (_, { host, port, logger, version }) => {
    logger(`starting HTTP server, version ${version}`);
    const onStart = () =>
        logger(`HTTP server is running on http://${host}:${port}`);
    return startHttpServer({ host, port, onStart });
});

initKey.when('logger', (_, { to }) => {
    console.log(`starting logging to '${to}'`);
    switch (to) {
        case 'stdout':
            return console.log;
        default:
            throw new Error(`cannot log to '${to}'`);
    }
});

// defining component deinitializers

haltKey.when('webserver', (_, server) => {
    console.log('stopping HTTP server...');
    stopHttpServer(server);
});

// loading system configuration

const configFileName = process.env.SYSTEM_CONFIG;

const readConfig = filename => {
    if (!filename) {
        throw new Error('system configuration file name is not specified');
    }
    console.log(`reading system configuration from '${filename}'`);
    return JSON.parse(readFileSync(filename));
};

// starting the system

const initSystem = () =>
    init(readConfig(configFileName), {
        onInit: () => console.log('*** system initialized ***'),
    });

const haltSystem = system =>
    halt(system, { onHalt: () => console.log('*** system halted ***') });

let system = initSystem();

// stopping/restarting the system on signals

['SIGHUP'].forEach(signal => {
    process.on(signal, () => {
        console.log(`caught ${signal}, restarting the system...`);
        haltSystem(system);
        system = initSystem();
    });
});

['SIGINT', 'SIGTERM', 'SIGQUIT'].forEach(signal => {
    process.on(signal, () => {
        console.log(`caught ${signal}, halting the system...`);
        haltSystem(system);
        process.exit(0);
    });
});
