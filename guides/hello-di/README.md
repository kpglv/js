# hello-di

Using DI component (`components/dependency-injection`) to declaratively
describe systems. See its documentation for more info.

## Getting Started

Explore the code and configuration in the `src` and `resources` directories.

Start the system

```
yarn start
```

Visit these endpoints to see that HTTP server is properly configured and started:

-   Default response: http://localhost:8000
-   Response served from a file: http://localhost:8000/file
-   Response in JSON format: http://localhost:8000/json
-   Response in CSV format: (will start a download): http://localhost:8000/csv

Send `SIGHUP` to restart the system, `SIGTERM`/`SIGINT`/`SIGQUIT` to halt it.
