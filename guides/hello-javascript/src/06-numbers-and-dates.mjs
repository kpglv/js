// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Numbers_and_dates


// numbers are double-precision 64-bit IEEE 754 floating-point numbers; have
// additional symbolic values '+Infinity', '-Infinity', 'NaN'


// numerical constants

const numericalConstants = {
  maxVal: Number.MAX_VALUE,
  minVal: Number.MIN_VALUE,
  posInfty: Number.POSITIVE_INFINITY,
  negInfty:  Number.NEGATIVE_INFINITY,
  nan: Number.NaN,
  // smallest representable numeric value
  epsilon: Number.EPSILON,
  maxSafeInteger: Number.MAX_SAFE_INTEGER,
  minSafeInteger: Number.MIN_SAFE_INTEGER,
};
console.log(numericalConstants);


// numerical standard functions

const numericalFunctions = {
  float: Number.parseFloat("-123.4s"),
  int: Number.parseInt("123a"),
  finite: Number.isFinite(12),
  integer: Number.isInteger(12),
  nan: Number.isNaN(12),
  safeInteger: Number.isSafeInteger(12),

  exp: (12.34).toExponential(),
  fixed: (12.34567).toFixed(1),
  precision: (12.34567).toPrecision(5),
};
console.log(numericalFunctions);


// Math: mathematical constants and functions

// - 'PI'
// - 'E'

// - 'abs()'
// - 'sin()', 'cos()', 'tan()'; 'asin()', 'acos()', 'atan()', 'atan2()'
// - 'sinh()', 'cosh()', 'tanh()'; 'asinh()', 'acosh()', 'atanh()'
// - 'pow()', 'exp()', 'expm1()', 'log()', 'log10()', 'log1p()', 'log2()'
// - 'floor()', 'ceil()'
// - 'min()', 'max()'
// - 'random()'
// - 'round()', 'fround()', 'trunc()'
// - 'sqrt()', 'cbrt()', 'hypot()'
// - 'sign()'
// - 'clz32()', 'imul()'


// bigints: arbitrarily long sequences of bits encoding integers; Math
// functions cannot be used


// Date

// constructing dates

console.log(Date()); // a string
console.log(new Date()); // a new Date object

// some values can be omitted, from the right, default to 0
const xmas95 = new Date("1995-12-25T00:00:00.000Z"); // uses UTC time
const xmas95_1 = new Date("1995-12-25");
const xmas95_2 = new Date(1995, 11, 25); // assumes local time
console.log(xmas95);
console.log(xmas95_1);
console.log(xmas95_2);

// Date methods: getting and setting elements of date and time (year, month,
// date, day, hours, minutes, seconds), converting to strings, parsing date
// strings
