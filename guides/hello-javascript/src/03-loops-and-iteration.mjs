// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration


// for

for (let step = 0; step < 5; step++) {
  // Runs 5 times, with values of step 0 through 4.
  console.log(step + ": walking east one step");
}


// do ... while

let i = 0;
do {
  console.log("do while", i);
  i++;
} while (i < 3);


// while

i = 0;
// labeled statement (can be used with break and continue)
end: while (true) {
  if (i > 2) {
    break end;
  }
  if (i === 1) {
    i++;
    continue;
  }
  console.log("while", i);
  i++;
}


// for ... in: iterates over all enumerable property names in the object

const obj = {
  id: 1,
  name: "Joe",
  toString() {
    return `Name: ${this.name}`;
  }
};

for (const key in obj) {
  console.log(key, obj[key]);
}
const arr = [1, 2, 3];
for (const index in arr) {
  console.log(index, arr[index]);
}


// for ... of: iterates over iterable objects (e.g., Array, Map, Set,
// arguments)

const arr1 = [4, 5, 6];
for (const i of arr1) {
  console.log(i);
}
// with destructuring
const obj1 = { foo: 1, bar: 2 };
for (const [key, val] of Object.entries(obj1)) {
  console.log(key, val);
}
