// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Typed_arrays


// array-like objects that allow read and write raw binary data in memory
// buffers; their elements are binary values in one of the supported formats


// buffers

// objects representing chunks of unstructured data; to access their contents,
// a view must be created, that provides a context (data type, starting
// offset, number of elements)

// 'ArrayBuffer' (owned by a single execution context at a time),
// 'SharedArrayBuffer' (shared between execution contexts): represent a memory
// span. They can be allocated, copied, transferred between execution contexts
// (without copying), resized

const buffer = new ArrayBuffer(16);
console.log(buffer);
console.log(buffer.byteLength);


// views

// object that provide access to underlying buffers

// typed array views have elements of numeric types like 'Int8' and 'Float64';
// they are usually fixed size (unless underlying buffer is resizable and
// typed array doesn't have a fixed 'byteLength', and thus automatically
// resizes with the buffer) and cannot be nested, so not all array methods are
// available for them

const int32View = new Int32Array(buffer);
for (let i = 0; i < int32View.length; i++) {
  int32View[i] = i * 2;
}
console.log(int32View);

// several views to one buffer

const int16View = new Int16Array(buffer);
for (let i = 0; i < int16View.length; i++) {
  console.log(`Entry ${i}: ${int16View[i]}`);
}

int16View[0] = 32;
console.log(`Entry 0 in the 32-bit array is now ${int32View[0]}`);

// reading text from a buffer

const buf = new ArrayBuffer(8);
const uint8 = new Uint8Array(buf);
uint8.set([228, 189, 160, 229, 165, 189]);
const text = new TextDecoder().decode(uint8);
console.log(text);

const buf1 = new ArrayBuffer(8);
const uint16 = new Uint16Array(buf1);
uint16.set([0x4f60, 0x597d]);
const text1 = String.fromCharCode(...uint16);
console.log(text1);

// non-numeric property access still works

int16View[true] = "foo";
console.log(int16View[true]);

// working with complex data structures

// represents a structure in memory with fields of different types at different offsets
const buf2 = new ArrayBuffer(24);
// read the data into the buffer...
const idView = new Uint32Array(buf2, 0, 1);
const usernameView = new Uint8Array(buf2, 4, 16);
const amountDueView = new Float32Array(buf2, 20, 1);

// conversion to regular arrays

const typedArray = new Uint8Array([1, 2, 3, 4]);
const regularArray1 = Array.from(typedArray);
const regularArray2 = [...typedArray];
console.log(regularArray1, regularArray2);

// DataView

// provides a getter/setter API to read and write data of arbitrary types at
// arbitrary byte offsets in the buffer

const buf3 = new ArrayBuffer(2);
const dv = new DataView(buf3);
dv.setInt16(0, 255, true /* littleEndian */);
console.log(dv);
console.log(dv.getInt16(0));
console.log(dv.getInt8(0));
