// https://www.w3schools.com/jsrEF/api_console.asp

// clear(): clears the console

console.log('console.clear():');
console.clear();

// info() 	Outputs an informational message to the console

console.log('console.info():');
console.info('info');

// log() 	Outputs a message to the console

console.log('console.log():');
console.info('a message');

// warn(): outputs a warning message to the console

console.log('console.warn():');
console.error('a warning');

// error(): outputs an error message to the console

console.log('console.error():');
console.error('an error');

// trace() 	Outputs a stack trace to the console

console.log('console.trace():');
console.error('a stack trace');

// assert(): writes an error message to the console if a assertion is false

console.log('console.assert():');
console.assert(false, 'nope');

// table(): displays tabular data as a table

console.log('console.table():');
console.table({ firstname: 'John', lastname: 'Doe' });

// group(): creates a new inline group in the console. This indents following
// console messages by an additional level, until console.groupEnd() is called

// groupCollapsed(): creates a new inline group in the console. However,
// the new group is created collapsed. The user will need to use the disclosure
// button to expand it

// groupEnd(): exits the current inline group in the console

console.log('console.group(), console.groupEnd():');
console.log('Hello world!');
console.group('a group');
console.log('Hello again, this time inside a group!');
console.groupEnd('a group');

console.log('console.groupCollapsed(), console.groupEnd():');
console.log('Hello world!');
console.groupCollapsed('collapsed');
console.log('Hello again, this time inside a collapsed group!');
console.groupEnd('collapsed');

// time(): starts a timer(can track how long an operation takes)

// timeEnd(): stops a timer that was previously started by console.time()

console.log('console.time(), console.timeEnd():');
console.time('loop');
for (let i = 0; i < 100000; i++) {
    // some code
}
console.timeEnd('loop');

// count(): logs the number of times that this particular call to count()
// has been called

console.log('console.count():');
for (let i = 0; i < 5; i++) {
    console.count('myLabel');
}
