// https://www.w3schools.com/jsrEF/api_geolocation.asp

// access via `navigator.geolocation` property; only in secure contexts;
// requests user permission; most accurate on devices with GPS

// methods

// - getCurrentPosition(callback, errorCallback, positionOptions): passes
//   the current position of the device or error to the supplied
//   callback / errorCallback
// - watchPosition(callback, errorCallback, positionOptions): returns a watch
//   ID that can be used to unregister the handler(s) by passing it to
//   clearWatch()
// - clearWatch(watchId): unregister location/error monitoring handlers
//   previously installed using watchPosition()

// position object (the callback argument)

// - timestamp: a DOMTimeStamp representing the time at which the location
//   was retrieved
// - coords: the position and altitude of the device on Earth

// positionError object: the reason of an error occurring when using
// - code: integer
// - message: string

// positionOptions object: describes an object containing option properties
// to pass as a parameter of Geolocation.getCurrentPosition() and
// watchPosition()
// - maximumAge: integer (milliseconds) | infinity - maximum period of
//   position caching
// - timeout: integer (milliseconds) - timeout before calling errorCallback
//   (0 for never)
// - enableHighAccuracy: false | true

globalThis.navigator &&
    navigator?.geolocation.getCurrentPosition(console.log, console.log, {
        timeout: 100,
    });

const watchId =
    globalThis.navigator &&
    navigator?.geolocation.watchPosition(console.log, console.log, {
        timeout: 0,
        enableHighAccuracy: true,
    });

globalThis.navigator && navigator?.geolocation.clearWatch(watchId);
