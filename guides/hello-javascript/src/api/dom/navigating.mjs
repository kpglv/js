// https://javascript.info/dom-navigation

import { dom } from './example-dom.mjs';

// the root node is 'document'

console.log(dom.window.document);

// html, head and body

console.log(dom.window.document.documentElement);
console.log(dom.window.document.head);
console.log(dom.window.document.body);
console.log(dom.window.document.body.parentNode);
console.log(dom.window.document.body.parentElement);

// children: array-like (iterable, works with for ... of)

console.log(Array.from(dom.window.document.documentElement.childNodes));
console.log(Array.from(dom.window.document.documentElement.children));

// siblings

console.log(dom.window.document.head.nextSibling);
console.log(dom.window.document.body.previousSibling);
console.log(dom.window.document.head.nextElementSibling);
console.log(dom.window.document.body.previousElementSibling);
