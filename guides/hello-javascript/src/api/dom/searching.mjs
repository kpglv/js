// https://javascript.info/searching-elements-dom

import { dom } from './example-dom.mjs';

// child by id; on document only

// ids must be unique
console.log(dom.window.document.getElementById('smart').textContent);
// elements are automatically accessible under their ids as global variables
// (deprecated)
console.log(dom.window.smart.textContent);

// child(ren) by arbitrary CSS selector (pseudoclasses too); on any element

console.log(dom.window.document.querySelector('#smart').textContent);
// check if element matches selector
console.log(dom.window.document.querySelector('#smart').matches('#smart'));
// check if one element contains another
console.log(
    dom.window.document
        .querySelector('ol')
        .contains(dom.window.document.querySelector('#smart'))
);
console.log(
    Array.from(dom.window.document.querySelectorAll('li')).map(
        el => el.textContent
    )
);

// closest parent by arbitrary CSS selector; on any element

console.log(dom.window.document.querySelector('#smart').closest('ol'));

// children by tag, class, etc (deprecated, use querySelector)
// - getElementsByTagName
// - getElementsByClassName
// - getElementsByName (name attribute)
