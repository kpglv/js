import { JSDOM } from 'jsdom';

// an example DOM

export const dom = new JSDOM(`
<!DOCTYPE HTML>
<html>
<head>
  <title>Hello JSDOM!</title>
</head>
<body>
  The truth about elk.
  <ol>
    <li id="smart">An elk is a smart</li>
    <!-- comment -->
    <li>...and cunning animal!</li>
  </ol>
</body>
</html>`);
console.log(dom.window.document.querySelector('title').textContent);
