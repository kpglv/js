import http from 'http';

const sendEvents = (req, res) => {
    const lastEventId = parseInt(req.headers['last-event-id']);
    console.log('last event id:', lastEventId);
    let counter = lastEventId ? lastEventId + 1 : 0;

    res.setHeader('Content-Type', 'text/event-stream');
    res.setHeader('Connection', 'keep-alive');
    res.setHeader('Cache-Control', 'no-cache');
    res.writeHead(200);

    // configure retry timeout for automatic reconnects
    res.write('retry: 3000\n\n');

    res.write('event: message\n');
    res.write(`data: You are now subscribed!\n`);
    // event ID, can be used to figure out which
    // events were received by client (will be sent in
    // Last-Event-ID header by EventSource on reconnect)
    res.write(`id: ${counter}\n\n`);
    counter += 1;

    const loop = setInterval(() => {
        res.write('event: ping\n');
        res.write(`data: ${new Date().toLocaleString()}\n`);
        res.write(`id: ${counter}\n\n`);
        counter += 1;
    }, 5000);

    req.on('close', () => {
        console.log('connection to the client closed');
        clearInterval(loop);
        res.end('OK');
    });
};

const host = 'localhost';
const port = 8000;
const server = http.createServer(sendEvents);

server.on('connection', () => {
    console.log('a client connected');
});

server.listen(port, host, () => {
    console.log(`HTTP server is running on http://${host}:${port}`);
});
