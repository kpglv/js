import EventSource from 'eventsource';

// reconnects automatically (unless an error or 204 No Content)
// to reconnect after error create new event source

const eventSource = new EventSource('http://localhost:8000', {
    // for cross-origin requests
    withCredentials: true,
});

// messages sent from the server that don't have an 'event' field
// are received as 'message' events

eventSource.onmessage = event => {
    console.log('received server-sent event:', event);
};

// messages sent from the server that do have an 'event' field
// are received as events with the name given in 'event' field

eventSource.addEventListener('ping', event => {
    console.log('received server-sent event:', event);
});

// note: there is a per-browser domain connection limit (6); HTTP/2 helps

// begin reading from stdin so the process does not exit
console.log('Press Ctrl-c or Ctrl-D to exit.');
process.stdin.resume();

process.on('SIGINT', () => {
    console.log('Received SIGINT, exiting...');
    process.exit();
});
