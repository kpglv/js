# sse

Example usage of Server-Sent Events (SSE).

-   https://javascript.info/server-sent-events
-   https://developer.mozilla.org/en-US/docs/Web/API/Server-sent_events/Using_server-sent_events

## Getting Started

Start the server

```sh
node server.js
```

Run the client to observe the stream of events

```sh
node client.js
```

(or simply `curl http://localhost:8000`)
