// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_objects


// an object is a collection of properties; property is an association between
// a name (key) and a value; function property value is a method


// creating new objects

// with object literal

const obj = {
  id: 1,
  name: "Joe",
};

// by invoking a constructor function (with the 'new' operator)

// will implicitly return 'this'
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}

const car = new Car("Eagle", "Talon TSi", 1993);
// can always add another property
car.color = "black";
console.log(car);

// using 'Object.create()': can specify the prototype object

const Animal = {
  type: "Invertebrates",
  displayType() {
    console.log(this.type);
  },
};

const animal1 = Object.create(Animal);
animal1.displayType();

const fish = Object.create(Animal);
fish.type = "Fishes";
fish.displayType();


// objects and properties

// all keys are converted to strings unless they are symbols; nonexistent
// properties have value undefined

// the same
car.make;
car.model;
car.year = 1999;

car["make"];
car["model"];
car["year"] = 1999;

// enumerating properties:

// - 'for ... in' (enumerable string property names, own or from prototype)
// - 'Object.keys()' (enumerable own string property names)
// - 'Object.getOwnPropertyNames()' (own string property names)

// deleting properties

const myobj = new Object();
myobj.a = 5;
myobj.b = 12;
delete myobj.a;
console.log("a" in myobj);


// inheritance

const car1 = new Car("Eagle", "Talon TSi", 1993);
Car.prototype.color = "red";
console.log(car1.color);


// defining methods

// some standard functions allow to explicitly specify 'this'

Car.prototype.whoosh = () => console.log(this, "whooosh!"); // fat arrow --- no 'this'
car1.whoosh();

car1.honk = function() { console.log(this.make, "honk!"); };
car1.honk();

const myObj = {
  myMethod: function (params) {
    console.log("myMethod", this);
  },
  myOtherMethod(params) {
    console.log("myOtherMethod", this);
  },
};
myObj.myMethod();
myObj.myOtherMethod();


// defining getters and setters

const myObj1 = {
  a: 7,
  get b() {
    return this.a + 1;
  },
  set c(x) {
    this.a = x / 2;
  },
};

Object.defineProperties(myObj1, {
  x: {
    get() {
      return -this.a;
    },
  },
});

console.log(myObj1.a);
console.log(myObj1.b);
myObj1.c = 50;
console.log(myObj1.a);
console.log(myObj1.x);


// comparing objects

// different objects are never equal
