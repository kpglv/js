// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Event_loop


// event loop is responsible for
// - executing the code
// - collecting and processing events
// - executing the queued subtasks


// runtime concepts

// queue: a message queue that contains messages to be processed; each message
// has an associated function that gets called to handle the message;
// messages, starting from the oldest one (FIFO), are removed from the queue,
// their corresponding functions are called (placing frames on the stack),
// with the message as an input parameter; the processing of functions
// continues until the stack is empty; then the event loop processes the next
// message in the queue (if any)

// stack: function calls form a stack of frames that contain their arguments
// and local variables; when a function returns, its frame is popped out of
// the stack (LIFO); but arguments and locals may continue to exist if they
// are stored outside the stack (on the heap)

// heap: a (mostly) unstructured region of memory where objects are allocated


// event loop

// a loop that waits synchronously for a message to arrive in the queue to be
// processed

// each message is processed completely before any other message is processed
// (cannot be preempted)


// adding messages

// messages are added anytime an event occurs and there is an event listener
// attached to it; if there is no listener, the event is lost


// communicating between runtimes

// different runtimes (e.g., a web worker or a cross-origin iframe, that has
// its own stack, heap and message queue) can communicate by sending messages
// using 'postMessage' method, that adds messages to the other runtime's queue
// if the latter listens to 'message' events


// never blocking

// operations that usually block in other runtimes (e.g., I/O) are handled via
// events and callbacks
