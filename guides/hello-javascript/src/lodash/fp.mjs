import fp from 'lodash/fp.js';
const _ = fp.convert({
  cap: false,
  curry: false,
  fixed: false,
  immutable: true,
  rearg: false,
});

// https://lodash.com/docs/4.17.15

const c = console.log;
c("FP\n-----");

const o1 = {id: 1, name: "Joe"};
c("immutable set:", o1, "=>", _.set(o1, ["id"], 2));
c("source object has not changed:", o1);
