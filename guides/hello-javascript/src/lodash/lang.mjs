import _ from 'lodash';

// https://lodash.com/docs/4.17.15

const c = console.log;
c("Lang\n-----");

const o1 = {id: 1, name: "Joe", children: [{id: 2, name: "Mary"}]};
const o2 = {id: 1, name: "Joe", children: [{id: 2, name: "Mary"}]};
c("objects are deep equal:", _.isEqual(o1, o2));
