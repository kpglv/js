import _ from 'lodash';

// https://lodash.com/docs/4.17.15

const c = console.log;
c('Arrays\n-----');

// array -> scalar

// returns the first element of given array; undefined if the array is empty
// (null, undefined, not an array)
c('head:', _.head([1, 2, 3]));
c('head of empty array:', _.head([]));

// returns an array without the first element; an empty array if the array is
// empty (null, undefined, not an array)
c('tail:', _.tail([1, 2, 3]));
c('tail of empty array:', _.tail());

// returns element of the array at the given index (default is 0); negative
// indices return elements from the end
c('nth: default (0):', _.nth([1, 2, 3]));
c('nth: 1:', _.nth([1, 2, 3], 1));
c('nth: -2:', _.nth([1, 2, 3], -2));

// array -> array

// returns an array with given number of elements dropped from the beginning
// (default is 1)
c('dropped default (1):', _.drop([1, 2, 3]));
c('dropped 5:', _.drop([1, 2, 3], 5));
c('dropped 2:', _.drop([1, 2, 3], 2));
c('dropped 0:', _.drop([1, 2, 3], 0));

// returns a given number of elements from the array
c('taken default (1):', _.take([1, 2, 3]));
c('taken 5:', _.take([1, 2, 3], 5));
c('taken 2:', _.take([1, 2, 3], 2));
c('taken 0:', _.take([1, 2, 3], 0));

// chunks (partitions) the array into groups of given size (default is 1); the
// last chunk may contain less elements
c('chunked by default (1):', _.chunk(['a', 'b', 'c', 'd']));
c('chunked by 2:', _.chunk(['a', 'b', 'c', 'd'], 2));
c('chunked by 3:', _.chunk(['a', 'b', 'c', 'd'], 3));

// returns an array with all falsey values removed
c('compacted:', _.compact([0, 1, false, 2, '', 3]));

// arrays -> array

// returns an array that is concatenation of given array with any additional
// values/arrays; does not flatten nested arrays beyond the first level of
// nesting
c('concatenated:', _.concat([1], 2, [3], [[4]]));

// returns an array of arrays created from elements of input arrays with
// the same index; the length of the result array is equal to the length of
// the longest input
c('zipped:', _.zip([1, 2], [3, 4, 5]));
