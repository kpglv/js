import _ from 'lodash';

// https://lodash.com/docs/4.17.15

const c = console.log;
c("Objects\n-----");


const o1 = { 'a': [{ 'b': { 'c': 3 } }] };

c("get by path:", _.get(o1, 'a[0].b.c'));
c("get by path:", _.get(o1, ['a', '0', 'b', 'c']));
c("get by path:", _.get(o1, 'a.b.c', 'default'));

const o2 = {id: 1, name: "Joe", age: 23.4, children: []};
c("filter on object goes over values:", _.filter(o2, x => _.isInteger(x)));

c("merged objects:", _.merge(o2, {wife: "Dora"}));
