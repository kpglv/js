// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Indexed_collections


// an array is an ordered list of values that can be accessed by their
// indices; there is no array type, but there is a predefined Array object


// creating arrays

// with items
const arr = Array.of(123);
const arr1 = new Array(1, 2, 3);
const arr2 = Array(4, 5, 6);
const arr3 = [7, 8, 9];
console.log(arr, arr1, arr2, arr3);

// without items (sparse arrays --- with empty slots, that are skipped by some
// operations but treated as undefined by others)
const arrayLength = 2;
const arr4 = new Array(arrayLength);
const arr5 = Array(arrayLength);
const arr6 = [];
// length is just a property, array can be expanded or truncated by setting it
arr6.length = arrayLength;
console.log(arr4, arr5, arr6);


// referring to array elements (and other properties)

console.log(arr3[1]);
console.log(arr3['length']);


// populating an array

// a property will be created instead of element if the index is non-integer
const emp = [];
emp[0] = "Casey Jones";
emp[1] = "Phil Lesh";
emp[2] = "August West";


// iterating over arrays: 'for', 'for ... in'


// array methods

const a = [1, 2, 3];
const arrayMethods = {
  concat: a.concat([4, 5, 6]),
  join: a.join("|"),
  slice: a.slice(1, 3),
  at2: a.at(2),
  indexOf2: a.indexOf(2),
  lastIndexOf2: a.lastIndexOf(2),
  reverse: a.reverse(),
  flat: [1, [2, 3], 4].flat(),
  sort: [7, -1, 0, 12].sort(),
  sortDesc: [7, -1, 0, 12].sort((a, b) => a > b ? -1 : a < b ? 1 : 0),

  // mutate the array
  push: a.push(4),
  pop: a.pop(),
  unshift: a.unshift(0),
  shift: a.shift(0),
  splice: a.slice().splice(1, 1, "foo", "bar"),

  forEach: a.forEach((x, i) => console.log(i, x)),
  map: a.map(x => x + 1),
  flatMap: a.flatMap(x => x + 1),
  filter: a.filter(x => x % 2 != 0),
  find: a.find(x => x % 2 != 0),
  findLast: a.findLast(x => x % 2 != 0),
  findIndex: a.findIndex(x => x % 2 != 0),
  findLastIndex: a.findLastIndex(x => x % 2 != 0),
  reduce: a.reduce((acc, val) => acc + val, 0),
  reduceRight: a.reduceRight((acc, val) => acc + val, 0),
  every: a.every(Number.isInteger),
  some: a.some(x => x % 2 != 0),
};
console.log(arrayMethods);


// array transformations

const inventory = [
  { name: "asparagus", type: "vegetables" },
  { name: "bananas", type: "fruit" },
  { name: "goat", type: "meat" },
  { name: "cherries", type: "fruit" },
  { name: "fish", type: "meat" },
];
// not available on node
// console.log(Object.groupBy(({type}) => type));


// arrays can be multidimensional and store arbitrary properties


// working with array-like objects, that do not implement all array methods
// (e.g., arguments, Nodelist; maybe even strings)

function printArguments() {
  Array.prototype.forEach.call(arguments, (item) => {
    console.log(item);
  });
}
printArguments(1, 2, "foo");

Array.prototype.forEach.call("a string", (chr) => {
  console.log(chr);
});
