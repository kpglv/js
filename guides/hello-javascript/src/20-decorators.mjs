// https://github.com/tc39/proposal-decorators
// https://blog.logrocket.com/understanding-javascript-decorators/
// https://javascriptdecorators.org/tutorial

// decorators are functions that wrap functions, classes, and class members
// to add some functionality to them

// function decorators (manual wrapping, no syntax)

// higher-order functions that take a function as argument and
// return a new function that enhances the function argument without
// modifying it --- e.g., executes some code before and/or after
// calling the function argument

// a function
const logger = console.log;

// a decorator; uses the function argument to log some additional info
// in addition to its normal operation
const loggerDecorator = loggerFn => message => {
    loggerFn(`message logged at ${new Date().toLocaleString()}:`);
    loggerFn(message);
};

const decoratedLogger = loggerDecorator(logger);
decoratedLogger('Hello decorators!');

// validating function arguments and return value

// validating decorator
const validate = (fn, { params = [], ret }) => {
    return (...args) => {
        args.forEach((argument, index) => {
            const predicate = params[index];
            // if no predicate supplied, the argument is considered valid
            const valid = predicate ? predicate(argument) : true;
            if (!valid) {
                throw new TypeError(
                    `${
                        fn.name || fn
                    }: argument value '${argument}' does not satisfy type predicate '${
                        predicate.name || predicate
                    }'`
                );
            }
        });
        const returnValue = fn(...args);
        // if no predicate supplied, the return value is considered valid
        const valid = ret ? ret(returnValue) : true;
        if (!valid) {
            throw new TypeError(
                `${
                    fn.name || fn
                }: return value '${returnValue}' does not satisfy type predicate '${
                    ret.name || ret
                }'`
            );
        }
        return returnValue;
    };
};

// a function
const myAdd = (x, y) => {
    const sum = x + y;
    return sum;
};

// predicates
const isNumber = x => !isNaN(x);
const isSmall = x => x < 123;
const isSmallNumber = x => isNumber(x) && isSmall(x);

// validating function
const validatedMyAdd = validate(myAdd, {
    params: [isNumber, isNumber],
    ret: isSmallNumber,
});

console.log('validated addition of 2 and 3:', validatedMyAdd(2, 3));
try {
    console.log(
        'validated addition of "foo" and 3:',
        validatedMyAdd('foo', 3)
    );
} catch (e) {
    console.log(e.message);
}
try {
    console.log(
        'validated addition of 200 and 300:',
        validatedMyAdd(200, 300)
    );
} catch (e) {
    console.log(e.message);
}

// class decorators

function decorator(value, context) {
    console.log('decorated value is:', value);
    console.log('context is: ', context);
}

// TODO: requires transpilation (babel or TS)

/*
@decorator
class C {
    @decorator // decorates a class field
    p = 5;

    @decorator // decorates a method
    m() {}

    @decorator // decorates a getter
    get x() {}

    @decorator // decorates a setter
    set x(v) {}
}

 */
