// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_generators

// iterators and iterables

// Iterator is an object which defines a sequence of values (and potentially a
// value that signals the end of the sequence); any object that implements the
// iterator protocol --- required 'next()', optional 'return()' and 'throw()'
// methods that return 'IteratorResult' (object with optional any 'value' and
// boolean 'done' properties)

// Iterable is an object that can return an (possibly async) Iterator
// (has a method with name Symbol.iterator and/or Symbol.asyncIterator)

// Iterables can be passed to 'for ... of'

// built-in Iterables: 'String', 'Array', 'TypedArray', 'Map' and Set'

class Seq {
    #values;

    constructor(coll) {
        if (coll) {
            this.#values = Array.from(coll);
        } else {
            this.#values = [];
        }
    }

    // Iterable interface
    [Symbol.iterator]() {
        return this;
    }

    [Symbol.asyncIterator]() {
        return this;
    }

    // Iterator interface
    next() {
        if (this.#values.length === 0) {
            return { done: true };
        } else {
            return { value: this.#values.shift() };
        }
    }

    return(value) {}
    throw(e) {}
}

const seq = new Seq([1, 2, 3, 4]);
for (const x of seq) {
    console.log('from iterator', x);
}

class AsyncSeq {
    #values;

    constructor(coll) {
        if (coll) {
            this.#values = Array.from(coll);
        } else {
            this.#values = [];
        }
    }

    // async Iterable interface
    [Symbol.asyncIterator]() {
        return this;
    }

    // async Iterator interface; each IteratorResult is wrapped in a promise
    next() {
        if (this.#values.length === 0) {
            return Promise.resolve({ done: true });
        } else {
            return Promise.resolve({ value: this.#values.shift() });
        }
    }

    return(value) {}
    throw(e) {}
}

const asyncSeq = new AsyncSeq(['a', 's', 'y', 'n', 'c']);
for await (const x of asyncSeq) {
    console.log('from async iterator:', x);
}

// generators

// a Generator is an iterable iterator returned by a generator function

// a GeneratorFunction (object defined with function* syntax), when called,
// returns a new Generator object

// when the generator's next() method is called, the generator function's
// body is executed until the first yield expression, which specifies the
// value to be returned from the iterator or, with yield*, delegates to
// another generator function

// return statement yields a value and terminates iteration

function* generator() {
    yield 1;
    yield 2;
    yield 3;
}

// simple generator

const gen = generator();
console.log('from simple generator:', gen.next().value);
console.log('from simple generator:', gen.next().value);
console.log('from simple generator:', gen.next().value);

// infinite sequence

function* infinite() {
    let index = 0;

    while (true) {
        yield index++;
    }
}

const naturals = infinite();

console.log('from infinite seq:', naturals.next().value);
console.log('from infinite seq:', naturals.next().value);
console.log('from infinite seq:', naturals.next().value);

// delegating to another generator

function* aGenerator(i) {
    yield i + 1;
    yield i + 2;
    yield i + 3;
}

function* anotherGenerator(i) {
    yield i;
    yield* aGenerator(i);
    yield i + 10;
}

const delegatingGen = anotherGenerator(10);

console.log('from delegating generator:', delegatingGen.next().value);
console.log('from delegating generator:', delegatingGen.next().value);
console.log('from delegating generator:', delegatingGen.next().value);
console.log('from delegating generator:', delegatingGen.next().value);
console.log('from delegating generator:', delegatingGen.next().value);

// passing arguments into generator

function* logGenerator() {
    console.log(0);
    console.log(1, yield);
    console.log(2, yield);
    console.log(3, yield);
    console.log(4);
}

const yetAnotherGen = logGenerator();

// the first call of next executes from the start of the function
// until the first yield statement
yetAnotherGen.next();
yetAnotherGen.next('pretzel');
yetAnotherGen.next('california');
yetAnotherGen.next('mayonnaise');

// returning from generator

function* yieldAndReturn() {
    yield 'Y';
    return 'R';
    yield 'unreachable';
}

const oneMoreGen = yieldAndReturn();
console.log('from generator on yield:', oneMoreGen.next()); // { value: "Y", done: false }
console.log('from generator on return', oneMoreGen.next()); // { value: "R", done: true }
console.log('iteration is finished', oneMoreGen.next()); // { value: undefined, done: true }
