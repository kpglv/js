// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_types


// comments

// line comment

/* multiline
   comment */

/** JSDoc comment */


// variable declarations

// global/module/function-scoped; hoisted (declaration, not initialization)
var a = 1;
var a1; // initialization is optional (will be undefined)

// block-scoped; not hoisted
let b = 2; // initialization is optional (will be undefined)
const c = 3; // initialization is required; cannot be reassigned


// primitive data types (ECMA standard)

// - undefined: not defined
// - null: no value
// - boolean: true, false
// - number: integer or floating point
// - bigint: integer with arbitrary precision
// - string: sequence of characters
// - symbol: unique immutable value

// note: do not use capitalized (boxed) types (String, Boolean, etc)


// literals


// boolean

true;
false;


// numeric

123; // decimal
0o123; // octal
// 0123; // also octal; not allowed in strict mode
0x123; // hexadecimal
0b1001; // binary

123n; 0x123n; // bigint

3.1415926; // floating-point
3.1E+12;
.1e-23;


// string

// string literals; special characters/sequences escaped by backslash
// the same
const foo1 = "foo";
const foo2 = 'foo';
const escaped = "\n\tescaped\u00a9 \
\"\u{2F804}\"";
console.log(escaped);

// template literal; multiline, interpolated
const foo3 = "bar";
const foo4 = `foo is ${foo3}`;
console.log(foo4);

// tagged template literal
const print = (segments, ...args) => {
  let message = segments[0];
  for (let i = 0; i < args.length; i++) {
    message += (segments[i+1] + args[i]);
  }
  console.log(message);
};
const one = '- learn JS\n';
const two = '- learn Web APIs\n';
print`I need to do:
${one}${two}`;


// array

const arr = [ 1, /* empty */ , 2, 3, "foo", /* trailing comma ignored*/ ];
console.log(arr);


// object

// note: curly bracket at the start of line is interpreted as a block, not as
// an object literal

const obj = {
  // a regular property; can be accessed by dot or bracket notation
  x: 1,
  // a property with non-identifier name; must be accessed by bracket notation
  // only
  '!': "foo",
  // shorthand for 'arr: arr'
  arr,
  // __proto__
  __proto__: {
    toString() {
      return "prototype";
    }
  },
  // methods
  toString() {
    // super calls
    return "d " + super.toString();
  },
  // computed (dynamic) property names
  ["prop_" + (() => 42)()]: 42,
};
console.log(obj.toString());


// regexp

const re = /ab+c/;
