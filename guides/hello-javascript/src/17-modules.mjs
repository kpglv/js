// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules


// ES6 modules


// exporting module features

export const shape = "square";


// importing features from another module; import declarations are hoisted

// default imports
import square from './module.mjs';
// named imports
import { name } from './module.mjs';
// renamed imports
import { name as nomen, default as sq } from './module.mjs';
// module imports
import * as m from './module.mjs';

console.log(name, nomen, m.name);
console.log(square(5), sq(5), m.default(5));


// TODO: import maps


// aggregating modules

// export features from several submodules
export * from './module.mjs';
export { name } from './module.mjs';


// dynamic module loading

import('./module.mjs')
  .then((module) => console.log('dynamic', module.default(5)));


// top-level 'await'

const colorsUrl =
      "https://mdn.github.io/js-examples/module-examples/top-level-await/data/colors.json";
const colors = await fetch(colorsUrl)
      .then((response) => response.json());
console.log(colors);
export default colors;
// or the same:
// const colors = fetch(colorsUrl).then((response) => response.json());
// export default await colors;


// writing isomorphic modules

// - separate the code into pure 'core' and impure 'binding' parts (where
//   access to platform-specific APIs is done); one shared core, ifferent
//   bindings for different platforms/execution environments

// - use feature detection, test for specific globals to exist

// - use polyfills to provide fallbacks for missing features
