// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Text_formatting


// strings are sequences of 16-bit unsigned integer values (UTF-16 code
// units); string length is measured in these

// note: all properties of a String object can be accessed on a string
// literal; do not use String objects unless necessary

const s = 'foo';
const stringMethods = {
  length: s.length,

  charAt0: s.charAt(0),
  charCodeAt0: s.charCodeAt(0),
  codePointAt0: s.codePointAt(0),

  indexOfO: s.indexOf('o'),
  lastIndexOfO: s.lastIndexOf('o'),

  startsWithF: s.startsWith('f'),
  endsWithO: s.endsWith('o'),
  includesX: s.includes('x'),

  concat: s.concat("bar"),

  split: s.split(''),

  slice: s.slice(1, 3),

  substring: s.substring(1, 3),
  substr: s.substr(1, 2),

  match: s.match(/\w+/),
  matchAll: Array.from(s.matchAll(/\w/g)),
  replace: s.replace('o', 'a'),
  replaceAll: s.replaceAll('o', 'a'),
  search: s.search(/o/),

  lowerCase: s.toLowerCase(),
  upperCase: s.toUpperCase(),

  normalize: s.normalize(),

  repeat: s.repeat(3),

  trim: s.trim(),

};
console.log(stringMethods);


// internationalization


// date and time formatting

const july172014 = new Date("2014-07-17");
const options = {
  year: "2-digit",
  month: "2-digit",
  day: "2-digit",
  hour: "2-digit",
  minute: "2-digit",
  timeZoneName: "short",
};
const americanDateTime = new Intl.DateTimeFormat("en-US", options).format;
console.log(americanDateTime(july172014));


// number formatting

const gasPrice = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 3,
});
console.log(gasPrice.format(5.259));

const hanDecimalRMBInChina = new Intl.NumberFormat("zh-CN-u-nu-hanidec", {
  style: "currency",
  currency: "CNY",
});
console.log(hanDecimalRMBInChina.format(1314.25));


// string collation

const names = ["Hochberg", "Hönigswald", "Holzman"];

const germanPhonebook = new Intl.Collator("de-DE-u-co-phonebk");
// as if sorting ["Hochberg", "Hoenigswald", "Holzman"]:
console.log(names.sort(germanPhonebook.compare).join(", "));

const germanDictionary = new Intl.Collator("de-DE-u-co-dict");
// as if sorting ["Hochberg", "Honigswald", "Holzman"]:
console.log(names.sort(germanDictionary.compare).join(", "));
