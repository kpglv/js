// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Keyed_collections


// keyed (associative) collections are collections of elements indexed by key,
// iterable in the order of insertion


// maps

// can have keys of any type, not just strings or symbols; maintains size
// information; maintains insertion order; no default keys (no prototype)
const sayings = new Map();
sayings.set("dog", "woof");
sayings.set("cat", "meow");
sayings.set("elephant", "toot");
sayings.size;
sayings.get("dog");
sayings.get("fox");
sayings.has("bird");
sayings.delete("dog");
sayings.has("dog");

for (const [key, value] of sayings) {
  console.log(`${key} goes ${value}`);
}

sayings.clear();
sayings.size;


// weak maps

// can only have objects and non-registered symbols as keys; presence of a key
// in a WeakMap does not prevent it from being garbage collected (and then
// probably the value too); cannot be enumerated (keyset is not deterministic)


// sets

// collections of unique values

const mySet = new Set();
mySet.add(1);
mySet.add("some text");
mySet.add("foo");

mySet.has(1);
mySet.delete("foo");
mySet.size;

for (const item of mySet) {
  console.log(item);
}

// convert to array and back
console.log(Array.from(mySet));
console.log([...mySet]);
console.log(new Set([1, 2, 3, 4, 4]));


// weak sets

// analogous to WeakMap
