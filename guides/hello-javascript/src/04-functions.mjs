// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Functions


// parameters are passed by value


// function declaration; function-scoped; hoisted (both declaration and initialization)

// named
function square(number) {
  return number * number;
}


// function expression

// anonymous
const square1 = function(number) {
  return number * number;
};

// named
const square2 = function sqr(number) {
  return number * number;
};


// calling functions

console.log(square(5));


// recursion

// function can refer to itself by name, 'arguments.callee', or an in-scope
// variable that refers to the function


// closures

// functions are lexically scoped; nested functions are private to the
// containing function, and close over arguments/variables in scope

function adderTo(x) {
  // closes over x
  return function addToX(y) {
    return x + y;
  };
}

const adderTo3 = adderTo(3);
console.log(adderTo3(5));


// name conflicts

// names defined in more nested scopes take precedence (shadow names defined
// in outer scopes)


// 'arguments' object: array-like (can be indexed, has length)


// parameters

// default parameters: will be used instead of undefined if the corresponding
// argument is missing

function multiply(a, b = 1) {
  return a * b;
}

console.log(multiply(5));

// rest parameters: variable number of arguments that will be collected to an
// array

function multiplyAll(multiplier, ...theArgs) {
  return theArgs.map((x) => multiplier * x);
}

console.log(multiplyAll(2, 1, 2, 3));


// arrow function expressions (fat arrow functions); do not have 'this',
// 'arguments', 'super', or 'new.target'; always anonymous

const a = ["Hydrogen", "Helium", "Lithium", "Beryllium"];
console.log(a.map((s) => s.length));


// 'this'

// until arrow functions, every new function defined its own 'this' (a new
// object in constructor, undefined in strict mode function calls, the base
// object for functions called as methods, etc)

// value of 'this' can be assigned to a variable to be closed over, or a bound
// function can be created with the proper 'this'

// an arrow function does not have its own 'this' and uses one of the
// enclosing context

function Person() {
  this.age = 0;

  // traditional workarounds of improper 'this' in functions

  let self = this;
  setTimeout(function() {
    self.age++;
    console.log(self.age);
  }, 100);

  let ageIncrementer = (function() {
    this.age++;
    console.log(this.age);
  }).bind(this);
  setTimeout(ageIncrementer, 200);

  // arrow function

  setTimeout(() => {
    this.age++; // `this` properly refers to the person object
    console.log(this.age);
  }, 300);
}

const p = new Person();
