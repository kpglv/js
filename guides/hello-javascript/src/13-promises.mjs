// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises


// a promise is an object that represents the eventual completion (or failure)
// of an asynchronous operation


// example: pyramid of callbacks

const onError = (e) => console.error('callback pyramid error:', e);

const produce = (x, onSuccess, onError) => onSuccess(x);
const increment = (x, onSuccess, onError) => {
  try {
    onSuccess(x + 1);
  } catch (e) {
    onError(e);
  };
};
const square = (x, onSuccess, onError) => {
  try {
    onSuccess(x * x);
  } catch (e) {
    onError(e);
  };
};
const report = (x) => console.log('callback pyramid result:', x);

const doSomethingAsyncWithCallbacks = (x, onSuccess, onError) => {
  produce(x, function(produced) {
    increment(produced, function(incremented) {
      square(incremented, function(squared) {
        report(squared);
      }, onError);
    }, onError);
  }, onError);
};
doSomethingAsyncWithCallbacks(12);


// example: chain of Promises

const doSomethingAsyncWithPromiseChain = (x) => {
  // each 'then()' returns a new Promise, that represents completion not just of
  // original Promise, but also of a callback passed to it
  return Promise.resolve(x)
  // callback receives a fulfillment value from the original Promise (on which
  // 'then' was called)
    .then(x => x + 1)
  // if a Promise is returned, next callback will receive its fulfillment value;
  // Promise that was started in a callback but was not returned is said to be
  // 'floating' and there is no way to track its settlement; also the next
  // callback in the chain will be called early
    .then(x => Promise.resolve(x * x))
  // 'then()' can also receive a failure callback as a second argument
    .then(x => console.log('promise chain result:', x))
  // short for 'then(null, failureCallback)'; catches all errors in the chain
  // (and in nested subchains, if they do not catch their errors themselves)
    .catch(e => console.error('promise chain error:', e));
};
doSomethingAsyncWithPromiseChain(12);


// example: async/await (a syntax sugar over Promises)

const doSomethingWithAsyncAwait = async (x) => {
  try {
    // awaiting for Promise does not stop the entire program, only the parts
    // that depend on its value
    const produced = await Promise.resolve(x);
    const incremented = x + 1;
    const squared = await Promise.resolve(incremented * incremented);
    console.log('async/await result:', squared);
  } catch (e) {
    console.error('async/await error:', e);
  }
};
await doSomethingWithAsyncAwait(12);


// nesting

// nested chains of promises can be made flat for ease of understanding; but
// nesting provides a way to limit the scope of 'catch' statements (e.g., to
// prevent optionsl steps from failing the entire chain); a nested 'catch'
// only catches failures in its scope and below, not higher up, and upper
// level failures are not swallowed by nested 'catch' (nested try/catch works
// the same for async/await)

Promise.resolve(12)
  .then(x => x + 1)
  .then(x => Promise.reject('oops, squaring failed')
        // ignore failures in optional subchain
        .catch(e => {
          console.error('ignoring nested promise subchain error:', e);
          return x;
        }))
  .then(x => console.log('nested promise chain result:', x))
  .catch(e => console.error('nested promise chain error:', e));


// chaining after a 'catch'

// may be useful to do something no matter if the chain succeeded or failed
// (like 'finally' clause in try/catch, or just the code after the try/catch);
// 'then()' or 'finally()' seem almost the same
Promise.reject('oops')
  .catch((e) => 'err') // result is accessible by following thens
  .then((resOrErr) => console.log('then after catch:', resOrErr));
Promise.reject('oops')
  .catch((e) => 'err') // result is not accessible by following finallys
  .finally((resOrErr) => console.log('finally after catch:', resOrErr));



// rejection events

// unhandled rejections bubble to the top of the call stack;
// 'unhandledrejection' (browsers) / 'unhandledRejection' (node) event is
// fired (on 'window' or Worker instance / 'process'), so a fallback handler
// can be provided


// composition

// - Promise.all(promises): returns a promise with an array of the fulfillment
//   values; if any promise is rejected, rejects the returned promise and
//   aborts all other operations

// - Promise.allSettled(promises): returns a promise with an array of outcomes;
//   ensures that all operations are complete before resolving

// - Promise.any(promises): returns a promise with the first fulfillment
//   value; rejects if all promises reject (and if there were no promises in
//   list), with an array of rejection reasons

// - Promise.race(promises): returns a promise that settles with the eventual
//   state of the first promise that settles

// promises run concurrently by default; organize them in a chain to get
// sequential composition


// creating a Promise around an old callback API

// wraps 'setTimeout'
const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms));

wait(3 * 1000)
  .then(() => console.log(`waited 3 seconds`))
  .catch((e) => console.error('waiting error:', e));


// timing

// in the callback-based API, when the callback gets called depends on
// API implementor

// promises are a form of inversion of control --- API implementor does not
// control when the callback gets called; the promise implementation maintains
// the callback queue and decides when to call them, and provides strong
// semantic guarantees on that (e.g., they will be called asynchronously, in
// the order of addition, and run even if added after the async operation the
// promise represents succeeds or fails)

queueMicrotask(() => console.log('->> microtask is run'));
