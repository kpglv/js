// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_operators


// expressions

// expression is a unit of code that resolves (evaluates) to a value; can be
// pure or have side-effects (e.g., assignment)


// operators

// operators join operands that are either basic expressions or formed by
// higher-precedence operators; operator precedence can be overridden by
// grouping with parentheses

// operators can be unary, binary or ternary


// assignment operators

// assign value of the right operand (expression) to the left operand (a
// variable or object property); there are number of combined assignment
// operators like '+='; destructuring is supported

const foo = ["one", "two", "three"];
const [one, two, three] = foo;

const bar = { id: 1, name: "Joe" };
const { id, name } = bar;

// note: although assignment is an expression, avoid assignment chaining or
// nesting; assignments are right-associative, but evaluate left to right


// comparison operators

// return logical value; compares numbers numerically, strings in
// lexicographic order (using Unicode values)

// '==', '!=': tries to convert to appropriate types first, '===' and '!==' do
// not (identity comparison)


// arithmetic operators

// '+', '-', '*', '/', '++', '--', '%' (remainder), '**' (exponentiation)


// bitwise operators; use 32-bit signed operands (convert from and to 64-bit
// floats before/after an operation)

// '&', '|', '^', '~', '<<', '>>', '>>>'


// logical operators (short-circuiting)

// '&&', '||', '!'


// bigint operators

// almost the same as numeric, but cannot be mixed with numbers (require
// explicit conversion)


// string operators

// '+' (concatenation)


// conditional operator

const age = 19;
const status = age >= 18 ? "adult" : "minor";


// comma operator

// evaluates both operands and returns the value of the last operand (e.g., to
// use multiple expressions in for loop init/update clauses)


// unary operators

// - 'delete' (deletes an object property)
// - 'typeof' (returns string representation of the value's type)
// - 'void' (evals expression without returning a value)


// relational operators

// 'in': returns true if the property is in the object

const trees = ["redwood", "bay", "cedar", "oak", "maple"];
console.log(0 in trees);

console.log("PI" in Math);

// 'instanceof': returns true if the object is of the given type

const theDay = new Date(1995, 12, 17);
console.log(theDay instanceof Date);


// basic expressions

// - literals
// - identifiers
// - grouping operator: ( )
// - 'new' operator: create object instance
// - 'this' keyword: current object
// - 'super' keyword: parent object
