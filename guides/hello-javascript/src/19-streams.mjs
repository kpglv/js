// https://developer.mozilla.org/en-US/docs/Web/API/Streams_API

// a stream represents some resource whose contents can be read/written in
// small chunks; streams can be readable, writable, and duplex

// a reader is used to read data from a stream; a writer is used to write data
// to a stream; a controller is used to control the stream

import readline from 'readline';

const r = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    error: process.stderr,
});

// line by line

/*
r.on('line', (line) => console.log(line));
r.once('close', () => console.log('bye'));
 */

/* for await (const line of r) {
    console.log(line);
}
 */


// character by character

process.stdin.on('data', data => console.log(data));
