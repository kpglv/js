// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling


// block statement

var x = 1;
let y = 1;
{
  var x = 2;
  let y = 2;
}
console.log(x); // 2
console.log(y); // 1


// conditional statements

// falsey values are false, null, undefined, 0, NaN, ""; anything else is
// truthy

// if-else

if (true) {
  console.log('ok');
} else {
  console.log('not ok');
}

// switch

const cond = 12;
switch (cond) {
case 1:
  console.log(1);
  break;
case 12:
  console.log(12);
  break;
default:
  console.log('oops');
}


// exception handling statements

try {
  // can throw anything, but Error is convenient
  throw new Error('oops');
} catch (e) {
  // e only exists in this clause
  console.error('caught:', e.name, e.message);
} finally {
  // e.g., release resources
  console.log('finally executed');
  // returning from there overrides return values of try and catch clauses
  // (including thrown errors)
}
