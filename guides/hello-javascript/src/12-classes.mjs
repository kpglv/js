// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_classes


// classes are syntactic sugar over existing prototypical inheritance
// mechanism; classes are values, and have their own prototype chains


// three key features of classes:
// - constructor (to create instances)
// - instance fields and methods; not accessible on class
// - static (class) fields and methods; not accessible on instances


// class declarations (not hoisted)

class MyClass {

  // static field
  static myStaticField = "bar";

  // static method
  static myStaticMethod() {
    console.log("myStaticMethod");
  }

  // static block (initializer)
  static {
    console.log("my static block");
  }


  // constructor
  constructor() {
    // implicitly returns 'this'; do not explicitly return anything from
    // constructor
    this.myAnotherField = "another " + this.myField;
  }

  // instance field
  myField = "foo";

  // fields, methods, static fields, and static methods all have "private"
  // forms; cannot be defined on the fly (after instantiation); cannot be
  // deleted
  #myPrivateField = "bar";

  // instance method
  myMethod() {
    console.log("myMethod");
  }
}

MyClass.myStaticMethod();
const cls = new MyClass();
cls.myMethod();
console.log(cls);


// class expressions

// can be anonymous and named
const SomeClass = class {};
// the name is only visible to the class body
const SomeOtherClass = class MyNamedClass {};


// constructors, fields, methods

class Color {

  #values;

  constructor(r, g, b) {
    this.#values = [r, g, b];
  }

  getRed() {
    return this.#values[0];
  }

  setRed(value) {
    this.#assertValid(value);
    this.#values[0] = value;
  }

  get green() {
    return this.#values[1];
  }

  set green(value) {
    this.#assertValid(value);
    this.#values[1] = value;
  }

  redDifference(anotherColor) {
    if (!(#values in anotherColor)) {
      throw new TypeError("Color instance expected");
    }
    // can access private fields of other instances of the same class
    return this.#values[0] - anotherColor.#values[0];
  }

  #assertValid(colorComponent) {
    if (colorComponent < 0 || colorComponent > 255) {
      throw new RangeError("Invalid RGB color component value");
    }
  }
}

const red = new Color(255, 0, 0);
const crimson = new Color(220, 20, 60);
console.log(red);
console.log(red.redDifference(crimson));


// inheritance (extends)

// single inheritance; derived classes don't have access to parent's private
// fields/methods
class ColorWithAlpha extends Color {

  #alpha;

  constructor(r, g, b, a) {
    // mandatory call before access to 'this'
    super(r, g, b);
    this.#alpha = a;
  }

  get alpha() {
    return this.#alpha;
  }

  set alpha(value) {
    if (value < 0 || value > 1) {
      throw new RangeError("Alpha value must be between 0 and 1");
    }
    this.#alpha = value;
  }
}

const color = new ColorWithAlpha(255, 200, 23, 0.7);
console.log(color.green, color.alpha);

// inheritance establishes an hierarchy; subclasses are instances of their
// superclasses
console.log(color instanceof Color, color instanceof ColorWithAlpha);
