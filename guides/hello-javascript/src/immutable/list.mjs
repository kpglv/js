import { c } from './helpers.mjs';
import Immutable from 'immutable';
// or import { List } from 'immutable'

// a factory function, not a class
const emptyList = Immutable.List();
c('an empty list:', emptyList.toString());

const plainArray = [1, 2, 3, 4];
const listFromPlainArray = Immutable.List(plainArray);
c('a list created from array:', listFromPlainArray.toString());

const plainSet = new Set([1, 2, 3, 4]);
const listFromPlainSet = Immutable.List(plainSet);
c('a list created from set:', listFromPlainSet.toString());

const arrayIterator = plainArray[Symbol.iterator]();
const listFromCollectionArray = Immutable.List(arrayIterator);
c('a list created from iterator:', listFromCollectionArray.toString());

c(
    'all these lists are equivalent:',
    listFromPlainArray.equals(listFromCollectionArray) &&
        listFromPlainSet.equals(listFromCollectionArray) &&
        listFromPlainSet.equals(listFromPlainArray)
);
