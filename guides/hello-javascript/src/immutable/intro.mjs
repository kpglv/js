// https://immutable-js.com/

import { Map, fromJS } from 'immutable';

const map1 = Map({ a: 1, b: 2, c: 3 });
const map2 = map1.set('b', 50);
// shallow conversion
console.log("map1.toArray():", map1.toArray());
console.log("map1.toObject():", map1.toObject());
// deep conversion
console.log("map1.toJS():", map1.toJS());
console.log("map2.toJSON():", map2.toJSON());
console.log("map1.b vs. map2.b:", map1.get('b') + ' vs. ' + map2.get('b'));

const map3 = fromJS({id: 1, index: 2, name: "Joe"});
console.log("from JS:", map3);
