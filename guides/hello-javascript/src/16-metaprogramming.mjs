// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Meta_programming


// 'Proxy' and 'Reflect' allow to intercept fundamental language operations
// (e.g., property lookup, assignment, enumeration, function invocation, etc)
// and define custom behavior for them


// proxies

// handler: object that contains traps
const handler = {
  // a 'get' trap: interceptor of an operation; traps must maintain some
  // invariants
  get(target, name) {
    // returns 42 for absent properties
    return name in target ? target[name] : 42;
  },
};

// takes a target (an object access to which will be proxied) and a handler
const p = new Proxy({}, handler);
p.a = 1;
console.log(p.a, p.b);

// proxy may be revocable (will throw TypeError if interceptable operations
// are used after revocation)


// reflection

// 'Reflect' provides interceptable operations as methods (the same as those
// of proxy handlers)

// the same
console.log(Function.prototype.apply.call(Math.floor, undefined, [1.75]));
console.log(Reflect.apply(Math.floor, undefined, [1.75]));

const obj = {};
if (Reflect.defineProperty(obj, "foo", {})) {
  console.log("ok");
} else {
  console.log("fail");
}
