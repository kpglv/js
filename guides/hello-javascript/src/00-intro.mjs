// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Introduction

// JavaScript is a dynamically-typed scripting language; it contains a set of
// standard types and objects (Array, Date, Math etc) and a core set of
// syntaxes such as operators, statements and control structures

// - Client-side JavaScript provides objects and APIs to control a browser and
//   its Document Object Model

// - Server-side JavaScript provides objects and APIs to manipulate a host
//   system (start processes, access file systems, use network etc)

// JavaScript is standardized at Ecma International; the ECMAScript standard
// is documented in the ECMA-262 specification
