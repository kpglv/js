// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_expressions


// creating a regular expression

// compiled at load time
const re = /ab+c/;
const re1 = /ab+c/ig;
// compiled at run time
const re2 = new RegExp("ab+c");
const re3 = new RegExp("ab+c", "ig");


// using regular expressions

const s = 'foobar123foobaz';
const r = /o+b/;

const regexFunctions = {
  exec: r.exec(s),
  test: r.test(s),
};
console.log(regexFunctions);
