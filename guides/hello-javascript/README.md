# hello-javascript

Exploring basics of JavaScript language, its standard library (with
third-party utility collections), and platform APIs.

- Introduction to JavaScript
  https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Introduction
- A modern JavaScript utility library delivering modularity, performance &
  extras https://lodash.com/
- Immutable collections for JavaScript https://immutable-js.com/

## Getting Started

Explore the code and comments in `src`.
