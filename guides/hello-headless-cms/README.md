# hello-headless-cms

Exploring headless CMSes.

- Strapi
  - Quick Start Guide https://docs.strapi.io/dev-docs/quick-start
  - A curated list of awesome things related to Strapi
    https://github.com/strapi/awesome-strapi.

Parts A, B, D of the quick start guide were completed (skipping deploying on
Strapi Cloud).

## Getting started

Credentials: n***@gmail.com/Admin1234567 (for local SQLite DB).

Start Strapi with hot reload
```
yarn develop
```

Start Strapi without hot reload

```
yarn start
```

Build the admin panel

```
yarn build
```

Execute arbitrary Strapi CLI command

```
yarn strapi <command>
```
