# hello-htmz

Using `htmz` --- a snippet of HTML that allows to swap arbitrary
page fragments with static HTML by clicking a link or a form button.

`htmz` is a generalization of HTML frames, that allows to load HTML
resources within any element (not only frame or iframe) on the page.

-   https://leanrada.com/htmz/
-   https://github.com/Kalabasa/htmz

Example code is based on examples provided by the `htmz` author.

## Getting Started

Serve the `src` directory, e.g.

```
serve src
```

and navigate to the exposed URL in the browser.
