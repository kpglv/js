# hello-headless-cms-git

Exploring Git-based headless CMSes fronted by static sites.

-   Open source content management for your Git workflow https://decapcms.org/
-   VitePress: Vite & Vue Powered Static Site Generator https://vitepress.dev/

## Getting Started

Start VitePress in development mode

```
yarn docs:dev
```

> Note: this is a work in progress; git-based headless CMSes require
> integration with some git service to be configured
