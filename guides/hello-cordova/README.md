# hello-cordova

Exploring Apache Cordova mobile development framework.

-   https://cordova.apache.org/

## Getting Started

See installed platforms:

```
cordova platform ls
```

Build the app for all platforms:

```
cordova build
```

Build and run the app:

-   In browser: `cordova run browser`
-   With Electron: `cordova run electron`
-   With Android device or emulator (requires Android SDK):
    -   `cordova run android`
    -   `cordova emulate android`
