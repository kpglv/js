# hello-vue

Exploring basics of Vue 3 and its common libraries and tools.

- Vue Router
- Pinia
- Vite
- Vitest & Vue Test Utils


## Getting Started

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```
