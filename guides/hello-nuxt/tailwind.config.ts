import type { Config } from 'tailwindcss';
import flowbite from 'flowbite/plugin';
import typography from '@tailwindcss/typography';

const config: Config = {
    content: [
        './components/**/*.{js,vue,ts}',
        './layouts/**/*.vue',
        './pages/**/*.vue',
        './plugins/**/*.{js,ts}',
        './nuxt.config.{js,ts}',
        './node_modules/flowbite/**/*.{js,ts}',
    ],
    theme: {
        extend: {},
    },
    plugins: [flowbite, typography],
};

export default config;
