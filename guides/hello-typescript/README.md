# hello-typescript

Exploring basics of Typescript.

-   The TypeScript handbook
    https://www.typescriptlang.org/docs/handbook/intro.html

## Getting Started

Explore the code and comments in `src`.

Run `yarn build` to compile Typescript sources; run `yarn clean`
to remove results of compilation.

After build, run example code from `dist` directory, e.g.,
`node dist/09-decorators.js`.
