// https://www.typescriptlang.org/docs/handbook/2/functions.html


// to make the file a ES module
export { };


// function type expressions; parameter names are required

let fn: (a: string) => void = (a) => console.log(a)
fn("hello")


// call signatures: make an object type callable

type DescribableFunction = {
    description: string;
    (someArg: number): boolean; // a call signature; can be overloaded
};

const myFunc: DescribableFunction = (someArg: number) => {
    return someArg > 3;
};
myFunc.description = "default description";

console.log(myFunc.description + " returned " + myFunc(6));


// construct signatures: for functions that can be invoked with 'new'

type SomeConstructor = {
    new(s: string): Date; // a construct signature
};

function fn1(ctor: SomeConstructor) {
    return new ctor("hello");
}

// some objects can be called with or without 'new'

interface CallOrConstruct {
    (n?: number): string;
    new(s: string): Date;
}


// generic functions

// types of parameters and or return value relate in some way

function firstElement<Type>(arr: Type[]): Type | undefined {
    return arr[0];
}

// 'Type' is inferred
const str = firstElement(["a", "b", "c"]);
const num = firstElement([1, 2, 3]);
const undef = firstElement([]);
console.log(str, num, undef)

// type parameters can be constrained

// 'Type' should have a 'length' property; return type is inferred
function longest<Type extends { length: number }>(a: Type, b: Type) {
    if (a.length >= b.length) {
        return a;
    } else {
        return b;
    }
}

const longerArray = longest([1, 2], [1, 2, 3]);
console.log(longerArray);
const longerString = longest("alice", "bob");
console.log(longerString);

// note: should return 'Type' (the same type as 'x'), not some
// other type matching the constraint
function someFn<Type extends { length: number }>(x: Type): Type {
    return x
}

// sometimes type arguments cannot be inferred and should be
// specified

function combine<Type>(arr1: Type[], arr2: Type[]): Type[] {
    return arr1.concat(arr2);
}

const arr = combine<string | number>([1, 2, 3], ["hello"]);
console.log(arr);


// guidelines for writing good generic functions

// - push type parameters down (use the type parameter itself rather
//   than constraining it)

// - use fewer type parameters (avoid unnecessary type parameters
//   --- that don't relate two values)

// - type parameters should appear twice (they intended to relate
//   types of multiple values)


// optional parameters; undefined can always be passed explicitly
// instead of an optional parameter

function f1(x?: number) { return x }


// default parameters

function f2(x = 10) { return x }


// optional parameters in callbacks: don't --- a function can always
// be called with more arguments than it requires (and ignore the
// excess)

// do not make 'index' optional
function myForEach(arr: any[], callback: (arg: any, index: number) => void) {
    for (let i = 0; i < arr.length; i++) {
        callback(arr[i], i);
    }
}

myForEach([1, 2, 3], (a) => console.log(a));
myForEach([1, 2, 3], (a, i) => console.log(a, i));


// function overloads

// overload signatures, can be called
function makeDate(timestamp: number): Date;
function makeDate(m: number, d: number, y: number): Date;
// implementation signature, cannot be called directly
function makeDate(mOrTimestamp: number, d?: number, y?: number): Date {
    if (d !== undefined && y !== undefined) {
        return new Date(y, mOrTimestamp, d);
    } else {
        return new Date(mOrTimestamp);
    }
}
// overload and implementation signatures must also be compatible

const d1 = makeDate(12345678);
const d2 = makeDate(5, 5, 5);
// nope, no such overload
// const d3 = makeDate(1, 3);

// prefer parameters with union types instead of overloads


// declaring 'this' in a function

const user1 = {
    id: 123,
    admin: false,
    becomeAdmin: function () {
        // type of 'this' is inferred
        this.admin = true;
    },
};

interface User {
    admin: boolean;
}

interface DB {
    // type of 'this' is explicitly specified
    filterUsers(filter: (this: User) => boolean): User[];
}

const users = [{ admin: true }, { admin: true }, { admin: false }];

const db = {
    filterUsers: function (f: (this: User) => boolean) {
        let us: User[] = []
        for (let u of users) {
            let meth = f.bind(u)
            if (meth()) {
                us.push(u);
            }
        }
        return us
    }
};

const admins = db.filterUsers(function (this: User) {
    return this.admin;
});
console.log(admins);


// other types to know about

// - void: represents the return value of functions which don't
//   return a value (not the same as undefined)

// - object: represents a value that isn't a primitive (string, number,
//   bigint, boolean, symbol, null, undefined); do not use 'Object'

// - unknown: any value; it is not legal to do anything with unknown
//   value (must be coerced first?)

// - never: represents values which are never observed (e.g., there
//   is nothing left in a union after narrowing)

// - Function: a function that returns 'any', with properties 'bind',
//   'call', 'apply', etc


// rest parameters and arguments

function multiply1(n: number, ...m: number[]) {
    return m.map((x) => n * x);
}
const a1 = multiply1(10, 1, 2, 3, 4);
console.log(a1);

const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];
arr1.push(...arr2);
console.log(arr1);

// an array of numbers, will not work
// const args = [8, 5];
// inferred as 2-length tuple
const args = [8, 5] as const;
const angle = Math.atan2(...args);


// parameter destructuring

function sum1({ a, b, c }: { a: number; b: number; c: number }) {
    console.log(a + b + c);
}

// the same
type ABC = { a: number; b: number; c: number };
function sum2({ a, b, c }: ABC) {
    console.log(a + b + c);
}

// assignability of functions

// functions of type that declare to return void can return any
// other value, but it will be ignored

type voidFunc = () => void;

const f3: voidFunc = () => {
    return true;
};

// but literal function definition that has a void return type
// must not return anything

function f4(): void {
    // @ts-expect-error
    return true;
}