// https://www.typescriptlang.org/docs/handbook/2/narrowing.html


// to make the file a ES module
export { };


// type guards


// 'typeof' narrowing

function doSomething1(stringOrArray: string | string[] | null) {
    if (typeof stringOrArray === 'string') {
        const s = stringOrArray.toUpperCase();
    }
}


// truthiness checks narrowing

function doSomething2(stringOrNull: string | null) {
    if (stringOrNull) {
        const s = stringOrNull.toUpperCase();
    }
}


// equality narrowing

function doSomething3(
    stringOrNull: string | null | undefined,
    stringOrNumber: string | number) {
    // check for equality to specific value
    if (stringOrNull !== null) {
        const s = stringOrNull!.toUpperCase();
    }
    // loose check for equality: both null and undefined
    if (stringOrNull != null) {
        const s = stringOrNull.toUpperCase();
    }
    // check for equality to a type
    if (stringOrNull === stringOrNumber) {
        const s = stringOrNull.toUpperCase();
    }
}


// the 'in' operator narrowing

type Fish = { swim: () => void };
type Bird = { fly: () => void };
type Human = { swim: () => void; fly?: () => void };

function move(animal: Fish | Bird | Human) {
    if ("swim" in animal) {
        return animal.swim();
    }
    return animal.fly();
}


// 'instanceof' narrowing

function doSomething4(stringOrDate: string | Date) {
    if (stringOrDate instanceof Date) {
        const s = stringOrDate.toUTCString();
    }
}


// assignments

let x = Math.random() < 0.5 ? 10 : "hello world!";
// assignability is always checked against the declared type
x = 1;
console.log(x);
x = "goodbye!";
console.log(x);


// control flow analysis: based on reachability

function doSomething5(stringOrNull: string | null) {
    if (stringOrNull != null) {
        return stringOrNull.toUpperCase();
    }
    // unreachable if 'stringOrNull' is a string, so the type is
    // narrowed to null
    return stringOrNull;
}


// using type predicates

// 'pet is Fish' is a type predicate; allows to narrow the
// variable to the type
function isFish(pet: Fish | Bird): pet is Fish {
    return (pet as Fish).swim !== undefined;
}

let pet = { swim: () => { } };

if (isFish(pet)) {
    pet.swim();
}


// using assertion functions: functions which raise errors when
// conditions are not met (TS 3.7)

// asserts that the expression 'value' is true
declare function assert(value: unknown): asserts value;

function multiply(x: number | null, y: number | null) {
    assert(typeof x === "number");
    assert(typeof y === "number");
    return x * y;
}


// discriminated unions

interface Circle {
    kind: "circle"; // a discriminant
    radius: number;
}

interface Square {
    kind: "square"; // a discriminant
    sideLength: number;
}

// when every type in a union contains a common property with
// literal types (a discriminant), it is a discriminated union

type Shape = Circle | Square;

function getArea(shape: Shape) {
    switch (shape.kind) {
        case "circle":
            return Math.PI * shape.radius ** 2;
        case "square":
            return shape.sideLength ** 2;
    }
}


// the 'never' type: represents a state which shouldn't exist;
// no type is assignable to 'never'

interface Triangle {
    kind: "triangle";
    sideLength: number;
}

type AnotherShape = Circle | Square | Triangle;

function getArea1(shape: AnotherShape) {
    switch (shape.kind) {
        case "circle":
            return Math.PI * shape.radius ** 2;
        case "square":
            return shape.sideLength ** 2;
        default:
            // union variants that are not handled by the switch
            // will be assigned to 'never', and that will fail

            // @ts-expect-error
            const _exhaustiveCheck: never = shape;
            return _exhaustiveCheck;
    }
}