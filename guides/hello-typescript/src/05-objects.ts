// https://www.typescriptlang.org/docs/handbook/2/objects.html


// to make the file a ES module
export { };


// object types can be anonymous, or named using either an
// interface or a type alias

const p1: { name: string, age: number } = { name: "Joe", age: 35 };

interface IPerson {
    name: string;
    age: number;
}

type TPerson = {
    name: string;
    age: number;
}


// property in an object type can specify the type, optionality and
// mutability

interface PaintOptions {
    readonly shape: string;
    xPos?: number;
    yPos?: number;
}

function paintShape({ shape, xPos = 0, yPos = 0 }: PaintOptions) {
    console.log("x coordinate at", xPos);
    console.log("y coordinate at", yPos);
}


// index signatures

interface StringArray {
    // only some types are allowed: string, number, symbol, template
    // string patterns, union types of these
    [index: number]: string;
    // can have both types of indexers, but types indexed
    // must be compatible (type returned from numeric indexer
    // must be a subtype of the type returned from the string indexer)
}

const myArray: StringArray = ["one", "two"];
const secondItem = myArray[1];


// excess property checks: object literals undergo excess property
// checking when assigning them to other variables or passing as
// arguments

interface SquareConfig {
    color?: string;
    width?: number;
}

function createSquare(config: SquareConfig): { color: string; area: number } {
    return {
        color: config.color || "red",
        area: config.width ? config.width * config.width : 20,
    };
}

// @ts-expect-error
let mySquare = createSquare({ colour: "red", width: 100 });
// get around with a type assertion
let mySquare1 =
    createSquare({ width: 100, opacity: 0.5 } as SquareConfig);

// or add a string index signature to the interface, like
// [propName: string]: any

// or assign the object literal to a variable first; will work as
// long as there are some common properties between type and value
let squareOptions = { colour: "red", width: 100 };
let mySquare2 = createSquare(squareOptions);
let squareOptions1 = { colour: "red" };
// @ts-expect-error
let mySquare3 = createSquare(squareOptions1);


// extending types

interface BasicAddress {
    name?: string;
    street: string;
    city: string;
    country: string;
    postalCode: string;
}

// more specific; copies members from other named types and adds
// new ones
interface AddressWithUnit extends BasicAddress {
    unit: string;
}

// can extend multiple types
interface IColorful {
    color: string;
}

interface ICircle {
    radius: number;
}

interface IColorfulCircle extends IColorful, ICircle { }

const cc: IColorfulCircle = {
    color: "red",
    radius: 42,
};


// intersection types; subtly different from extending interfaces
// (the principal difference is how conflicts are handled)

type TColorfulCircle = IColorful & ICircle;


// generic object types

interface IBox<Type> {
    contents: Type;
}

function setContents<Type>(box: IBox<Type>, newContents: Type) {
    box.contents = newContents;
}

let box: IBox<string> = { contents: "hello" };
setContents(box, "hello world!");
console.log(box);


// type aliases can also be generic

type TBox<Type> = {
    contents: Type;
};

// and do things interfaces cannot
type OrNull<Type> = Type | null;
type OneOrMany<Type> = Type | Type[];
type OneOrManyOrNull<Type> = OrNull<OneOrMany<Type>>;

let box1: TBox<OneOrManyOrNull<string>> = { contents: ["one"] };
let box2: TBox<OneOrManyOrNull<string>> = { contents: null };


// array types

// Type[] is just a shorthand for Array<Type>

// array can be readonly; cannot mutate readonly array; also
// cannot assign readonly array to a mutable one

let arr3: ReadonlyArray<string> = ["one", "two"];
let arr4: readonly string[] = ["one", "two"];


// tuple types

// sort of array with fixed length and different element types;
// no representation at runtime

type StringNumberPair = [string, number];

// can have optional properties at the end; affect the type
// of 'length' property
type Either2dOr3d = [number, number, number?];
const pos: Either2dOr3d = [1, 2];
console.log(pos.length)

// can have rest elements; such a tuple has no set 'length'
type StringNumberBooleans = [string, number, ...boolean[]];
type StringBooleansNumber = [string, ...boolean[], number];
type BooleansStringNumber = [...boolean[], string, number];

// can be readonly
type Either2dOr3dReadonly = readonly [number, number, number?];
const pos1: Either2dOr3dReadonly = [1, 2, 3];
let pos2 = [3, 4] as const;