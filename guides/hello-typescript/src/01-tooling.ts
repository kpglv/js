// https://www.typescriptlang.org/docs/handbook/typescript-tooling-in-5-minutes.html


// to make the file a ES module
export { };


interface Person {
  firstName: string;
  lastName: string;
}

class Student {
  fullName: string;
  constructor(
    public firstName: string,
    public middleInitial: string,
    public lastName: string
  ) {
    this.fullName = `${firstName} ${middleInitial} ${lastName}`;
  }
}

function greeter(person: Person) {
  return `Hello, ${person.firstName} ${person.lastName}`;
}

let user = { firstName: "Joe", lastName: "Smith" };
console.log(greeter(user));

let student = new Student("Mary", "M.", "Smith");
console.log(greeter(student));

// install Typescript compiler:
// npm install -g typescript

// compile the TS file (still emits JS even with errors, use --noEmitOnError to override)
// tsc greeter.ts

// see `tsconfig.json` for compiler configuration options; use tsc --init to
// generate one and explore available options (use 'tsc' without commandline
// options to use the config)
