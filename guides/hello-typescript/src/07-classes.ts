// https://www.typescriptlang.org/docs/handbook/2/classes.html


// to make the file a ES module
export { };


// an empty class

class Point { }


// fields and constructors

class Base { }

class Point1 extends Base {
    // 'strictPropertyInitialization' setting controls if
    // uninitialized (in constructor) fields are allowed
    x: number;
    // definite assignment assertion (no error if not initialized,
    // assumes that will be initialized by other means)
    y!: number;

    // overloads are supported
    constructor(...coords: number[]);
    // no type parameters (they belong to the class itself),
    // no return type (always the instance of the class)
    constructor(x: number, y = 0) {
        // must call superclass' constructor before accessing 'this'
        super();
        // should be initialized in constructor itself,
        // not in methods called from constructor
        this.x = x;
        this.y = y;
    }


}
const pt1 = new Point1(2, 1, 8);
console.log(`Point1 (${pt1.x},${pt1.y})`);
pt1.x = 0;
pt1.y = 0;
console.log(`Point1 (${pt1.x},${pt1.y})`);

// fields with initializers

class Point2 {
    x = 0; // types are inferred
    readonly y = 0; // readonly field
}
const pt2 = new Point2();
console.log(`Point2 (${pt2.x},${pt2.y})`);
// @ts-expect-error
pt2.x = "foo";

// constructors with parameter properties

class Params {
    constructor(
        public readonly x: number,
        protected y: number,
        private z: number
    ) { /* no body is necessary */ }
}
const a1 = new Params(1, 2, 3);
console.log("constructor parameter property x:", a1.x);
// @ts-expect-error: private property
console.log("constructor parameter property z:", a1.z);

// constructor signatures

// JavaScript classes are instantiated with the new operator; given
// the type of a class itself, the 'InstanceType' utility type
// models this operation (?)

class Point2d {
    createdAt: number;
    x: number;
    y: number
    constructor(x: number, y: number) {
        this.createdAt = Date.now()
        this.x = x;
        this.y = y;
    }
}
type PointInstance = InstanceType<typeof Point2d>

function moveRight(point: PointInstance) {
    point.x += 5;
}

const point = new Point2d(3, 4);
moveRight(point);
console.log(`Point2d: x: ${point.x}, y: ${point.y}`);


// methods

class Point3 {

    #x = 10;
    y = 10;

    // a method is a function property
    scale(n: number): void {
        // must use 'this' to access class members
        this.#x *= n;
        this.y *= n;
    }

    // accessors (getters and setters); must have the same
    // member visibility

    // if only 'get' is present, the property is readonly
    get x() {
        return this.#x;
    }

    set x(value: string | number | boolean) {
        const num = Number(value);
        if (!Number.isFinite(num)) {
            this.#x = 0;
            return;
        }
        this.#x = num;
    }
}

const p3 = new Point3();
p3.scale(2);
console.log(`Point3 (x: ${p3.x}, y: ${p3.y})`);
p3.x = false;
console.log(`Point3 (x: ${p3.x}, y: ${p3.y})`);

// index signatures
class MyClass {
    // should describe all fields and methods, so not very useful ---
    // prefer composition to store indexed data
    foo = true;
    [s: string]: boolean | ((s: string) => boolean);

    check(s: string) {
        return this[s] as boolean;
    }
}
const cls = new MyClass();
console.log("foo:", cls.check("foo"));
console.log("bar:", cls.check("bar"));


// class heritage

// check if the class implements an interface

interface Marker { }

interface Pingable {
    ping(): void;
}

class Sonar implements Pingable, Marker {
    ping() {
        console.log("ping!");
    }
}

// @ts-expect-error
class Ball implements Pingable, Marker {
    // no 'ping' method
    pong() {
        console.log("pong!");
    }
}

// 'implements' only checks if the class can be used as
// given interface type; does not affect how the types in the
// class are checked and inferred
interface A {
    x: number;
    y?: number;
}
class C implements A {
    x = 0;
}
const c = new C();
// @ts-expect-error
c.y = 10; // no such property

// extend the class

class Animal {
    move() {
        console.log("Moving along!");
    }
}

class Dog extends Animal {
    woof(times: number) {
        for (let i = 0; i < times; i++) {
            console.log("woof!");
        }
    }
}

const d = new Dog();
// base class method
d.move();
// derived class method
d.woof(3);

// a derived class can override fields and methods; use 'super'
// to access superclass methods; but there are no "superfields"

// parent class' fields are initialized and constructor runs before
// those of derived class

// derived class is always a subtype of its base class; derived
// class should follow its base class contract

// type-only field declarations: to avoid overwriting values set
// by parent class, only redeclare a more accurate type

interface Animal {
    dateOfBirth: any;
}

interface Dog extends Animal {
    breed: any;
}

class AnimalHouse {
    resident: Animal;
    constructor(animal: Animal) {
        this.resident = animal;
    }
}

class DogHouse extends AnimalHouse {
    // does not emit JavaScript code, only ensures the types
    // are correct; 'declare' means there will be no runtime effect
    declare resident: Dog;
    constructor(dog: Dog) {
        super(dog);
    }
}


// member visibility

// - public: can be accessed anywhere
// - protected: can only be accessed from the class that declares it
//   and its subclasses; subclass may make them public
// - private: can only be accessed from the class that declares it
//   (also allowed between different instances of the class); can
//   be accessed with bracket notation (by string names), unlike
//   JS #private fields


// static members

// can be public, protected and private; are inherited; certain
// names cannot be used as static members (name, length, call);
// static blocks (initializers) are supported

class MyClass1 {
    static x = 0;
    static printX() {
        console.log(MyClass1.x);
    }
}
console.log(MyClass1.x);
MyClass1.printX();

// there are no static classes; use regular objects and top-level
// functions instead

// unnecessary "static" class
class MyStaticClass {
    static doSomething() { }
}

// preferred (alternative 1)
function doSomething() { }

// preferred (alternative 2)
const MyHelperObject = {
    dosomething() { },
};


// generic classes: work the same way as generic interfaces


// 'this' at runtime: depends on how the function was called

class MyClass2 {
    name = "MyClass2";
    getName() {
        return this.name;
    }
}
const cls2 = new MyClass2();
const obj = {
    name: "obj",
    getName: cls2.getName,
};

// prints "obj", not "MyClass2"
console.log(obj.getName());

// mitigation: use arrow functions; will always be called correctly,
// but uses more memory, can't use super methods (no prototype
// chain to fetch the base class method from (?))

class MyClass3 {
    name = "MyClass3";
    getName = () => {
        return this.name;
    };
}
const cls3 = new MyClass3();
const g = cls3.getName;
// prints "MyClass3" instead of crashing
console.log(g());

// mitigation: use initial 'this' parameter, that will be erased
// during compilation; can be called incorrectly

class MyClass4 {
    name = "MyClass4";
    getName(this: MyClass4) {
        return this.name;
    }
}
const cls4 = new MyClass4();
// ok
cls4.getName();

// error, would crash
// const g4 = cls4.getName;
// console.log(g4());


// 'this' type: refers dynamically to the type of the current class

class Box {
    contents: string = "";
    set(value: string) {
        this.contents = value;
        return this;
    }
    sameAs(other: this) {
        return other.contents === this.contents;
    }
}

class ClearableBox extends Box {
    clear() {
        this.contents = "";
    }
}

const a = new ClearableBox();
const b = a.set("hello");
// const b: ClearableBox

const isSame = b.sameAs(a);
console.log("same?", isSame);


// 'this'-based type guards

// allow to narrow (e.g., with 'if') the type of the object to the
// specified type

class FileSystemObject {
    isFile(): this is FileRep {
        return this instanceof FileRep;
    }
    isDirectory(): this is Directory {
        return this instanceof Directory;
    }
    isNetworked(): this is Networked & this {
        return this.networked;
    }
    constructor(public path: string, private networked: boolean) { }
}

class FileRep extends FileSystemObject {
    constructor(path: string, public content: string) {
        super(path, false);
    }
}

class Directory extends FileSystemObject {
    children!: FileSystemObject[];
}

interface Networked {
    host: string;
}

const fso: FileSystemObject = new FileRep("foo/bar.txt", "foo");

if (fso.isFile()) {
    fso.content;
    // const fso: FileRep
} else if (fso.isDirectory()) {
    fso.children;
    // const fso: Directory
} else if (fso.isNetworked()) {
    fso.host;
    // const fso: Networked & FileSystemObject
}

// use case: removes undefined from the value held inside box,
// when 'hasValue' has been verified to be true

class Box1<T> {
    value?: T;
    hasValue(): this is { value: T } {
        return this.value !== undefined;
    }
}

const box1 = new Box1();
box1.value = "Gameboy";
box1.value;
// (property) Box<unknown>.value ?: unknown

if (box1.hasValue()) {
    box1.value;
    // (property) value: unknown
}


// class expressions

// similar to declarations; can be anonymous

const someClass = class <Type> {
    content: Type;
    constructor(value: Type) {
        this.content = value;
    }
};

const m = new someClass("Hello, world");


// abstract classes and members

// abstract methods and fields exist inside an abstract class, which
// cannot be directly instantiated; abstract classes used as
// base classes for derived concrete classes

abstract class BaseClass {
    abstract getName(): string;

    printName() {
        console.log("Hello, " + this.getName());
    }
}

// @ts-expect-error: cannot instantiate abstract class
const base = new BaseClass();

class Derived extends BaseClass {
    getName() {
        return "world";
    }
}

const derived = new Derived();
derived.printName();

// abstract construct signatures

function greet(ctor: new () => BaseClass) {
    const instance = new ctor();
    instance.printName();
}
greet(Derived);
// wrong constructor type
// greet(BaseClass);


// relationships between classes

// classes are mostly compared structurally, the same as other types

class APoint {
    x = 0;
    y = 0;
}

class AnotherPoint {
    x = 0;
    y = 0;
}

// ok
const p: APoint = new AnotherPoint();

// subtype relationship can exist even without explicit inheritance

class APerson {
    name!: string;
    age!: number;
}

class Employee {
    name!: string;
    age!: number;
    salary!: number;
}

// ok
const p1: APerson = new Employee();

// empty classes are supertypes of anything else (not recommended)