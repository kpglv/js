// https://www.typescriptlang.org/docs/handbook/2/everyday-types.html


// to make the file a ES module
export { };


// primitives: string, number and boolean (not capitalized)

const a: string = "hello";
const b: number = 42;
const c: boolean = true;


// any: disables all further type checks on the name

const g: any = "foo";


// arrays

const d: string[] = ["one", "two", "three"];
const e: Array<string> = ["four", "five"];
// type declaration can be omitted if it is inferrable
const f = ["six", "seven", "eight"]


// functions: parameter types and return type can be specified;
// parameter count is also checked

function greet(name: string): number {
  console.log("hello, " + name);
  return 42;
}

async function getFavoriteNumber(): Promise<number> {
  return Promise.resolve(42);
}


// object types (use , or ; as separator, the last one is optional)

// z is optional; should be checked for undefined on use
const h: { x: number; y: number, z?: number } = { x: 3, y: 7 };


// union types: only operations valid for every type are allowed;
// narrow the union with a type guard (e.g., typeof or type
// predicate; see the chapter on narrowing) to use type-specific
// operations

const i: number | string = "foo"
const j: number | string = 42

if (typeof i === 'string') {
  console.log(i.toUpperCase());
}

const k: string | string[] = ["one"]
if (Array.isArray(k)) {
  console.log(k.join(", "))
}


// type aliases: names for types (the same type under different names);
// closed (cannot add new properties)

type ID = number | string;

type Point = {
  x: number;
  y: number;
}

// extending by intersection (result is a new type)
type Point3D = Point & {
  z: number;
}


// interfaces: also a name for a type; used to declare shapes of
// objects; always extendable

interface Position {
  x: number;
  y: number;
}

// extending an interface (result is a new interface)
interface Position3D extends Position {
  z: number;
}

// adding new fields to an existing interface
interface Position3D {
  origin: number;
}

// type or interface, only structure matters (structural typing)


// type assertions: prevent impossible coercions; erased at runtime,
// no runtime checking

if (false) {
  const myCanvas =
    document.getElementById("main_canvas") as HTMLCanvasElement;
}

// the same (cannot be used in .tsx)
if (false) {
  const myAnotherCanvas =
    <HTMLCanvasElement>document.getElementById("main_canvas");
}

// if assertion prevents some complex, but valid coercion
const l = "foo" as any as number;


// literal types: specific values in type positions

// has type 'string'
let changingString = "hello";

// has type 'world' (literal type)
const constantString = "world";
let anotherConstantString: "world" = "world";

const m: -1 | 0 | 1 = 0;

interface Options {
  width: number;
}

const n: Options | "auto" = "auto";
const o: Options | "auto" = { width: 7 };

// all properties become type literals
const req = { url: "https://example.com", method: "GET" } as const;


// null and undefined: separate types

// with strictNullChecks values that can be null or undefined must be
// explicitly tested before properties or methods on them can be used

function doSomething(x: string | null) {
  if (x === null) {
    // do nothing
  } else {
    console.log("Hello, " + x.toUpperCase());
  }
}

// non-null assertion operator (!)

function liveDangerously(x?: number | null) {
  // no error
  console.log(x!.toFixed());
}


// less common primitives

// bigint: large integers

const oneHundred: bigint = BigInt(100);
const anotherHundred: bigint = 100n;

// symbol: globally unique references

const firstName = Symbol("name");
const secondName = Symbol("name");

console.log(typeof firstName)
console.log(typeof secondName)


// enums: not a type-level feature, exists at runtime

// numeric enums

// nonspecified values get autoincremented, starting with preceding
// number or zero
enum Direction {
  Up = 1,
  Down,
  Left,
  Right,
}

const d1: Direction = Direction.Up;

const announceDirection =
  (direction: Direction) => console.log(direction);

announceDirection(d1);

// can have constant (evaluated at build time) or computed members
enum FileAccess {
  // constant members
  None,
  Read = 1 << 1,
  Write = 1 << 2,
  ReadWrite = Read | Write,
  // computed member
  G = "123".length,
}

// string enums

// no autoincrement, but more readable
enum AlsoDirection {
  Up = "UP",
  Down = "DOWN",
  Left = "LEFT",
  Right = "RIGHT",
}

// heterogeneous enums (not recommended)

enum BooleanLikeHeterogeneousEnum {
  No = 0,
  Yes = "YES",
}

// union enums and enum member types

// only constant members
enum ShapeKind {
  Circle,
  Square,
}

// enum type is effectively a union type of each enum member
// @ts-expect-error
ShapeKind.Circle !== ShapeKind.Square;
type ShapeKindKeys = keyof typeof ShapeKind;

interface Circle {
  kind: ShapeKind.Circle; // such enum member is a type in itself
  radius: number;
}

interface Square {
  kind: ShapeKind.Square;
  sideLength: number;
}

let c1: Circle = {
  // @ts-expect-error
  kind: ShapeKind.Square,
  radius: 100,
};


// reverse mappings: only numeric enums

enum Enum {
  A,
}

let a1 = Enum.A;
let nameOfA1 = Enum[a1];
console.log(nameOfA1);


// 'const' enums: can only have constant members; removed (inlined)
// during compilation

// beware: ambient const enums (declared in '.d.ts' files) have
// some pitfalls
const enum ConstEnum {
  A = 1,
  B = A * 2,
}

console.log(ConstEnum.B);

// ambient enum (describing the shape of already existing enum)
declare enum AmbientEnum {
  A = 1,
  B,
  C = 2,
}


// objects vs enums

// an object with 'as const' could suffice
const ODirection = {
  Up: 0,
  Down: 1,
  Left: 2,
  Right: 3,
} as const;

console.log(ODirection.Down);