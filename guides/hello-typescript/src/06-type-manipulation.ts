// https://www.typescriptlang.org/docs/handbook/2/types-from-types.html


// to make the file a ES module
export { };


// types can be expressed in terms of other types


// generics (parameterized types)

// a generic function
function identity<Type>(arg: Type): Type {
  return arg;
}

let output = identity<string>("myString");
// Type is inferred
let output1 = identity("myString");
console.log(output, output1);

// a type of the generic function:
// as a function signature
type GenericIdentity = <Type>(arg: Type) => Type;
// as a call signature of an object literal type
type GenericIdentityAgain = { <Type>(arg: Type): Type };
// as an interface containing generic functions
interface GenericIdentityOnceMore {
  <Type>(arg: Type): Type;
}
// as a generic interface
interface GenericIdentityStrikesBack<Type> {
  // non-generic function that is a part of a generic type
  (arg: Type): Type;
}

let myIdentity: GenericIdentity = identity;
let myIdentityAgain: GenericIdentityAgain = identity;
let myIdentityOnceMore: GenericIdentityOnceMore = identity;
let myIdentityStrikesBack: GenericIdentityStrikesBack<number> =
  identity;
console.log(myIdentity(5), myIdentityAgain(5),
  myIdentityOnceMore(5), myIdentityStrikesBack(5));

// a generic class (with generic parameter default)
class GenericNumber<NumType extends number = number> {

  // static members cannot reference type parameters

  zeroValue: NumType;

  constructor(zeroValue: NumType) {
    this.zeroValue = zeroValue;
  }

  add(x: NumType, y: NumType): NumType {
    // quite silly, but should suffice for a demo
    if (typeof x === 'number' && typeof y === 'number') {
      return (x + y) as NumType;
    }
    throw Error('invalid NumType');
  }
}

// type parameter is optional since it has a default
let myGenericNumber = new GenericNumber<number>(0);
console.log('generic class:', myGenericNumber.add(12, 11));

// generic constraints
interface Lengthwise {
  length: number;
}

function loggingIdentity<Type extends Lengthwise>(arg: Type): Type {
  console.log("length:", arg.length); // type of arg is known to contain 'length'
  return arg;
}

loggingIdentity("foo");
loggingIdentity({ length: 10, value: 3 });

// using type parameters in generic constraints
function getProperty<Type, Key extends keyof Type>(obj: Type, key: Key) {
  return obj[key];
}

let x = { a: 1, b: 2, c: 3, d: 4 };
console.log("property:", getProperty(x, "a"));
// @ts-expect-error: "m" is not in the set of keys of x
console.log("property:", getProperty(x, "m"));

// using class types in generics
class BeeKeeper {
  hasMask: boolean = true;
}

class ZooKeeper {
  nametag: string = "Mikle";
}

class Animal {
  numLegs: number = 4;
}

class Bee extends Animal {
  numLegs = 6;
  keeper: BeeKeeper = new BeeKeeper();
}

class Lion extends Animal {
  keeper: ZooKeeper = new ZooKeeper();
}

function createInstance<A extends Animal>(c: new () => A): A {
  return new c();
}

console.log(createInstance(Lion).keeper.nametag);
console.log(createInstance(Bee).keeper.hasMask);


// 'keyof' type operator

// produces a string or numeric literal union of keys of an object type

type Point = { x: number; y: number };
type P = keyof Point;
// type P = "x" | "y"

type Arrayish = { [n: number]: unknown };
// the same as
// type A = number
type A = keyof Arrayish;

type Mapish = { [k: string]: boolean };
type M = keyof Mapish;
// type M = string | number
// (object keys are always coerced to a string)


// 'typeof' type operator

// in addition to expression context, 'typeof' can be used
// in a type context; can only be used on identifiers or
// their properties

let s = "hello";
let n: typeof s;
// let n: string

type Predicate = (x: unknown) => boolean;
type K = ReturnType<Predicate>;
// type K = boolean

function f() {
  return { x: 10, y: 3 };
}
type P1 = ReturnType<typeof f>;


// indexed access types

type Person = { age: number; name: string; alive: boolean };
type Age = Person["age"];
// type Age = number

type I1 = Person["age" | "name"];
// type I1 = string | number
type I2 = Person[keyof Person];
// type I2 = string | number | boolean
type AliveOrName = "alive" | "name";
type I3 = Person[AliveOrName];
// type I3 = string | boolean
// @ts-expect-error
type I4 = Person["alve"];

// get the type of array elements
const MyArray = [
  { name: "Alice", age: 15 },
  { name: "Bob", age: 23 },
  { name: "Eve", age: 38 },
];
type APerson = typeof MyArray[number];
type Age1 = typeof MyArray[number]["age"];
// type Age1 = number
type Age2 = Person["age"];
// type Age2 = number

// values cannot be used for indexing, only types
const key = "age";
// @ts-expect-error
type Age3 = Person[key];
// but type aliases are fine
type tkey = "age";
type Age4 = Person[tkey];


// conditional types

// describe the relation between the types of inputs and outputs

interface Animal {
  live(): void;
}
interface Dog extends Animal {
  woof(): void;
}

type Example1 = Dog extends Animal ? number : string;
// type Example1 = number
type Example2 = RegExp extends Animal ? number : string;
// type Example2 = string

// using with generics
interface IdLabel {
  id: number;
}
interface NameLabel {
  name: string;
}
type NameOrId<T extends number | string> = T extends number
  ? IdLabel
  : NameLabel;

function createLabel<T extends number | string>(idOrName: T): NameOrId<T> {
  throw "unimplemented";
}

try {
  let a = createLabel("typescript");
  // let a: NameLabel
  let b = createLabel(2.8);
  // let b: IdLabel
  let c = createLabel(Math.random() ? "hello" : 42);
  // let c: NameLabel | IdLabel
} catch (e) { }

// conditional type constraints
// without condition: T must have 'message' property
type MessageOf<T extends { message: unknown }> = T["message"];

interface Email {
  message: string;
}

type EmailMessageContents = MessageOf<Email>;
// type EmailMessageContents = string

// with condition: T can be any type
type AMessageOf<T> = T extends { message: unknown } ?
  T["message"] : never;

interface Dog {
  bark(): void;
}

type EmailMessageContents1 = AMessageOf<Email>;
// type EmailMessageContents = string
type DogMessageContents = AMessageOf<Dog>;
// type DogMessageContents = never

type Flatten<T> = T extends any[] ? T[number] : T;
// extracts out the array's element type
type Str = Flatten<string[]>;
// type Str = string
// leaves the type alone.
type Num = Flatten<number>;
// type Num = number

// inferring with conditional types
// compare to Flatten<T> above
type Flatten1<T> = T extends Array<infer ItemType> ? ItemType : T;

// extract the return type
type GetReturnType<Type> = Type extends (...args: never[]) => infer Return
  ? Return
  : never;

type ANum = GetReturnType<() => number>;
// type Num = number
type AStr = GetReturnType<(x: string) => string>;
// type Str = string
type Bools = GetReturnType<(a: boolean, b: boolean) => boolean[]>;
// type Bools = boolean[]
type Nope = GetReturnType<number>;
// type Nope = never

// with overloaded functions, inference is made from the last signature
declare function stringOrNum(x: string): number;
declare function stringOrNum(x: number): string;
declare function stringOrNum(x: string | number): string | number;
type T1 = ReturnType<typeof stringOrNum>;
// type T1 = string | number

// distributive conditional types
type ToArray<Type> = Type extends any ? Type[] : never;
// conditional type 'ToArray' distributes to each member of the union
type StrArrOrNumArr = ToArray<string | number>;
// results in ToArray<string> | ToArray<number>, and so
// type StrArrOrNumArr = string[] | number[]

// to avoid distributivity, use square brackets on both sides of 'extends'
type ToArrayNonDist<Type> = [Type] extends [any] ? Type[] : never;
// 'ArrOfStrOrNum' is no longer a union
type ArrOfStrOrNum = ToArrayNonDist<string | number>;
// type ArrOfStrOrNum = (string | number)[]


// mapped types

// build on index signatures:
interface Horse {
  neigh: () => void;
}
type OnlyBoolsAndHorses = {
  [key: string]: boolean | Horse;
};
const conforms: OnlyBoolsAndHorses = {
  del: true,
  rodney: false,
};

// a mapped type uses a union of 'PropertyKey's (usually created
// via 'keyof') to iterate through keys:
type OptionsFlags<Type> = {
  [Property in keyof Type]: boolean;
};

type Features = {
  darkMode: () => void;
  newUserProfile: () => void;
};

type FeatureOptions = OptionsFlags<Features>;
// type FeatureOptions = {
//   darkMode: boolean;
//   newUserProfile: boolean;
// }

// mapping modifiers: affect mutability and optionality

// removes 'readonly' attributes from a type's properties
type CreateMutable<Type> = {
  -readonly [Property in keyof Type]: Type[Property];
};

type LockedAccount = {
  readonly id: string;
  readonly name: string;
};
type UnlockedAccount = CreateMutable<LockedAccount>;
// type UnlockedAccount = {
// id: string;
// name: string;
// }

// removes 'optional' attributes from a type's properties
type Concrete<Type> = {
  [Property in keyof Type]-?: Type[Property];
};

type MaybeUser = {
  id: string;
  name?: string;
  age?: number;
};

type User = Concrete<MaybeUser>;
// type User = {
//   id: string;
//   name: string;
//   age: number;
// }

// key remapping via 'as' (not quite clear)
type NewKeyType = number;
type MappedTypeWithNewProperties<Type> = {
  [KeyType in keyof Type as NewKeyType]: Type[KeyType]
}
let val: MappedTypeWithNewProperties<{ message: string }>;

// use template literal types to create new property names
type Getters<Type> = {
  [Property in keyof Type as `get${Capitalize<string & Property>}`]: () => Type[Property]
};

interface Person2 {
  name: string;
  age: number;
  location: string;
}

type LazyPerson = Getters<Person2>;
// type LazyPerson = {
//   getName: () => string;
//   getAge: () => number;
//   getLocation: () => string;
// }

// filter out keys by producing never via a conditional type:
// remove the 'kind' property
type RemoveKindField<Type> = {
  [Property in keyof Type as Exclude<Property, "kind">]: Type[Property]
};

interface Circle {
  kind: "circle";
  radius: number;
}

type KindlessCircle = RemoveKindField<Circle>;
// type KindlessCircle = {
//   radius: number;
// }

// map over arbitrary unions, not just unions of
// string | number | symbol:

type EventConfig<Events extends { kind: string }> = {
  [E in Events as E["kind"]]: (event: E) => void;
}

type SquareEvent = { kind: "square", x: number, y: number };
type CircleEvent = { kind: "circle", radius: number };

type Config = EventConfig<SquareEvent | CircleEvent>
// type Config = {
//   square: (event: SquareEvent) => void;
//   circle: (event: CircleEvent) => void;
// }

// a mapped type using a conditional type which returns either true
// or false depending on whether an object has the property 'pii'
// set to the literal true

type ExtractPII<Type> = {
  [Property in keyof Type]: Type[Property] extends { pii: true } ?
  true : false;
};

type DBFields = {
  id: { format: "incrementing" };
  name: { type: string; pii: true };
};

type ObjectsNeedingGDPRDeletion = ExtractPII<DBFields>;
// type ObjectsNeedingGDPRDeletion = {
//   id: false;
//   name: true;
// }


// template literal types

// build on string literal types, can expand into many strings
// via unions

type World = "world";
type Greeting = `hello ${World}`;
// type Greeting = `hello ${World}`;

// every possible string literal, from each union member
type EmailLocaleIDs = "welcome_email" | "email_heading";
type FooterLocaleIDs = "footer_title" | "footer_sendoff";
type AllLocaleIDs = `${EmailLocaleIDs | FooterLocaleIDs}_id`;
// type AllLocaleIDs = "welcome_email_id" | "email_heading_id" |
//   "footer_title_id" | "footer_sendoff_id"

type AllLocaleIDs1 = `${EmailLocaleIDs | FooterLocaleIDs}_id`;
type Lang = "en" | "ja" | "pt";
// cartesian product of unions
type LocaleMessageIDs = `${Lang}_${AllLocaleIDs1}`;
// type LocaleMessageIDs = "en_welcome_email_id" |
//   "en_email_heading_id" | "en_footer_title_id" |
//   "en_footer_sendoff_id" | "ja_welcome_email_id" |
//   "ja_email_heading_id" | "ja_footer_title_id" |
//   "ja_footer_sendoff_id" | "pt_welcome_email_id" |
//   "pt_email_heading_id" | "pt_footer_title_id" |
//   "pt_footer_sendoff_id"

// string unions in types

// defining a new string based on information inside a type
type PropEventSource<Type> = {
  on(eventName: `${string & keyof Type}Changed`,
    callback: (newValue: any) => void): void;
};


// create a "watched object" with an `on` method
// so that you can watch for changes to properties
function makeWatchedObject<Type>(obj: Type): Type & PropEventSource<Type> {
  return {
    ...obj,
    on: (event, callback) => {
      console.log("registered handler for:", event)
    }
  }
};

const person = makeWatchedObject({
  firstName: "Saoirse",
  lastName: "Ronan",
  age: 26
});

// ok
person.on("firstNameChanged", () => { });
// @ts-expect-error
person.on("firstName", () => { });
// @ts-expect-error
person.on("frstNameChanged", () => { });

// inference with template literals

// to infer a type for the callback's argument
type PropEventSource1<Type> = {
  // literal type is given to the first argument of 'on' function; it
  // is a union constructed from valid attributes of the object
  on<Key extends string & keyof Type>(eventName: `${Key}Changed`,
    // type of the callback's argument is looked up in Type
    // using indexed access
    callback: (newValue: Type[Key]) => void): void;
};

function makeWatchedObject1<Type>(obj: Type): Type & PropEventSource<Type> {
  return {
    ...obj, on: (event, callback) => {
      console.log("registered handler for:", event)
    }
  }
};

const person1 = makeWatchedObject1({
  firstName: "Saoirse",
  lastName: "Ronan",
  age: 26
});

person1.on("firstNameChanged", newName => {
  console.log(`new name is ${newName.toUpperCase()}`);
});

person1.on("ageChanged", newAge => {
  if (newAge < 0) {
    console.warn("warning! negative age");
  }
});

// intrinsic string manipulation types (compiler built-ins)

// uppercase

type AGreeting = "Hello, world"
type ShoutyGreeting = Uppercase<AGreeting>
// type ShoutyGreeting = "HELLO, WORLD"

type ASCIICacheKey<Str extends string> = `ID-${Uppercase<Str>}`
type MainID = ASCIICacheKey<"my_app">
// type MainID = "ID-MY_APP"

// lowercase

type QuietGreeting = Lowercase<Greeting>
// type QuietGreeting = "hello, world"

type ASCIICacheKey1<Str extends string> = `id-${Lowercase<Str>}`
type MainID1 = ASCIICacheKey1<"MY_APP">
// type MainID1 = "id-my_app"

// capitalize

type AnotherGreeting = Capitalize<QuietGreeting>;
// type AnotherGreeting = "Hello, world"

// uncapitalize

type UncomfortableGreeting = Uncapitalize<ShoutyGreeting>;
// type UncomfortableGreeting = "hELLO WORLD"