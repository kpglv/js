// https://github.com/tc39/proposal-decorators
// https://www.typescriptlang.org/docs/handbook/decorators.html
// https://blog.logrocket.com/understanding-javascript-decorators/

// this describes an experimental stage 2 decorators implementation;
// stage 3 decorator support (ECMAScript proposal/standard) is available
// since Typescript 5.0

// a decorator is a special kind of declaration that can be attached to a
// class declaration, method, accessor, property, or parameter (in TypeScript)

// decorator expressions are evaluated top-to-bottom, then results
// called bottom-to-top

// decorator application order:
// - parameter decorators, then method/accessor/property decorators
//   for each instance member
// - parameter decorators, then method/accessor/property decorators
//   for each static member
// - parameter decorators for the constructor, then class (constructor)
//   decorator

// a method decorator

const log = (
    target: Function,
    propertyKey: string,
    descriptor: PropertyDescriptor
) => {
    console.log(`method decorator's target (class): ${target.name}`);
    console.log(`method decorator's decorated property name: ${propertyKey}`);
    console.log(
        `method decorator's decorated property value: '${JSON.stringify(
            descriptor
        )}'`
    );
};

// a class (constructor) decorator

const logClass = (target: Function) => {
    console.log(`class decorator target (class): ${target.name}`);
};

// a method decorator factory is a function that returns a decorator

const logWith = (loggerFn: (...args: unknown[]) => void) => {
    return (
        target: Function,
        propertyKey: string,
        descriptor: PropertyDescriptor
    ) =>
        loggerFn(
            `logging with: '${loggerFn.name || loggerFn}', target: ${
                target.name
            }, propertyKey: ${propertyKey}, descriptor: ${JSON.stringify(
                descriptor
            )}`
        );
};

class Arith {
    @log
    static square(x: number): number {
        return x * x;
    }
    @logWith(console.error)
    static add(x: number, y: number) {
        return x + y;
    }
}

console.log(Arith.square(5));
console.log(Arith.add(1, 2));
