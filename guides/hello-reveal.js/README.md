# hello-reveal.js

Exploring a library for web-based presentations.

-   The HTML Presentation Framework https://revealjs.com

## Getting Started

Open `.html` files (from the local filesystem or serve them first,
e.g., with `serve .`):

-   `markup.html`: the default way to write presentations
-   `markdown.html`: writing presentations in markdown
