# hello-vue-histoire

Using Histoire component explorer to do Component-Driven Development
with Vue 3.

- Component Driven User Interfaces https://www.componentdriven.org/

## Getting Started

```
yarn story:dev
```

## Creating a New Project

Use a project template (see https://vitejs.dev/guide/ for more options)

```
yarn create vite <project name> --template vue-ts
```

Add Histoire and TailwindCSS

```
yarn add -D histoire @histoire/plugin-vue tailwindcss postcss autoprefixer
```

Create configuration file for TailwindCSS (`tailwind.config.js`),
add content to scan (e.g., `./src/**/*.vue`)

```
npx tailwindcss init
```

Create configuration file for PostCSS (`postcss.config.js`)

```js
const config = {
    plugins: {
        tailwindcss: {},
        autoprefixer: {},
    },
};

export default config;
```

Create configuration file for Histoire (`histoire.config.js`)

```js
import { defineConfig, defaultColors } from 'histoire';
import { HstVue } from '@histoire/plugin-vue';

export default defineConfig({
    setupFile: '/src/setup.ts',
    theme: {
        colors: {
            primary: defaultColors.green,
        },
    },
    plugins: [HstVue()],
});
```

Create a TailwindCSS styles file (`src/tailwind.css`)

```css
@tailwind base;
@tailwind components;
@tailwind utilities;
```

Create setup file for Histoire (`src/setup.js`) that imports the styles (and maybe
does some other initializations)

```js
import { defineSetupVue3 } from '@histoire/plugin-vue';
import './tailwind.css';

export const setupVue3 = defineSetupVue3(({ app, story, variant }) => {
    // add initializations
});
```
