import { defineConfig, defaultColors } from 'histoire';
import { HstVue } from '@histoire/plugin-vue';

export default defineConfig({
    setupFile: '/src/setup.ts',
    theme: {
        colors: {
            primary: defaultColors.green,
        },
    },
    plugins: [HstVue()],
});
