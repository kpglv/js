# hello-leaflet-clustering

Exploring clustering in context of Leaflet map library.

-   Leaflet: https://leafletjs.com
-   Supercluster: https://github.com/mapbox/supercluster
-   useSuperCluster: https://github.com/leighhalliday/use-supercluster

# Getting Started

Download GeoJSON data at https://data-cyc.opendata.arcgis.com/datasets/863dfeabfa4c4705ae82083a3df5233a_21.geojson
and put it in `resources/points.json` (see also other datasets at https://open-innovations.org/data/geojson.html).

Start development server

```
npm run dev
```
