import { useCallback, useEffect, useState } from 'react';
import { Marker, useMap } from 'react-leaflet';

import { BBox, Feature, Point } from 'geojson';
import { DivIcon } from 'leaflet';
import useSupercluster from 'use-supercluster';

import './Points.css';

/** Properties of a GeoJSON point from the dataset */
export interface PointProperties {
    OBJECTID: number;
}

export interface PointsProps {
    points: Feature<Point, PointProperties>[];
}

/**
 * A set of points.
 */
export const Points = ({ points }: PointsProps) => {
    const map = useMap();

    const maxZoom = map.getMaxZoom();

    const [bounds, setBounds] = useState<BBox>();
    const [zoom, setZoom] = useState(12);

    const preparedPoints = points.map(point => ({
        ...point,
        properties: {
            ...point.properties,
            cluster: false,
            point_count: 0,
        },
    }));

    const { clusters, supercluster } = useSupercluster({
        points: preparedPoints,
        bounds,
        zoom,
        options: { radius: 500, maxZoom },
    });

    const updateMap = useCallback(() => {
        console.log('updating the map');
        const bounds = map.getBounds();
        setBounds([
            bounds.getSouthWest().lng,
            bounds.getSouthWest().lat,
            bounds.getNorthEast().lng,
            bounds.getNorthEast().lat,
        ]);
        setZoom(map.getZoom());
    }, [map]);

    const onMove = useCallback(() => {
        updateMap();
    }, [updateMap]);

    useEffect(() => {
        onMove();
    }, [map, onMove]);

    useEffect(() => {
        map.on('move', onMove);
        return () => {
            map.off('move', onMove);
        };
    }, [map, onMove]);

    return (
        <>
            {clusters.map(cluster => {
                const [longitude, latitude] = cluster.geometry.coordinates;
                const { cluster: isCluster, point_count: pointCount } =
                    cluster.properties;

                if (isCluster) {
                    return (
                        <Marker
                            key={`cluster-${cluster.id}`}
                            position={[latitude, longitude]}
                            icon={fetchIcon(
                                pointCount,
                                10 + (pointCount / points.length) * 40
                            )}
                            eventHandlers={{
                                click: () => {
                                    const expansionZoom = Math.min(
                                        supercluster?.getClusterExpansionZoom(
                                            cluster.id as number
                                        ) ?? maxZoom,
                                        maxZoom
                                    );
                                    map.setView(
                                        [latitude, longitude],
                                        expansionZoom,
                                        {
                                            animate: true,
                                        }
                                    );
                                },
                            }}
                        />
                    );
                }

                return (
                    <Marker
                        key={`crime-${cluster.properties.OBJECTID}`}
                        position={[latitude, longitude]}
                    />
                );
            })}
        </>
    );
};

const icons: Record<string, DivIcon> = {};

const fetchIcon = (count: number, size: number) => {
    if (!icons[count]) {
        icons[count] = new DivIcon({
            html: `<div class="cluster-marker" style="width: ${size}px; height: ${size}px;">
        ${count}
      </div>`,
        });
    }
    return icons[count];
};
