import { Map } from './Map';
import { PointProperties, Points } from './Points';

import pointsFeatureCollection from '../resources/points.json';
import { FeatureCollection, Point } from 'geojson';

const center: [number, number] = [-1.076607405691166, 53.953671548823806];
const zoom = 13;

/**
 * An example app that includes a Leaflet map.
 */
export const App = () => (
    <Map center={center} zoom={zoom} maxZoom={22}>
        <Points
            points={
                (
                    pointsFeatureCollection as FeatureCollection<
                        Point,
                        PointProperties
                    >
                ).features
            }
        />
    </Map>
);
