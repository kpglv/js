import { PropsWithChildren } from 'react';
import { MapContainer, TileLayer } from 'react-leaflet';

import '../node_modules/leaflet/dist/leaflet.css';
import './Map.css';

export interface MapProps extends PropsWithChildren {
    center: [number, number];
    zoom: number;
    maxZoom: number;
}

/**
 * A generic map instance.
 */
export const Map = ({ center, zoom, maxZoom, children }: MapProps) => {
    return (
        <MapContainer
            center={center}
            zoom={zoom}
            maxZoom={maxZoom}
            attributionControl={false}
        >
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            />
            {children}
        </MapContainer>
    );
};
