# hello-nextjs-blog

An example application using Next.js --- a simple blog.

- Learn Next.js https://nextjs.org/learn
- Create a Next.js App
  https://nextjs.org/learn-pages-router/basics/create-nextjs-app

## Getting Started

Run the development server

```
yarn dev
```

Build the app for production

```
yarn build
```

Run the built app in production mode

```
yarn start
```
