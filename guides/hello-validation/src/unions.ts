// parsing, serializing and validating discriminated unions
// https://ajv.js.org/json-type-definition.html#discriminator-form

import Ajv from 'ajv/dist/jtd';
import { JTDSchemaType } from 'ajv/dist/jtd';

const ajv = new Ajv({ allErrors: true });

// types

interface Foo {
    frob: string;
}

interface Bar {
    quux: number;
}

// schemas

const FooSchema: JTDSchemaType<Foo> = {
    properties: {
        frob: { type: 'string' },
    },
    additionalProperties: true,
};

const BarSchema: JTDSchemaType<Bar> = {
    properties: {
        quux: { type: 'int32' },
    },
    additionalProperties: true,
};

// don't know how to type this schema; errors like
// 'JTD: discriminator tag used in properties' arise when 'type'
// attribute is included in Foo and Bar, and schema is invalid if
// it is not; seems like JTDSchemaType<Foo | Bar> does not cut the ice
const FooOrBarSchema = {
    discriminator: 'type',
    mapping: {
        foo: FooSchema,
        bar: BarSchema,
    },
};

const BazSchema = {
    properties: {
        docs: {
            elements: FooOrBarSchema,
        },
    },
};

// using schemas

const validateFoo = ajv.compile(FooSchema);
const validateBar = ajv.compile(BarSchema);
const validateFooOrBar = ajv.compile(FooOrBarSchema);
const validateBaz = ajv.compile(BazSchema);

// data examples

const foo = {
    type: 'foo',
    frob: 'hey',
};

const bar = {
    type: 'bar',
    quux: 1,
};

const baz = { docs: [foo, bar] };

// ignores type tag
console.log('foo is valid Foo:', validateFoo(foo));
console.log('bar is valid Bar:', validateBar(bar));
// uses the type tag, will fail without it
console.log('foo is valid FooOrBar:', validateFooOrBar(foo));
console.log('bar is valid FooOrBar:', validateFooOrBar(bar));
// discriminated union nested in another schema
console.log('baz is valid Baz:', validateBaz(baz));
