// https://ajv.js.org/guide/getting-started.html

import Ajv from 'ajv';
import fooSchema from './foo.schema.mjs';
import fooJtd from './foo.jtd.mjs';

const data = [
  {
    foo: 1,
    bar: 'abc',
  },
  {
    foo: "1",
    bar: 2,
    baz: "yeah",
  }
];

const jsonData = [
  '{"foo": 1, "bar": "abc"}',
  '{"unknown": "abc"}',
];


// basic data validation

const ajv = new Ajv({ allErrors: true });
// validator should be compiled once and reused
const validate = ajv.compile(fooSchema);

data.forEach(x => {
  const valid = validate(x);
  if (valid) {
    console.log('✅', x);
  } else {
    // errors are overwritten on every call to 'validate'
    console.log('❌', validate.errors);
  }
});


// parsing and serializing JSON (JTD only)

// parsing this way obviates the need for separate validation step and almost
// as fast as generic 'JSON.parse' (and fails faster); serializing is faster
// than generic 'JSON.stringify'

import AjvJtd from 'ajv/dist/jtd.js';
const ajvjtd = new AjvJtd({ allErrors: true });

const serialize = ajvjtd.compileSerializer(fooJtd);
const parse = ajvjtd.compileParser(fooJtd);

data.forEach(x => {
  console.log('serializing:', '✅', serialize(x));
});


jsonData.forEach(json => {
  const data = parse(json);
  if (data === undefined) {
    // parser does not throw, returns undefined instead
    // error message and position is overwritten on every call to 'parse'
    console.log('parsing:', '❌', parse.message, ': position', parse.position);
  } else {
    console.log('parsing:', '✅', data);
  }
});
