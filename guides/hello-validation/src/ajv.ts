// https://ajv.js.org/guide/typescript.html


import Ajv from 'ajv';
// utility types for schemas
import fooSchema from './foo.schema';
import fooJtd from './foo.jtd';

const data = [
  {
    foo: 1,
    bar: 'abc',
  },
  {
    foo: "1",
    bar: 2,
    baz: "yeah",
  }
];

const jsonData = [
  '{"foo": 1, "bar": "abc"}',
  '{"unknown": "abc"}',
];


// basic data validation

const ajv = new Ajv({ allErrors: true });
const validate = ajv.compile(fooSchema);

data.forEach(x => {
  const valid = validate(x);
  if (valid) {
    console.log('validating:', '✅', x);
  } else {
    console.log('validating:', '❌', validate.errors);
  }
});

// utility types for data (JTD only)

const jtd = {
  properties: {
    foo: {type: "int32"}
  },
  optionalProperties: {
    bar: {type: "string"}
  }
} as const;

import AjvJtd, { JTDDataType } from 'ajv/dist/jtd';
const ajvjtd = new AjvJtd({ allErrors: true });

type FooDataType = JTDDataType<typeof jtd>;
const validateTyped = ajvjtd.compile<FooDataType>(jtd);

data.forEach(x => {
  const valid = validateTyped(x);
  if (valid) {
    console.log('validating typed:', '✅', x);
  } else {
    console.log('validating typed:', '❌', validate.errors);
  }
});


// parsing and serializing JSON (JTD only)

const serialize = ajvjtd.compileSerializer(fooJtd);
const parse = ajvjtd.compileParser(fooJtd);

// will not compile for invalid data
// data.forEach(x => {
//   console.log('serializing:', '✅', serialize(x));
// });
console.log('serializing:', '✅', serialize({ foo: 1, bar: "yeah" }));

jsonData.forEach(json => {
  const data = parse(json);
  if (data === undefined) {
    // parser does not throw, returns undefined instead
    // error message and position is overwritten on every call to 'parse'
    console.log('parsing:', '❌', parse.message, ': position', parse.position);
  } else {
    console.log('parsing:', '✅', data);
  }
});


// type-safe error handling: TODO

// type-safe parsers and serializers: TODO

// type-safe unions: TODO
