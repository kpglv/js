// Utility types for schemas

import { JTDSchemaType } from 'ajv/dist/jtd';

interface FooData {
  foo: number;
  bar?: string;
}

const fooJtd: JTDSchemaType<FooData> = {
  properties: {
    foo: { type: 'int32' },
  },
  optionalProperties: {
    bar: { type: 'string' },
  },
  additionalProperties: true,
};

export default fooJtd;
