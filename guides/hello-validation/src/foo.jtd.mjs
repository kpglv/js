const fooJtd = {
  properties: {
    foo: { type: 'int32' },
  },
  optionalProperties: {
    bar: { type: 'string' },
  },
  additionalProperties: true,
};

export default fooJtd;
