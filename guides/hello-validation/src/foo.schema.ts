// Utility types for schemas

import { JSONSchemaType } from 'ajv';

interface FooData {
  foo: number;
  bar?: string;
}

const fooSchema: JSONSchemaType<FooData> = {
  type: 'object',
  properties: {
    foo: { type: 'integer' },
    bar: { type: 'string', nullable: true },
  },
  required: ['foo'],
  additionalProperties: false,
};

export default fooSchema;
