# hello-validation

Exploring data schema definition approaches and data validation libraries.

- Schema definitions
  - JSON Schema
    - https://json-schema.org/
    - https://ajv.js.org/json-schema.html
  - JSON Type Definition
    - https://datatracker.ietf.org/doc/rfc8927/
    - https://ajv.js.org/json-type-definition.html
- Ajv JSON schema validator https://ajv.js.org/

## Getting Started

Install dependencies

```
yarn
```

Explore the code in `src` directory.
