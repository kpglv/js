# hello-ionic

Exploring Ionic component framework for web/mobile/desktop application
development.

# Getting Started

## Web

Serve the development build

```
ionic serve
```

## Android

Run the project on a device or in an emulator

```
npx cap run android
```

or use Android Studio to select a device and start the app
