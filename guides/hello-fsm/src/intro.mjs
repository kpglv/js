// https://github.com/jakesgordon/javascript-state-machine

import StateMachine from 'javascript-state-machine';
import StateMachineHistory from 'javascript-state-machine/lib/history.js';


const fsm = new StateMachine({
  init: 'solid',
  // a transition table
  transitions: [
    { name: 'melt',     from: 'solid',  to: 'liquid' },
    { name: 'freeze',   from: 'liquid', to: 'solid'  },
    { name: 'vaporize', from: 'liquid', to: 'gas'    },
    { name: 'condense', from: 'gas',    to: 'liquid' }
  ],
  methods: {
    onMelt:     function() { console.log('I melted');    },
    onFreeze:   function() { console.log('I froze');     },
    onVaporize: function() { console.log('I vaporized'); },
    onCondense: function() { console.log('I condensed'); }
  },
  plugins: [
    new StateMachineHistory({ max: 10 })
  ]
});

console.log('all possible states:', fsm.allStates());
console.log('all possible transitions:', fsm.allTransitions());

console.log('current state:', fsm.state);
console.log('current state is "solid":', fsm.is('solid'));
console.log('transitions available in the current state:', fsm.transitions());
console.log('transition "freeze" is allowed from the current state:', fsm.can("freeze"));

console.log('some input event triggers transition "melt":', fsm.melt());
console.log('some input event triggers transition "vaporize":', fsm.vaporize());
console.log('state history:', fsm.history);

// create a graph description (a transition diagram) in DOT format that can be
// processed, e.g., by GraphViz

import visualize from 'javascript-state-machine/lib/visualize.js';

const graph = visualize(fsm);
console.log(graph);

import { existsSync, mkdirSync, writeFileSync } from 'fs';

if (!existsSync('target')) {
  mkdirSync('target');
}
writeFileSync('target/fsm.dot', graph);
