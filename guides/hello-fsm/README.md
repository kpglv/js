# hello-fsm

Exploring libraries to define finite state machines.

- A javascript finite state machine library
  https://github.com/jakesgordon/javascript-state-machine

## Getting Started

Explore and run the code in the `src` directory.

To produce an image from the DOT diagram

```
dot -Tsvg target/fsm.dot > target/fsm.svg
```
