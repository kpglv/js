# hello-redux

Exploring state management libraries.

- Redux: a JS library for predictable and maintainable global
  state management https://redux.js.org/
- Redux Toolkit (RTK): the official, opinionated, batteries-included
  toolset for efficient Redux development https://redux-toolkit.js.org/
- React Redux: official React bindings for Redux https://react-redux.js.org/

> Note: it is specifically recommended to use Redux Toolkit (the
> `@reduxjs/toolkit` package), and not to use the legacy `redux` core package
> for any new Redux code.

## Getting Started

- `dev`/`start` - start dev server and open browser
- `build` - build for production
- `preview` - locally preview production build
- `test` - launch test runner

See and run examples in `test/examples.ts`.
