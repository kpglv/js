import {
  // a slice
  counterSlice,
  // action creators
  increment,
  decrement,
  incrementByAmount,
  incrementAsync,
  incrementIfOdd,
  // selectors
  selectCount,
  selectStatus,
} from '../src/features/counter/counterSlice';

const identity = (x: unknown) => x;
const constantly = (x: unknown) => () => x;

console.log('counter slice keys:', Object.keys(counterSlice));
console.log('create an increment action:', increment());
console.log('create an decrement action:', decrement());
console.log('create an increment by amount action:', incrementByAmount(7));
console.log('create an increment if odd action:', incrementIfOdd(7));
console.log(
  'create an async increment by amount action:',
  await incrementAsync(12)(identity, constantly(0), 1),
);

console.log(
  'select count:',
  selectCount({ counter: { value: 12, status: 'idle' } }),
);
console.log(
  'select status:',
  selectStatus({ counter: { value: 12, status: 'loading' } }),
);
