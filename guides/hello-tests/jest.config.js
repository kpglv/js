const config = {
  // do not transform away ES6 imports
  transform: {},
  // match .mjs as well as .js, .ts, .jsx, .tsx
  testMatch: ["**/?(*.)+(spec|test).?(m)[tj]s?(x)"]
};

module.exports = config;
