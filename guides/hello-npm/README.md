# hello-npm

Exploring NPM usage and configuration options.

Contains an NPM package that consumes (imports values from, depends on) other
packages externally, on the artifact level (requiring them to be published).

## Getting Started

Prepare and source the `.env` file (see `example.env`) to set/export
required environment variables (including package registry's auth token).

```
source .env
```

Install dependencies (should be published first)

```
npm install
```

### Build the code

```
npm run build
```

### Run the built code

```
npm run start
```
