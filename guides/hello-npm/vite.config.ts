import { resolve } from 'path';
import { defineConfig } from 'vite';

export default defineConfig({
    build: {
        rollupOptions: {
            input: 'src/external-consumer.ts',
        },
        lib: {
            entry: resolve(__dirname, 'src/external-consumer.ts'),
            name: 'external-consumer',
            fileName: 'external-consumer',
        },
    },
});
