import { add } from '@example/provider/dist';
import { add as addSrc } from '@example/provider/src';

import { printSum } from '@example/consumer/dist';
import { printSum as printSumSrc } from '@example/consumer/src';

console.log('-----');
console.log(
    'Using "provider" as an external (published) dependency, from "dist"'
);
console.log('Adding %o and %o: %o', 111, 234, add(111, 234));

console.log('-----');

console.log(
    'Using "provider" as an external (published) dependency, from "src"'
);
console.log('Adding %o and %o: %o', 222, 234, addSrc(222, 234));

console.log('-----');
console.log(
    'Using "consumer" as an external (published) dependency, from "dist"'
);
printSum(1, 2);

console.log('-----');
console.log(
    'Using "consumer" as an external (published) dependency, from "src"'
);
printSumSrc(2, 3);
