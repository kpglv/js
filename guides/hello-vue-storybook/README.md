# hello-vue-storybook

Example Vue 3 + Vite application along with Storybook component explorer.

- Component Driven User Interfaces https://www.componentdriven.org/
- Storybook for Vue tutorial
  https://storybook.js.org/tutorials/intro-to-storybook/vue/en/get-started/
- Code for Learn Storybook https://github.com/chromaui/learnstorybook-code

## Getting Started

## Development

Start the component explorer

```
yarn storybook
```

Start the app

```
yarn dev
```

Start interaction tests (alongside with component explorer)

```
yarn test-storybook --watch
```

## Production

Build a static site

```
yarn build-storybook
```
