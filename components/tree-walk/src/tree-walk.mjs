// based on `clojure.walk`
// https://github.com/clojure/clojure/blob/master/src/clj/clojure/walk.clj


export const identity = (x) => x;
export const partial = (f, ...args) => (...more) => f(...args, ...more);
export const isArray = Array.isArray;
export const isSet = (x) => x instanceof Set;
export const isMap = (x) => x instanceof Map;
export const isObject = (x) => typeof x === 'object' && !!x;


/**
 * Walks the tree. Applies outer function to atomic nodes; applies inner
 * function to each element of composite nodes (arrays, sets, maps/objects),
 * building the same data structure, then applies outer function to the
 * result.
 */
export const walk = (inner, outer, tree) => {
    if (isArray(tree)) {
        return outer(tree.map(inner));
    } else if (isSet(tree)) {
        return outer(new Set([...tree.values()].map(inner)));
    } else if (isMap(tree)) {
        return outer(
            new Map([...tree.entries()]
                    .map(([key, value]) => [inner(key), inner(value)])));
    } else if (isObject(tree)) {
        return outer(
            Object.fromEntries(
                Object.entries(tree)
                    .map(([key, value]) => [inner(key), inner(value)])));
    } else {
        return outer(tree);
    }
};

/**
 * Performs depth-first, pre-order traversal of the tree, calling f on each
 * subtree and using f's return value in place of the original value.
 */
export const prewalk = (f, tree) => walk(partial(prewalk, f), identity, f(tree));

/**
 * Performs depth-first, post-order traversal of the tree, calling f on each
 * subtree and using f's return value in place of the original value.
 */
export const postwalk = (f, tree) => walk(partial(postwalk, f), f, tree);

export const prewalkDemo =
    (tree) => prewalk(x => (console.log("prewalked:", x), x), tree);

export const postwalkDemo =
    (tree) => postwalk(x => (console.log("postwalked:", x), x), tree);
