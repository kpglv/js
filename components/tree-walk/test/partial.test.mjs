import {
    partial,
} from '../src/tree-walk.mjs';

const sum = (x, y) => x + y;

describe('partial', () => {
    it('correctly partially applies binary function to one argument', () => {
        const add3 = partial(sum, 3);
        expect(add3(4)).toBe(7);
    });
    it('correctly partially applies binary function to two arguments', () => {
        const add3and4 = partial(sum, 3, 4);
        expect(add3and4(4)).toBe(7);
    });
    it('correctly partially applies binary function to three arguments', () => {
        const add3and4and8 = partial(sum, 3, 4, 8);
        expect(add3and4and8(4)).toBe(7);
    });
});
