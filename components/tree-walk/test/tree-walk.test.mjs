import {
    walk,
    prewalk,
    postwalk,
    partial,
    identity
} from '../src/tree-walk.mjs';

const upperKeyMultiplyValue = x =>
      typeof x === 'number' ? x * 10 :
      typeof x === 'string' ? x.toUpperCase() : x;

const a1 = [1, 2, 3, [4, 5], {id: 134}];
const m1 = {id: 1, children: [{id: 2, age: 5}, {id: 3, age: 6}, 12, "foo"]};
const a2 = [
    1,
    [2, 3],
    {id: 1},
    new Map([["foo", 123], ["bar", 345], ["quux", new Set([1, 2, 3])]])
];

const a1transformed = [10, 20, 30, [40, 50], {ID: 1340}];
const m1transformed = {
    ID: 10, CHILDREN: [{ID: 20, AGE: 50}, {ID: 30, AGE: 60}, 120, "FOO"]
};
const a2transformed = [
    10,
    [20, 30],
    {ID: 10},
    new Map([["FOO", 1230], ["BAR", 3450], ["QUUX", new Set([10, 20, 30])]])
];

describe('prewalk', () => {
    it('can prewalk with identity', () => {
        expect(prewalk(identity, a1)).toEqual(a1);
        expect(prewalk(identity, m1)).toEqual(m1);
        expect(prewalk(identity, a2)).toEqual(a2);
    });
    it('can prewalk with transforming function', () => {
        expect(prewalk(upperKeyMultiplyValue, a1)).toEqual(a1transformed);
        expect(prewalk(upperKeyMultiplyValue, m1)).toEqual(m1transformed);
        expect(prewalk(upperKeyMultiplyValue, a2)).toEqual(a2transformed);
    });
});

describe('postwalk', () => {
    it('can postwalk with identity', () => {
        expect(postwalk(identity, a1)).toEqual(a1);
        expect(postwalk(identity, m1)).toEqual(m1);
        expect(postwalk(identity, a2)).toEqual(a2);
    });

    it('can postwalk with transforming function', () => {
        expect(postwalk(upperKeyMultiplyValue, a1)).toEqual(a1transformed);
        expect(postwalk(upperKeyMultiplyValue, m1)).toEqual(m1transformed);
        expect(postwalk(upperKeyMultiplyValue, a2)).toEqual(a2transformed);
    });
});
