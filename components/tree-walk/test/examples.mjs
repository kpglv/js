import {
    prewalk,
    postwalk,
    prewalkDemo,
    postwalkDemo,
    partial,
    identity
} from '@kpglv/tree-walk';

const add3 = partial((x, y) => x + y, 3);
console.log('add3:', add3(4));

const tree = {id: 1, children: [{id: 2, age: 5}, {id: 3, age: 6}, 12]};
prewalkDemo(tree);
postwalkDemo(tree);

prewalkDemo([1, 2, 3, [4, 5]]);

const upperKeyMultiplyValue = x =>
      typeof x === 'number' ? x * 10 :
      typeof x === 'string' ? x.toUpperCase() : x;
const t1 = prewalk(upperKeyMultiplyValue, tree);
console.log('original tree:', tree);
console.log('prewalked tree (uppercased keys, multiplied values):', t1);

const numbers = [];
const t2 = postwalk(x => typeof x === 'number' ? (numbers.push(x), x) : x, tree);
console.log('numbers collected from original tree:', numbers);
// the produced tree is irrelevant though
console.log('the produced tree is the same as original:', t2);

const t3 = [
    1,
    [2, 3],
    {id: 1},
    new Map([["foo", 123], ["bar", 345], ["quux", new Set([1, 2, 3])]])
];
console.log('tree with sets and maps:', t3);
console.log('postwalked:', postwalk(identity, t3));

console.log('postwalked atomic value:', "foo", postwalk(identity, "foo"));
