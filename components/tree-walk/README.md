# tree-walk

Walking arbitrary trees represented by composite data structures (arrays,
sets, maps/objects) that can contain arbitrary values.

Based on `clojure.walk`
https://github.com/clojure/clojure/blob/master/src/clj/clojure/walk.clj


## Getting Started

### Usage Examples

```js
import { prewalk } from 'tree-walk';

const tree = {id: 1, children: [{id: 2, age: 5}, {id: 3, age: 6}, 12]};
const upperKeyMultiplyValue = x =>
      typeof x === 'number' ? x * 10 :
      typeof x === 'string' ? x.toUpperCase() : x;

console.log('tree:', tree);
// => {id: 1, children: [{id: 2, age: 5}, {id: 3, age: 6}, 12]}

console.log('transformed tree:', prewalk(upperKeyMultiplyValue, tree));
// => {ID: 10, CHILDREN: [{ID: 20, AGE: 50}, {ID: 30, AGE: 60}, 120]}
```

See and run examples in `test/examples.mjs`

```
node test/examples.mjs
```

## Testing

Run unit tests

```
yarn test
```

## TODO

- [ ] add more unit tests
