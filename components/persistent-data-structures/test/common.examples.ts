import { fragment, hash } from '../src/common';

const fooHash = hash('foo');
const size = 5;

console.log(`hash of "foo": 0b${fooHash.toString(2)}`);
[0, 5, 10, 15, 20, 25].forEach(offset => {
    console.log(`fragment of size ${size} at ${offset}:`);
    console.log(fragment(fooHash, size, offset).toString(2));
});
