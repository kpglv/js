import { it, describe, expect } from 'vitest';

import fc from 'fast-check';
import {
    hash,
    fnil,
    toBin,
    hammingWeight,
    toBitmap,
    fromBitmap,
    fragment,
} from '../src/common.mjs';

describe('fnil', () => {
    it('always returns functions', () => {
        fc.assert(
            fc.property(fc.func(fc.anything()), fc.anything(), (fn, arg) => {
                expect(typeof fnil(fn, arg)).toBe('function');
            })
        );
    });
});

describe('hash', () => {
    it('always returns integers', () => {
        fc.assert(
            fc.property(fc.anything(), datum => {
                expect(Number.isInteger(hash(datum))).toBe(true);
            })
        );
    });
    it('for integers, returns integers themselves', () => {
        fc.assert(
            fc.property(fc.integer(), datum => {
                expect(hash(datum)).toBe(datum);
            }),
            { numRuns: 1000 }
        );
    });
    it('converts all other values to strings beforehand', () => {
        expect(hash({})).toBe(3938);
        expect(hash([])).toBe(2914);
        expect(hash(new Date(1709382592705))).toBe(-1080350592);
    });
});

describe('toBin', () => {
    it('always returns binary strings of length 32', () => {
        fc.assert(
            fc.property(fc.oneof(fc.integer(), fc.double()), datum => {
                const binaryString = toBin(datum);
                expect(typeof binaryString).toBe('string');
                expect(binaryString.length).toBe(32);
                [...binaryString].forEach(letter =>
                    expect(letter === '0' || letter === '1').toBe(true)
                );
            })
        );
    });
});

describe('hammingWeight', () => {
    it('always returns natural numbers', () => {
        fc.assert(
            fc.property(
                fc.stringOf(fc.constantFrom('0', '1'), {
                    minLength: 32,
                    maxLength: 32,
                }),
                binaryString => {
                    const hw = hammingWeight(binaryString);
                    expect(Number.isInteger(hw) && hw >= 0).toBe(true);
                }
            )
        );
    });
});
