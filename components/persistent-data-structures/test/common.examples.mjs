// helpers

const nilPatchedParseInt = fnil(parseInt, '123');
console.log(
    `fnil: providing default ${'123'} for parseInt(): ${nilPatchedParseInt()}`
);
console.log(
    `fnil: providing default ${'123'} for parseInt('456'): ${nilPatchedParseInt(
        '456'
    )}`
);

// hashing

console.log('hash of 123:', hash(123));
console.log('hash of "foo":', hash('foo'));
console.log('hash of [1, 2, 3]:', hash([1, 2, 3]));
console.log('hash of [3, 2, 1]:', hash([3, 2, 1]));

// Hamming weight

[0, 0b10, 0b111, 0b1101, 0b1011, 0b11010110010110, -1, 7.8].forEach(n =>
    console.log(`hamming weight of ${toBin(n)} (${n}): ${hammingWeight(n)}`)
);

// bitmap operations

[0, 1, 2, 3].forEach(n =>
    console.log(`bitmap for ${n}: ${toBin(toBitmap(n))} (${toBitmap(n)})`)
);
const bm = 0b101010101;
[0, 1, 2].forEach(index =>
    console.log(
        `${index}-th bit in the bitmap ${toBin(bm)} (${bm}): ${fromBitmap(
            bm,
            index
        )}`
    )
);

const fooHash = hash('foo');
const size = HASH_FRAGMENT_SIZE;
console.log('hash of "foo":');
[0, 5, 10, 15, 20, 25].forEach(offset => {
    console.log(`fragment of size ${size} at ${offset}`);
    console.log(toBin(fooHash));
    console.log(toBin(fragment(fooHash, size, offset) << offset));
    const indicator = new Array(32).fill(' ');
    indicator[31 - offset] = '^';
    indicator[31 - (offset + size - 1)] = '^';
    console.log(indicator.join(''));
});
