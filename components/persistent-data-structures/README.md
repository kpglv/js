# persistent-data-structures

A component that contains basic implementations of persistent data structures
based on hash tries.

> Note: this component is currently a work in progress.

## Links

Papers

-   http://lampwww.epfl.ch/papers/idealhashtrees.pdf

Algorithms

-   Hashing https://stackoverflow.com/a/7616484
-   Hamming weight https://jsperf.com/hamming-weight

Existing implementations

-   https://blog.mattbierner.com/persistent-hash-tries-in-javavascript/
-   https://blog.mattbierner.com/hash-array-mapped-tries-in-javascript/
-   https://idea.popcount.org/2012-07-25-introduction-to-hamt/

-   https://github.com/mattbierner/hamt
-   https://github.com/mattbierner/hashtrie
-   https://github.com/mattbierner/js-hashtrie-benchmark

-   https://github.com/immutable-js/immutable-js/blob/main/src/Map.js

Guides and explanations

-   Understanding Clojure's Persistent Vectors
    https://hypirion.com/musings/understanding-persistent-vector-pt-1

## Getting Started

TODO

## Testing

Run unit tests

```
yarn test
```
