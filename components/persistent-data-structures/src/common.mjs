/**
 * Returns a 32-bit integer hash of the value.
 * @param {any} x
 * @returns
 */
export const hash = x => {
    // integers are their own hashes
    if (Number.isInteger(x)) return x;
    // any other data type is coerced to string
    const s = typeof x === 'string' ? x : JSON.stringify(x) || '';

    let hash = 0;
    for (let i = 0; i < s.length; i++) {
        const c = s.charCodeAt(i);
        // left shift by 5 is the same as multiplication by 32,
        // with hash subtracted, it is multiplication by 31, but faster
        // (a small prime); bitwise OR converts (the first operand)
        // to 32 - bit integer (maybe replace it with '& 0xffff'?
        // more explicit, but gives a different hash)
        hash = ((hash << 5) - hash + c) | 0;
    }
    return hash;
};

/**
 * Provides a default first argument for f if the first argument is absent,
 * undefined or null.
 */
export const fnil =
    (f, x) =>
    (...args) => {
        if (args[0] == null) {
            args[0] = x;
        }
        return f(...args);
    };

/**
 * Converts a number to a 32-bit signed integer and returns it
 * as a binary string, left-padded by zeros by default.
 */
export const toBin = (x, pad = true) => {
    const s = (x >>> 0).toString(2);
    if (pad && s.length < 32) {
        return '0'.repeat(32 - s.length) + s;
    } else {
        return s;
    }
};

/**
 * Returns the count of ones in a binary representation of an integer.
 * TODO: implement more efficient algorithm
 * @param {number} x
 * @returns
 */
export const hammingWeight = n => toBin(n).split(1).length - 1;

/**
 * Returns a bitmap (an integer) with n-th bit set to 1.
 * @param {integer} n
 * @returns
 */
export const toBitmap = n => 1 << n;

/**
 * Returns the value of bit at the index (1-based) in the bitmap.
 * @param {integer} bitmap
 * @param {integer} index
 * @returns
 */
export const fromBitmap = (bitmap, index) =>
    hammingWeight(bitmap & (index - 1));

/**
 * Returns an integer that is a fragment of the hash of size least
 * significant bits, starting at offset.
 */
export const fragment = (hash, size, offset) =>
    (hash >>> offset) & ((1 << size) - 1);
