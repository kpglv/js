import {
  hash,
  hammingWeight,
  fragment,
  fnil,
  toBin,
  fromBitmap,
  toBitmap
} from './common.mjs';

// a hash is consumed by fragments of 5 bits
const HASH_FRAGMENT_SIZE = 5;
// the size of an array that can be indexed by a hash fragment is 32
const HASH_ARRAY_SIZE = Math.pow(2, HASH_FRAGMENT_SIZE);


// there are two types of nodes: internal and leaf

/**
 * An internal node (that holds an array of child nodes and a bitmap that
 * determine which child nodes exist).
 */
class InternalNode {
  constructor(bitmap, children) {
    // the array of child nodes
    this.children = children;
    // bitmap that determines which children are in the array
    this.bitmap = bitmap;
  }
}

// there are two types of leaf nodes: leaf and collision leaf

// the empty node

const emptyNode = Symbol.for('emptyNode');

// absence of an entry

const nothing = Symbol.for('nothing');

/**
 * A leaf node (that holds a key-value pair).
 */
class LeafNode {
  constructor(hash, key, value) {
    // the hash of the key
    this.hash = hash;
    // the key
    this.key = key;
    // the value
    this.value = value;
  }

  get(key) {
    return key === this.key ? this.value : nothing;
  }
}

/**
 * A collision leaf node (that holds a list of references to leaf nodes that
 * share the hash but have different keys).
 */
class CollisionLeafNode {
  constructor(hash, children) {
    // the hash
    this.hash = hash;
    // the list of leaf nodes that share the hash but have different keys
    this.children = children;
  }

  get(key) {
    for (let i = 0; i < this.children.length; i++) {
      if (key === this.children[i].key) {
        return this.children[i].value;
      }
    }
    return nothing;
  }
}
