export type Hash = number;
export type HashFragment = number;

/**
 * Returns a 32-bit integer hash of `x`.
 */
export const hash = (x: unknown): Hash => {
    // integers are their own hashes
    if (typeof x === 'number' && Number.isInteger(x)) return x;
    // any other data type is coerced to string
    const s = typeof x === 'string' ? x : JSON.stringify(x) || '';

    let hash = 0;
    for (let i = 0; i < s.length; i++) {
        const c = s.charCodeAt(i);
        // left shift by 5 is the same as multiplication by 32,
        // with hash subtracted, it is multiplication by 31, but faster
        // (a small prime); bitwise OR converts (the first operand)
        // to 32 - bit integer (maybe replace it with '& 0xffff'?
        // more explicit, but gives a different hash)
        hash = ((hash << 5) - hash + c) | 0;
    }
    return hash;
};

/**
 * Returns an integer that is a fragment of the `hash` of `size` least
 * significant bits, starting at `offset`.
 */
export const fragment = (
    hash: Hash,
    size: number,
    offset: number = 0
): HashFragment => (hash >>> offset) & ((1 << size) - 1);

export const popcount = () => {};
