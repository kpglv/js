import { isObject } from '@kpglv/core';
import { Hash, HashFragment } from './common';

/** Types */

export type HashTrie = InternalNode | CollisionNode | LeafNode;

export interface InternalNode {
    children: Record<HashFragment, HashTrie>;
}

export interface CollisionNode {
    hash: Hash;
    siblings: LeafNode[];
}

export interface LeafNode {
    hash: Hash;
    key: unknown;
    value: unknown;
}

export const isInternalNode = (x: unknown): x is InternalNode =>
    isObject(x) && !!x.children;

export const isCollisionNode = (x: unknown): x is CollisionNode =>
    isObject(x) && !!x.siblings;

export const isLeafNode = (x: unknown): x is LeafNode =>
    isObject(x) && Object.hasOwn(x, 'key') && Object.hasOwn(x, 'value');

/** Constants */

export const hashSize = 32;
export const hashFragmentSize = 4;
export const hashFragmentsMaxCount = hashSize / hashFragmentSize;

export const emptyLeafNode = null;

/** API */

/**
 * Creates an empty hash trie.
 */
export const empty = (): HashTrie => ({ children: {} });

/**
 * Returns `trie` with `value` inserted under `key`.
 * If `key` is already present, replaces the existing value.
 */
export const insert = (
    trie: HashTrie,
    key: unknown,
    value: unknown
): HashTrie => empty();

/**
 * Returns `trie` without value associated with `key`.
 */
export const remove = (trie: HashTrie, key: unknown): HashTrie => empty();

/**
 * Returns the value associated with `key` in `trie`, or `null`.
 */
export const get = (trie: HashTrie, key: unknown): unknown => key;
