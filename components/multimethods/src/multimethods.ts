/** Types */

/**
 * A function --- receives an argument and returns a value.
 * The argument can be an associative data structure (object) with arbitrary number of fields.
 */
export type Fn<A, R> = (arg: A) => R;

/**
 * A method (generic function implementation) --- an ordinary function, just not intended to be called
 * directly (only through multimethod's polymorphic dispatch mechanism).
 */
export type Method<A, R> = Fn<A, R>;

/**
 * A multimethod (generic function) --- a set of methods and a dispatch function.
 * The dispatch function is called with an argument and returns a dispatch value,
 * by which an appropriate method is selected and executed with the same argument,
 * returning a value (usually of some other type than the dispatch value).
 */
export type Multimethod<A, D, R> = Fn<A, R> & {
    /** Registers the method (associates it with the dispatch value) */
    when: (dispatchValue: D, method: Method<A, R>) => Multimethod<A, D, R>;
    /** Registers the default method (that is called for dispatch values with no methods registered) */
    default: (method: Method<A, R>) => Multimethod<A, D, R>;
    /** Deregisters method for the dispatch value */
    remove: (dispatchValue: D) => Multimethod<A, D, R>;
    /** Returns an object containing the map of registered methods and the default method */
    methods: () => {
        methods: Map<D, Method<A, R>>;
        defaultMethod: Method<A, R>;
    };
};

/** Implementation */

/**
 * Does nothing.
 * Can be used as a default method.
 */
export const noop = () => {};

/**
 * Returns the argument.
 * Can be used as a dispatch function or default method.
 */
export const identity = <T>(x: T): T => x;

/**
 * Default method that throws an error.
 */
export const throwNoMethod = <A>(arg: A): never => {
    const argValues = JSON.stringify(arg);
    throw new Error(`no method implementation for arguments: ${argValues}`);
};

/**
 * Returns a multimethod with the given dispatch function (default is identity).
 * Specific methods and the dispatch function should accept the argument of the same type.
 */
export const multi = <A, D, R>(
    dispatchFn?: Fn<A, D>
): Multimethod<A, D, R> => {
    const methods = new Map<D, Method<A, R>>();
    let defaultMethod: Method<A, R> = throwNoMethod<A>;

    /**
     * A multimethod instance (a closure with additional properties).
     * Implements a polymorphic dispatch mechanism: when a multimethod is called, it
     * - invokes the dispatch function with the given argument to obtain a dispatch value
     * - uses the dispatch value to find a registered method implementation
     * - invokes the found method (or default method, if no method was found) with the given argument
     *   and returns its return value
     */
    const multimethod: Multimethod<A, D, R> = arg => {
        const dispatch = dispatchFn || identity<A>;
        const dispatchValue = dispatch(arg);
        const method = methods.has(dispatchValue as D)
            ? methods.get(dispatchValue as D) ?? defaultMethod
            : defaultMethod;
        return method(arg);
    };

    /**
     * Adds the method implementation for the dispatch value.
     */
    multimethod.when = (dispatchValue, methodImplementation) => {
        if (methods.has(dispatchValue)) {
            console.warn(
                "multimethod: redefining method implementation for dispatch value '%o'",
                dispatchValue
            );
        }
        methods.set(dispatchValue, methodImplementation);
        return multimethod;
    };

    /**
     * Sets the default method implementation, that will be called when no
     * method can be found for the dispatch value.
     */
    multimethod.default = methodImplementation => {
        defaultMethod = methodImplementation;
        return multimethod;
    };

    /**
     * Removes  method implementation for the dispatch value.
     */
    multimethod.remove = dispatchValue => {
        methods.delete(dispatchValue);
        return multimethod;
    };

    /**
     * Returns an object containing the map of dispatch values to methods,
     * and the default method.
     */
    multimethod.methods = () => ({
        methods,
        defaultMethod,
    });

    return multimethod;
};
