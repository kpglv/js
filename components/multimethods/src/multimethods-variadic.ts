/* Variadic version */

/** Types */

/**
 * A function --- receives arbitrary number of arguments (possibly of different types)
 * and returns a value.
 */
export type Fn<Args extends unknown[], ReturnValue> = (
    ...args: Args
) => ReturnValue;

/**
 * A method (generic function implementation) --- an ordinary function, just not intended to be called
 * directly (only through multimethod's polymorphic dispatch mechanism).
 */
export type Method<Args extends unknown[], ReturnValue> = Fn<
    Args,
    ReturnValue
>;

/**
 * A multimethod (generic function) --- a set of methods and a dispatch function.
 * When a multimethod is called, the dispatch function is invoked with the arguments
 * and returns a dispatch value, by which an appropriate method is selected and invoked
 * with the same arguments, returning a value.
 */
export type Multimethod<
    Args extends unknown[],
    DispatchValue,
    ReturnValue
> = Fn<Args, ReturnValue> & {
    /** Registers the method (associates it with the dispatch value), returns the multimethod instance */
    when: (
        dispatchValue: DispatchValue,
        method: Method<Args, ReturnValue>
    ) => Multimethod<Args, DispatchValue, ReturnValue>;
    /** Registers the default method (that is called for dispatch values with no methods registered),
        returns the multimethod instance */
    default: (
        method: Method<Args, ReturnValue>
    ) => Multimethod<Args, DispatchValue, ReturnValue>;
    /** Deregisters method for the dispatch value, returns the multimethod instance */
    remove: (
        dispatchValue: DispatchValue
    ) => Multimethod<Args, DispatchValue, ReturnValue>;
    /** Returns an object containing the map of registered methods and the default method */
    methods: () => {
        methods: Map<DispatchValue, Method<Args, ReturnValue>>;
        defaultMethod: Method<Args, ReturnValue>;
    };
};

/** Implementation */

/**
 * Default method that throws an error.
 */
export const throwNoMethod = <Args extends unknown[]>(
    ...args: Args
): never => {
    const argValues = JSON.stringify(args);
    throw Error(
        `multimethod: no method implementation for arguments: ${argValues}`
    );
};

/**
 * Returns an instance of multimethod with the given dispatch function.
 * Specific methods and the dispatch function should accept the same number
 * of arguments with the same types.
 */
export const multi = <Args extends unknown[], DispatchValue, ReturnValue>(
    dispatchFn?: Fn<Args, DispatchValue>
): Multimethod<Args, DispatchValue, ReturnValue> => {
    if (!dispatchFn)
        throw new Error(
            'multimethod: a dispatch function must be provided when creating an instance'
        );

    const methods = new Map<DispatchValue, Method<Args, ReturnValue>>();
    let defaultMethod: Method<Args, ReturnValue> = throwNoMethod<Args>;

    /**
     * A multimethod instance (a closure with additional properties).
     * Implements a polymorphic dispatch mechanism: when a multimethod is called, it
     * - invokes the dispatch function with given arguments to obtain a dispatch value
     * - uses the dispatch value to find a registered method implementation
     * - invokes the found method (or default method, if no method was found) with the same arguments
     *   and returns its return value
     */
    const multimethod: Multimethod<Args, DispatchValue, ReturnValue> = (
        ...args
    ) => {
        const dispatchValue = dispatchFn(...args);
        const method = methods.has(dispatchValue)
            ? methods.get(dispatchValue) ?? defaultMethod
            : defaultMethod;
        return method(...args);
    };

    /**
     * Adds the method implementation for the dispatch value.
     * Returns the multimethod instance.
     */
    multimethod.when = (dispatchValue, methodImplementation) => {
        if (methods.has(dispatchValue)) {
            console.warn(
                "multimethod: redefining method implementation for dispatch value '%o'",
                dispatchValue
            );
        }
        methods.set(dispatchValue, methodImplementation);
        return multimethod;
    };

    /**
     * Sets the default method implementation, that will be called when no
     * method can be found for the dispatch value.
     * Returns the multimethod instance.
     */
    multimethod.default = methodImplementation => {
        defaultMethod = methodImplementation;
        return multimethod;
    };

    /**
     * Removes method implementation for the dispatch value.
     * Returns the multimethod instance.
     */
    multimethod.remove = dispatchValue => {
        methods.delete(dispatchValue);
        return multimethod;
    };

    /**
     * Returns an object that contains the map of dispatch values to methods,
     * and the default method.
     */
    multimethod.methods = () => ({
        methods,
        defaultMethod,
    });

    return multimethod;
};
