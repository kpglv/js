/**
 * Does nothing.
 */
export const noop = () => {};

/**
 * Returns the argument.
 */
export const identity = x => x;

/**
 * Default method implementation that throws an error.
 */
export const throwNoMethod = (...args) => {
    const values = JSON.stringify(args);
    throw new Error(`no method implementation for arguments: ${values}`);
};

/**
 * Returns a multimethod with the given dispatch function (default is
 * identity). Specific methods should accept the same arguments as the
 * dispatch function.
 */
export const multi = dispatch => {
    const methods = new Map();
    let defaultMethod = throwNoMethod;

    /**
     * Returns the multimethod instance (a closure).
     */
    const multimethod = (...args) => {
        const dispatchFn = dispatch || identity;
        const dispatchValue = dispatchFn(...args);
        const method = methods.has(dispatchValue)
            ? methods.get(dispatchValue)
            : defaultMethod;
        return method(...args);
    };

    /**
     * Adds the method implementation for the dispatch value.
     */
    multimethod.when = (dispatchValue, methodImplementation) => {
        if (methods.has(dispatchValue)) {
            console.warn(
                'redefining method implementation for dispatch value %o',
                dispatchValue
            );
        }
        methods.set(dispatchValue, methodImplementation);
        return multimethod;
    };

    /**
     * Sets the default method implementation, that will be called when no
     * method can be found for the dispatch value.
     */
    multimethod.default = methodImplementation => {
        defaultMethod = methodImplementation;
        return multimethod;
    };

    /**
     * Removes  method implementation for the dispatch value.
     */
    multimethod.remove = dispatchValue => {
        methods.delete(dispatchValue);
        return multimethod;
    };

    /**
     * Returns a map of dispatch values to methods.
     */
    multimethod.methods = () => methods;

    return multimethod;
};
