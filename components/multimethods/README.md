# multimethods

Multimethods provide runtime polymorphism with multiple dispatch.

This component provides a simplified implementation of multimethods; it only
supports primitives as dispatch values, cannot have several methods for one
dispatch value, cannot redefine the dispatch function, doesn't have any
convenience features, etc. See links below for rationale, explanations and a
full-fledged implementation.

- http://clojure.org/multimethods
- https://blog.klipse.tech/javascript/2021/10/04/multimethod.html
- https://github.com/KrisJordan/multimethod-js


## Getting Started

### Usage Examples

```js
import { multi } from '@kpglv/multimethods';

// create a multimethod that dispatches on the 'shape' field
// of its argument, implement methods for different dispatch
// values

const area = multi(x => x.shape)
      .when("square", x => Math.pow(x.side, 2));

// call the multimethod

const aSquare = { shape: "square", "side": 2 };
const aCircle = { shape: "circle", "radius": 5 };
const anOctagon = { shape: "octagon", "side": 12 };

console.log('square:', area(aSquare));
try {
  area(anOctagon);
} catch (e) {
  console.log(e.message, e.cause);
}
try {
  area(aCircle);
} catch (e) {
  console.log(e.message, e.cause);
}

// methods can be defined later

area.when('circle', x => Math.PI * Math.pow(x.radius, 2));
console.log('circle:', area(aCircle));
```

See and run examples in `test/examples.mjs`

```
node test/examples.mjs
```

## Testing

Run unit tests

```
yarn test
```

## TODO

- [ ] add more unit tests
