import { multi, throwNoMethod } from '@kpglv/multimethods/typed';

/** Types */

type OneDimensional = 'line' | 'curve';

type TwoDimensional = 'square' | 'circle' | 'octagon';

interface Shape<T> {
    shape: T;
}

interface Line extends Shape<'line'> {
    length: number;
}

interface Square extends Shape<'square'> {
    side: number;
}

interface Circle extends Shape<'circle'> {
    radius: number;
}

interface Octagon extends Shape<'octagon'> {
    side: number;
}

/** Usage examples */

// create a multimethod that computes shape area, provide it with a dispatch function,
// and define some methods

const area = multi<Shape<OneDimensional | TwoDimensional>, string, number>(
    ({ shape }) => shape
)
    .when('line', () => 0)
    // alas, the dispatch function does no type narrowing (yet)
    .when('square', x => Math.pow((x as Square).side, 2));
console.log("multimethod 'area':", area);
console.log('methods:', area.methods());

// create some shapes

const aLine: Line = { shape: 'line', length: 12 };
const aSquare: Square = { shape: 'square', side: 2 };
const aCircle: Circle = { shape: 'circle', radius: 5 };
const anOctagon: Octagon = { shape: 'octagon', side: 12 };

// and pass them to the multimethod to see what happens

console.log('area of a line:', area(aLine));
console.log('area of a square:', area(aSquare));
try {
    area(anOctagon);
} catch (e) {
    console.log(e.message);
}
try {
    area(aCircle);
} catch (e) {
    console.log(e.message);
}

// define (and redefine) some more methods and use them

area.when('circle', x => Math.PI * Math.pow((x as Circle).radius, 2));
area.when('circle', x => Math.PI * Math.pow((x as Circle).radius, 2));
console.log('area of a circle:', area(aCircle));

console.log('methods:', area.methods());

// remove some methods, then try to use them

area.remove('circle');
try {
    area(aCircle);
} catch (e) {
    console.log(e.message);
}

// set the default method and get it called

area.default(() => NaN);
console.log('area of a circle:', area(aCircle));

// advanced example: computing Fibonacci numbers

const fib = multi<number, number, number>()
    .when(0, () => 0)
    .when(1, () => 1)
    .default(n => fib(n - 1) + fib(n - 2));
console.log('fib(20):', fib(20));

area.default(throwNoMethod);
