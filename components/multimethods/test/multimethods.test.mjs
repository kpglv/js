import { multi } from '../src/multimethods.mjs';

const square = () => ({ shape: 'square', side: 2 });
const circle = () => ({ shape: 'circle', radius: 5 });
const octagon = () => ({ shape: 'octagon', side: 12 });

describe('multimethod', () => {
    it('throws if the specific method is absent and no default specified', () => {
        const area = multi(x => x.shape);
        expect(() => area(circle())).toThrow(/no method implementation/);
    });
    it('calls the specific method if present', () => {
        const area = multi(x => x.shape).when('square', shape =>
            Math.pow(shape.side, 2)
        );
        expect(area(square())).toBe(4);
    });
    it('calls the default method if present and specific method is absent', () => {
        const area = multi(x => x.shape).default(() => 'dunno');
        expect(area('foo')).toBe('dunno');
    });
});
