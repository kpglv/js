import { multi, throwNoMethod } from '@kpglv/multimethods';

const aSquare = { shape: 'square', side: 2 };
const aCircle = { shape: 'circle', radius: 5 };
const anOctagon = { shape: 'octagon', side: 12 };

const area = multi(x => x.shape).when('square', x => Math.pow(x.side, 2));
console.log('multimethod:', area);
console.log('methods:', area.methods());

console.log('square:', area(aSquare));
try {
    area(anOctagon);
} catch (e) {
    console.log(e.message);
}
try {
    area(aCircle);
} catch (e) {
    console.log(e.message);
}

area.when('circle', x => Math.PI * Math.pow(x.radius, 2));
area.when('circle', x => Math.PI * Math.pow(x.radius, 2));
console.log('circle:', area(aCircle));

console.log('methods:', area.methods());

area.remove('circle');
try {
    area(aCircle);
} catch (e) {
    console.log(e.message);
}

area.default(() => 'dunno');
console.log('circle:', area(aCircle));

const fib = multi()
    .when(0, () => 0)
    .when(1, () => 1)
    .default(n => fib(n - 1) + fib(n - 2));
console.log('fib(20):', fib(20));

area.default(throwNoMethod);
