import { multi } from '@kpglv/multimethods/typed/variadic';

/** Usage examples */

// create a multimethod and define some methods

const stringify = multi(
    (type: string, {}: number, {}: Record<string, unknown>) => type
)
    .when(
        'line',
        (_, id, properties) =>
            `>>> a line, id ${id}, ${
                Object.keys(properties).length
            } properties`
    )
    .when(
        'square',
        (_, id, properties) =>
            `>>> a square, id ${id}, ${
                Object.keys(properties).length
            } properties`
    );

// use the multimethod and see what happens

console.log(
    'stringify a line:',
    stringify('line', 1, { name: 'none', children: [], direction: 'west' })
);
console.log(
    'stringify a square:',
    stringify('square', 2, { foo: 3, bar: 'quux' })
);
try {
    console.log('stringify a circle:', stringify('circle', 3, {}));
} catch (e) {
    console.log('cannot stringify a circle:', e.message);
}

// define (and redefine) some more methods and use them

stringify.when(
    'circle',
    (_, id, properties) =>
        `>>> a circle again, id ${id}, ${
            Object.keys(properties).length
        } properties`
);
stringify.when(
    'circle',
    (_, id, properties) =>
        `>>> a circle once more, id ${id}, ${
            Object.keys(properties).length
        } properties`
);

console.log('stringify a circle:', stringify('circle', 3, {}));

// remove some methods, then try to use them

stringify.remove('circle');
try {
    console.log('stringify a circle:', stringify('circle', 3, {}));
} catch (e) {
    console.log('once more cannot stringify a circle:', e.message);
}

// set the default method and get it called

stringify.default(() => 'I dunno');
console.log(
    'stringify an octagon:',
    stringify('octagon', 4, {
        area: 128,
        color: 'blue',
        orientation: undefined,
        size: 'large',
    })
);

// advanced example: computing Fibonacci numbers

const identity = <A>(arg: A): A => arg;

const fib = multi<[number], number, number>(identity)
    .when(0, () => 0)
    .when(1, () => 1)
    .default(n => fib(n - 1) + fib(n - 2));
console.log('fib(20):', fib(20));
