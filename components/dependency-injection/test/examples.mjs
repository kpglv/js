import { init, halt, initKey, haltKey } from '@kpglv/dependency-injection';

// or load from file
const config = {
    version: '1.0',
    webserver: {
        host: 'localhost',
        port: 8000,
        db: '#ref database',
        logger: '#ref logger',
    },
    database: {
        type: 'sqlite',
        name: 'example.db',
        logger: '#ref logger',
    },
    logger: {
        to: 'stdout',
    },
    metrics: {
        targets: {
            log: ['#ref logger', 'syslog'],
            webhook: 'https://metrics.local/collect',
        },
        web: '#ref webserver',
        db: '#ref database',
    },
};

const invalidConfig = {
    webserver: {
        logger: '#ref logger',
    },
};

console.log('config:', config);

// components

class WebServer {}
class Database {}
const getLogger = config => x => console.log(x);
const createMetrics = ({ web, db, targets }) => {
    return { m: [web, db, targets] };
};

// component initializations; will throw if there is a component without
// an initializer

initKey.when('database', (key, value) => {
    value.logger(`>>> connected to database: ${value.name}`);
    return new Database();
});

initKey.when('webserver', (key, value) => {
    value.logger(`>>> started web server on port ${value.port}`);
    return new WebServer();
});

initKey.when('logger', (key, value) => {
    console.log(`>>> initialized logging to ${value.to}`);
    return getLogger();
});

initKey.when('metrics', (key, value) => {
    console.log('>>> initialized metrics collector');
    return createMetrics(value);
});

initKey.when('version', (key, value) => value);

// component deinitializations; not always necessary

haltKey.when('database', (key, value) =>
    console.log('<<< disconnecting from database')
);
haltKey.when('webserver', (key, value) =>
    console.log('<<< stopping web server')
);
haltKey.when('logger', (key, value) => console.log('<<< stopping logger'));

const system = init(config);
console.log('*** system initialized:', system);

halt(system);
console.log('*** system halted');

try {
    const system = init(invalidConfig);
    console.log('*** system initialized:', system);

    halt(system);
    console.log('*** system halted');
} catch (e) {
    console.log('system init failed:', e.message);
}

const invalidConfig1 = {
    version: '1.0',
    nope: 123,
};
try {
    const system = init(invalidConfig1);
    console.log('*** system initialized:', system);

    halt(system);
    console.log('*** system halted');
} catch (e) {
    console.log('system init failed:', e.message);
}
