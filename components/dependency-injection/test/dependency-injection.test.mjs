import {
    init,
    halt,
    initKey,
    haltKey,
} from '../../dependency-injection/src/dependency-injection.mjs';

const config = {
    version: '1.0',
    webserver: {
        host: 'localhost',
        port: 8000,
        db: '#ref database',
        logger: '#ref logger',
    },
    database: {
        type: 'sqlite',
        name: 'example.db',
        logger: '#ref logger',
    },
    logger: {
        to: 'stdout',
    },
    metrics: {
        targets: {
            log: ['#ref logger', 'syslog'],
            webhook: 'https://metrics.local/collect',
        },
        web: '#ref webserver',
        db: '#ref database',
    },
};

const configWithCycles = {
    webserver: {
        db: '#ref database',
    },
    database: '#ref webserver',
};

const configWithoutKeyInitializer = {
    nope: 'foo',
};

class WebServer {}
class Database {}
const getLogger = config => x => console.log(x);
const createMetrics = ({ web, db, targets }) => {
    return { m: [web, db, targets] };
};

initKey.when('database', (key, value) => {
    value.logger(`>>> connected to database: ${value.name}`);
    return new Database();
});
initKey.when('webserver', (key, value) => {
    value.logger(`>>> started web server on port ${value.port}`);
    return new WebServer();
});
initKey.when('logger', (key, value) => {
    console.log(`>>> initialized logging to ${value.to}`);
    return getLogger();
});
initKey.when('metrics', (key, value) => {
    console.log('>>> initialized metrics collector');
    return createMetrics(value);
});
initKey.when('version', (key, value) => value);

haltKey.when('database', (key, value) =>
    console.log('<<< disconnecting from database')
);
haltKey.when('webserver', (key, value) =>
    console.log('<<< stopping web server')
);
haltKey.when('logger', (key, value) => console.log('<<< stopping logger'));

describe('DI container', () => {
    it('can init and halt a system with a valid config', () => {
        const system = init(config);
        expect(system.components.version).toBe('1.0');
        halt(system);
    });
    it('init throws on a config with cycles', () => {
        expect(() => init(configWithCycles)).toThrow(/graph has cycles/);
    });
    it('init throws on a config with components without initializers', () => {
        expect(() => init(configWithoutKeyInitializer)).toThrow(/no method/);
    });
});
