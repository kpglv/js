# dependency-injection

A simple dependency injection container, inspired by [Integrant](https://github.com/weavejester/integrant).

Builds a working system from a declarative description (a configuration map;
e.g., loaded from JSON file or received over network).

## Overview

A **system** is a set of **components** and a set of pairwise dependency
relationships between them, and can be represented by a directed acyclic graph
(DAG) of components (as nodes) and dependency relationships (as edges).
Structure of the system is described declaratively in **system
configuration**.

In a system, a component may depend on an arbitrary value (**component
configuration**), that may include references to other components; component's
configuration is provided to it at the initialization time (with the
references replaced with initialized components they refer to).

A system configuration is represented by a **configuration map** (JavaScript
object), whose top-level keys are the component names and their values are the
component configurations, that may be arbitrary values or data structures
(usually objects); a reference to other component is represented by a string
that starts with `#ref` prefix and ends with a component name, separated from
the prefix by whitespace (e.g., `#ref database`).

An initialized, working system is represented by a **system map**, whose
top-level key-value pairs represent component names and their initialized
values.

> Note: a system map should normally be used only to stop the system; there is
> generally no need to search components in a system map (except for testing
> and debugging purposes) --- if some component needs access to another
> component, it should declare it as a dependency and receive it at
> initialization time.

## Getting Started

### Usage Example

```js
import { init, halt, initKey, haltKey } from '@kpglv/dependency-injection';

// or load from file, get over network, etc
const config = {
    version: '1.0',
    webserver: {
        host: 'localhost',
        port: 8000,
        db: '#ref database',
        logger: '#ref logger',
    },
    database: {
        type: 'sqlite',
        name: 'example.db',
        logger: '#ref logger',
    },
    logger: {
        to: 'stdout',
    },
};

// components

class WebServer {}
class Database {}
const getLogger = config => x => console.log(x);

// component initializers; init will throw if there is a component without
// an initializer

initKey.when('database', (key, value) => {
    value.logger(`>>> connected to database: ${value.name}`);
    return new Database();
});
initKey.when('webserver', (key, value) => {
    value.logger(`>>> started web server on port ${value.port}`);
    return new WebServer();
});
initKey.when('logger', (key, value) => {
    console.log(`>>> initialized logging to ${value.to}`);
    return getLogger();
});
initKey.when('version', (key, value) => value);

// component deinitializers; not always necessary

haltKey.when('database', (key, value) =>
    console.log('<<< disconnecting from database')
);
haltKey.when('webserver', (key, value) =>
    console.log('<<< stopping web server')
);
haltKey.when('logger', (key, value) => console.log('<<< stopping logger'));

// init and halt the system

const system = init(config);
console.log('*** system initialized:', system);

halt(system);
console.log('*** system halted');
```

See and run examples in `test/examples.mjs`; see also `guides/hello-di` for
usage in an example application.

```
node test/examples.mjs
```

## Testing

Run unit tests

```
yarn test
```

## TODO

-   [ ] add more unit tests
-   [ ] support async functions (initializers/deinitializers)?
-   [ ] add system/component config validation (spec?)
-   [ ] add types (?)
-   [ ] add error handling (e.g., dealing with partially initialized systems due
        to an error)
-   [ ] support initializing/halting a subset of the components (accept an array
        of component names)
