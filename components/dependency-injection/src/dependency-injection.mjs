import { multi, noop } from '@kpglv/multimethods';
import { graph, depend, nodes, topologicalSort } from '@kpglv/dependency-graph';
import { postwalk } from '@kpglv/tree-walk';

/**
 * Regular expression to match a reference string.
 */
export const refRegex = /^#ref\s+(\S*)$/;

/**
 * Returns true if the value is a reference string.
 */
export const isRef = value =>
    typeof value === 'string' && refRegex.test(value);

/**
 * Returns name of the component referred to by the reference string.
 */
export const resolveRef = ref => {
    const [_, componentName] = refRegex.exec(ref);
    return componentName;
};

/**
 * Returns an array of component names referred to by any references that can
 * be found in the component configuration.
 */
export const findRefs = componentConfig => {
    const refs = [];
    postwalk(x => isRef(x) && refs.push(resolveRef(x)), componentConfig);
    return refs;
};

/**
 * Returns the component configuration with all references found in it
 * replaced by initialized components they refer to, retrieved from the
 * system.
 */
export const replaceRefs = (componentConfig, system) => {
    return postwalk(x => (isRef(x) ? system.components[resolveRef(x)] : x),
                    componentConfig );
};

/**
 * A component initializer multimethod. Accepts component name and
 * configuration, dispatches on the component name. Throws if the method for
 * the dispatch value is not defined.
 */
export const initKey = multi(
    (componentName, componentConfig) => componentName
);

/**
 * A component deinitializer multimethod. Accepts component name and
 * initialized instance, dispatches on the component name. Does nothing if the
 * method for the dispatch value is not defined.
 */
export const haltKey = multi(
    (componentName, componentConfig) => componentName
).default(noop);

/**
 * Returns an initialized system, composed from components initialized using
 * user-defined initializers, in dependency order (dependencies before
 * dependants).
 */
export const init = (config, options) => {
    // a graph that represents dependencies between components (represented by
    // their names)
    const dependencyGraph = graph();

    // produce a set of all component names in the config
    const componentNames = new Set(Object.keys(config));

    // for each component name, find all references to other components in its
    // configuration, resolve the references to component names and add the
    // names to the dependency graph as dependencies of the component
    componentNames.forEach(componentName => {
        const componentConfiguration = config[componentName];
        const componentDepNames = new Set(findRefs(componentConfiguration));
        componentDepNames.forEach(depName => {
            // reference to a nonexistent component is an error
            if (!componentNames.has(depName)) {
                const names = JSON.stringify([...componentNames]);
                throw new Error(
                    `no such component: "${depName}" in ${names}`
                );
            }
            depend(dependencyGraph, componentName, depName);
        });
    });
    console.log('graph:', dependencyGraph);
    // determine the names of system components that are not in the dependency
    // graph (seems like components on which no one depends are useless; maybe
    // we should throw an error? or maybe it can be useful for
    // introspection/debugging?)
    const dependencyGraphNodes = nodes(dependencyGraph);
    const independentComponents = [...componentNames].filter(
        componentName => !dependencyGraphNodes.has(componentName)
    );

    // sort the dependency graph topologically to determine the order in which
    // to init and halt components, and add the independent components to the
    // result; it doesn't matter if independent components are added before or
    // after interdependent ones
    const haltSequence = [
        ...independentComponents,
        ...topologicalSort(dependencyGraph),
    ];
    const initSequence = haltSequence.toReversed();

    // create an empty system, add init and halt sequences to it
    const system = { components: {}, initSequence, haltSequence };

    // traverse the init sequence, initializing each component with its
    // configuration (where dependency refs are replaced with already
    // initialized components) and putting it in the system map
    system.initSequence.forEach(componentName => {
        const componentConfig = replaceRefs(config[componentName], system);
        const component = initKey(componentName, componentConfig);
        system.components[componentName] = component;
    });

    // run a lifecycle callback
    if (options && options.onInit) {
        options.onInit();
    }
    return system;
};

/**
 * Halts the system, using user-defined deinitializers for its components, in
 * reverse dependency order (dependants before dependencies).
 */
export const halt = (system, options) => {
    system.haltSequence.forEach(componentName => {
        const component = system.components[componentName];
        haltKey(componentName, component);
    });

    // run a lifecycle callback
    if (options && options.onHalt) {
        options.onHalt();
    }
};
