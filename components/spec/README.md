# spec

A **spec** is a predicate-based data specification that can be used
to describe, validate, coerce and generate this kind of data.

Inspired by `clojure.spec`.

-   http://clojure.org/about/spec
-   https://clojure.org/guides/spec

> Note: this component's terminology and API is likely to differ
> from `clojure.spec`'s.

Existing implementations

-   https://github.com/clojure/spec.alpha
-   https://github.com/clojure/spec-alpha2
-   https://github.com/prayerslayer/js.spec

> Note: this component is a work in progress.

## Overview

The primary use of a spec is to **conform** some piece of data ---
transform it according to constraints and rules contained in the spec
(defined by its **conformer function**).
The result is either a conformant value or special symbol `invalid`,
that indicates that the value cannot be conformed (e.g., an error occurs
during transformation). (The opposite operation is **unform**, that
transforms a conformant value back to the original one and is defined
by an **unformer function**.)

Specs can be atomic or composite. Atomic specs are created from
regular functions:

-   _predicate_ (`any => boolean`): the spec's conformer function returns
    the original value as is if the value satisfies the predicate, or
    `invalid` otherwise; predicates are converted to specs automatically
    if needed
-   _conformer_ (`original => conformed | 'invalid'`) and optionally
    _unformer_ (`conformed => original | 'invalid'`) that contain custom
    conforming/unforming logic

Composite specs are composed from other specs, whose conformer functions
are used in the conformer function of the composite spec, in different ways.
(This component provides constructors for basic kinds of composite
specs like `and`, `or`, `keys` etc.)

Specs are usually bound to variables in some module, and derive their names
from these variables; the name captures the semantics of the spec.

Spec API:

-   `constructor(...args)`: create a spec from functions or other specs:
    -   `predicate`
    -   `conformer`
    -   `and`
    -   `or`
    -   `keys`
-   `conform(spec, value)`: conform value with spec
-   `unform(spec, value)`: unform (undo the conform) value with spec
-   `valid(spec, value)`: validate value with spec, return true/false
-   `explain(spec, value)`: return a data structure with explanation why
    the value violates the spec, or null if the value conforms with spec

## Getting Started

### Usage Examples

See `test/examples.mjs`.
