import { invalid } from './symbols.mjs';

// users can and should write their own predicates

// spec-related predicates

/**
 * Returns true if the value is a spec.
 * @param {*} value
 */
export const isSpec = value =>
    isObject(value) && Object.hasOwn(value, 'kind');

/**
 * Returns true if the value is a symbol that indicates that value
 * cannot be conformed with a spec.
 * @param {*} value
 */
export const isInvalid = value => value === invalid;

// regular predicates

/**
 * Returns true if the value is nil (null or undefined).
 * @param {*} value
 */
export const isNil = value => value == null;

/**
 * Returns true if the value is a boolean.
 * @param {*} value
 */
export const isBoolean = value => value === true || value === false;

/**
 * Returns true if the value is a number.
 * @param {*} value
 */
export const isNumber = value => typeof value === 'number';

/**
 * Returns true if the value is a string.
 * @param {*} value
 */
export const isString = value => typeof value === 'string';

/**
 * Returns true if the value is a symbol.
 * @param {*} value
 */
export const isSymbol = value => typeof value === 'symbol';

/**
 * Returns true if the value is a function.
 * @param {*} value
 */
export const isFunction = value => typeof value === 'function';

/**
 * Returns true if the value is an object (including functions).
 * @param {*} value
 */
export const isObject = value =>
    !!value && (typeof value === 'object' || isFunction(value));

/**
 * Returns true if the value is an array.
 * @param {*} value
 */
export const isArray = value => Array.isArray(value);

/**
 * Returns true if the value is a map.
 * @param {*} value
 */
export const isMap = value => value instanceof Map;

/**
 * Returns true if the value is a set.
 * @param {*} value
 */
export const isSet = value => value instanceof Set;
