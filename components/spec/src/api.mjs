import { multi } from '@kpglv/multimethods';
import { invalid } from './symbols.mjs';
import { isFunction, isInvalid, isSpec } from './predicates.mjs';
import { ensureSpec } from './specs.mjs';

export { and, or, conformer } from './specs.mjs';

/**
 * A dispatch function for multimethods that accept specs as their
 * first arguments.
 * Returns the kind of a spec, or null if the value is not a spec.
 * @param {*} maybeSpec
 */
const dispatchOnSpecKind = maybeSpec =>
    isSpec(maybeSpec)
        ? maybeSpec.kind
        : isFunction(maybeSpec)
        ? 'predicate'
        : null;

/**
 * A method for multimethods that accept specs as their first arguments.
 * Throws an error indicating that the value is not a spec.
 * @param {*} spec
 */
const throwNotASpec = spec => {
    throw new Error(`'${spec}' is not a spec`);
};

// public API

/**
 * Conforms the value with the spec.
 * Returns the conformed value or symbol 'invalid'.
 * @param {*} spec
 * @param {*} value
 */
export const conform = multi(dispatchOnSpecKind)
    .when(null, throwNotASpec)
    .when('predicate', (spec, value) => {
        try {
            const conforms = ensureSpec(spec).predicate(value);
            if (conforms) {
                return value;
            } else {
                return invalid;
            }
        } catch (e) {
            return invalid;
        }
    })
    .when('conformer', (spec, value) => {
        try {
            return spec.conformer(value);
        } catch (e) {
            return invalid;
        }
    })
    .when('and', (spec, value) => {
        const conformedValues = spec.specs.map(s => conform(s, value));
        if (conformedValues.some(isInvalid)) {
            return invalid;
        } else {
            return conformedValues.length ? conformedValues.at(-1) : null;
        }
    })
    .when('or', (spec, value) => {
        const subspecs = spec.specs;
        for (const label in subspecs) {
            const conformed = conform(subspecs[label], value);
            if (isInvalid(conformed)) continue;
            return [label, conformed];
        }
        return invalid;
    })
    .when('keys', (spec, value) => {});

/**
 * Unforms the (previously conformed) value with the spec.
 * Returns the unformed value of symbol 'invalid'.
 * @param {*} spec
 * @param {*} value
 */
export const unform = multi(dispatchOnSpecKind)
    .when(null, throwNotASpec)
    .when('conformer', (spec, value) =>
        spec.unformer ? spec.unformer(value) : value
    )
    // TODO: implement unforming for different kinds of specs
    .default((_, value) => value);

/**
 * Returns true if the value conforms with the spec.
 * @param {*} spec
 * @param {*} value
 */
export const valid = (spec, value) => conform(spec, value) !== invalid;

/**
 * Returns a data structure explaining why the value does not conform
 * with the spec, or null if the value conforms with the spec.
 * @param {*} spec
 * @param {*} value
 */
export const explain = multi(dispatchOnSpecKind)
    .when(null, throwNotASpec)
    // TODO: implement explanation logic
    .default((spec, value) => ({
        spec: ensureSpec(spec),
        value,
    }));
