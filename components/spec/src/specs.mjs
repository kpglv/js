import { isSpec, isFunction } from './predicates.mjs';
import { isOdd, into, last, partition } from '../../../dev/core.mjs';

// users can, but generally shouldn't write their own spec kinds;
// custom conformer functions should probably suffice

/**
 * Returns a spec created from the given value, if it is
 * a predicate (a function).
 * If the value is already a spec, returns it as is.
 * Throws if the value is not a predicate or spec.
 * @param {*} predicateOrSpec
 */
export const ensureSpec = predicateOrSpec => {
    if (isSpec(predicateOrSpec)) {
        return predicateOrSpec;
    } else {
        return predicate(predicateOrSpec);
    }
};

/**
 * Returns a predicate spec.
 * When conformed (unformed) with, the value is returned as is,
 * if the pred is satisfied, or 'invalid' otherwise.
 * @param {function} pred
 */
export const predicate = pred => {
    if (!isFunction(pred)) {
        throw new Error(`predicate is not a function: '${pred}'`);
    }
    return {
        kind: 'predicate',
        predicate: pred,
    };
};

/**
 * Returns a conformer spec, that describes a procedure to coerce a value
 * to a conformant one. Can be preceded (using 'and') with another spec
 * to ensure that the original value meets the conformer's requirements.
 * When conformed (unformed) with, returns the value transformed with the
 * provided conformer (unformer), or 'invalid' if an error occurs.
 * @param {*} conf conformer
 * @param {*} unf unformer
 */
export const conformer = (conf, unf) => {
    if (conf == null) {
        throw new Error('conformer function is not supplied');
    }
    return {
        kind: 'conformer',
        conformer: conf,
        unformer: unf,
    };
};

/**
 * Returns an 'and' spec, that describes a value conformant with all
 * provided specs.
 * When conformed (unformed) with, conforms (unforms) the value with all
 * given specs in order and, if all conforms were successful, returns
 * the result of the last one, or 'invalid' otherwise.
 * @param {...any} specs
 */
export const and = (...specs) => ({
    kind: 'and',
    specs: specs.map(ensureSpec),
});

/**
 * Returns an 'or' spec, that describes a tagged union.
 * When conformed (unformed) with, returns the result of the first successful
 * conform (unform) tagged with its label (a two-element array), or 'invalid'
 * if none were successful.
 * @param {any} clauses
 */
export const or = clauses => {
    const specs = Object.keys(clauses).reduce(
        (acc, val) => ({
            ...acc,
            [val]: ensureSpec(clauses[val]),
        }),
        {}
    );
    return {
        kind: 'or',
        specs,
    };
};

/**
 * Returns a 'keys' spec, that describes an associative data structure.
 * When conformed (unformed) with, the value is returned as is, if it
 * contains all keys with specified names.
 * @param {string[]} names
 */
export const keys = names => ({
    kind: 'keys',
    names,
});
