/**
 * Indicates that a value does not conform to a spec.
 */
export const invalid = Symbol('invalid');
