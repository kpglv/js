import { isNumber, isString } from '@kpglv/spec/predicates';
import * as spec from '@kpglv/spec';

const isEven = x => x % 2 === 0;
const isSmall = x => x < 100;

// usage

// invalid specs

try {
    console.log('conforming 11 to a string "foo":', spec.conform('foo', 11));
} catch (e) {
    console.log('error:', e.message);
}

// predicate specs

console.log('conforming 11 with isNumber:', spec.conform(isNumber, 11));
console.log('conforming 11 with isString:', spec.conform(isString, 11));
console.log(
    'explaining why 11 fails to conform with isString: %o',
    spec.explain(isString, 11)
);

// 'and' specs

const EvenNumber = spec.and(isNumber, isEven);
console.log('creating an "and" spec:', EvenNumber);
console.log('conforming 11 with EvenNumber:', spec.conform(EvenNumber, 11));
console.log(
    'explaining why 11 fails to conform with EvenNumber: %o',
    spec.explain(EvenNumber, 11)
);

// 'or' specs

const StringOrEvenNumber = spec.or({ str: isString, evenNum: EvenNumber });
console.log('creating an "or" spec: %o', StringOrEvenNumber);
console.log(
    'conforming 12 with StringOrEvenNumber:',
    spec.conform(StringOrEvenNumber, 12)
);
console.log(
    "conforming 'foo' with StringOrEvenNumber:",
    spec.conform(StringOrEvenNumber, 'foo')
);
console.log(
    'conforming 11 with StringOrEvenNumber:',
    spec.conform(StringOrEvenNumber, 11)
);
console.log(
    'explaining why 11 fails to conform with StringOrEvenNumber: %o',
    spec.explain(StringOrEvenNumber, 11)
);

// conformer specs

const stringToNumber = string => parseFloat(string);
const numberToString = number => number.toString();
const CoercedNumber = spec.conformer(stringToNumber, numberToString);
console.log('creating a conformer spec:', CoercedNumber);

const conformedNum = spec.conform(CoercedNumber, '-123.4');
const unformedNum = spec.unform(CoercedNumber, conformedNum);
console.log(
    'conforming "-123.4" to a number and unforming the result back:',
    [conformedNum, typeof conformedNum],
    [unformedNum, typeof unformedNum]
);
