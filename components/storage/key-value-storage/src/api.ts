import { JSONSerializableValue } from '@kpglv/core';
import { Datasource } from '@kpglv/key-value-datasource';

/**
 * A storage of
 */
export interface KeyValueStorage<T extends Datasource> {
    datasource: T;
}
