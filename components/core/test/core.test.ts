import { it, describe, expect } from 'vitest';

import fc from 'fast-check';
import { isObject } from '../src/predicates';

describe('isObject', () => {
    it('always returns true for objects', () => {
        fc.assert(
            fc.property(fc.object(), obj => {
                expect(isObject(obj)).toBe(true);
            })
        );
    });
    it('never returns true for non-objects', () => {
        fc.assert(
            fc.property(
                fc.oneof(
                    fc.boolean(),
                    fc.nat(),
                    fc.float(),
                    fc.string(),
                    fc.falsy()
                ),
                val => {
                    expect(isObject(val)).toBe(false);
                }
            )
        );
    });
});
