export const noop = <A extends unknown[]>(..._: A): void => {};

export const identity = <A>(x: A): A => x;
