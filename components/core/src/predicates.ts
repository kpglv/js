export const isArray = Array.isArray;
export const isSet = <T>(x: unknown): x is Set<T> => x instanceof Set;
export const isMap = <K, V>(x: unknown): x is Map<K, V> => x instanceof Map;
export const isObject = (x: unknown): x is Record<string, unknown> =>
    typeof x === 'object' && !!x;
