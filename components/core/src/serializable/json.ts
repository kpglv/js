/**
 * A value that can be serialized to a JSON string
 * (and deserialized back).
 */
export type JSONSerializableValue =
    | null
    | boolean
    | number
    | string
    | { toJSON: (value: unknown) => string }
    | { [key: number]: JSONSerializableValue }
    | { [key: string]: JSONSerializableValue };
