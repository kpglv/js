import React from "react";

interface ButtonProps {
  className?: string;
  children?: React.ReactNode;
  onClick?: (event: React.MouseEvent<HTMLElement>) => void;
  disabled?: boolean;
}

const Button = ({ className = "", disabled, onClick, children, ...rest }: ButtonProps) => {
  return (
    <button
      className={className}
      disabled={disabled}
      onClick={onClick}
      {...rest}>

      {children}
    </button>
  );
};

export default Button;
