import React from 'react';

export interface LayoutProps {
    className?: string;
    header: React.ReactNode;
    drawer: React.ReactNode;
    content: React.ReactNode;
    footer: React.ReactNode;
}

const layoutClasses = [
    // full-width header, fixed-width side drawer,
    // flexible-width main content, full-width footer
    "grid grid-cols-1 auto-rows-max",
    "md:grid-cols-[256px,_1fr]",
];

const Layout = ({ header, drawer, content, footer, className }: LayoutProps) => {
    const classes = className ? layoutClasses.concat(className) : layoutClasses;
    return (
        <div className={classes.join(" ")}>
            {header}
            {drawer}
            {content}
            {footer}
        </div>
    );
};

export default Layout;