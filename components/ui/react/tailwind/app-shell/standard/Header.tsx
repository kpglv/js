import React from 'react';

export interface HeaderProps {
    start?: React.ReactNode;
    center?: React.ReactNode;
    end?: React.ReactNode;
}

const headerClasses =
    [
        "fixed col-span-2",
        "flex items-center justify-between align-center",
        "h-16 w-full px-4",
        "backdrop-blur-sm bg-black/20 dark:bg-slate-900/80",
        "text-xl font-bold tracking-wide",
    ].join(" ");

const Header = ({ start, center, end }: HeaderProps) => {
    return (
        <header className={headerClasses}>
            {start}
            {center}
            {end}
        </header>
    );
};

export default Header;