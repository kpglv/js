import React from 'react';

export interface DrawerProps {
    children: React.ReactNode;
}

const drawerClasses =
    [
        "mt-16",
        "hidden md:flex md:flex-col md:items-start",
        "border-r border-slate-500/50 dark:border-slate-300/30"
    ].join(" ");


const Drawer = ({ children }: DrawerProps) => {
    return (
        <aside className={drawerClasses}>
            {children}
        </aside>
    );
};

export default Drawer;