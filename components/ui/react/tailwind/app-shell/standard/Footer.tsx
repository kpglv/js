import React from 'react';

export interface FooterProps {
    start?: React.ReactNode;
    center?: React.ReactNode;
    end?: React.ReactNode;
}

const footerClasses = [
    "col-span-2",
    "flex items-center justify-start align-center",
    "h-8 w-full px-4",
    "border-t border-slate-500/50 dark:border-slate-300/30"
].join(" ");

const Footer = ({ start, center, end }: FooterProps) => {
    return (
        <footer className={footerClasses}>
            {start}
            {center}
            {end}
        </footer>
    );
};

export default Footer;