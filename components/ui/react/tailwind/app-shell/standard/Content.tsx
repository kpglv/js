import React from 'react';

export interface ContentProps {
    children: React.ReactNode;
}

const contentClasses =
    [
        "mt-16",
        // responsive grid (4 to 12 columns)
        "grid auto-rows-auto",
        "grid-cols-4 md:grid-cols-8 lg:grid-cols-12",
        "min-h-screen",
    ].join(" ");

const Content = ({ children }: ContentProps) => {
    return (
        <main className={contentClasses}>
            {children}
        </main>
    );
};

export default Content;