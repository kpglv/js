import React from "react";

interface ListProps {
    className?: string;
    children?: React.ReactNode;
    direction?: "row" | "column";
}

const defaultClasses = "flex";

const List = ({
    className,
    children,
    direction = "row",
    ...rest }: ListProps) => {

    const classes = [defaultClasses, className].join(" ");
    return (
        <div className={classes}
            {...rest}>
            {children}
        </div >
    );
}

export default List;