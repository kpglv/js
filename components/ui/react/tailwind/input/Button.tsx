import React from "react";
import UnstyledButton from '../../unstyled/input/Button';

interface ButtonProps {
  className?: string;
  children?: React.ReactNode;
  onClick?: (event: React.MouseEvent<HTMLElement>) => void;
  disabled?: boolean;
}

const commonClasses =
  "px-8 py-4 rounded-md font-semibold uppercase " +
  "flex items-center justify-center " +
  "transition ";

const enabledClasses =
  // light theme
  "bg-primary-300 text-neutral-800 " +
  "hover:bg-primary-600 active:bg-primary-700 " +
  // dark theme
  "dark:bg-primary-700 dark:text-neutral-100 " +
  "dark:hover:bg-primary-800 dark:active:bg-primary-900 ";

const disabledClasses = "bg-neutral-700 text-neutral-400 ";

const Button = ({ className = "", disabled, onClick, children, ...rest }: ButtonProps) => {

  const classes = commonClasses +
    (disabled ? disabledClasses : enabledClasses);

  return (
    <UnstyledButton
      className={classes + className}
      onClick={onClick}
      disabled={disabled}
      {...rest}
    >
      {children}
    </UnstyledButton>
  );
};

export default Button;
