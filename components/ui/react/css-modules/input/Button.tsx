import React from "react";
import UnstyledButton from '../../unstyled/input/Button';
import styles from './Button.module.css';

interface ButtonProps {
  className?: string;
  children?: React.ReactNode;
  onClick?: (event: React.MouseEvent<HTMLElement>) => void;
  disabled?: boolean;
}

const commonClasses = styles.default;

const disabledClasses = styles.disabled;

const Button = ({ className = "", disabled, onClick, children, ...rest }: ButtonProps) => {

  const classes = commonClasses + " " +
    (disabled ? disabledClasses : "");

  return (
    <UnstyledButton
      className={classes + className}
      onClick={onClick}
      disabled={disabled}
      {...rest}
    >
      {children}
    </UnstyledButton>
  );
};

export default Button;
