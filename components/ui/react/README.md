# react

A small library of React components.

Several kinds of components are provided:

- `unstyled`: without any visual styles applied
- `tailwind`: styled with Tailwind CSS
- `css-modules`: styled with CSS Modules

>Note: this is a work in progress.

## Getting Started

Include path to components in the base/project TypeScript configuration
(`tsconfig.json`):

```json
{
    "compilerOptions": {
        ... ,
        "paths": {
            ... ,
            "@recom-unstyled/*": ["../../components/ui/react/unstyled/*"],
            "@recom-css-modules/*": ["../../components/ui/react/css-modules/*"],
            "@recom-tailwind/*": ["../../components/ui/react/tailwind/*"]
        }
    },
    ... ,
}
```

Import a component

```js
import Button from '@react-unstyled/input/Button';
```

Use the component

```js
<Button onClick={() => alert("Hello, components!")}>
    Hello, components!
</Button>
```

### CSS Modules

Ensure that the base/project that uses the components is configured for CSS
Modules.

### Tailwind CSS

Add the components to the Tailwind CSS configuration (`tailwind.config.ts`) of
the base/project:

```js
const config: Config = {
  content: [
    ...,
    '../../components/ui/react/tailwind/**/*.{ts,tsx}',
  ],
  ...,
}
```
