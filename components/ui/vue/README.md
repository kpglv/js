# vue

A small library of Vue components.

Several kinds of components are provided:

- `unstyled`: without any visual styles applied
- `tailwind`: styled with Tailwind CSS

>Note: this is a work in progress.

## Getting Started

Include path to components in the base/project TypeScript configuration
(`tsconfig.json`):

```json
{
    "compilerOptions": {
        ... ,
        "paths": {
            ... ,
            "@vuecom-unstyled/*": ["../../components/ui/vue/unstyled/*"],
            "@vuecom-tailwind/*": ["../../components/ui/vue/tailwind/*"]
        }
    },
    ... ,
}
```

Import a component

```js
import MyButton from '@react-unstyled/input/Button';
```

Use the component

```js
<my-button @click="alert('Hello, components!')">
    Hello, components!
</my-button>
```

### CSS Modules

Ensure that the base/project that uses the components is configured for CSS
Modules.

### Tailwind CSS

Add the components to the Tailwind CSS configuration (`tailwind.config.ts`) of
the base/project:

```js
const config: Config = {
  content: [
    ...,
    '../../components/ui/vue/tailwind/**/*.vue',
  ],
  ...,
}
```
