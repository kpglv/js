# key-value-datasource

A component that provides

-   an interface to a key-value datasource, allowing
    -   connect to a datasource, providing options (type, key prefix, ...)
    -   use the connection to
        -   get/insert/update/remove values under given keys
        -   introspect the datasource (get total number of stored keys, check
            if a value is present under some key, ...)
    -   close the connection (and release resources used by it)
-   several implementations of it (backed by in-memory map, localStorage,
    sessionStorage, IndexedDB, ...)

> Note: this component is currently a work in progress.

## Getting Started

TODO

## Testing

Run unit tests

```
npm run test
```
