import { noop } from '@kpglv/core';
import {
    create,
    connect,
    close,
    get,
    insert,
    remove,
    has,
    Key,
    Value,
} from '../api';

/** Re-exporting the public API */

export { create, connect, close, get, insert, remove, has };

/** Implementation */

export const datasourceType = 'in-memory-map';

create.when(datasourceType, options => ({
    options,
    data: new Map<Key, Value>(),
}));

connect.when(datasourceType, datasource => ({ datasource }));

close.when(datasourceType, connection => {
    connection.datasource.data = null;
});

get.when(
    datasourceType,
    (
        {
            datasource: {
                options: { namespace },
                data,
            },
        },
        key
    ) => (data as Map<Key, Value>).get(getNamespacedKey(key, namespace))
);

insert.when(
    datasourceType,
    (
        {
            datasource: {
                options: { namespace },
                data,
            },
        },
        key,
        value
    ) => {
        (data as Map<Key, Value>).set(
            getNamespacedKey(key, namespace),
            value
        );
    }
);

remove.when(
    datasourceType,
    (
        {
            datasource: {
                options: { namespace },
                data,
            },
        },
        key
    ) => {
        (data as Map<Key, Value>).delete(getNamespacedKey(key, namespace));
    }
);

has.when(
    datasourceType,
    (
        {
            datasource: {
                options: { namespace },
                data,
            },
        },
        key
    ) => (data as Map<Key, Value>).has(getNamespacedKey(key, namespace))
);

/**
 * Returns the key prefixed with the namespace.
 * If namespace is not specified, the key is returned.
 */
const getNamespacedKey = (key: Key, namespace?: string): Key =>
    namespace ? `${namespace}-${key}` : key;
