import { JSONSerializableValue } from '@kpglv/core';
import { multi } from '@kpglv/multimethods/typed/variadic';

/** Public types */

/**
 * A connection to a source of data.
 */
export interface Connection {
    readonly datasource: Datasource;
}

/**
 * A source of data.
 */
export interface Datasource {
    readonly options: DatasourceOptions;
    [key: string]: unknown;
}

/**
 * Options of a datasource.
 */
export interface DatasourceOptions {
    readonly type: DatasourceType;
    readonly namespace?: string;
}

/**
 * Type of a source of data.
 */
export type DatasourceType = string;

/**
 * Type of keys supported by sources of data.
 */
export type Key = string;

/**
 * Type of values supported by sources of data.
 */
export type Value = JSONSerializableValue;

/** Public API */

/**
 * Creates and returns a new datasource with given options.
 */
export const create = multi<[DatasourceOptions], DatasourceType, Datasource>(
    ({ type }: DatasourceOptions) => type
);

/**
 * Creates and returns a new connection to the datasource.
 */
export const connect = multi<[Datasource], DatasourceType, Connection>(
    ({ options: { type } }: Datasource) => type
);

/**
 * Closes the connection and releases the resources used by it.
 */
export const close = multi<[Connection], DatasourceType, void>(
    ({
        datasource: {
            options: { type },
        },
    }: Connection) => type
);

/**
 * Returns a value stored under given key in the connected datasource.
 */
export const get = multi<
    [Connection, Key],
    DatasourceType,
    Value | undefined
>(
    ({
        datasource: {
            options: { type },
        },
    }: Connection) => type
);

/**
 * Inserts (or replaces) a value under given key in the datasource.
 */
export const insert = multi<[Connection, Key, Value], DatasourceType, void>(
    ({
        datasource: {
            options: { type },
        },
    }: Connection) => type
);

/**
 * Removes a value under given key in the datasource.
 */
export const remove = multi<[Connection, Key], DatasourceType, void>(
    ({
        datasource: {
            options: { type },
        },
    }: Connection) => type
);

/**
 * Returns `true` if the datasource has some value under given key.
 */
export const has = multi<[Connection, Key], DatasourceType, boolean>(
    ({
        datasource: {
            options: { type },
        },
    }: Connection) => type
);
