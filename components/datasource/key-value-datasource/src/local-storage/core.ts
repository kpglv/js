import { noop } from '@kpglv/core';
import {
    create,
    connect,
    close,
    get,
    insert,
    remove,
    has,
    Key,
} from '../api';

/** Re-exporting the public API */

export { create, connect, close, get, insert, remove, has };

/** Implementation */

export const datasourceType = 'local-storage';

create.when(datasourceType, options => ({ options }));
connect.when(datasourceType, datasource => ({ datasource }));
close.when(datasourceType, noop);

get.when(
    datasourceType,
    (
        {
            datasource: {
                options: { namespace },
            },
        },
        key
    ) =>
        JSON.parse(
            localStorage.getItem(getNamespacedKey(key, namespace)) ?? ''
        )
);

insert.when(
    datasourceType,
    (
        {
            datasource: {
                options: { namespace },
            },
        },
        key,
        value
    ) => {
        const serializedValue = JSON.stringify(value);
        localStorage.setItem(
            getNamespacedKey(key, namespace),
            serializedValue
        );
    }
);

remove.when(
    datasourceType,
    (
        {
            datasource: {
                options: { namespace },
            },
        },
        key
    ) => {
        localStorage.removeItem(getNamespacedKey(key, namespace));
    }
);

has.when(
    datasourceType,
    (
        {
            datasource: {
                options: { namespace },
            },
        },
        key
    ) => localStorage.getItem(getNamespacedKey(key, namespace)) != null
);

/**
 * Returns the key prefixed with the namespace.
 * If namespace is not specified, the key is returned.
 */
const getNamespacedKey = (key: Key, namespace?: string): Key =>
    namespace ? `${namespace}-${key}` : key;
