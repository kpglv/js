import { it, describe, expect } from 'vitest';

import fc from 'fast-check';

describe('key-value datasource', () => {
    it('a regular test example', () => {
        expect(true).toBe(true);
    });

    it('a generative test example', () => {
        fc.assert(
            fc.property(fc.anything(), obj => {
                expect(obj).toBe(obj);
            })
        );
    });
});
