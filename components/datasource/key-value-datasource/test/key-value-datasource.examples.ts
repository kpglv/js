// Usage examples (e.g., `bun key-value-datasource.examples.ts` )

import { create, connect, close, get, insert, remove, has } from '../src/api';
import { datasourceType as localStorageDatasourceType } from '../src/local-storage/core';
import { datasourceType as mapDatasourceType } from '../src/in-memory-map/core';

/** datasource of nonexistent type (all multimethods will throw by default) */

console.log('>>> using nonexistent type of key-value datasource:');
try {
    create({ type: 'nonexistent-type' });
} catch (e) {
    console.log('*** error creating datasource');
    console.log('*** %o', e.message);
}

/** local storage datasource */

import storage from 'localstorage-nodejs';
globalThis.localStorage = storage('/runtime/local-storage');

const localStorageDatasource = create({
    type: 'local-storage',
    namespace: 'local',
});
console.log(
    '>>> using `%o` datasource: %o',
    localStorageDatasourceType,
    localStorageDatasource
);

console.log(
    'creating a connection (does nothing for this type of datasource except wrapping it)'
);
const localStorageConnection = connect(localStorageDatasource);

console.log(
    'is there a value under key `foo`? %o',
    has(localStorageConnection, 'foo')
);

console.log('inserting a value under key `foo`');
insert(localStorageConnection, 'foo', [
    1,
    'bar',
    true,
    { id: 1, name: 'Joe' },
]);
console.log(
    'is there a value under key `foo` now? %o',
    has(localStorageConnection, 'foo')
);
console.log('value under key `foo`: %o', get(localStorageConnection, 'foo'));

console.log('removing the value under key `foo`');
remove(localStorageConnection, 'foo');
console.log(
    'is there a value under key `foo` now? %o',
    has(localStorageConnection, 'foo')
);
console.log('closing the connection: %o', localStorageConnection);
close(localStorageConnection);
console.log('connection closed: %o', localStorageConnection);

/** in-memory map datasource */

const mapDatasource = create({
    type: 'in-memory-map',
    namespace: 'local',
});
console.log(
    '>>> using `%o` datasource: %o',
    mapDatasourceType,
    mapDatasource
);

console.log(
    'creating a connection (does nothing for this type of datasource except wrapping it)'
);
const mapConnection = connect(mapDatasource);

console.log(
    'is there a value under key `foo`? %o',
    has(mapConnection, 'foo')
);

console.log('inserting a value under key `foo`');
insert(mapConnection, 'foo', [1, 'bar', true, { id: 1, name: 'Joe' }]);
console.log(
    'is there a value under key `foo` now? %o',
    has(mapConnection, 'foo')
);
console.log('value under key `foo`: %o', get(mapConnection, 'foo'));

console.log('removing the value under key `foo`');
remove(mapConnection, 'foo');
console.log(
    'is there a value under key `foo` now? %o',
    has(mapConnection, 'foo')
);
console.log('closing the connection: %o', mapConnection);
close(mapConnection);
console.log('connection closed: %o', mapConnection);
