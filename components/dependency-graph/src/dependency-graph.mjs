/**
 * Returns a new dependency graph.
 *
 * A dependency graph is a map (JavaScript object) that expresses dependencies
 * between nodes (strings); it contains a map of nodes to sets of their
 * immediate dependencies and a map of nodes to sets of their immediate
 * dependants.
 * @param {Map<Node, Set<Node>>} dependencies
 * @param {Map<Node, Set<Node>>} dependants
 * @returns {DependencyGraph}
 */
export const graph = (dependencies = new Map(), dependants = new Map()) => ({
    dependencies,
    dependants,
});

/**
 * Adds dependency to the node in the graph.
 * @param {DependencyGraph} graph
 * @param {Node} node
 * @param {Node} dep
 * @returns {void}
 */
export const depend = (graph, node, dep) => {
    if (graph.dependencies.has(node)) {
        graph.dependencies.get(node).add(dep);
    } else {
        graph.dependencies.set(node, new Set([dep]));
    }
    if (graph.dependants.has(dep)) {
        graph.dependants.get(dep).add(node);
    } else {
        graph.dependants.set(dep, new Set([node]));
    }
};

/**
 * Removes dependency from the node in the graph.
 * @param {DependencyGraph} graph
 * @param {Node} node
 * @param {Node} dep
 * @returns {void}
 */
export const undepend = (graph, node, dep) => {
    if (graph.dependencies.has(node)) {
        graph.dependencies.get(node).delete(dep);
        if (graph.dependencies.get(node).size === 0) {
            graph.dependencies.delete(node);
        }
    }
    if (graph.dependants.has(dep)) {
        graph.dependants.get(dep).delete(node);
        if (graph.dependants.get(dep).size === 0) {
            graph.dependants.delete(dep);
        }
    }
};

/**
 * Returns a set of immediate dependencies of the node in the graph.
 * @param {DependencyGraph} graph
 * @param {Map<Node, Set<Node>>} graph.dependencies
 * @param {Node} node
 * @returns {Set<Node>}
 */
export const immediateDependencies = ({ dependencies }, node) =>
    dependencies.get(node) || new Set();

/**
 * Returns a set of immediate dependants of the node in the graph.
 * @param {DependencyGraph} graph
 * @param {Map<Node, Set<Node>>} graph.dependants
 * @param {Node} node
 * @returns {Set<Node>}
 */
export const immediateDependants = ({ dependants }, node) =>
    dependants.get(node) || new Set();

/**
 * Returns a set of all dependencies of the node in the graph.
 * @param graph
 * @param node
 */
export const dependencies = (graph, node) => {
    const nodes = immediateDependencies(graph, node);
    if (!nodes.size) {
        return nodes;
    } else {
        return new Set([
            ...nodes,
            ...[...nodes].flatMap(dependency => [
                ...dependencies(graph, dependency),
            ]),
        ]);
    }
};

/**
 * Returns a set of all dependants of the node in the graph.
 * @param graph
 * @param node
 */
export const dependants = (graph, node) => {
    const nodes = immediateDependants(graph, node);
    if (!nodes.size) {
        return nodes;
    } else {
        return new Set([
            ...nodes,
            ...[...nodes].flatMap(dependency => [
                ...dependants(graph, dependency),
            ]),
        ]);
    }
};

/**
 * Returns a set of all nodes in the graph.
 * @param {DependencyGraph} graph
 * @param {Map<Node, Set<Node>>} graph.dependencies
 * @param {Map<Node, Set<Node>>} graph.dependants
 * @returns {Set<Node>}
 */
export const nodes = ({ dependencies, dependants }) =>
    new Set([...dependencies.keys(), ...dependants.keys()]);

/**
 * Returns an array of all nodes in the graph sorted topologically. Uses
 * Kahn's algorithm {@link https://doi.org/10.1145%2F368996.369025}.
 * @param {DependencyGraph} graph
 * @returns {Node[]}
 */
export const topologicalSort = graph => {
    // otherwise the original graph will be destroyed by sorting; could be
    // avoided if graph update operations returned new graphs instead of
    // mutating the existing one, but it will make graph API clumsier (as we do
    // not have threading macros in JS and method chaining is also not
    // applicable here)
    graph = structuredClone(graph);

    // a list of root nodes (that have no dependants)
    let rootNodes = [...nodes(graph)].filter(
        node => immediateDependants(graph, node).size === 0
    );
    let sorted = [];
    while (rootNodes.length !== 0) {
        // get the first node from the list of root nodes
        let [node, ...more] = rootNodes;
        // remove the node from the list of root nodes, add it to list of sorted
        // nodes
        rootNodes = more;
        sorted.push(node);
        // for each dependency of the node
        let deps = [...immediateDependencies(graph, node)];
        deps.forEach(dep => {
            // remove the edge that links the node and the dependency
            undepend(graph, node, dep);
            // if the dependency has no more dependants
            if (immediateDependants(graph, dep).size === 0) {
                // add it to the list of root nodes
                rootNodes.push(dep);
            }
        });
    }
    // graph still has edges --- it has at least one cycle
    if (graph.dependencies.size !== 0 || graph.dependants.size !== 0) {
        throw new Error(
            'graph has cycles and cannot be topologically sorted'
        );
    }
    return sorted;
};
