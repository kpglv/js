# dependency-graph

A dependency graph --- a set of interdependent nodes. Based on
https://github.com/weavejester/dependency

Can be updated (dependencies added/removed), queried about its structure,
topologically sorted (if represents a DAG).

## Getting Started

### Usage Examples

```js
import {
    graph,
    depend,
    undepend,
    nodes,
    topologicalSort,
} from '@kpglv/dependency-graph';

const g1 = graph();
depend(g1, 'b', 'a');
depend(g1, 'c', 'b');
depend(g1, 'c', 'a');
depend(g1, 'd', 'c');

console.log('graph:', g1);
console.log('graph nodes:', nodes(g1));
console.log('immediate dependencies of "c":', immediateDependencies(g1, 'c'));
console.log('immediate dependants of "a":', immediateDependants(g1, 'a'));

const sorted = topologicalSort(g1);
console.log('sorted:', sorted);
```

See and run examples in `test/examples.mjs`

```
node test/examples.mjs
```

## Testing

Run unit tests

```
yarn test
```

## TODO

-   [ ] add more unit tests
-   [ ] implement more graph queries
-   [ ] add example usage in a browser
-   [ ] fix package typing (types do not work as expected if
        the package is imported)
