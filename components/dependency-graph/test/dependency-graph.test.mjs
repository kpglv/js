import {
    graph,
    depend,
    immediateDependants,
    immediateDependencies,
    dependencies,
    dependants,
    nodes,
    topologicalSort,
} from '@kpglv/dependency-graph';

/**
 *      (a)
 *       |
 *       v
 *      (b)--\
 *       |   |
 *       v   |
 *   /<-(c)  |
 *   |   |   |
 *   |   v   |
 *  (d) (e)<-/
 *       |
 *       v
 *      (f)
 */
const simpleDAG = () => {
    const g = graph();
    depend(g, 'a', 'b');
    depend(g, 'b', 'c');
    depend(g, 'b', 'e');
    depend(g, 'c', 'd');
    depend(g, 'c', 'e');
    depend(g, 'e', 'f');
    return g;
};

/**
 * Added a cycle dependency of 'e' <-> 'f' to simpleDAG
 */
const simpleWithCycles = () => {
    const g = simpleDAG();
    depend(g, 'f', 'e');
    return g;
};

describe('dependency graph', () => {
    it('topological sort works on DAGs', () => {
        const g = simpleDAG();
        const sortedDAG = topologicalSort(g);
        // order of nodes on the same level ('d', 'e')?
        expect(sortedDAG).toEqual(['a', 'b', 'c', 'd', 'e', 'f']);
    });
    it('topological sort does not work on graphs with cycles', () => {
        const g = simpleWithCycles();
        expect(() => topologicalSort(g)).toThrow(/graph has cycles/);
    });
});

describe('immediateDependencies', () => {
    it('returns all immediate dependencies of the node', () => {
        const nodes = immediateDependencies(simpleDAG(), 'c');
        expect(nodes).toStrictEqual(new Set(['d', 'e']));
    });
});

describe('immediateDependants', () => {
    it('returns all immediate dependants of the node', () => {
        const nodes = immediateDependants(simpleDAG(), 'e');
        expect(nodes).toStrictEqual(new Set(['b', 'c']));
    });
});

describe('dependencies', () => {
    it('returns all dependencies of the node', () => {
        const nodes = dependencies(simpleDAG(), 'c');
        expect(nodes).toStrictEqual(new Set(['d', 'e', 'f']));
    });
});

describe('dependants', () => {
    it('returns all dependants of the node', () => {
        const nodes = dependants(simpleDAG(), 'e');
        expect(nodes).toStrictEqual(new Set(['a', 'b', 'c']));
    });
});

describe('nodes', () => {
    it('returns all nodes in the graph', () => {
        const allNodes = nodes(simpleDAG());
        expect(allNodes).toStrictEqual(
            new Set(['a', 'b', 'c', 'd', 'e', 'f'])
        );
    });
});
