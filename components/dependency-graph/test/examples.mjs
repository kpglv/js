import {
    graph,
    depend,
    immediateDependencies,
    immediateDependants,
    nodes,
    topologicalSort,
} from '@kpglv/dependency-graph';

const g1 = graph();
depend(g1, 'b', 'a');
depend(g1, 'c', 'b');
depend(g1, 'c', 'a');
depend(g1, 'd', 'c');

// a DAG

console.log('graph:', g1);
console.log('graph nodes:', nodes(g1));
console.log('immediate dependencies of "c":', immediateDependencies(g1, 'c'));
console.log('immediate dependants of "a":', immediateDependants(g1, 'a'));

const sorted = topologicalSort(g1);
console.log('sorted:', sorted);

// create a cycle

depend(g1, 'a', 'b');
try {
    const sorted = topologicalSort(g1);
    console.log('sorted:', sorted);
} catch (e) {
    console.log('sorting error:', e.message);
}
